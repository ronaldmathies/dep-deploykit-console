package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class ProfileCredentials extends WidgetCollectionField<ProfileCredential> {

    private OptionType<DefaultOption> groupOptionType = null;

    private ProfileCredentials() {}

    public ProfileCredentials(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    @Override
    public WidgetSupplier<ProfileCredential> createWidgetSupplier() {
        return callback -> {
            ProfileCredential profileCredential = new ProfileCredential();
            profileCredential.setGroup(groupOptionType);
            callback.created(profileCredential);
        };
    }
}
