package nl.sodeso.deploykit.console.client.application.tooling.delete.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

/**
 * @author Ronald Mathies
 */
public interface DeleteRpcAsync {

    /**
     * @see DeleteRpc#canBeDeletedSafely(String, VersionOption)
     */
    void canBeDeletedSafely(String uuid, VersionOption version, AsyncCallback<ValidationResult> result);

    /**
     * @see DeleteRpc#delete(String, VersionOption)
     */
    void delete(String uuid, VersionOption version, AsyncCallback<Void> result);

}
