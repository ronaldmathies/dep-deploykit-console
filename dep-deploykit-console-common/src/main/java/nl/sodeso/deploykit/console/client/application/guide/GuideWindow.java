package nl.sodeso.deploykit.console.client.application.guide;

import nl.sodeso.deploykit.console.client.application.guide.steps.WelcomeStep;
import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.wizard.WizardWindow;

/**
 * @author Ronald Mathies
 */
public class GuideWindow extends WizardWindow {

    private static GuideWindow instance = null;

    public GuideWindow() {
        super(new PopupWindowPanel("guide", "Guide", WindowPanel.Style.INFO));
        getWindow().showCollapseButton(true);
        getWindow().setWidth("500px");

        getWindow().addToFooter(Align.LEFT, new CloseButton.WithLabel((event) -> setVisible(false)));
    }

    public static GuideWindow instance() {
        if (instance == null) {
            instance = new GuideWindow();
        }

        return instance;
    }

    public void resetWizard() {
        setWizardStep(new WelcomeStep(null));
    }

}
