package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplierCallback;

/**
 * @author Ronald Mathies
 */
public class ProfileNodes extends WidgetCollectionField<ProfileNode> {

    private OptionType<DefaultOption> groupOptionType = null;

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    @Override
    public WidgetSupplier<ProfileNode> createWidgetSupplier() {
        return callback -> {
            ProfileNode profileNode = new ProfileNode();
            profileNode.setGroup(groupOptionType);
            callback.created(profileNode);
        };
    }
}
