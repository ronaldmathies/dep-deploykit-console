package nl.sodeso.deploykit.console.client.application.tooling.usage.rpc;

import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface UsedByRpc {

    ArrayList<UsedBy> findUsages(String uuid, VersionOption version);

}
