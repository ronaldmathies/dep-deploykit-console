package nl.sodeso.deploykit.console.client.application.ui.validation;

import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.StringUtils;

/**
 * @author Ronald Mathies
 */
public abstract class LabelUniqueValidationRule implements ValidationRule {

    private LabelUniqueRpcAsync labelUniqueRpcAsync = null;

    /**
     * Constructs a new validation rule.
     */
    public LabelUniqueValidationRule(final LabelUniqueRpcAsync labelUniqueRpcAsync) {
        this.labelUniqueRpcAsync = labelUniqueRpcAsync;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(final ValidationCompletedHandler handler) {
        if (getLabel() != null && StringUtils.isNotEmpty(getLabel().getValue())) {
            labelUniqueRpcAsync.isLabelUnique(getUuid(), getLabel().getValue(), new DefaultAsyncCallback<ValidationResult>() {

                @Override
                public void success(ValidationResult result) {
                    handler.completed(result);
                }
            });
        } else {
            handler.completed(new ValidationResult());
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return !getLabel().getValue().isEmpty();
    }

    public abstract String getUuid();

    public abstract StringType getLabel();

}
