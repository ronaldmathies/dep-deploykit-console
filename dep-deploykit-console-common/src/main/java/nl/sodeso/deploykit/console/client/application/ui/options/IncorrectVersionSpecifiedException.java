package nl.sodeso.deploykit.console.client.application.ui.options;

/**
 * @author Ronald Mathies
 */
public class IncorrectVersionSpecifiedException extends RuntimeException {

    public IncorrectVersionSpecifiedException() {}

    public IncorrectVersionSpecifiedException(String message) {
        super(message);
    }

}
