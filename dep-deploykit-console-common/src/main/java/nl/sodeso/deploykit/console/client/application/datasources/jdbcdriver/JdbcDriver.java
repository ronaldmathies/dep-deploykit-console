package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc.JdbcDriverRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CREDENTIAL_I18N;
import static nl.sodeso.deploykit.console.client.application.Resources.JDBC_DRIVER_I18N;

/**
 * @author Ronald Mathies
 */
public class JdbcDriver extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";

    private String uuid = null;

    private boolean showJdbcDriverClasses = false;
    private boolean jdbcDriverClassesListChanged = false;

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private OptionType<DefaultOption> jdbcDriverClassOptionType = new OptionType<>();

    private JdbcDriverJars jars = new JdbcDriverJars();

    private transient LegendPanel legendPanel;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;

    public JdbcDriver() {

    }

    public Widget toEditForm() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        EntryForm entryForm = new EntryForm(null);

        this.groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        this.groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        this.groupField.setEnabled(getUuid() == null);

        this.labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        entryForm.addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, JDBC_DRIVER_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, JDBC_DRIVER_I18N.labelField(), labelTextField)
                    .addValidationRules(
                        new LabelUniqueValidationRule(GWT.create(JdbcDriverRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return JdbcDriver.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return JdbcDriver.this.getLabel();
                            }
                        }
                    ));

        EntryForm javaArchiveFilesEntryForm = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation("java-archive-files", JDBC_DRIVER_I18N.javaArchiveFilesDocumentation()),
                        new EntryWithContainer(null, jars.toEditForm()));

        LegendPanel javaArchiveFilesLegendPanel = new LegendPanel(null, JDBC_DRIVER_I18N.javaArchiveFilesPanel());
        javaArchiveFilesLegendPanel.add(javaArchiveFilesEntryForm);
        entryForm.addEntry(new EntryWithContainer(null, javaArchiveFilesLegendPanel));

        if (showJdbcDriverClasses) {
            LegendPanel jdbcDriverClassesLegendPanel = new LegendPanel(null, JDBC_DRIVER_I18N.jdbcDriverClassesPanel());
            EntryForm jdbcDriverClassesForm = new EntryForm(null);
            JdbcDriverClassComboboxField jdbcDriverClassField = new JdbcDriverClassComboboxField(jdbcDriverClassOptionType, getUuid(), getVersion());
            jdbcDriverClassesForm.addEntry(new EntryWithDocumentation("jdbcdriverclasses", JDBC_DRIVER_I18N.jdbcDriverClassesDocumentation()));
            jdbcDriverClassesForm.addEntry(new EntryWithLabelAndWidget("jdbcdriverclass", JDBC_DRIVER_I18N.jdbcDriverClassField(), jdbcDriverClassField));
            jdbcDriverClassesLegendPanel.add(jdbcDriverClassesForm);
            entryForm.addEntry(new EntryWithContainer(null, jdbcDriverClassesLegendPanel));
        }

        this.labelTextField.setFocus();

        this.legendPanel = new LegendPanel(null, JDBC_DRIVER_I18N.jdbcDriverPanel());
        this.legendPanel.add(entryForm);

        this.legendPanel.addAttachHandler(event -> {

            if (jdbcDriverClassesListChanged) {
                jdbcDriverClassesListChanged = false;

                PopupWindowPanel jdbcDriverClassesChangedInfoPanel = new PopupWindowPanel("jdbc-classes-changed-popup", "JDBC driver classes changed", WindowPanel.Style.ALERT)
                    .addToBody(
                        new EntryForm(null)
                            .addEntry(new EntryWithDocumentation(null, "There is a change in available JDBC driver classes due to a change in the supplied jars.<br/>Please check if the correct JDBC driver class is selected.")));

                jdbcDriverClassesChangedInfoPanel.addToFooter(Align.RIGHT, new OkButton.WithLabel((clickEvent) -> jdbcDriverClassesChangedInfoPanel.close()));
                jdbcDriverClassesChangedInfoPanel.center(true, true);
            }

        });

        return this.legendPanel;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, legendPanel);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public JdbcDriverJars getJars() {
        return this.jars;
    }

    public OptionType<DefaultOption> getJdbcDriverClass() {
        return jdbcDriverClassOptionType;
    }

    public void setJdbcDriverClass(OptionType<DefaultOption> jdbcDriverClassOptionType) {
        this.jdbcDriverClassOptionType = jdbcDriverClassOptionType;
    }

    public void setShowJdbcDriverClasses(boolean showJdbcDriverClasses) {
        this.showJdbcDriverClasses = showJdbcDriverClasses;
    }

    public void setJdbcDriverClassesListChanged(boolean jdbcDriverClassesListChanged) {
        this.jdbcDriverClassesListChanged = jdbcDriverClassesListChanged;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof JdbcDriver && ((JdbcDriver) obj).getUuid().equals(uuid);
    }
}
