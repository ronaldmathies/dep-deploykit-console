package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.security.user.UserComboboxField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class RoleUser implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> userOptionType = new OptionType<>();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient UserComboboxField userField = null;

    public RoleUser() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        userField = new UserComboboxField(userOptionType);
        userField.addValidationRule(new ComboboxMandatoryValidationRule("user-mandatory", ValidationMessage.Level.ERROR, userOptionType));

        entry = new EntryWithWidgetAndWidget("ldap-service", null,
            new HorizontalPanel()
                .addWidget(userField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> RoleMenuItem.click(userOptionType.getValue().getKey()))));

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getUser() {
        return this.userOptionType;
    }

    public void setUser(OptionType<DefaultOption> user) {
        this.userOptionType = user;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
