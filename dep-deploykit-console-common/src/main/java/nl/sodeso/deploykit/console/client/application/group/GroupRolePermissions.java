package nl.sodeso.deploykit.console.client.application.group;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class GroupRolePermissions extends WidgetCollectionField<GroupRolePermission> {

    @Override
    public WidgetSupplier<GroupRolePermission> createWidgetSupplier() {
        return callback -> callback.created(new GroupRolePermission());
    }
}
