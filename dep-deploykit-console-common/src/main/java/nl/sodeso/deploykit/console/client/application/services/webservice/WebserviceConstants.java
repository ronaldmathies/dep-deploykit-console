package nl.sodeso.deploykit.console.client.application.services.webservice;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface WebserviceConstants extends Messages {

    @DefaultMessage("Web Services")
    String webservicesMenuItem();

    @DefaultMessage("Webservice")
    String webservicesPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Placeholder Prefix")
    String placeholderPrefix();

    @DefaultMessage("Webservice URL Destination")
    String webserviceUrlDestinationPanel();

    @DefaultMessage("Protocol")
    String protocolField();

    @DefaultMessage("Domain Name")
    String domainNameField();

    @DefaultMessage("Port")
    String portField();

    @DefaultMessage("Context")
    String contextField();

    @DefaultMessage("Path")
    String pathField();

    @DefaultMessage("Timeout")
    String timeoutField();

    @DefaultMessage("Milliseconds")
    String timeoutFieldHint();

    @DefaultMessage("WS-Addressing (WSA)")
    String wsAddressingPanel();

    @DefaultMessage("Use WS-Addressing")
    String useWsAddressingField();

    @DefaultMessage("From")
    String fromField();

    @DefaultMessage("To")
    String toField();

    @DefaultMessage("Action")
    String actionField();

    @DefaultMessage("Only enter this when the response should use a different EPR (Endpoint Reference) then specified in the <b>From</b>.")
    String replyToDocumentation();

    @DefaultMessage("Reply To")
    String replyToField();

    @DefaultMessage("Only enter this when the SOAP fault should use a different EPR (Endpoint Reference).")
    String faultToDocumentation();

    @DefaultMessage("Fault To")
    String faultToField();

    @DefaultMessage("Enable the following option when the webservice uses WS-Security (WSSEC).")
    String useWsSecurityDocumentation();

    @DefaultMessage("Use WS-Security")
    String useWsSecurityField();

    @DefaultMessage("Credential")
    String credentialField();

    @DefaultMessage("Enable the following option when the webservice uses WS-Addressing (WSA).")
    String useWsAddressingDocumentation();

    @DefaultMessage("WS-Security (WSSEC)")
    String wsSecurityPanel();

    @DefaultMessage("Custom Properties")
    String customPropertiesPanel();

    @DefaultMessage("The custom properties allow you to pass in arbitrary properties that don&apos;t fit in any of the above categories but are applicable for the webservice itself. The key of the property is the placeholder which will be used during the processing of the application resources.")
    String customPropertiesDocumentation();

    @DefaultMessage("Checking Webservice Connection")
    String checkTitle();

    @DefaultMessage("Checking if webservice configuration is correct, this process may take a while so please do not close the browser.")
    String checkMessage();

    @DefaultMessage("Webservice Connection")
    String checkResultTitle();

    @DefaultMessage("Connection was made successfully.")
    String checkSuccesfullMessage();

    @DefaultMessage("Webservice {0} Used By")
    String webserviceUsedByTitle(String name);

    @DefaultMessage("Specify the URL of the webservice. Passing of the information to the webservice can be different for various frameworks, to achieve the most flexible situation it is advisable to separating the components of the URL as much as possible.")
    String urlDocumentation();

    @DefaultMessage("Provide the fully qualified domain name of the service without any slashes, protocols or other information.")
    String domainDocumentation();

    @DefaultMessage("ie. &apos;webservice.organization.com&apos;")
    String domainHint();

    @DefaultMessage("1 - 65535")
    String portHint();

    @DefaultMessage("For consistency the context should start with a slash but not end with a slash, leave empty when there is no context.")
    String contextDocumentation();

    @DefaultMessage("ie. &apos;/context&apos;")
    String contextHint();

    @DefaultMessage("For consistency, the path should start with a slash but not end with a slash, leave empty when there is no path.")
    String pathDocumentation();

    @DefaultMessage("ie. &apos;/additional/path&apos;")
    String pathHint();

}