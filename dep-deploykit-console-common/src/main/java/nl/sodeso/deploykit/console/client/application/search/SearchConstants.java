package nl.sodeso.deploykit.console.client.application.search;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface SearchConstants extends Messages {

    @DefaultMessage("Search")
    String searchTitle();

    @DefaultMessage("Search")
    String searchField();

    @DefaultMessage("No results found.")
    String noResultsFoundMessage();

    @DefaultMessage("Enter search criteria...")
    String enterSearchCriteriaMessage();
}