package nl.sodeso.deploykit.console.client.application.ui.validation.rpc;

import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

/**
 * @author Ronald Mathies
 */
public interface LabelUniqueRpc {

    ValidationResult isLabelUnique(String uuid, String label);

}
