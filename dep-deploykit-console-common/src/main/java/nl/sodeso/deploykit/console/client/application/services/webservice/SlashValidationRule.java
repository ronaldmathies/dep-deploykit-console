package nl.sodeso.deploykit.console.client.application.services.webservice;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;

/**
 * @author Ronald Mathies
 */
public abstract class SlashValidationRule implements ValidationRule {

    @Override
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult validationResult = new ValidationResult();

        if (getValue() != null && getValue().length() > 0 && !getValue().startsWith("/")) {
            validationResult.add(new ValidationMessage("start-with-slash", ValidationMessage.Level.ERROR, "The value must start with a slash ( / )."));
        }
        if (getValue() != null && getValue().length() > 0 && getValue().endsWith("/")) {
            validationResult.add(new ValidationMessage("not-end-with-slash", ValidationMessage.Level.ERROR, "The value must not end with a slash ( / )."));
        }

        handler.completed(validationResult);
    }

    @Override
    public boolean isApplicable() {
        return true;
    }

    public abstract String getValue();
}