package nl.sodeso.deploykit.console.client.application.security.user.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.security.user.User;
import nl.sodeso.deploykit.console.client.application.security.user.UserSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-user")
public interface UserRpc extends XsrfProtectedService, DeleteRpc {

    ArrayList<UserSummaryItem> findSummaries();
    User findSingle(String uuid);
    ArrayList<DefaultOption> asOptions();

    UserSummaryItem save(User user);
    ArrayList<UsedBy> usage(String uuid);
    UserSummaryItem duplicate(String uuid);

}
