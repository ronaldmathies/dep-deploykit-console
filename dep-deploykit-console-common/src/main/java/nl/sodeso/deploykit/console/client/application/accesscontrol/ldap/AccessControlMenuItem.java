package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;

/**
 * @author Ronald Mathies
 */
public class AccessControlMenuItem extends MenuItem {

    private static final String KEY = "mi-ldap";

    private static AccessControls accessControls = null;

    public AccessControlMenuItem() {
        super(KEY, ACCESS_CONTROL_I18N.accessControlMenuItem(), arguments -> {
            if (accessControls == null) {
                accessControls = new AccessControls();
            }
            accessControls.setArguments(arguments);
            CenterController.instance().setWidget(accessControls);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
