package nl.sodeso.deploykit.console.client.application.search;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class SearchResult implements Serializable {

    private String uuid;
    private VersionOption version;
    private String label;
    private Type type;

    public SearchResult() {}

    public SearchResult(String uuid, VersionOption version, String label, Type type) {
        this.uuid = uuid;
        this.version = version;
        this.label = label;
        this.type = type;
    }

    public String getUuid() {
        return this.uuid;
    }

    public VersionOption getVersion() {
        return this.version;
    }

    public String getLabel() {
        return this.label;
    }

    public Type getType() {
        return type;
    }

}
