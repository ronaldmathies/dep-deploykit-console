package nl.sodeso.deploykit.console.client.application.group.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.group.Group;
import nl.sodeso.deploykit.console.client.application.group.GroupSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-group")
public interface GroupRpc extends XsrfProtectedService, DeleteRpc {

    ArrayList<GroupSummaryItem> findSummaries();
    Group findSingle(String uuid);
    ArrayList<DefaultOption> asOptions();

    GroupSummaryItem save(Group group);
    ArrayList<UsedBy> usage(String uuid);
    GroupSummaryItem duplicate(String uuid);

}
