package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface RoleConstants extends Messages {

    @DefaultMessage("Roles")
    String roleMenuItem();

    @DefaultMessage("Roles define the permissions the users have on groups. A user can have multiple roles and a role can be used for multiple groups, for example, a user can have a role for editing and deploying of all the deployables that are created within a group. Or the user can only view the information stored about deployables within a group. A role itself can be as small as a role for a single member or team, or as large for all the developers or testers for the global development or test environment.")
    String roleDocumentation();

    @DefaultMessage("Role")
    String rolePanel();

    @DefaultMessage("Add one or more groups that are part of this role, it is allowed to use groups that are already associated with a different role, please not that, groups associated with multiple roles with different permissions will result in the evaluation of each individual permission for being positive.")
    String roleGroupsDocumentation();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Role {0} Used By")
    String roleUsedByTitle(String name);

    @DefaultMessage("Groups & Permissions")
    String groupsAndPemissionsPanel();

    @DefaultMessage("Users")
    String usersPanel();

    @DefaultMessage("Add one or more users that are part if this role, these users will have the permissions define within this role for the groups defined in this roles.")
    String roleUsersDocumentation();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("View deployables")
    String allowToViewField();

    @DefaultMessage("Use deployables")
    String allowToUseField();

    @DefaultMessage("Edit deployable")
    String allowToEditField();

    @DefaultMessage("Deploy deployable")
    String allowToDeployField();
}