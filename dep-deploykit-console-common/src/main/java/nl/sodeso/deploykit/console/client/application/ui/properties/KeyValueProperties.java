package nl.sodeso.deploykit.console.client.application.ui.properties;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */

public class KeyValueProperties extends WidgetCollectionField<KeyValueProperty> {

    @Override
    public WidgetSupplier<KeyValueProperty> createWidgetSupplier() {
        return callback -> callback.created(new KeyValueProperty());
    }
}
