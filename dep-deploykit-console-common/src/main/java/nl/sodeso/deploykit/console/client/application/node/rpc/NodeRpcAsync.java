package nl.sodeso.deploykit.console.client.application.node.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.node.Node;
import nl.sodeso.deploykit.console.client.application.node.NodeSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface NodeRpcAsync extends DeleteRpcAsync, LabelUniqueRpcAsync, UsedByRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<NodeSummaryItem>> result);
    void findSingle(String uuid, AsyncCallback<Node> result);

    void asOptions(DefaultOption group, AsyncCallback<ArrayList<DefaultOption>> result);

    void save(Node node, AsyncCallback<NodeSummaryItem> result);
    void duplicate(String uuid, AsyncCallback<NodeSummaryItem> result);

}
