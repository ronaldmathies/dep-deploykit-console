package nl.sodeso.deploykit.console.client.application.node;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.node.rpc.NodeRpcGateway;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class NodeComboboxField extends ComboboxField<DefaultOption> {

    public NodeComboboxField(OptionType<DefaultOption> optionValue) {
        super(optionValue);
    }

    public void listenToGroup(final OptionType<DefaultOption> optionType) {
        optionType.addValueChangedEventHandler(new ValueChangedEventHandler<OptionType<DefaultOption>>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<OptionType<DefaultOption>> event) {
                refresh(optionType);
            }
        });
    }

    public void refresh(OptionType<DefaultOption> group) {
        if (group != null && group.getValue() != null) {
            ((NodeRpcGateway) GWT.create(NodeRpcGateway.class)).asOptions(group.getValue(), new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                @Override
                public void success(ArrayList<DefaultOption> result) {
                    replaceItemsRetainSelection(result);
                }
            });
        } else {
            removeAllItems();
        }
    }
}
