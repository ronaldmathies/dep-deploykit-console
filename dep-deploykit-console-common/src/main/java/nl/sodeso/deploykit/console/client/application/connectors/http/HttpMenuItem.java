package nl.sodeso.deploykit.console.client.application.connectors.http;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.HTTP_L18N;

/**
 * @author Ronald Mathies
 */
public class HttpMenuItem extends MenuItem {

    private static final String KEY = "mi-http";

    private static Httpes httpes = null;

    public HttpMenuItem() {
        super(KEY, HTTP_L18N.httpMenuItem(), arguments -> {
            if (httpes == null) {
                httpes = new Httpes();
            }
            httpes.setArguments(arguments);
            CenterController.instance().setWidget(httpes);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
