package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import nl.sodeso.gwt.ui.client.panel.ValidationResultPanel;

/**
 * @author Ronald Mathies
 */
public class CommitProcess {

    private Versionable versionable = null;
    private VersioningRpcAsync commitRpcAsync;
    private CommitCallback callback;

    public CommitProcess(VersioningRpcAsync commitRpcAsync, Versionable versionable, CommitCallback callback) {
        this.commitRpcAsync = commitRpcAsync;
        this.versionable = versionable;
        this.callback = callback;
    }

    /**
     * Starts the commit process.
     */
    public void start() {
        commitAllowedCheck();
    }

    /**
     * Checks if yhe commit action is allowed according to the business rules.
     */
    private void commitAllowedCheck() {
        commitRpcAsync.isCommitAllowed(versionable.getUuid(), versionable.getVersion().getValue(),
            new DefaultAsyncCallback<ValidationResult>() {
                @Override
                public void success(ValidationResult result) {
                    if (result.hasMessages()) {
                        new ValidationResultPanel(result, "Cannot Commit", "Please fix the following issues before trying to commit this version.").center(true, true);
                    } else {
                        chooseCommitLabel();
                    }
                }
            });
    }

    /**
     * Asks the user to specify a version number for the commit label.
     */
    private void chooseCommitLabel() {
        final SpecifyVersionQuestionPanel questionPanel = new SpecifyVersionQuestionPanel(toVersion -> commitRpcAsync.commit(versionable.getUuid(), versionable.getVersion().getValue(), toVersion, new DefaultAsyncCallback<VersionOption>() {
            @Override
            public void success(VersionOption version) {
                callback.onSuccess(version);
            }
        }));
        questionPanel.center(true, true);
    }

}
