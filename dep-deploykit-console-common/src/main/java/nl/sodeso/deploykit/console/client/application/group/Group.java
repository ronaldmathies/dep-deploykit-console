package nl.sodeso.deploykit.console.client.application.group;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.GROUP_I18N;

/**
 * @author Ronald Mathies
 */
public class Group implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_LABEL = "label";
    private static final String KEY_GROUP = "group";
    private static final String KEY_GROUP_ROLES = "group-roles";

    private String uuid = null;

    private StringType label = new StringType();
    private GroupRolePermissions groupRolePermissions = new GroupRolePermissions();

    private transient EntryForm entryForm = null;
    private transient TextField labelTextField = null;
    private transient LegendPanel groupRolesPanel = null;
    public Group() {
    }

    public Widget toEditForm() {
        if (entryForm != null) {
            return entryForm;
        }

        labelTextField = new TextField<>(KEY_LABEL, this.label);
        labelTextField.addValidationRule(new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return labelTextField.getValue();
            }
        });

        LegendPanel groupPanel = new LegendPanel(KEY_GROUP, GROUP_I18N.groupPanel());
        groupPanel.add(
            new EntryForm(KEY_GROUP)
                .addEntry(new EntryWithLabelAndWidget(KEY_LABEL, GROUP_I18N.labelField(), labelTextField)));

        groupRolesPanel = new LegendPanel(KEY_GROUP_ROLES, GROUP_I18N.rolesPanel());
        groupRolesPanel.add(
            new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(null, GROUP_I18N.rolesDocumentation()))//,
//                    new EntryWithContainer(null, groupRolePermissions.toEditForm()))
                );

        entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(null, groupPanel),
                new EntryWithContainer(null, groupRolesPanel)
            );

        labelTextField.setFocus();

        return entryForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public GroupRolePermissions getGroupRolePermissions() {
        return groupRolePermissions;
    }

    public void setGroupRolePermissions(GroupRolePermissions groupRolePermissions) {
        this.groupRolePermissions = groupRolePermissions;
    }

    public int hashCode() {
        return uuid.hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof Group && ((Group) obj).getUuid().equals(uuid);
    }
}
