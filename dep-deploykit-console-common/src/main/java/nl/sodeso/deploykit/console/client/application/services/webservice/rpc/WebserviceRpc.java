package nl.sodeso.deploykit.console.client.application.services.webservice.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.services.webservice.Webservice;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-webservice")
public interface WebserviceRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, UsedByRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<WebserviceSummaryItem> findSummaries();

    Webservice findSingle(String uuid, VersionOption version);

    WebserviceSummaryItem save(Webservice webservice) throws RemoteException;
    String check(String uuid, VersionOption version);
    WebserviceSummaryItem duplicate(String uuid, VersionOption version);

}
