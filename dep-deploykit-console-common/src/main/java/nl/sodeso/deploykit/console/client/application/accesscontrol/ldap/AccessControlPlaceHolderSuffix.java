package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

/**
 * @author Ronald Mathies
 */
public class AccessControlPlaceHolderSuffix {

    public static final String AC_REALM = "-ac-realm";
    public static final String AC_DS_USER_TABLE = "-ac-ds-user-table";
    public static final String AC_DS_USER_USER_COLUMN = "-ac-ds-user-user-column";
    public static final String AC_DS_USER_CREDENTIAL_COLUMN = "-ac-ds-user-credential-column";
    public static final String AC_DS_ROLE_TABLE = "-ac-ds-role-table";
    public static final String AC_DS_ROLE_USER_COLUMN = "-ac-ds-role-user-column";
    public static final String AC_DS_ROLE_ROLE_COLUMN = "-ac-ds-role-role-column";

    public static final String AC_LDAP_USER_BASEDN = "-ac-ldap-user-basedn";
    public static final String AC_LDAP_USER_ATTRIBUTE = "-ac-ldap-user-attribute";
    public static final String AC_LDAP_USER_PASSWORD_ATTRIBUTE = "-ac-ldap-user-password-attribute";
    public static final String AC_LDAP_USER_OBJECTCLASS = "-ac-ldap-user-objectclass";
    public static final String AC_LDAP_ROLE_BASEDN = "-ac-ldap-role-basedn";
    public static final String AC_LDAP_ROLE_MEMBER_ATTRIBUTE = "-ac-ldap-role-member-attribute";
    public static final String AC_LDAP_ROLE_NAME_ATTRIBUTE = "-ac-ldap-role-name-attribute";
    public static final String AC_LDAP_ROLE_OBJECTCLASS = "-ac-ldap-role-objectclass";

    public static String AC_REALM(String prefix) {
        return prefix + AC_REALM;
    }

    public static String AC_DS_USER_TABLE(String prefix) {
        return prefix + AC_DS_USER_TABLE;
    }

    public static String AC_DS_USER_USER_COLUMN(String prefix) {
        return prefix + AC_DS_USER_USER_COLUMN;
    }

    public static String AC_DS_USER_CREDENTIAL_COLUMN(String prefix) {
        return prefix + AC_DS_USER_CREDENTIAL_COLUMN;
    }

    public static String AC_DS_ROLE_TABLE(String prefix) {
        return prefix + AC_DS_ROLE_TABLE;
    }

    public static String AC_DS_ROLE_USER_COLUMN(String prefix) {
        return prefix + AC_DS_ROLE_USER_COLUMN;
    }

    public static String AC_LDAP_USER_BASEDN(String prefix) {
        return prefix + AC_LDAP_USER_BASEDN;
    }

    public static String AC_LDAP_USER_ATTRIBUTE(String prefix) {
        return prefix + AC_LDAP_USER_ATTRIBUTE;
    }

    public static String AC_LDAP_USER_PASSWORD_ATTRIBUTE(String prefix) {
        return prefix + AC_LDAP_USER_PASSWORD_ATTRIBUTE;
    }

    public static String AC_LDAP_USER_OBJECTCLASS(String prefix) {
        return prefix + AC_LDAP_USER_OBJECTCLASS;
    }

    public static String AC_LDAP_ROLE_BASEDN(String prefix) {
        return prefix + AC_LDAP_ROLE_BASEDN;
    }

    public static String AC_LDAP_ROLE_MEMBER_ATTRIBUTE(String prefix) {
        return prefix + AC_LDAP_ROLE_MEMBER_ATTRIBUTE;
    }

    public static String AC_LDAP_ROLE_NAME_ATTRIBUTE(String prefix) {
        return prefix + AC_LDAP_ROLE_NAME_ATTRIBUTE;
    }

    public static String AC_LDAP_ROLE_OBJECTCLASS(String prefix) {
        return prefix + AC_LDAP_ROLE_OBJECTCLASS;
    }

    public static String AC_DS_ROLE_ROLE_COLUMN(String prefix) {
        return prefix + AC_DS_ROLE_ROLE_COLUMN;
    }
}
