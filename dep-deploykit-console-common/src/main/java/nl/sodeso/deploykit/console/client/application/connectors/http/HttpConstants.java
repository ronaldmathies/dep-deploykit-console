package nl.sodeso.deploykit.console.client.application.connectors.http;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface HttpConstants extends Messages {

    @DefaultMessage("HTTP")
    String httpMenuItem();

    @DefaultMessage("HTTP")
    String httpPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Port this connector listens on. If set the 0 a random port is assigned.")
    String portFieldDocumentation();

    @DefaultMessage("Port")
    String portField();

    @DefaultMessage("Timeout")
    String timeoutField();

    @DefaultMessage("Milliseconds")
    String timeoutFieldHint();

    @DefaultMessage("HTTP {0} Used By")
    String httpUsedByTitle(String name);
}