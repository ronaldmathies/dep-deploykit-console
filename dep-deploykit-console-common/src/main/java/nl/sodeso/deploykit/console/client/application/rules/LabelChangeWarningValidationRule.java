package nl.sodeso.deploykit.console.client.application.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.form.validation.rules.ValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public abstract class LabelChangeWarningValidationRule implements ValidationRule {

    private static final String KEY_CHANGED_SUFFIX = "-changed";

    private String key = null;
    private StringType valueType;

    /**
     * Constructs a new validation rule.
     *
     * @param key The key to use in the validation result.
     */
    public LabelChangeWarningValidationRule(String key, StringType valueType) {
        this.key = key;
        this.valueType = valueType;
    }

    /**
     * {@inheritDoc}
     */
    public void validate(ValidationCompletedHandler handler) {
        ValidationResult result = new ValidationResult();
        if (valueType.getOriginalValue() != null && !valueType.getOriginalValue().equals(getValue())) {
            result.add(new ValidationMessage(key != null ? key + KEY_CHANGED_SUFFIX : KEY_CHANGED_SUFFIX, ValidationMessage.Level.INFO, "Please note that by changing the label you change the label for all the existing versions."));
        }

        handler.completed(result);
    }

    /**
     * {@inheritDoc}
     */
//    public boolean isValid() {
//        return true;
//    }

    /**
     * {@inheritDoc}
     */
    public boolean isApplicable() {
        return true;
    }

    /**
     * Implement this method to return the string value to check for.
     *
     * @return the string value to check.
     */
    public abstract String getValue();

}