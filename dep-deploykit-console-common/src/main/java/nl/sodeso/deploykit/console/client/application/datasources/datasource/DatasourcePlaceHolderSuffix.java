package nl.sodeso.deploykit.console.client.application.datasources.datasource;

/**
 * @author Ronald Mathies
 */
public class DatasourcePlaceHolderSuffix {

    public static final String JDBC_JNDI = "-jdbc-jndi";
    public static final String JDBC_URL = "-jdbc-url";
    public static final String JDBC_DRIVER_CLASS = "-jdbc-driver-class";
    public static final String JDBC_USERNAME = "-jdbc-username";
    public static final String JDBC_PASSWORD = "-jdbc-password";

    public static String JDBC_JNDI(String prefix) {
        return prefix + JDBC_JNDI;
    }

    public static String JDBC_URL(String prefix) {
        return prefix + JDBC_URL;
    }

    public static String JDBC_DRIVER_CLASS(String prefix) {
        return prefix = JDBC_DRIVER_CLASS;
    }

    public static String JDBC_USERNAME(String prefix) {
        return prefix + JDBC_USERNAME;
    }

    public static String JDBC_PASSWORD(String prefix) {
        return prefix + JDBC_PASSWORD;
    }

}
