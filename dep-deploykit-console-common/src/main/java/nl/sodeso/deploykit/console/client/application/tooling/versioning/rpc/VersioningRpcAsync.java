package nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public interface VersioningRpcAsync {

    /**
     * @see VersioningRpc#isCommitAllowed(String, VersionOption)
     */
    void isCommitAllowed(String uuid, VersionOption fromVersion, AsyncCallback<ValidationResult> result);

    /**
     * @see VersioningRpc#commit(String, VersionOption, StringType)
     */
    void commit(String uuid, VersionOption fromVersion, StringType toVersion, AsyncCallback<VersionOption> result);

    /**
     * @see VersioningRpc#isBranchAllowed(String, VersionOption)
     */
    void isBranchAllowed(String uuid, VersionOption fromVersion, AsyncCallback<ValidationResult> result);

    /**
     * @see VersioningRpc#branch(String, VersionOption)
     */
    void branch(String uuid, VersionOption fromVersion, AsyncCallback<VersionOption> result);

    /**
     * @see VersioningRpc#versions(String)
     */
    void versions(String uuid, AsyncCallback<List<VersionOption>> result);
}
