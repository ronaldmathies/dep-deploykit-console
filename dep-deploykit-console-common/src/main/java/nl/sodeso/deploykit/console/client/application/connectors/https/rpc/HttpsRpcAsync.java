package nl.sodeso.deploykit.console.client.application.connectors.https.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.connectors.https.Https;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface HttpsRpcAsync extends OptionRpcAsync, VersioningRpcAsync, UsedByRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<HttpsSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<Https> result);

    void save(Https https, AsyncCallback<HttpsSummaryItem> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<HttpsSummaryItem> result);

}
