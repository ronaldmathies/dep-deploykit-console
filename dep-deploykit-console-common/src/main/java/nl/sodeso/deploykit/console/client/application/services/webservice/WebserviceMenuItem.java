package nl.sodeso.deploykit.console.client.application.services.webservice;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.WEBSERVICE_L18N;

/**
 * @author Ronald Mathies
 */
public class WebserviceMenuItem extends MenuItem {

    private static final String KEY = "mi-webservice";

    private static Webservices webservices = null;

    public WebserviceMenuItem() {
        super(KEY, WEBSERVICE_L18N.webservicesMenuItem(), arguments -> {
            if (webservices == null) {
                webservices = new Webservices();
            }

            webservices.setArguments(arguments);
            CenterController.instance().setWidget(webservices);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
