package nl.sodeso.deploykit.console.client.application.node;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface NodeConstants extends Messages {

    @DefaultMessage("Nodes")
    String nodeMenuItem();

    @DefaultMessage("Connection Pool")
    String nodePanel();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Node {0} Used By")
    String nodeUsedByTitle(String name);

}