package nl.sodeso.deploykit.console.client.application.datasources.datasource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc.ConnectionPoolRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class DatasourceConnectionPool implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> connectionPoolOptionType = new OptionType<>();
    private VersionOptionType connectionPoolVersionType = new VersionOptionType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient VersionSelectionField<ConnectionPoolRpcGateway> connectionPoolField = null;

    public DatasourceConnectionPool() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        connectionPoolField = new VersionSelectionField<>(GWT.create(ConnectionPoolRpcGateway.class), connectionPoolOptionType, connectionPoolVersionType);
        connectionPoolField.listenToGroup(groupOptionType);

        entry = new EntryWithWidgetAndWidget("connectionpool", null,
                new HorizontalPanel()
                        .addWidget(connectionPoolField, 100.0, Style.Unit.PCT)
                        .addWidget(new SearchButton.WithIcon((event) -> DatasourceMenuItem.click(connectionPoolOptionType.getValue().getKey(), connectionPoolVersionType.getValue()))));

        entry.addValidationRule(
                new ValidationCriteria.or(new ComboboxMandatoryValidationRule("connectionpool-group-mandatory", ValidationMessage.Level.ERROR, groupOptionType),
                        new ValidationCriteria.or(
                                new ComboboxMandatoryValidationRule("connectionpool-option-mandatory", ValidationMessage.Level.ERROR, connectionPoolOptionType),
                                new ComboboxMandatoryValidationRule("connectionpool-version-mandatory", ValidationMessage.Level.ERROR, connectionPoolVersionType)
                        )
                )
        );

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getConnectionPool() {
        return this.connectionPoolOptionType;
    }

    public void setConnectionPool(OptionType<DefaultOption> connectionPool) {
        this.connectionPoolOptionType = connectionPool;
    }

    public VersionOptionType getConnectionPoolVersion() {
        return connectionPoolVersionType;
    }

    public void setConnectionPoolVersion(VersionOptionType connectionPoolVersionType) {
        this.connectionPoolVersionType = connectionPoolVersionType;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
