package nl.sodeso.deploykit.console.client.application.services.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlMenuItem;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.profile.ProfileMenuItem;
import nl.sodeso.deploykit.console.client.application.services.ldap.rpc.LdapServiceRpcGateway;
import nl.sodeso.deploykit.console.client.application.tooling.delete.DeleteProcess;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsageWindow;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.BranchProcess;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.CommitProcess;
import nl.sodeso.deploykit.console.client.application.ui.button.BranchButton;
import nl.sodeso.deploykit.console.client.application.ui.button.CheckButton;
import nl.sodeso.deploykit.console.client.application.ui.button.CommitButton;
import nl.sodeso.deploykit.console.client.application.ui.button.UsedByButton;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.dialog.UnsavedChangesQuestionPanel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static nl.sodeso.deploykit.console.client.application.Resources.*;
import static nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper.*;

/**
 * @author Ronald Mathies
 */
public class LdapServices extends AbstractCenterPanel {

    private static final String KEY_TABLE= "table";
    private static final String KEY_DETAILS = "details";
    private static final String KEY_CHECK_QUESTION = "check-question";
    private static final String KEY_CHECK_RESULT = "check-result";

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<LdapServiceSummaryItem> table = null;

    private LdapService currentObject = new LdapService();

    private Map<String, String> arguments = null;

    private RemoveButton.WithLabel removeButton;
    private UsedByButton.WithLabel usedByButton;
    private RefreshButton.WithIcon refreshButton;
    private CloneButton.WithLabel cloneButton;
    private AddButton.WithIcon addButton;
    private SaveButton.WithLabel saveButton;
    private CancelButton.WithLabel cancelButton;
    private CheckButton.WithLabel checkButton;
    private CommitButton.WithLabel commitButton;
    private BranchButton.WithLabel branchButton;

    private OptionType<VersionOption> versionOptionType = new OptionType<>();
    private ComboboxField<VersionOption> versionsField = null;

    private LdapServiceRpcGateway gateway = GWT.create(LdapServiceRpcGateway.class);

    public LdapServices() {
        super();

        setFullHeight(true);
    }

    public void setArguments(final Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public void beforeAdd(final Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            table = new SummaryTableField<>();
            table.setFullHeight(true);
            table.addSelectionChangeHandler(event -> loadVersions(() -> {
                if (table.getSelected() != null) {
                    selectVersion();
                }
            }));

            refreshButton = new RefreshButton.WithIcon((event) -> refresh(null));
            addButton = new AddButton.WithIcon((event) -> add());

            final WindowPanel tableWindowPanel = new WindowPanel(KEY_TABLE, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .addToBody(table)
                    .addToToolbar(Align.LEFT, refreshButton)
                    .addToToolbar(Align.RIGHT, addButton);

            horizontalPanel.addWidget(tableWindowPanel, 20, Style.Unit.PCT);

            cloneButton = new CloneButton.WithLabel((event) -> prepareCopy());
            checkButton = new CheckButton.WithLabel((event) -> prepareCheck());
            usedByButton = new UsedByButton.WithLabel((event) -> usedBy());
            removeButton = new RemoveButton.WithLabel((event) -> delete());
            saveButton = new SaveButton.WithLabel((event) -> {
                save(() -> loadVersionDetails());
            });
            cancelButton = new CancelButton.WithLabel((event) -> {
                if (currentObject.getUuid() == null) {
                    table.first();
                } else {
                    detailWindowPanel.revert();
                }
            });

            detailWindowPanel = new WindowPanel(KEY_DETAILS, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .addToBody(currentObject.toEditForm())
                    .addToToolbar(Align.LEFT, cloneButton)
                    .addToToolbar(Align.RIGHT, checkButton, usedByButton)
                    .addToFooter(Align.LEFT, removeButton)
                    .addToFooter(Align.RIGHT, saveButton, cancelButton);

            createVersioningComponents();

            horizontalPanel.addWidget(detailWindowPanel, 80, Style.Unit.PCT);

            evaluateButtonStates();

            add(horizontalPanel);
        }


        Trigger preselect = () -> {
            if (arguments != null && arguments.containsKey(ARG_UUID)) {
                final String uuid = arguments.get(ARG_UUID);
                if (table.getSelected() != null && table.getSelected().getUuid().equals(uuid)) {
                    selectVersion();
                } else {
                    for (LdapServiceSummaryItem summary : table.allItems()) {
                        if (summary.getUuid().equals(uuid)) {
                            table.select(summary);

                            break;
                        }
                    }
                }
            } else {
                if (table.getSelected() == null) {
                    table.first();
                }
            }

            trigger.fire();
        };

        if (table.allItems().isEmpty()) {
            refresh(() -> {
                preselect.fire();
            });
        } else {
            preselect.fire();
        }
    }

    private void selectVersion() {
        String version = "SNAPSHOT";
        VersionType type = VersionType.SNAPSHOT;
        if (arguments != null && arguments.containsKey(ARG_VERSION)) {
            version = arguments.get(ARG_VERSION);
            type = VersionType.valueOf(arguments.get(ARG_TYPE));
        }
        arguments = null;
        versionsField.setSelection(new VersionOption(version, type));
    }

    private void createVersioningComponents() {
        commitButton = new CommitButton.WithLabel((event) -> saveBeforeCommitCheck());
        branchButton = new BranchButton.WithLabel((event) -> saveBeforeBranchCheck());

        versionsField = new ComboboxField<>(versionOptionType);
        versionOptionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                loadVersionDetails();
            }
        });
        versionsField.setWidth("200px");

        detailWindowPanel.addToToolbar(Align.LEFT, versionsField, commitButton, branchButton);
    }

    private void evaluateButtonStates() {
        boolean isSnapshotOrBranch = versionOptionType.getValue() != null && !versionOptionType.getValue().getType().isRelease();
        boolean isCreating = versionOptionType.getValue() == null && currentObject != null;
        boolean isFirst = table.getListDataProvider().getList().isEmpty();

        commitButton.setEnabled(isSnapshotOrBranch);
        saveButton.setEnabled(isSnapshotOrBranch || isCreating);
        cancelButton.setEnabled((isSnapshotOrBranch || isCreating) && !isFirst);
        removeButton.setEnabled(isSnapshotOrBranch || !isCreating);
        usedByButton.setEnabled(!isCreating);
        branchButton.setEnabled(!isSnapshotOrBranch && !isCreating);
        cloneButton.setEnabled(!isCreating);
    }

    private void saveBeforeCommitCheck() {
        saveBeforeAction(this::commit);
    }

    private void commit() {
        new CommitProcess(gateway, currentObject, toVersion ->
                loadVersions(() -> versionsField.setSelection(toVersion))).start();
    }

    private void saveBeforeBranchCheck() {
        saveBeforeAction(this::branch);
    }

    private void branch() {
        new BranchProcess(gateway, currentObject, toVersion ->
                loadVersions(() -> versionsField.setSelection(toVersion))).start();
    }

    private void prepareCopy() {
        saveBeforeAction(this::copy);
    }

    private void copy() {
        gateway.duplicate(currentObject.getUuid(), currentObject.getVersion().getValue(), new DefaultAsyncCallback<LdapServiceSummaryItem>() {
            @Override
            public void success(LdapServiceSummaryItem summary) {
                table.add(summary);
                table.select(summary);
            }
        });
    }

    private void prepareCheck() {
        saveBeforeAction(this::check);
    }

    private void check() {
        EntryForm entryForm = new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, LDAP_SERVICE_I18N.checkMessage()));

        PopupWindowPanel checkStartPanel = new PopupWindowPanel(KEY_CHECK_QUESTION, LDAP_SERVICE_I18N.checkTitle(), WindowPanel.Style.INFO)
                .addToBody(entryForm);

        OkButton.WithLabel okButton = new OkButton.WithLabel((event) -> {
            gateway.check(currentObject.getUuid(), currentObject.getVersion().getValue(), new DefaultAsyncCallback<CheckResult>() {
                @Override
                public void success(CheckResult result) {
                    checkStartPanel.close();

                    EntryForm entryForm = new EntryForm(null)
                            .addEntry(new EntryWithDocumentation(null, result.getMessage()));

                    PopupWindowPanel popupWindowPanel = new PopupWindowPanel(KEY_CHECK_RESULT, LDAP_SERVICE_I18N.checkResultTitle(), result.isSuccess() ? WindowPanel.Style.INFO : WindowPanel.Style.ALERT)
                            .addToBody(entryForm);
                    popupWindowPanel.addToFooter(Align.RIGHT, new OkButton.WithLabel((event) -> popupWindowPanel.close()));

                    popupWindowPanel.center(true, false);
                }
            });
        });

        checkStartPanel.addToFooter(Align.RIGHT, okButton);
        checkStartPanel.center(true, true);
    }

    public void refresh(final Trigger trigger) {
        gateway.findSummaries(new DefaultAsyncCallback<ArrayList<LdapServiceSummaryItem>>() {
            @Override
            public void success(ArrayList<LdapServiceSummaryItem> result) {
                table.replaceAll(result);

                if (trigger != null) {
                    trigger.fire();
                }
            }
        });
    }

    private void delete() {
        new DeleteProcess(gateway, currentObject.getUuid(), currentObject.getVersion().getValue(), () -> {
            loadVersions(() -> {
                if (versionsField.getItems().isEmpty()) {
                    table.remove(table.getSelected());
                } else {
                    versionsField.setSelection(versionsField.getItems().get(0));
                }
            });
        }, () -> usedBy()).start();
    }

    public void saveBeforeAction(final Trigger trigger) {
        if (currentObject.hasChanges()) {
            UnsavedChangesQuestionPanel questionPanel = new UnsavedChangesQuestionPanel(() -> save(trigger));
            questionPanel.center(true, true);
        } else {
            trigger.fire();
        }
    }

    private void save(final Trigger afterSaveTrigger) {
        if (currentObject.hasChanges()) {
            ValidationUtil.validate(result -> {
                if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                    gateway.save(currentObject, new DefaultAsyncCallback<LdapServiceSummaryItem>() {
                        @Override
                        public void success(LdapServiceSummaryItem summary) {
                            if (currentObject.getUuid() == null) {
                                table.add(summary);
                                table.select(summary);
                            } else {
                                table.replace(table.getSelected(), summary);
                            }

                            if (afterSaveTrigger != null) {
                                afterSaveTrigger.fire();
                            }
                        }
                    });
                }
            }, detailWindowPanel);

        }
    }

    private void add() {
        table.deselect();
        detailWindowPanel.clearBody();

        Scheduler.get().scheduleDeferred(() -> {
            currentObject = new LdapService();
            detailWindowPanel.addToBody(currentObject.toEditForm());

            evaluateButtonStates();
        });
    }

    private void usedBy() {
        ((LdapServiceRpcGateway) GWT.create(LdapServiceRpcGateway.class)).findUsages(currentObject.getUuid(), currentObject.getVersion().getValue(), new DefaultAsyncCallback<ArrayList<UsedBy>>() {
            @Override
            public void success(ArrayList<UsedBy> result) {
                UsageWindow usageWindow = new UsageWindow(result, LDAP_SERVICE_I18N.ldapServiceUsedByTitle(currentObject.getLabel().getValue()));
                usageWindow.setUsedByClickHandler(usedBy -> {
                    if (usedBy.getCategory().equals(PROFILE_I18N.profilesMenuItem())) {
                        ProfileMenuItem.click(usedBy.getUuid(), usedBy.getVersion());
                    } else if (usedBy.getCategory().equals(ACCESS_CONTROL_I18N.accessControlMenuItem())) {
                        AccessControlMenuItem.click(usedBy.getUuid(), usedBy.getVersion());
                    }
                });
                usageWindow.center(false, false);
            }
        });
    }

    private void loadVersions(final Trigger versionsLoadedTrigger) {
        LdapServiceSummaryItem item = table.getSelected();
        if (item != null) {
            gateway.versions(item.getUuid(), new DefaultAsyncCallback<List<VersionOption>>() {
                @Override
                public void success(List<VersionOption> result) {
                    versionsField.removeAllItems();
                    versionsField.addItems(result);
                    versionsLoadedTrigger.fire();
                }
            });
        } else {
            versionsField.removeAllItems();
            versionsLoadedTrigger.fire();
        }
    }

    private void loadVersionDetails() {
        VersionOption selectedVersion = versionsField.getSelectedItem();
        if (selectedVersion != null) {
            String uuid = table.getSelected().getUuid();

            gateway.findSingle(uuid, selectedVersion, new DefaultAsyncCallback<LdapService>() {
                @Override
                public void success(LdapService result) {
                    currentObject = result;

                    detailWindowPanel.clearBody();
                    detailWindowPanel.addToBody(currentObject.toEditForm());

                    evaluateButtonStates();
                }
            });
        } else {
            detailWindowPanel.clearBody();
            evaluateButtonStates();
        }
    }

}