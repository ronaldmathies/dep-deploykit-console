package nl.sodeso.deploykit.console.client.application.profile.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.profile.Profile;
import nl.sodeso.deploykit.console.client.application.profile.ProfileSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ProfileRpcAsync extends VersioningRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<ProfileSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<Profile> result);

    void save(Profile profile, AsyncCallback<ProfileSummaryItem> result);
    void check(String uuid, VersionOption version, AsyncCallback<CheckResult> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<ProfileSummaryItem> result);

}
