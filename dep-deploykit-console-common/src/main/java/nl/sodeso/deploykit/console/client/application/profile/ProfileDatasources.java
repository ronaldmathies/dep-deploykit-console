package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class ProfileDatasources extends WidgetCollectionField<ProfileDatasource> {

    private OptionType<DefaultOption> groupOptionType = null;

    private ProfileDatasources() {}

    public ProfileDatasources(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    @Override
    public WidgetSupplier<ProfileDatasource> createWidgetSupplier() {
        return callback -> {
            ProfileDatasource profileDatasource = new ProfileDatasource();
            profileDatasource.setGroup(groupOptionType);
            callback.created(profileDatasource);
        };
    }
}
