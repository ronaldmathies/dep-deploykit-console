package nl.sodeso.deploykit.console.client.application.datasources.connectionpool;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc.ConnectionPoolRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.JavaServerProviderComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.SwitchPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CONNECTION_POOL_I18N;

/**
 * @author Ronald Mathies
 */
public class ConnectionPool extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private transient static final String KEY_GROUP = "group";
    private transient static final String KEY_LABEL = "label";
    private transient static final String KEY_JAVA_PROVIDER = "java-provider";

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private OptionType<DefaultOption> javaServerProviderOptionType = new OptionType<>();

    private JettyConnectionPool jettyConnectionPool = new JettyConnectionPool();
    private JBossConnectionPool jbossConnectionPool = new JBossConnectionPool();

    private transient EntryForm entryForm = null;

    private transient LegendPanel globalConnectionPoolSettings = null;
    private transient TextField labelTextField = null;

    private transient SwitchPanel switchPanel = null;

    public ConnectionPool() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGlobalConnectionPoolSettings();

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntry(new EntryWithContainer(null, this.globalConnectionPoolSettings));

        this.switchPanel = new SwitchPanel(null, true);
        this.switchPanel.addWidget(jettyConnectionPool.toEditForm());
        this.switchPanel.addWidget(jbossConnectionPool.toEditForm());

        if (this.javaServerProviderOptionType.getValue() != null) {
            this.switchPanel.switchTo(this.javaServerProviderOptionType.getValue().getKey());
        }

        this.entryForm.addEntry(new EntryWithContainer(null, this.switchPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupGlobalConnectionPoolSettings() {
        GroupComboboxField groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        this.labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        JavaServerProviderComboboxField javaServerProviderComboboxField = new JavaServerProviderComboboxField(KEY_JAVA_PROVIDER, javaServerProviderOptionType);
        this.javaServerProviderOptionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(javaServerProviderComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                switchPanel.switchTo(javaServerProviderOptionType.getValue().getKey());
            }
        });

        EntryForm connectionPoolForm = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, CONNECTION_POOL_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, CONNECTION_POOL_I18N.labelField(), labelTextField)
                    .addValidationRule(
                        new LabelUniqueValidationRule(GWT.create(ConnectionPoolRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return ConnectionPool.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return ConnectionPool.this.getLabel();
                            }
                        }
                    ),
                new EntryWithLabelAndWidget(KEY_JAVA_PROVIDER, CONNECTION_POOL_I18N.javaServerTypeField(), javaServerProviderComboboxField));

        this.globalConnectionPoolSettings = new LegendPanel(null, CONNECTION_POOL_I18N.connectionPoolPanel());
        this.globalConnectionPoolSettings.add(connectionPoolForm);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public OptionType<DefaultOption> getJavaServerProvider() {
        return javaServerProviderOptionType;
    }

    public void setJavaServerProvider(OptionType<DefaultOption> javaServerProviderOptionType) {
        this.javaServerProviderOptionType = javaServerProviderOptionType;
    }

    public JettyConnectionPool getJettyConnectionPool() {
        return jettyConnectionPool;
    }

    public void setJettyConnectionPool(JettyConnectionPool jettyConnectionPool) {
        this.jettyConnectionPool = jettyConnectionPool;
    }

    public JBossConnectionPool getJBossConnectionPool() {
        return jbossConnectionPool;
    }

    public void setJBossConnectionPool(JBossConnectionPool jbossConnectionPool) {
        this.jbossConnectionPool = jbossConnectionPool;
    }

}
