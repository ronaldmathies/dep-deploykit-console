package nl.sodeso.deploykit.console.client.application.search.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class SearchRpcGateway implements RpcGateway<SearchRpcAsync>, SearchRpcAsync {

    /**
     * {@inheritDoc}
     */
    public SearchRpcAsync getRpcAsync() {
        return GWT.create(SearchRpc.class);
    }
}
