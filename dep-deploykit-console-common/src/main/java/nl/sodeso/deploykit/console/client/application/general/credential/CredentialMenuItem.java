package nl.sodeso.deploykit.console.client.application.general.credential;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.CREDENTIAL_I18N;

/**
 * @author Ronald Mathies
 */
public class CredentialMenuItem extends MenuItem {

    private static final String KEY = "mi-credential";

    private static Credentials credentials = null;

    public CredentialMenuItem() {
        super(KEY, CREDENTIAL_I18N.credentialMenuItem(), arguments -> {
            if (credentials == null) {
                credentials = new Credentials();
            }
            credentials.setArguments(arguments);
            CenterController.instance().setWidget(credentials);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
