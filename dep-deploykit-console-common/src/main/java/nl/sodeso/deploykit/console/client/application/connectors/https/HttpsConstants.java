package nl.sodeso.deploykit.console.client.application.connectors.https;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface HttpsConstants extends Messages {

    @DefaultMessage("SSL/HTTPS")
    String httpsMenuItem();

    @DefaultMessage("SSL/HTTPS")
    String httpsPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Key Store Password")
    String keystorePasswordField();

    @DefaultMessage("Trust Store Password")
    String trustStorePasswordField();

    @DefaultMessage("Key Manager Password")
    String keyManagerPasswordField();

    @DefaultMessage("Keystore")
    String keystoreField();

    @DefaultMessage("Port this connector listens on. If set the 0 a random port is assigned.")
    String portFieldDocumentation();

    @DefaultMessage("Port")
    String portField();

    @DefaultMessage("Timeout")
    String timeoutField();

    @DefaultMessage("Milliseconds")
    String timeoutFieldHint();

    @DefaultMessage("Certificates")
    String certificates();

    @DefaultMessage("Below is a listing of all certificates found in the keystore. By default when a certificate expires an e-mail will be send to the administrators notifying them of this situation.")
    String certificatesDocumentation();

    @DefaultMessage("Alias")
    String aliasColumn();

    @DefaultMessage("Not valid before")
    String notValidBeforeColumn();

    @DefaultMessage("Not valid after")
    String notValidAfterColumn();

    @DefaultMessage("Subject DN")
    String subjectDnColumn();

    @DefaultMessage("Algorithm")
    String algorithmColumn();

    @DefaultMessage("The keystore does not contain any certificates.")
    String noCertificateEntriesMessage();

    @DefaultMessage("SSL/HTTPS {0} Used By")
    String httpsUsedByTitle(String name);
}