package nl.sodeso.deploykit.console.client.application.ui.properties;

import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class KeyValueProperty implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_KEY = "key";
    private static final String KEY_VALUE = "value";

    private String uuid = null;
    private StringType key = new StringType();
    private StringType value = new StringType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient TextField keyField = null;
    private transient TextField valueField = null;

    public KeyValueProperty() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        keyField = new TextField<>(KEY_KEY, this.key)
            .setPlaceholder(KEY_KEY);
        keyField.addValidationRule(new MandatoryValidationRule(KEY_KEY, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return keyField.getValue();
            }
        });
        valueField = new TextField<>(KEY_VALUE, this.value)
            .setPlaceholder(KEY_VALUE);
        valueField.addValidationRule(new MandatoryValidationRule(KEY_VALUE, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return valueField.getValue();
            }
        });

        entry = new EntryWithWidgetAndWidget(null, keyField, valueField);

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getKey() {
        return this.key;
    }

    public void setKey(StringType key) {
        this.key = key;
    }

    public StringType getValue() {
        return this.value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
