package nl.sodeso.deploykit.console.client.application.ui.options.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ReferenceTablesRpcGateway implements RpcGateway<ReferenceTablesRpcAsync>, ReferenceTablesRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ReferenceTablesRpcAsync getRpcAsync() {
        return GWT.create(ReferenceTablesRpc.class);
    }
}
