package nl.sodeso.deploykit.console.client.application.group;

import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.security.role.RoleComboboxField;
import nl.sodeso.deploykit.console.client.application.security.role.RoleMenuItem;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.GROUP_I18N;
import static nl.sodeso.deploykit.console.client.application.Resources.ROLE_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class GroupRolePermission implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private static final String KEY_PERMISSIONS = "permissions";
    private static final String KEY_ROLE = "role";
    private static final String KEY_ALLOW_TO_READ = "allow-to-read";
    private static final String KEY_ALLOW_TO_USE = "allow-to-use";
    private static final String KEY_ALLOW_TO_EDIT = "allow-to-edit";
    private static final String KEY_ALLOW_TO_DEPLOY = "allow-to-deploy";

    private String uuid = null;
    private OptionType<DefaultOption> roleOptionType = new OptionType<>();

    private BooleanType allowToRead = new BooleanType(true);
    private BooleanType allowToUse = new BooleanType(false);
    private BooleanType allowToEdit = new BooleanType(false);
    private BooleanType allowToDeploy = new BooleanType(false);

    private transient EntryWithSingleWidget entry = null;
    private transient RoleComboboxField roleField = null;
    private transient RadioButtonGroupField allowToViewField = null;
    private transient RadioButtonGroupField allowToUseField = null;
    private transient RadioButtonGroupField allowToEditField = null;
    private transient RadioButtonGroupField allowToDeployField = null;

    public GroupRolePermission() {
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        roleField = new RoleComboboxField(roleOptionType);
        roleField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_ROLE, ValidationMessage.Level.ERROR, roleOptionType));

        allowToViewField = new RadioButtonGroupField(KEY_ALLOW_TO_READ, this.allowToRead);
        allowToViewField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToViewField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToViewField.setAlignment(Align.HORIZONTAL);

        allowToUseField = new RadioButtonGroupField(KEY_ALLOW_TO_USE, this.allowToUse);
        allowToUseField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToUseField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToUseField.setAlignment(Align.HORIZONTAL);

        allowToEditField = new RadioButtonGroupField(KEY_ALLOW_TO_EDIT, this.allowToEdit);
        allowToEditField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToEditField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToEditField.setAlignment(Align.HORIZONTAL);

        allowToDeployField = new RadioButtonGroupField(KEY_ALLOW_TO_DEPLOY, this.allowToDeploy);
        allowToDeployField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToDeployField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToDeployField.setAlignment(Align.HORIZONTAL);

        entry = new EntryWithSingleWidget(null,
            new EntryForm(KEY_PERMISSIONS)
                .addEntries(
                    new EntryWithLabelAndWidget(null, GROUP_I18N.roleField(),
                        new VerticalPanel(
                            new HorizontalPanel()
                                .addWidget(roleField, 100.0, Style.Unit.PCT)
                                .addWidget(new SearchButton.WithIcon((event) -> RoleMenuItem.click(roleOptionType.getValue().getKey()))),
                            new EntryForm(null)
                                .addEntries(
                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToViewField(), allowToViewField),
                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToUseField(), allowToUseField),
                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToEditField(), allowToEditField),
                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToDeployField(), allowToDeployField)
                                )
                            )
                        )));

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getRole() {
        return this.roleOptionType;
    }

    public void setRole(OptionType<DefaultOption> node) {
        this.roleOptionType = node;
    }

    public BooleanType getAllowToRead() {
        return allowToRead;
    }

    public void setAllowToRead(BooleanType allowToRead) {
        this.allowToRead = allowToRead;
    }

    public BooleanType getAllowToUse() {
        return allowToUse;
    }

    public void setAllowToUse(BooleanType allowToUse) {
        this.allowToUse = allowToUse;
    }

    public BooleanType getAllowToEdit() {
        return allowToEdit;
    }

    public void setAllowToEdit(BooleanType allowToEdit) {
        this.allowToEdit = allowToEdit;
    }

    public BooleanType getAllowToDeploy() {
        return allowToDeploy;
    }

    public void setAllowToDeploy(BooleanType allowToDeploy) {
        this.allowToDeploy = allowToDeploy;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
