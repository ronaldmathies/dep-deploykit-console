package nl.sodeso.deploykit.console.client.application.search;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.form.table.DefaultTableCellGroupHeader;

import java.util.List;

import static nl.sodeso.deploykit.console.client.application.Resources.SEARCH_I18N;

/**
 * @author Ronald Mathies
 */
public class ResultTable extends CellTableField<SearchResult> {

    public ResultTable() {
        super("result-table", 10);

        this.setTableBuilder(new DefaultTableCellGroupHeader<SearchResult>(this, getListDataProvider()) {
            @Override
            public String getHeaderText(SearchResult result) {
                return result.getType().name();
            }
        });

        // Add a column for displaying the labels.
        TextColumn<SearchResult> column = new TextColumn<SearchResult>() {
            @Override
            public String getValue(SearchResult result) {
                return result.getLabel() + (result.getVersion() != null ? " " + result.getVersion().getDisplayDescription() : "");
            }
        };
        this.addColumn(column);

        // Create a sort handler.
        ColumnSortEvent.ListHandler<SearchResult> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(column, (o1, o2) -> o1.getLabel().compareTo(o2.getLabel()));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        column.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(column, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 100, com.google.gwt.dom.client.Style.Unit.PCT);

        this.setEmptyTableWidget(new DefaultEmptyTableMessage("not-used", SEARCH_I18N.noResultsFoundMessage()));
    }

    public void setItems(List<SearchResult> list) {
        this.getListDataProvider().setList(list);
    }

}
