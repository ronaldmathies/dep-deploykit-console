package nl.sodeso.deploykit.console.client.application.ui.options;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.ReferenceTablesRpcGateway;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class AccessControlTypeComboboxField extends ComboboxField<DefaultOption> {

    public AccessControlTypeComboboxField(String key, OptionType<DefaultOption> optionValue) {
        super(key, optionValue);

        this.addValidationRule(new ComboboxMandatoryValidationRule("accesscontroltype", ValidationMessage.Level.ERROR, optionValue));

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                ((ReferenceTablesRpcGateway) GWT.create(ReferenceTablesRpcGateway.class)).accessControlTypes(new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                    @Override
                    public void success(ArrayList<DefaultOption> result) {
                        getItems().clear();
                        addItems(result);
                    }
                });
            }
        });
    }
}
