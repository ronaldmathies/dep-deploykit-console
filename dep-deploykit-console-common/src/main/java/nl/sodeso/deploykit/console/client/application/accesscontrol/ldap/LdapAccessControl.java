package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceMenuItem;
import nl.sodeso.deploykit.console.client.application.services.ldap.rpc.LdapServiceRpcGateway;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;

/**
 * @author Ronald Mathies
 */
public class LdapAccessControl implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_LDAP = "ldap";
    private static final String KEY_LDAP_SERVICE = "ldap-service";

    private static final String KEY_SERVER = "server";

    private static final String KEY_USER_BASE_DN = "user-base-dn";
    private static final String KEY_USER_ATTRIBUTE = "user-attribute";
    private static final String KEY_USER_PASSWORD_ATTRIBUTE = "user-password-attribute";
    private static final String KEY_USER_OBJECT_CLASS = "user-object-class";

    private static final String KEY_ROLE_BASE_DN = "role-base-dn";
    private static final String KEY_ROLE_MEMBER_ATTRIBUTE = "role-member-attribute";
    private static final String KEY_ROLE_NAME_ATTRIBUTE = "role-name-attribute";
    private static final String KEY_ROLE_OBJECT_CLASS = "role-object-class";

    private OptionType<DefaultOption> ldapServiceOptionType = new OptionType<>();
    private VersionOptionType ldapServiceVersionType = new VersionOptionType();

    private StringType userBaseDn = new StringType();
    private StringType userAttribute = new StringType();
    private StringType roleBaseDn = new StringType();
    private StringType roleMemberAttribute = new StringType();
    private StringType roleNameAttribute = new StringType();
    private StringType userPasswordAttribute = new StringType();
    private StringType userObjectClass = new StringType();
    private StringType roleObjectClass = new StringType();

    private transient AccessControl accessControl = null;

    private transient EntryForm entryForm = null;

    private transient LegendPanel serverLegendPanel = null;
    private transient VersionSelectionField<LdapServiceRpcGateway> ldapServiceSelectionField = null;

    private transient EntryForm ldapSettingsForm = null;
    private transient TextField userBaseDnTextField = null;
    private transient TextField userAttributeTextField = null;
    private transient TextField roleBaseDnTextField = null;
    private transient TextField roleMemberAttributeTextField = null;
    private transient TextField roleNameAttributeTextField = null;
    private transient TextField userPasswordAttributeTextField = null;
    private transient TextField userObjectClassTextField = null;
    private transient TextField roleObjectClassTextField = null;

    public LdapAccessControl() {}

    public Widget toEditForm(AccessControl accessControl) {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        this.accessControl = accessControl;

        setupServerSettings();
        setupLdapSettings();

        this.entryForm = new EntryForm(KEY_LDAP);
        this.entryForm.addEntry(new EntryWithContainer("", this.serverLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer("", this.ldapSettingsForm));

        return this.entryForm;
    }

    private void setupServerSettings() {
        this.ldapServiceSelectionField = new VersionSelectionField<>(GWT.create(LdapServiceRpcGateway.class), ldapServiceOptionType, ldapServiceVersionType);
        this.ldapServiceSelectionField.listenToGroup(accessControl.getGroup());

        HorizontalPanel horizontalPanel = new HorizontalPanel()
            .addWidget(ldapServiceSelectionField, 100.0, Style.Unit.PCT)
            .addWidget(new SearchButton.WithIcon((event) -> LdapServiceMenuItem.click(ldapServiceOptionType.getValue().getKey(), ldapServiceVersionType.getValue())));

        EntryWithLabelAndWidget entry = new EntryWithLabelAndWidget(KEY_LDAP_SERVICE, ACCESS_CONTROL_I18N.ldapServiceField(), horizontalPanel);
        entry.addValidationRule(
                new ValidationCriteria.or(
                        new ComboboxMandatoryValidationRule(KEY_LDAP_SERVICE, ValidationMessage.Level.ERROR, ldapServiceOptionType),
                        new ComboboxMandatoryValidationRule(KEY_LDAP_SERVICE, ValidationMessage.Level.ERROR, ldapServiceVersionType)
                )
        );

        EntryForm form = new EntryForm(null)
            .addEntries(
                    new EntryWithDocumentation(KEY_SERVER, ACCESS_CONTROL_I18N.ldapServiceDocumentation()),
                    entry);

        this.serverLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.ldapServicePanel());
        this.serverLegendPanel.add(form);
    }

    private void setupLdapSettings() {
        ldapSettingsForm = new EntryForm(null);

        {
            this.userBaseDnTextField = new TextField<>(KEY_USER_BASE_DN, this.userBaseDn);
            this.userBaseDnTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_BASE_DN, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userBaseDnTextField.getValue();
                }
            });

            this.userAttributeTextField = new TextField<>(KEY_USER_ATTRIBUTE, this.userAttribute);
            this.userAttributeTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_ATTRIBUTE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userAttributeTextField.getValue();
                }
            });

            this.userPasswordAttributeTextField = new TextField<>(KEY_USER_PASSWORD_ATTRIBUTE, this.userPasswordAttribute);
            this.userPasswordAttributeTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_PASSWORD_ATTRIBUTE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userPasswordAttributeTextField.getValue();
                }
            });

            this.userObjectClassTextField = new TextField<>(KEY_USER_OBJECT_CLASS, this.userObjectClass);
            this.userObjectClassTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_OBJECT_CLASS, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userObjectClassTextField.getValue();
                }
            });

            EntryForm userForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_USER_BASE_DN, ACCESS_CONTROL_I18N.userBaseDnDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USER_BASE_DN, ACCESS_CONTROL_I18N.userBaseDnField(), new VerticalPanel(userBaseDnTextField,
                        new PlaceholderSpanField(KEY_USER_BASE_DN, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_USER_BASEDN)))
                        .setHint(ACCESS_CONTROL_I18N.userBaseDnFieldHint())
                        .addValidatableWidget(userBaseDnTextField),
                    new EntryWithDocumentation(KEY_USER_ATTRIBUTE, ACCESS_CONTROL_I18N.userAttributeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USER_ATTRIBUTE, ACCESS_CONTROL_I18N.userAttributeField(), new VerticalPanel(userAttributeTextField,
                        new PlaceholderSpanField(KEY_USER_ATTRIBUTE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_USER_ATTRIBUTE)))
                        .setHint(ACCESS_CONTROL_I18N.userAttributeFieldHint())
                        .addValidatableWidget(userAttributeTextField),
                    new EntryWithDocumentation(KEY_USER_PASSWORD_ATTRIBUTE, ACCESS_CONTROL_I18N.passwordAttributeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USER_PASSWORD_ATTRIBUTE, ACCESS_CONTROL_I18N.passwordAttributeField(), new VerticalPanel(userPasswordAttributeTextField,
                        new PlaceholderSpanField(KEY_USER_PASSWORD_ATTRIBUTE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_USER_PASSWORD_ATTRIBUTE)))
                        .setHint(ACCESS_CONTROL_I18N.passwordAttributeFieldHint()),
                    new EntryWithDocumentation(KEY_USER_OBJECT_CLASS, ACCESS_CONTROL_I18N.userObjectClassDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USER_OBJECT_CLASS, ACCESS_CONTROL_I18N.userObjectClassField(), new VerticalPanel(userObjectClassTextField,
                        new PlaceholderSpanField(KEY_USER_OBJECT_CLASS, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_USER_OBJECTCLASS)))
                        .setHint(ACCESS_CONTROL_I18N.userObjectClassFieldHint())
                        .addValidatableWidget(userObjectClassTextField));

            LegendPanel userLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.userConfigurationPanel());
            userLegendPanel.add(userForm);
            ldapSettingsForm.addEntry(new EntryWithContainer(null, userLegendPanel));
        }

        {
            this.roleBaseDnTextField = new TextField<>(KEY_ROLE_BASE_DN, this.roleBaseDn);
            this.roleBaseDnTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_BASE_DN, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleBaseDnTextField.getValue();
                }
            });

            this.roleMemberAttributeTextField = new TextField<>(KEY_ROLE_MEMBER_ATTRIBUTE, this.roleMemberAttribute);
            this.roleMemberAttributeTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_MEMBER_ATTRIBUTE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleMemberAttributeTextField.getValue();
                }
            });

            this.roleNameAttributeTextField = new TextField<>(KEY_ROLE_NAME_ATTRIBUTE, this.roleNameAttribute);
            this.roleNameAttributeTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_NAME_ATTRIBUTE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleNameAttributeTextField.getValue();
                }
            });

            this.roleObjectClassTextField = new TextField<>(KEY_ROLE_OBJECT_CLASS, this.roleObjectClass);
            this.roleObjectClassTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_OBJECT_CLASS, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleObjectClassTextField.getValue();
                }
            });

            EntryForm roleForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_ROLE_BASE_DN, ACCESS_CONTROL_I18N.roleBaseDnDocumentation()),
                    new EntryWithLabelAndWidget(KEY_ROLE_BASE_DN, ACCESS_CONTROL_I18N.roleBaseDnField(), new VerticalPanel(roleBaseDnTextField,
                        new PlaceholderSpanField(KEY_ROLE_BASE_DN, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_BASEDN)))
                        .setHint(ACCESS_CONTROL_I18N.roleBaseDnFieldHint())
                        .addValidatableWidget(roleBaseDnTextField),
                    new EntryWithDocumentation(KEY_ROLE_MEMBER_ATTRIBUTE, ACCESS_CONTROL_I18N.roleMemberAttributeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_ROLE_MEMBER_ATTRIBUTE, ACCESS_CONTROL_I18N.roleMemberAttributeField(), new VerticalPanel(roleMemberAttributeTextField,
                        new PlaceholderSpanField(KEY_ROLE_MEMBER_ATTRIBUTE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_MEMBER_ATTRIBUTE)))
                        .setHint(ACCESS_CONTROL_I18N.roleMemberAttributeFieldHint())
                        .addValidatableWidget(roleMemberAttributeTextField),
                    new EntryWithDocumentation(KEY_ROLE_NAME_ATTRIBUTE, ACCESS_CONTROL_I18N.roleNameAttributeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_ROLE_NAME_ATTRIBUTE, ACCESS_CONTROL_I18N.roleNameAttributeField(), new VerticalPanel(roleNameAttributeTextField,
                        new PlaceholderSpanField(KEY_ROLE_NAME_ATTRIBUTE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_NAME_ATTRIBUTE)))
                        .setHint(ACCESS_CONTROL_I18N.roleNameAttributeFieldHint())
                        .addValidatableWidget(roleNameAttributeTextField),
                    new EntryWithDocumentation(KEY_ROLE_OBJECT_CLASS, ACCESS_CONTROL_I18N.roleObjectClassDocumentation()),
                    new EntryWithLabelAndWidget(KEY_ROLE_OBJECT_CLASS, ACCESS_CONTROL_I18N.roleObjectClassField(), new VerticalPanel(roleObjectClassTextField,
                        new PlaceholderSpanField(KEY_ROLE_OBJECT_CLASS, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_LDAP_ROLE_OBJECTCLASS)))
                        .setHint(ACCESS_CONTROL_I18N.roleObjectClassFieldHint())
                        .addValidatableWidget(roleObjectClassTextField));

            LegendPanel roleLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.roleConfigurationPanel());
            roleLegendPanel.add(roleForm);
            ldapSettingsForm.addEntry(new EntryWithContainer(null, roleLegendPanel));
        }
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getLdapService() {
        return ldapServiceOptionType;
    }

    public void setLdapService(OptionType<DefaultOption> optionType) {
        this.ldapServiceOptionType = optionType;
    }

    public VersionOptionType getLdapServiceVersion() {
        return ldapServiceVersionType;
    }

    public void setLdapServiceVersion(VersionOptionType ldapServiceVersionType) {
        this.ldapServiceVersionType = ldapServiceVersionType;
    }

    public StringType getUserBaseDn() {
        return userBaseDn;
    }

    public void setUserBaseDn(StringType userBaseDn) {
        this.userBaseDn = userBaseDn;
    }

    public StringType getUserAttribute() {
        return userAttribute;
    }

    public void setUserAttribute(StringType userAttribute) {
        this.userAttribute = userAttribute;
    }

    public StringType getRoleBaseDn() {
        return roleBaseDn;
    }

    public void setRoleBaseDn(StringType roleBaseDn) {
        this.roleBaseDn = roleBaseDn;
    }

    public StringType getRoleMemberAttribute() {
        return roleMemberAttribute;
    }

    public void setRoleMemberAttribute(StringType roleMemberAttribute) {
        this.roleMemberAttribute = roleMemberAttribute;
    }

    public StringType getRoleNameAttribute() {
        return roleNameAttribute;
    }

    public void setRoleNameAttribute(StringType roleNameAttribute) {
        this.roleNameAttribute = roleNameAttribute;
    }

    public StringType getUserPasswordAttribute() {
        return userPasswordAttribute;
    }

    public void setUserPasswordAttribute(StringType userPasswordAttribute) {
        this.userPasswordAttribute = userPasswordAttribute;
    }

    public StringType getUserObjectClass() {
        return userObjectClass;
    }

    public void setUserObjectClass(StringType userObjectClass) {
        this.userObjectClass = userObjectClass;
    }

    public StringType getRoleObjectClass() {
        return roleObjectClass;
    }

    public void setRoleObjectClass(StringType roleObjectClass) {
        this.roleObjectClass = roleObjectClass;
    }

}
