package nl.sodeso.deploykit.console.client.application.services.ldap.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapService;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface LdapServiceRpcAsync extends OptionRpcAsync, VersioningRpcAsync, LabelUniqueRpcAsync, DeleteRpcAsync, UsedByRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<LdapServiceSummaryItem>> result);
    void findSingle(String uuid, VersionOption version, AsyncCallback<LdapService> result);

    void save(LdapService ldapService, AsyncCallback<LdapServiceSummaryItem> result);

    void check(String uuid, VersionOption version, AsyncCallback<CheckResult> result);

    void duplicate(String uuid, VersionOption version, AsyncCallback<LdapServiceSummaryItem> result);

}
