package nl.sodeso.deploykit.console.client.application.node.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.node.Node;
import nl.sodeso.deploykit.console.client.application.node.NodeSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-node")
public interface NodeRpc extends XsrfProtectedService, UsedByRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<NodeSummaryItem> findSummaries();
    Node findSingle(String uuid);

    ArrayList<DefaultOption> asOptions(DefaultOption group);

    NodeSummaryItem save(Node node) throws RemoteException;
    NodeSummaryItem duplicate(String uuid);

}
