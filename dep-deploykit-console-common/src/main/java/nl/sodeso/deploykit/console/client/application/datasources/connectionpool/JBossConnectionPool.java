package nl.sodeso.deploykit.console.client.application.datasources.connectionpool;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CONNECTION_POOL_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * Documentation for JBoss connection pool:
 *
 * http://www.ironjacamar.org/doc/userguide/1.2/en-US/html_single/index.html
 *
 * @author Ronald Mathies
 */
public class JBossConnectionPool implements Serializable, IsValidationContainer, IsRevertable {

    private transient static final String KEY_JBOSS = "jboss";
    private transient static final String KEY_MIN_POOL_SIZE = "minpool-size";
    private transient static final String KEY_MAX_POOL_SIZE = "maxpool-size";
    private transient static final String KEY_INITIAL_POOL_SIZE = "initialpool-size";
    private transient static final String KEY_VALIDATION_QUERY = "validation-query";
    private transient static final String KEY_CONNECTION_INIT_SQLS = "connection-init-sqls";
    private transient static final String KEY_PREFILL = "prefill";
    private transient static final String KEY_DEFAULTT_RANSACTION_ISOLATION = "default-transaction-isolation";
    private transient static final String KEY_BLOCKING_TIMEOUT_MILLIS = "blocking-timeout-millis";
    private transient static final String KEY_IDLE_TIMEOUT_MINUTES = "idle-timeout-minutes";
    private transient static final String KEY_ALLOCATION_RETRY = "allocation-retry";
    private transient static final String KEY_ALLOCATION_RETRY_WAIT_MILLIS = "allocation-retry-wait-millis";

    private IntType minPoolSize = new IntType(0);
    private IntType maxPoolSize = new IntType(20);
    private IntType initialPoolSize = new IntType(0);
    private BooleanType prefill = new BooleanType(false);

    private IntType blockingTimeoutMillis = new IntType(30000);
    private IntType idleTimeoutMinutes = new IntType(0);
    private IntType allocationRetry = new IntType(0);
    private IntType allocationRetryWaitMillis = new IntType(5000);

    private OptionType<DefaultOption> defaultTransactionIsolation = new OptionType<>(new DefaultOption("default", "default", "DRIVER_DEFAULT"));

    private StringType validationQuery = new StringType();
    private StringType connectionInitSqls = new StringType();

    private transient EntryForm entryForm = null;
    private transient LegendPanel connectionPoolLegendPanel = null;
    private transient EntryForm connectionPoolEntryForm = null;
    private transient TextField minPoolSizeField = null;
    private transient TextField maxPoolSizeField = null;
    private transient TextField initialPoolSizeField = null;
    private transient TextField validationQueryField = null;
    private transient ComboboxField<DefaultOption> defaultTransactionIsolationField = null;
    private transient TextField connectionInitSqlsField = null;
    private transient RadioButtonGroupField prefillGroupField = null;

    private transient LegendPanel timeoutLegendPanel;
    private transient EntryForm timeoutEntryForm = null;
    private transient TextField blockingTimeoutMillisField = null;
    private transient TextField idleTimeoutMinutesField = null;
    private transient TextField allocationRetryField = null;
    private transient TextField allocationRetryWaitMillisField = null;

    public JBossConnectionPool() {
    }

    public Widget toEditForm() {
        if (entryForm != null) {
            return entryForm;
        }

        setupConnectionPoolSettings();
        setupTimeoutSettings();

        entryForm = new EntryForm(KEY_JBOSS)
            .addEntry(new EntryWithContainer(null, connectionPoolLegendPanel))
            .addEntry(new EntryWithContainer(null, timeoutLegendPanel));

        return entryForm;
    }

    private void setupConnectionPoolSettings() {
        connectionPoolLegendPanel = new LegendPanel(null, CONNECTION_POOL_I18N.connectionPoolPropertiesPanel());
        connectionPoolEntryForm = new EntryForm(null);

        minPoolSizeField = new TextField<>(KEY_MIN_POOL_SIZE, minPoolSize).addFilter(new NumericInputFilter(false));
        maxPoolSizeField = new TextField<>(KEY_MAX_POOL_SIZE, maxPoolSize).addFilter(new NumericInputFilter(false));
        initialPoolSizeField = new TextField<>(KEY_INITIAL_POOL_SIZE, initialPoolSize).addFilter(new NumericInputFilter(false));
        validationQueryField = new TextField<>(KEY_VALIDATION_QUERY, validationQuery);
        connectionInitSqlsField = new TextField<>(KEY_CONNECTION_INIT_SQLS, connectionInitSqls);

        prefillGroupField = new RadioButtonGroupField(KEY_PREFILL, prefill);
        prefillGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        prefillGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        prefillGroupField.setAlignment(Align.HORIZONTAL);

        defaultTransactionIsolationField = new ComboboxField<>(KEY_DEFAULTT_RANSACTION_ISOLATION, defaultTransactionIsolation);
        defaultTransactionIsolationField.addItems(
                new DefaultOption("default", "default", "DRIVER_DEFAULT"),
                new DefaultOption("none", "none", "NONE"),
                new DefaultOption("readcommited", "readcommited", "READ_COMMITED"),
                new DefaultOption("readuncommited", "readuncommited", "READ_UNCOMMITTED"),
                new DefaultOption("repeatableread", "repeatableread", "REPEATABLE_READ"),
                new DefaultOption("serializable", "serializable", "SERIALIZABLE"));

        connectionPoolEntryForm.addEntries(
                new EntryWithDocumentation(KEY_MIN_POOL_SIZE, CONNECTION_POOL_I18N.minimumPoolSizeDocumentation()),
                new EntryWithLabelAndWidget(KEY_MIN_POOL_SIZE, CONNECTION_POOL_I18N.minimumPoolSizeField(), minPoolSizeField),
                new EntryWithDocumentation(KEY_MAX_POOL_SIZE, CONNECTION_POOL_I18N.maximumPoolSizeDocumentationJboss()),
                new EntryWithLabelAndWidget(KEY_MAX_POOL_SIZE, CONNECTION_POOL_I18N.maximumPoolSizeField(), maxPoolSizeField),
                new EntryWithDocumentation(KEY_INITIAL_POOL_SIZE, CONNECTION_POOL_I18N.initialPoolSizeDocumentation()),
                new EntryWithLabelAndWidget(KEY_INITIAL_POOL_SIZE, CONNECTION_POOL_I18N.initialPoolSizeField(), initialPoolSizeField),
                new EntryWithDocumentation(KEY_PREFILL, CONNECTION_POOL_I18N.prefillDocumentation()),
                new EntryWithLabelAndWidget(KEY_PREFILL, CONNECTION_POOL_I18N.prefillField(), prefillGroupField),
                new EntryWithDocumentation(KEY_DEFAULTT_RANSACTION_ISOLATION, CONNECTION_POOL_I18N.transactionIsolationDocumentation()),
                new EntryWithLabelAndWidget(KEY_DEFAULTT_RANSACTION_ISOLATION, CONNECTION_POOL_I18N.transactionIsolationField(), defaultTransactionIsolationField),
                new EntryWithDocumentation(KEY_VALIDATION_QUERY, CONNECTION_POOL_I18N.validationQueryDocumentationJboss()),
                new EntryWithLabelAndWidget(KEY_VALIDATION_QUERY, CONNECTION_POOL_I18N.validationQueryFieldJboss(), validationQueryField),
                new EntryWithDocumentation(KEY_CONNECTION_INIT_SQLS, CONNECTION_POOL_I18N.initSqlStatementDocumentation()),
                new EntryWithLabelAndWidget(KEY_CONNECTION_INIT_SQLS, CONNECTION_POOL_I18N.initSqlStatementField(), connectionInitSqlsField));

        this.connectionPoolLegendPanel.add(this.connectionPoolEntryForm);
    }

    private void setupTimeoutSettings() {
        blockingTimeoutMillisField = new TextField<>(KEY_BLOCKING_TIMEOUT_MILLIS, blockingTimeoutMillis).addFilter(new NumericInputFilter(false));
        idleTimeoutMinutesField = new TextField<>(KEY_IDLE_TIMEOUT_MINUTES, idleTimeoutMinutes).addFilter(new NumericInputFilter(false));
        allocationRetryField = new TextField<>(KEY_ALLOCATION_RETRY, allocationRetry).addFilter(new NumericInputFilter(false));
        allocationRetryWaitMillisField = new TextField<>(KEY_ALLOCATION_RETRY_WAIT_MILLIS, allocationRetryWaitMillis).addFilter(new NumericInputFilter(false));

        timeoutEntryForm = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_BLOCKING_TIMEOUT_MILLIS, CONNECTION_POOL_I18N.blockingTimeoutMillisecondsDocumentation()),
                        new EntryWithLabelAndWidget(KEY_BLOCKING_TIMEOUT_MILLIS, CONNECTION_POOL_I18N.blockingTimeoutMillisecondsField(), blockingTimeoutMillisField),
                        new EntryWithDocumentation(KEY_IDLE_TIMEOUT_MINUTES, CONNECTION_POOL_I18N.idleTimeoutMinutesDocumentation()),
                        new EntryWithLabelAndWidget(KEY_IDLE_TIMEOUT_MINUTES, CONNECTION_POOL_I18N.idleTimeoutMinutesField(), idleTimeoutMinutesField),
                        new EntryWithDocumentation(KEY_ALLOCATION_RETRY, CONNECTION_POOL_I18N.allocationRetryAttemtsDocumentation()),
                        new EntryWithLabelAndWidget(KEY_ALLOCATION_RETRY, CONNECTION_POOL_I18N.allocationRetryAttemtsField(), allocationRetryField),
                        new EntryWithDocumentation(KEY_ALLOCATION_RETRY_WAIT_MILLIS, CONNECTION_POOL_I18N.allocationRetryWaitDocumentation()),
                        new EntryWithLabelAndWidget(KEY_ALLOCATION_RETRY_WAIT_MILLIS, CONNECTION_POOL_I18N.allocationRetryWaitField(), allocationRetryWaitMillisField));

        timeoutLegendPanel = new LegendPanel(null, CONNECTION_POOL_I18N.timeoutSettingsPanel());
        timeoutLegendPanel.add(timeoutEntryForm);
    }


        /**
         * {@inheritDoc}
         */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public IntType getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(IntType minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public IntType getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(IntType maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public IntType getInitialPoolSize() {
        return initialPoolSize;
    }

    public void setInitialPoolSize(IntType initialPoolSize) {
        this.initialPoolSize = initialPoolSize;
    }

    public BooleanType getPrefill() {
        return prefill;
    }

    public void setPrefill(BooleanType prefill) {
        this.prefill = prefill;
    }

    public IntType getBlockingTimeoutMillis() {
        return blockingTimeoutMillis;
    }

    public void setBlockingTimeoutMillis(IntType blockingTimeoutMillis) {
        this.blockingTimeoutMillis = blockingTimeoutMillis;
    }

    public IntType getIdleTimeoutMinutes() {
        return idleTimeoutMinutes;
    }

    public void setIdleTimeoutMinutes(IntType idleTimeoutMinutes) {
        this.idleTimeoutMinutes = idleTimeoutMinutes;
    }

    public IntType getAllocationRetry() {
        return allocationRetry;
    }

    public void setAllocationRetry(IntType allocationRetry) {
        this.allocationRetry = allocationRetry;
    }

    public IntType getAllocationRetryWaitMillis() {
        return allocationRetryWaitMillis;
    }

    public void setAllocationRetryWaitMillis(IntType allocationRetryWaitMillis) {
        this.allocationRetryWaitMillis = allocationRetryWaitMillis;
    }

    public StringType getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(StringType validationQuery) {
        this.validationQuery = validationQuery;
    }

    public StringType getConnectionInitSqls() {
        return connectionInitSqls;
    }

    public void setConnectionInitSqls(StringType connectionInitSqls) {
        this.connectionInitSqls = connectionInitSqls;
    }

    public OptionType<DefaultOption> getDefaultTransactionIsolation() {
        return defaultTransactionIsolation;
    }

    public void setDefaultTransactionIsolation(OptionType<DefaultOption> defaultTransactionIsolation) {
        this.defaultTransactionIsolation = defaultTransactionIsolation;
    }
}
