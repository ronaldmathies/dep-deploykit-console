package nl.sodeso.deploykit.console.client.application.connectors.https;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.HTTPS_I18N;

/**
 * @author Ronald Mathies
 */
public class HttpsMenuItem extends MenuItem {

    private static final String KEY = "mi-https";

    private static Httpses httpses = null;

    public HttpsMenuItem() {
        super(KEY, HTTPS_I18N.httpsMenuItem(), arguments -> {
            if (httpses == null) {
                httpses = new Httpses();
            }
            httpses.setArguments(arguments);
            CenterController.instance().setWidget(httpses);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
