package nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.Datasource;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface DatasourceRpcAsync extends OptionRpcAsync, VersioningRpcAsync, UsedByRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {


    void findSummaries(AsyncCallback<ArrayList<DatasourceSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<Datasource> result);

    void save(Datasource datasource, AsyncCallback<DatasourceSummaryItem> result);
    void check(String uuid, VersionOption version, AsyncCallback<CheckResult> result);

    void duplicate(String uuid, VersionOption version, AsyncCallback<DatasourceSummaryItem> result);

}
