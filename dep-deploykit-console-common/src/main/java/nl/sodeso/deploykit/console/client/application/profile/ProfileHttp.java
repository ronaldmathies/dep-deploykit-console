package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.http.rpc.HttpRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.PROFILE_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class ProfileHttp implements Serializable, IsValidationContainer, IsRevertable  {

    private BooleanType useHttp = new BooleanType(false);
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> httpOptionType = new OptionType<>();
    private VersionOptionType httpVersionType = new VersionOptionType();

    private transient EntryForm editForm = null;
//    private transient GroupComboboxField groupField = null;
    private transient RadioButtonGroupField useHttpGroupField = null;
    private transient VersionSelectionField<HttpRpcGateway> versionSelectionField = null;

    public Widget toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        editForm = new EntryForm("profile-http-form");

        useHttpGroupField = new RadioButtonGroupField("usehttp", this.useHttp);
        useHttpGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useHttpGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useHttpGroupField.setAlignment(Align.HORIZONTAL);
        useHttpGroupField.addClickHandler(event -> {
            boolean visible = useHttp.getValue();
            editForm.setEntriesVisible(visible,  "configuration"); // "group",

            if (!visible) {
//                groupOptionType.setValue(null);
                httpOptionType.setValue(null);
            }
        });
        editForm.addEntry(new EntryWithDocumentation("usehttp", PROFILE_I18N.httpDocumentation()));
        editForm.addEntry(new EntryWithLabelAndWidget("usehttp", PROFILE_I18N.httpUseField(), this.useHttpGroupField));

//        groupField = new GroupComboboxField("group", groupOptionType);
//        editForm.addEntry(new EntryWithLabelAndWidget("group", PROFILE_I18N.groupField(), groupField));

        versionSelectionField = new VersionSelectionField<>(GWT.create(HttpRpcGateway.class), httpOptionType, httpVersionType);
        versionSelectionField.listenToGroup(groupOptionType);

        EntryWithLabelAndWidget entry = new EntryWithLabelAndWidget("configuration", PROFILE_I18N.httpConfigurationField(),
            new HorizontalPanel()
                .addWidget(versionSelectionField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> HttpMenuItem.click(httpOptionType.getValue().getKey(), httpVersionType.getValue()))));

        entry.addValidationRule(
//                new ValidationCriteria.or(new ComboboxMandatoryValidationRule("http-group", ValidationMessage.Level.ERROR, groupOptionType),
                        new ValidationCriteria.or(
                                new ComboboxMandatoryValidationRule("http-option", ValidationMessage.Level.ERROR, httpOptionType),
                                new ComboboxMandatoryValidationRule("http-version", ValidationMessage.Level.ERROR, httpVersionType)
                        )
//                )
                {

                    @Override
                    public boolean isApplicable() {
                        return useHttp.getValue();
                    }
                }
        );

        this.editForm.addEntry(entry);

        this.editForm.addAttachHandler(event -> {
            if (event.isAttached()) {
                useHttpGroupField.fireClickEvent();
                versionSelectionField.refreshOptionList();
            }
        });


        return this.editForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public BooleanType getUseHttp() {
        return useHttp;
    }

    public void setUseHttp(BooleanType useHttp) {
        this.useHttp = useHttp;
    }

    public OptionType getGroup() {
        return groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public OptionType<DefaultOption> getHttpOptionType() {
        return httpOptionType;
    }

    public void setHttpOptionType(OptionType<DefaultOption> httpOptionType) {
        this.httpOptionType = httpOptionType;
    }

    public VersionOptionType getHttpVersionType() {
        return httpVersionType;
    }

    public void setHttpVersionType(VersionOptionType httpVersionType) {
        this.httpVersionType = httpVersionType;
    }
}
