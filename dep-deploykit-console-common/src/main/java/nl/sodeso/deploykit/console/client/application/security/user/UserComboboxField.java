package nl.sodeso.deploykit.console.client.application.security.user;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.security.user.rpc.UserRpcGateway;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class UserComboboxField extends ComboboxField<DefaultOption> {

    public UserComboboxField(OptionType<DefaultOption> optionValue) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                ((UserRpcGateway) GWT.create(UserRpcGateway.class)).asOptions(new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                    @Override
                    public void success(ArrayList<DefaultOption> result) {
                        getItems().clear();
                        addItems(result);
                    }
                });
            }
        });
    }
}
