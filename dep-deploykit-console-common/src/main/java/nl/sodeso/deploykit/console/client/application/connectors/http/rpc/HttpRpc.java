package nl.sodeso.deploykit.console.client.application.connectors.http.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.connectors.http.Http;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-http")
public interface HttpRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, UsedByRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<HttpSummaryItem> findSummaries();

    Http findSingle(String uuid, VersionOption version);

    HttpSummaryItem save(Http http) throws RemoteException;
    HttpSummaryItem duplicate(String uuid, VersionOption version);

}
