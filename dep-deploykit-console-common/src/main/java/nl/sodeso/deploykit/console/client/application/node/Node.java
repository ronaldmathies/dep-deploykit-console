package nl.sodeso.deploykit.console.client.application.node;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.PasswordField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Node implements Serializable, IsValidationContainer, IsRevertable {

    private String uuid = null;

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();

    private StringType hostname = new StringType();
    private IntType port = new IntType(8080);

    private StringType username = new StringType();
    private StringType password = new StringType();

    private transient EntryForm entryForm = null;

    private transient LegendPanel nodeGeneralLegendPanel = null;
    private transient TextField labelTextField = null;

    private transient LegendPanel connectionSettingsLegendPanel = null;
    private transient TextField hostnameField = null;
    private transient TextField portField = null;

    private transient LegendPanel securitySettingsLegendPanel = null;
    private transient EntryForm securitySettingsForm = null;
    private transient TextField usernameTextField = null;
    private transient PasswordField passwordField = null;

    public Node() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupNodeGeneral();
        setupConnectionSettings();
        setupSecuritySettings();

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntry(new EntryWithContainer(null, this.nodeGeneralLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.connectionSettingsLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.securitySettingsLegendPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupNodeGeneral() {
        EntryForm nodeGeneralForm = new EntryForm(null);

        GroupComboboxField groupField = new GroupComboboxField("group", groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule("group", ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        nodeGeneralForm.addEntry(new EntryWithLabelAndWidget("group", "Group", groupField));

        this.labelTextField = new TextField<>("label", this.label);
        this.labelTextField.addValidationRule(new MandatoryValidationRule("label", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return labelTextField.getValue();
            }
        });
        nodeGeneralForm.addEntry(new EntryWithLabelAndWidget("label", "Label", labelTextField));

        this.nodeGeneralLegendPanel = new LegendPanel(null, "Node");
        this.nodeGeneralLegendPanel.add(nodeGeneralForm);
    }

    private void setupConnectionSettings() {
        EntryForm connectionSettingsForm = new EntryForm(null);
        connectionSettingsForm.addEntry(new EntryWithDocumentation("connectionsettings", "Please enter the hostname and port number on which the Jetty server will listen to for incoming HTTP connections."));

        this.hostnameField = new TextField<>("host", this.hostname);
        this.hostnameField.addValidationRule(new MandatoryValidationRule("host", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return hostnameField.getValue();
            }
        });
        connectionSettingsForm.addEntry(new EntryWithLabelAndWidget("hostname", "Hostname", hostnameField));

        this.portField = new TextField<>("port", this.port)
                .addFilter(new NumericInputFilter(false));
        this.portField.addValidationRule(new MandatoryValidationRule("port", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return portField.getValue();
            }
        });
        connectionSettingsForm.addEntry(new EntryWithLabelAndWidget("port", "Port", this.portField));

        this.connectionSettingsLegendPanel = new LegendPanel(null, "Connection Information");
        this.connectionSettingsLegendPanel.add(connectionSettingsForm);
    }

    private void setupSecuritySettings() {
        this.securitySettingsForm = new EntryForm(null);
        this.securitySettingsForm.addEntry(new EntryWithDocumentation("securitysettings", "Please enter the username and password which will be used to connect to the DeployKit Jetty Service, configuration of these settings can be made on the local installation of the DeployKit Jetty Service."));

        this.usernameTextField = new TextField<>("username", this.username);
        this.usernameTextField.addValidationRule(new MandatoryValidationRule("username", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return usernameTextField.getValue();
            }
        });
        this.securitySettingsForm.addEntry(new EntryWithLabelAndWidget("username", "Username", usernameTextField));

        this.passwordField = new PasswordField("password", this.password);
        this.passwordField.addValidationRule(new MandatoryValidationRule("password", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return passwordField.getValue();
            }
        });
        this.securitySettingsForm.addEntry(new EntryWithLabelAndWidget("password", "Password", passwordField));

        this.securitySettingsLegendPanel = new LegendPanel(null, "AccessControl");
        this.securitySettingsLegendPanel.add(this.securitySettingsForm);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getHostname() {
        return hostname;
    }

    public void setHostname(StringType hostname) {
        this.hostname = hostname;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public StringType getUsername() {
        return username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() {
        return password;
    }

    public void setPassword(StringType password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Node && ((Node) obj).getUuid().equals(uuid);
    }


}
