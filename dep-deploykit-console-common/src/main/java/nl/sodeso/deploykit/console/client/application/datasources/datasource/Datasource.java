package nl.sodeso.deploykit.console.client.application.datasources.datasource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc.DatasourceRpcGateway;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc.JdbcDriverRpcGateway;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.DATASOURCE_I18N;

/**
 * @author Ronald Mathies
 */
public class Datasource extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_DATASOURCE = "datasource";
    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_PLACEHOLDER_PREFIX = "placeholder-prefix";
    private static final String KEY_JNDI = "jndi";
    private static final String KEY_JDBC_DRIVER = "jdbc-driver";
    private static final String KEY_DRIVER_CLASS = "driver-class";
    private static final String KEY_URL = "url";
    private static final String KEY_CREDENTIAL = "credential";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_CONNECTIONPOOLS = "connectionpools";
    private static final String KEY_PROPERTIES = "properties";

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();

    private StringType label = new StringType();
    private StringType placeholderPrefix = new StringType();
    private StringType jndi = new StringType();
    private StringType url = new StringType();
    private OptionType<DefaultOption> credentialOptionType = new OptionType<>();
    private VersionOptionType credentialVersionType = new VersionOptionType();

    private OptionType<DefaultOption> jdbcDriverOptionType = new OptionType<>();
    private VersionOptionType jdbcDriverVersionType = new VersionOptionType();

    private DatasourceConnectionPools datasourceConnectionPools = new DatasourceConnectionPools(groupOptionType);
    private KeyValueProperties properties = new KeyValueProperties();

    private transient EntryForm entryForm = null;

    private transient LegendPanel datasourceLegendPanel = null;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;
    private transient TextField placeholderPrefixField = null;

    private transient LegendPanel connectionLegendPanel = null;
    private transient TextField jndiTextField = null;
    private transient TextField urlTextField = null;
    private transient VersionSelectionField<CredentialRpcGateway> credentialField = null;
    private transient VersionSelectionField<JdbcDriverRpcGateway> jdbcDriverField = null;

    private transient LegendPanel connectionPoolsLegendPanel = null;
    private transient LegendPanel propertiesLegendPanel = null;

    public Datasource() {

    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupDatasource();
        setupConnection();
        setupConnectionPools();
        setupProperties();

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntry(new EntryWithContainer(null, this.datasourceLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.connectionLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.connectionPoolsLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.propertiesLegendPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupDatasource() {
        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        placeholderPrefixField = new TextField<>(KEY_PLACEHOLDER_PREFIX, this.placeholderPrefix);
        placeholderPrefixField.addValidationRule(new MandatoryValidationRule(KEY_PLACEHOLDER_PREFIX, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return placeholderPrefix.getValue();
            }
        });

        EntryForm form = new EntryForm(KEY_DATASOURCE)
                .addEntries(
                    new EntryWithLabelAndWidget(KEY_GROUP, DATASOURCE_I18N.groupField(), groupField),
                    new EntryWithLabelAndWidget(KEY_LABEL, DATASOURCE_I18N.labelField(), labelTextField)
                            .addValidationRule(
                                new LabelUniqueValidationRule(GWT.create(DatasourceRpcGateway.class)) {
                                    @Override
                                    public String getUuid() {
                                        return Datasource.this.getUuid();
                                    }

                                    @Override
                                    public StringType getLabel() {
                                        return Datasource.this.getLabel();
                                    }
                                }
                            ),
                    new EntryWithLabelAndWidget(KEY_PLACEHOLDER_PREFIX, DATASOURCE_I18N.placeholderPrefixField(), placeholderPrefixField));


        this.datasourceLegendPanel = new LegendPanel(KEY_DATASOURCE, DATASOURCE_I18N.datasourcePanel());
        this.datasourceLegendPanel.add(form);
    }

    private void setupConnection() {
        jndiTextField = new TextField<>(KEY_JNDI, this.jndi);
        jndiTextField.addValidationRule(new MandatoryValidationRule(KEY_JNDI, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return jndiTextField.getValue();
            }
        });

        urlTextField = new TextField<>(KEY_URL, this.url);
        urlTextField.addValidationRule(new MandatoryValidationRule(KEY_URL, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return urlTextField.getValue();
            }
        });

        jdbcDriverField = new VersionSelectionField<>(GWT.create(JdbcDriverRpcGateway.class), jdbcDriverOptionType, jdbcDriverVersionType);
        jdbcDriverField.listenToGroup(groupOptionType);
        credentialField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), credentialOptionType, credentialVersionType);
        credentialField.listenToGroup(groupOptionType);

        EntryForm form = new EntryForm("connection")
            .addEntries(
                new EntryWithLabelAndWidget(KEY_JNDI, DATASOURCE_I18N.jndiField(),
                    new VerticalPanel(
                        jndiTextField,
                        new PlaceholderSpanField(KEY_JNDI, placeholderPrefix, DatasourcePlaceHolderSuffix.JDBC_JNDI)))
                    .setHint("jdbc/database")
                    .addValidatableWidget(jndiTextField),
                new EntryWithLabelAndWidget(KEY_URL, DATASOURCE_I18N.urlField(),
                    new VerticalPanel(
                        urlTextField,
                        new PlaceholderSpanField(KEY_URL, placeholderPrefix, DatasourcePlaceHolderSuffix.JDBC_URL)))
                    .addValidatableWidget(urlTextField),
                new EntryWithLabelAndWidget(KEY_JDBC_DRIVER, DATASOURCE_I18N.jdbcDriverField(),
                    new VerticalPanel(
                        new HorizontalPanel()
                            .addWidget(jdbcDriverField, 100.0, Style.Unit.PCT)
                            .addWidget(new SearchButton.WithIcon((event) -> JdbcDriverMenuItem.click(jdbcDriverOptionType.getValue().getKey(), jdbcDriverVersionType.getValue()))),
                        new PlaceholderSpanField(KEY_DRIVER_CLASS, placeholderPrefix, DatasourcePlaceHolderSuffix.JDBC_DRIVER_CLASS)))
                    .addValidationRules(
                        new ValidationCriteria.or(
                            new ComboboxMandatoryValidationRule(KEY_JDBC_DRIVER, ValidationMessage.Level.ERROR, jdbcDriverOptionType),
                            new ComboboxMandatoryValidationRule(KEY_JDBC_DRIVER, ValidationMessage.Level.ERROR, jdbcDriverVersionType)
                        )
                    ),
                new EntryWithLabelAndWidget(KEY_CREDENTIAL, DATASOURCE_I18N.credentialField(),
                    new VerticalPanel(
                        new HorizontalPanel()
                            .addWidget(credentialField, 100.0, Style.Unit.PCT)
                            .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(credentialOptionType.getValue().getKey(), credentialVersionType.getValue()))),
                        new PlaceholderSpanField(KEY_USERNAME, placeholderPrefix, DatasourcePlaceHolderSuffix.JDBC_USERNAME),
                        new PlaceholderSpanField(KEY_PASSWORD, placeholderPrefix, DatasourcePlaceHolderSuffix.JDBC_PASSWORD)))
                    .addValidationRules(
                        new ValidationCriteria.or(
                            new ComboboxMandatoryValidationRule(KEY_CREDENTIAL, ValidationMessage.Level.ERROR, credentialOptionType),
                            new ComboboxMandatoryValidationRule(KEY_CREDENTIAL, ValidationMessage.Level.ERROR, credentialVersionType)
                        )
                    ));

        this.connectionLegendPanel = new LegendPanel("connection-panel", DATASOURCE_I18N.connectionInformationPanel());
        this.connectionLegendPanel.add(form);

//        this.connectionLegendPanel.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                credentialField.refresh(groupOptionType);
//            }
//        });
    }

    private void setupConnectionPools() {
        connectionPoolsLegendPanel = new LegendPanel(KEY_CONNECTIONPOOLS, DATASOURCE_I18N.connectionPoolsPanel());
        connectionPoolsLegendPanel.add(datasourceConnectionPools.toEditForm());
    }

    private void setupProperties() {
        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(KEY_PROPERTIES, DATASOURCE_I18N.customPropertiesDocumentation(), Align.LEFT),
                new EntryWithContainer(null, properties.toEditForm()));

        propertiesLegendPanel = new LegendPanel(KEY_PROPERTIES, DATASOURCE_I18N.customPropertiesPanel());
        propertiesLegendPanel.add(entryForm);
        propertiesLegendPanel.showCollapseButton(true);
        propertiesLegendPanel.setCollapsed(true);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getJndi() {
        return this.jndi;
    }

    public void setJndi(StringType jndi) {
        this.jndi = jndi;
    }

    public StringType getUrl() {
        return url;
    }

    public void setUrl(StringType url) {
        this.url = url;
    }

    public OptionType<DefaultOption> getCredential() {
        return this.credentialOptionType;
    }

    public void setCredential(OptionType<DefaultOption> credentialOptionType) {
        this.credentialOptionType = credentialOptionType;
    }

    public VersionOptionType getCredentialVersion() {
        return credentialVersionType;
    }

    public void setCredentialVersion(VersionOptionType credentialVersionType) {
        this.credentialVersionType = credentialVersionType;
    }

    public void setJdbcDriver(OptionType<DefaultOption> jdbcDriverOptionType) {
        this.jdbcDriverOptionType = jdbcDriverOptionType;
    }

    public OptionType<DefaultOption> getJdbcDriver() {
        return this.jdbcDriverOptionType;
    }

    public VersionOptionType getJdbcDriverVersion() {
        return jdbcDriverVersionType;
    }

    public void setJdbcDriverVersion(VersionOptionType jdbcDriverVersionType) {
        this.jdbcDriverVersionType = jdbcDriverVersionType;
    }

    public DatasourceConnectionPools getDatasourceConnectionPools() {
        return this.datasourceConnectionPools;
    }

    public KeyValueProperties getProperties() {
        return this.properties;
    }

}
