package nl.sodeso.deploykit.console.client.application.connectors.http.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class HttpRpcGateway implements RpcGateway<HttpRpcAsync>, HttpRpcAsync {

    /**
     * {@inheritDoc}
     */
    public HttpRpcAsync getRpcAsync() {
        return GWT.create(HttpRpc.class);
    }
}
