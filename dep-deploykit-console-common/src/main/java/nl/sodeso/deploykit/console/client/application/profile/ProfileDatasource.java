package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc.DatasourceRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ProfileDatasource implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType;
    private OptionType<DefaultOption> datasourceOptionType = new OptionType<>();
    private VersionOptionType datasourceVersionType = new VersionOptionType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient VersionSelectionField<DatasourceRpcGateway> datasourceField = null;

    public ProfileDatasource() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        datasourceField = new VersionSelectionField<>(GWT.create(DatasourceRpcGateway.class), datasourceOptionType, datasourceVersionType);
        datasourceField.listenToGroup(groupOptionType);

        entry = new EntryWithWidgetAndWidget("datasource", null,
            new HorizontalPanel()
                .addWidget(datasourceField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> DatasourceMenuItem.click(datasourceOptionType.getValue().getKey(), datasourceVersionType.getValue()))));

        entry.addValidationRule(
            new ValidationCriteria.or(
                new ComboboxMandatoryValidationRule("datasource-option-mandatory", ValidationMessage.Level.ERROR, datasourceOptionType),
                new ComboboxMandatoryValidationRule("datasource-version-mandatory", ValidationMessage.Level.ERROR, datasourceVersionType)
            )
        );

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getDatasource() {
        return this.datasourceOptionType;
    }

    public void setDatasource(OptionType<DefaultOption> datasource) {
        this.datasourceOptionType = datasource;
    }

    public VersionOptionType getDatasourceVersion() {
        return datasourceVersionType;
    }

    public void setDatasourceVersion(VersionOptionType datasourceVersionType) {
        this.datasourceVersionType = datasourceVersionType;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }




}
