package nl.sodeso.deploykit.console.client.application.security.role.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.security.role.Role;
import nl.sodeso.deploykit.console.client.application.security.role.RoleSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface RoleRpcAsync extends DeleteRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<RoleSummaryItem>> result);
    void findSingle(String uuid, AsyncCallback<Role> result);
    void asOptions(AsyncCallback<ArrayList<DefaultOption>> options);

    void usage(String uuid, AsyncCallback<ArrayList<UsedBy>> result);
    void save(Role role, AsyncCallback<RoleSummaryItem> result);
    void duplicate(String uuid, AsyncCallback<RoleSummaryItem> result);

}
