package nl.sodeso.deploykit.console.client.application;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlConstants;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpConstants;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsConstants;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPoolConstants;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceConstants;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverConstants;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialConstants;
import nl.sodeso.deploykit.console.client.application.group.GroupConstants;
import nl.sodeso.deploykit.console.client.application.node.NodeConstants;
import nl.sodeso.deploykit.console.client.application.profile.ProfileConstants;
import nl.sodeso.deploykit.console.client.application.search.SearchConstants;
import nl.sodeso.deploykit.console.client.application.security.role.RoleConstants;
import nl.sodeso.deploykit.console.client.application.security.user.UserConstants;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceConstants;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceConstants;

/**
 * The resources class can be used to register all instances created of Constants and Message interfaces for
 * i18n purposes.
 *
 * @author Ronald Mathies
 */
public final class Resources {

    private Resources() {}

    public final static ProfileConstants PROFILE_I18N = GWT.create(ProfileConstants.class);
    public final static CredentialConstants CREDENTIAL_I18N = GWT.create(CredentialConstants.class);
    public final static SearchConstants SEARCH_I18N = GWT.create(SearchConstants.class);
    public final static LdapServiceConstants LDAP_SERVICE_I18N = GWT.create(LdapServiceConstants.class);
    public final static WebserviceConstants WEBSERVICE_L18N = GWT.create(WebserviceConstants.class);
    public final static HttpConstants HTTP_L18N = GWT.create(HttpConstants.class);
    public final static HttpsConstants HTTPS_I18N = GWT.create(HttpsConstants.class);
    public final static ConnectionPoolConstants CONNECTION_POOL_I18N = GWT.create(ConnectionPoolConstants.class);
    public final static DatasourceConstants DATASOURCE_I18N = GWT.create(DatasourceConstants.class);
    public final static AccessControlConstants ACCESS_CONTROL_I18N = GWT.create(AccessControlConstants.class);
    public final static JdbcDriverConstants JDBC_DRIVER_I18N = GWT.create(JdbcDriverConstants.class);
    public final static GroupConstants GROUP_I18N = GWT.create(GroupConstants.class);
    public final static RoleConstants ROLE_I18N = GWT.create(RoleConstants.class);
    public final static UserConstants USER_I18N = GWT.create(UserConstants.class);
    public final static NodeConstants NODE_I18N = GWT.create(NodeConstants.class);
}