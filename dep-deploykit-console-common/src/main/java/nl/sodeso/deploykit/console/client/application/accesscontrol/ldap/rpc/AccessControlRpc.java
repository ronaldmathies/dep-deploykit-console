package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlSummaryItem;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-accesscontrol")
public interface AccessControlRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, UsedByRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<AccessControlSummaryItem> findSummaries();

    AccessControl findSingle(String uuid, VersionOption version);

    AccessControlSummaryItem save(AccessControl accessControl) throws RemoteException;
    CheckResult check(String uuid, VersionOption version, String username);
    AccessControlSummaryItem duplicate(String uuid, VersionOption version);

}
