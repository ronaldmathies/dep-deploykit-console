package nl.sodeso.deploykit.console.client.application.ui.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;

/**
 * @author Ronald Mathies
 */
public class CheckButton {

    public static final String KEY = "check";

    public static class WithLabel extends SimpleButton {

        public WithLabel() {
            this(null);
        }

        public WithLabel(ClickHandler clickHandler) {
            super(KEY, "Check", Style.GREEN);

            if (clickHandler != null) {
                addClickHandler(clickHandler);
            }
        }
    }

}
