package nl.sodeso.deploykit.console.client.application.security.user;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class UserRoles extends WidgetCollectionField<UserRole> {

    @Override
    public WidgetSupplier<UserRole> createWidgetSupplier() {
        return callback -> callback.created(new UserRole());
    }
}
