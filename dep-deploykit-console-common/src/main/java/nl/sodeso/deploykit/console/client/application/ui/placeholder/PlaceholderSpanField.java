package nl.sodeso.deploykit.console.client.application.ui.placeholder;

import nl.sodeso.gwt.ui.client.form.input.SpanField;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

/**
 * @author Ronald Mathies
 */
public class PlaceholderSpanField extends SpanField {

    private static final String KEY_PLACEHOLDER_SUFFIX = "-placeholder";

    private static final String VALUE = "Placeholder: %s";

    private String placeholderSuffix = null;

    @SuppressWarnings("unchecked")
    public PlaceholderSpanField(String key, ValueType listenTo, String placeholderSuffix) {
        super(key != null ? key + KEY_PLACEHOLDER_SUFFIX : KEY_PLACEHOLDER_SUFFIX, listenTo.getValueAsString());
        this.placeholderSuffix = placeholderSuffix;

        listenTo.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this) {

            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                setText(event.getValueType().getValueAsString());
            }
        });

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                setText(listenTo.getValueAsString());
            }
        });
    }

    public void setText(String value) {
        if (value != null && value.length() > 0) {
            super.setText(VALUE.replace("%s", value + placeholderSuffix));
        }
    }

}
