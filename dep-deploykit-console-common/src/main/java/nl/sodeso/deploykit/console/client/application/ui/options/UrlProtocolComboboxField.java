package nl.sodeso.deploykit.console.client.application.ui.options;

import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class UrlProtocolComboboxField extends ComboboxField<DefaultOption> {

    public UrlProtocolComboboxField(OptionType<DefaultOption> optionValue) {
        super(optionValue);

        addItems(
                new DefaultOption("http", "http", "HTTP"),
                new DefaultOption("https", "https", "HTTPS"));
    }
}
