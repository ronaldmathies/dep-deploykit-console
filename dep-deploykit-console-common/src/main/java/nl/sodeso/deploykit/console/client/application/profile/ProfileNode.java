package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.node.NodeComboboxField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ProfileNode implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> nodeOptionType = new OptionType<>();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient NodeComboboxField nodeField = null;

    public ProfileNode() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        nodeField = new NodeComboboxField(nodeOptionType);
        nodeField.listenToGroup(groupOptionType);
        nodeField.addValidationRule(new ComboboxMandatoryValidationRule("node", ValidationMessage.Level.ERROR, nodeOptionType));

        entry = new EntryWithWidgetAndWidget("node", null, nodeField);

//        this.entry.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                nodeField.refresh(groupOptionType);
//            }
//        });

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getNode() {
        return this.nodeOptionType;
    }

    public void setNode(OptionType<DefaultOption> node) {
        this.nodeOptionType = node;
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
