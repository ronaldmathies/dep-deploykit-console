package nl.sodeso.deploykit.console.client.application.ui.menu;

import com.google.gwt.user.client.History;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.add;
import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.toArgs;
import static nl.sodeso.gwt.ui.client.controllers.menu.ArgumentsUtil.toToken;

/**
 * @author Ronald Mathies
 */
public class HistoryHelper {

    public static final String ARG_UUID = "uuid";
    public static final String ARG_VERSION = "version";
    public static final String ARG_TYPE = "type";

    /**
     * Pushes a new token on the history with the specified token (key) and arguments.
     *
     * @param key the history token.
     * @param uuid the uuid
     * @param option the version option
     */
    public static void click(String key, String uuid, VersionOption option) {
        History.newItem(
            toToken(key,
                toArgs(
                    add(ARG_UUID, uuid),
                    add(ARG_VERSION, option.getKey()),
                    add(ARG_TYPE, option.getType().name())
                )
            )
        );
    }

    /**
     * Pushes a new token on the history with the specified token (key) and arguments.
     *
     * @param key the history token.
     * @param uuid the uuid
     */
    public static void click(String key, String uuid) {
        History.newItem(
            toToken(key,
                toArgs(
                    add(ARG_UUID, uuid)
                )
            )
        );
    }

}
