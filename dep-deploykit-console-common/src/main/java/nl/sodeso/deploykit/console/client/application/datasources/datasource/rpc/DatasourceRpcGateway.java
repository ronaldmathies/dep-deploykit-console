package nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class DatasourceRpcGateway implements RpcGateway<DatasourceRpcAsync>, DatasourceRpcAsync {

    /**
     * {@inheritDoc}
     */
    public DatasourceRpcAsync getRpcAsync() {
        return GWT.create(DatasourceRpc.class);
    }
}
