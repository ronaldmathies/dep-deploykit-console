package nl.sodeso.deploykit.console.client.application.ui.options;

/**
 * @author Ronald Mathies
 */
public enum VersionType {

    SNAPSHOT,
    BRANCH,
    RELEASE;

    VersionType() {
    }

    public boolean isSnapshot() {
        return this.equals(VersionType.SNAPSHOT);
    }

    public boolean isBranch() {
        return this.equals(VersionType.BRANCH);
    }

    public boolean isRelease() {
        return this.equals(VersionType.RELEASE);
    }
}
