package nl.sodeso.deploykit.console.client.application.connectors.http;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.http.rpc.HttpRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.rules.PortRangeValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.IntegerRangeValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.HasUuid;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.HTTP_L18N;

/**
 * @author Ronald Mathies
 */
public class Http extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_PORT = "port";
    private static final String KEY_TIMEOUT = "timeout";


    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private StringType host = new StringType();
    private IntType port = new IntType(8080);
    private IntType timeout = new IntType(30000);

    private transient LegendPanel legendPanel;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;
    private transient TextField portField = null;
    private transient TextField timeoutField = null;

    public Http() {
    }

    public Widget toEditForm() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label);

        portField = new TextField<>(KEY_PORT, this.port)
                .addFilter(new NumericInputFilter(false));
        portField.addValidationRule(new PortRangeValidationRule(KEY_PORT, port));
        timeoutField = new TextField<>(KEY_TIMEOUT, this.timeout)
                .addFilter(new NumericInputFilter(false));

        EntryForm form = new EntryForm(null)
                .addEntries(
                        new EntryWithLabelAndWidget(KEY_GROUP, HTTP_L18N.groupField(), groupField),
                        new EntryWithLabelAndWidget(KEY_LABEL, HTTP_L18N.labelField(), labelTextField)
                                .addValidationRules(
                                    new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                                        @Override
                                        public String getValue() {
                                            return labelTextField.getValue();
                                        }
                                    },
                                    new ValidationCriteria.or(
                                        new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                                            @Override
                                            public String getValue() {
                                                return labelTextField.getValue();
                                            }
                                        },
                                        new LabelUniqueValidationRule(GWT.create(HttpRpcGateway.class)) {
                                            @Override
                                            public String getUuid() {
                                                return Http.this.getUuid();
                                            }

                                            @Override
                                            public StringType getLabel() {
                                                return Http.this.getLabel();
                                            }
                                        }
                                    )
                                ),
                        new EntryWithDocumentation(KEY_PORT, HTTP_L18N.portFieldDocumentation()),
                        new EntryWithLabelAndWidget(KEY_PORT, HTTP_L18N.portField(), portField),
                        new EntryWithLabelAndWidget(KEY_TIMEOUT, HTTP_L18N.timeoutField(), timeoutField).setHint(HTTP_L18N.timeoutFieldHint()));


        this.legendPanel = new LegendPanel(null, HTTP_L18N.httpPanel());
        this.legendPanel.add(form);

        this.labelTextField.setFocus();

        return this.legendPanel;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.legendPanel);
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public OptionType<DefaultOption> getGroup() {
        return groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public StringType getHost() {
        return host;
    }

    public void setHost(StringType host) {
        this.host = host;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

}
