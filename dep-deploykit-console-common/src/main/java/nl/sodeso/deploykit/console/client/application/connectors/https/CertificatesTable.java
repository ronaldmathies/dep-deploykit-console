package nl.sodeso.deploykit.console.client.application.connectors.https;

import com.google.gwt.user.cellview.client.TextColumn;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.util.DateFormatterUtil;

import java.util.List;

import static nl.sodeso.deploykit.console.client.application.Resources.HTTPS_I18N;

/**
 * @author Ronald Mathies
 */
public class CertificatesTable extends CellTableField<Certificate> {

    public CertificatesTable() {
        super("certificates-table");

        TextColumn<Certificate> column = new TextColumn<Certificate>() {
            @Override
            public String getValue(Certificate certificate) {
                return certificate.getAlias();
            }
        };
        this.addColumn(column, HTTPS_I18N.aliasColumn());
        column = new TextColumn<Certificate>() {
            @Override
            public String getValue(Certificate certificate) {
                return DateFormatterUtil.formatDate(certificate.getNotBefore());
            }
        };
        this.addColumn(column, HTTPS_I18N.notValidBeforeColumn());
        column = new TextColumn<Certificate>() {
            @Override
            public String getValue(Certificate certificate) {
                return DateFormatterUtil.formatDate(certificate.getNotAfter());
            }
        };
        this.addColumn(column, HTTPS_I18N.notValidAfterColumn());
        column = new TextColumn<Certificate>() {
            @Override
            public String getValue(Certificate certificate) {
                return certificate.getSubjectDN();
            }
        };
        this.addColumn(column, HTTPS_I18N.subjectDnColumn());
        column = new TextColumn<Certificate>() {
            @Override
            public String getValue(Certificate certificate) {
                return certificate.getSigAlgName();
            }
        };
        this.addColumn(column, HTTPS_I18N.algorithmColumn());
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("no-entries", HTTPS_I18N.noCertificateEntriesMessage()));
    }

    public void setCertificates(List<Certificate> certificates) {
        replaceAll(certificates);
    }


}
