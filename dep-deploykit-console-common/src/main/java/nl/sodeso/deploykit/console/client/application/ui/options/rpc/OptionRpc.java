package nl.sodeso.deploykit.console.client.application.ui.options.rpc;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface OptionRpc {

    ArrayList<DefaultOption> asOptions(DefaultOption group);

}
