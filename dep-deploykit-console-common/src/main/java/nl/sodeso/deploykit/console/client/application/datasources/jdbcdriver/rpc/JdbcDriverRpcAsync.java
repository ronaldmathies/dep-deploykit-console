package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface JdbcDriverRpcAsync extends OptionRpcAsync, VersioningRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync, UsedByRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<JdbcDriverSummaryItem>> result);
    void findSingle(String uuid, VersionOption version, AsyncCallback<JdbcDriver> result);

    void save(JdbcDriver jdbcDriver, AsyncCallback<JdbcDriverSummaryItem> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<JdbcDriverSummaryItem> result);

    void classes(String uuid, VersionOption version, AsyncCallback<ArrayList<DefaultOption>> result);

}
