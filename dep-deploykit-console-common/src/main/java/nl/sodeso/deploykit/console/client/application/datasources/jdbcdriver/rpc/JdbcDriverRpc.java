package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverSummaryItem;
import nl.sodeso.deploykit.console.client.application.general.credential.Credential;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-jdbcdriver")
public interface JdbcDriverRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, DeleteRpc, LabelUniqueRpc, UsedByRpc {

    ArrayList<JdbcDriverSummaryItem> findSummaries();
    JdbcDriver findSingle(String uuid, VersionOption version);

    JdbcDriverSummaryItem save(JdbcDriver jdbcDriver) throws RemoteException;
    JdbcDriverSummaryItem duplicate(String uuid, VersionOption version);

    ArrayList<DefaultOption> classes(String uuid, VersionOption version);

}
