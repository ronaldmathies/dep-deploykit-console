package nl.sodeso.deploykit.console.client.application.ui.options;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public class VersionOption extends DefaultOption {

    private VersionType type;

    public VersionOption() {}

    public VersionOption(VersionType type) {
        this(null, type);
    }

    public VersionOption(String version, VersionType type) {
        super(type.isSnapshot() ? "SNAPSHOT" : version);

        if ((type.isRelease() || type.isBranch()) && (version == null || version.isEmpty())) {
            throw new IncorrectVersionSpecifiedException("Version is mandatory when specifying a 'release' or 'branch' version.");
        }

        this.type = type;
    }

    public void setType(VersionType type) {
        this.type = type;
    }

    public VersionType getType() {
        return this.type;
    }

    public String getDisplayDescription() {
        return super.getDescription() + (type.isBranch() ? "-branch" : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        VersionOption that = (VersionOption) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(getKey(), that.getKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type.hashCode());
    }

    public VersionOption clone() {
        return new VersionOption(getKey(), type);
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return "key: " + getKey() + ", type: " + type.name();
    }
}
