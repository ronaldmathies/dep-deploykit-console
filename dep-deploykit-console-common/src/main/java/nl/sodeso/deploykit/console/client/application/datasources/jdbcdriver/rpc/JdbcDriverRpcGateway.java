package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class JdbcDriverRpcGateway implements RpcGateway<JdbcDriverRpcAsync>, JdbcDriverRpcAsync {

    /**
     * {@inheritDoc}
     */
    public JdbcDriverRpcAsync getRpcAsync() {
        return GWT.create(JdbcDriverRpc.class);
    }
}
