package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class CheckResult implements Serializable {

    private boolean success;
    private String message;

    public CheckResult() {}

    public CheckResult(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
