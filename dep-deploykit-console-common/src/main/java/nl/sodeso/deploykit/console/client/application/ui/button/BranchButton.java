package nl.sodeso.deploykit.console.client.application.ui.button;

import com.google.gwt.event.dom.client.ClickHandler;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;

/**
 * @author Ronald Mathies
 */
public class BranchButton {

    public static final String KEY = "branch";

    public static class WithLabel extends SimpleButton {

        public WithLabel() {
            this(null);
        }

        public WithLabel(ClickHandler clickHandler) {
            super(KEY, "Branch", Style.BLUE);

            if (clickHandler != null) {
                addClickHandler(clickHandler);
            }
        }
    }

}
