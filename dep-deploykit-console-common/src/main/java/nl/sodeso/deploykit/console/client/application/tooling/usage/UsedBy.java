package nl.sodeso.deploykit.console.client.application.tooling.usage;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class UsedBy implements Serializable {

    private String uuid;
    private VersionOption version;
    private String category;
    private String label;

    public UsedBy() {}

    public UsedBy(String uuid, String category, String label) {
        this(uuid, null, category, label);
    }

    public UsedBy(String uuid, VersionOption version, String category, String label) {
        this.uuid = uuid;
        this.version = version;
        this.category = category;
        this.label = label;
    }

    public String getCategory() {
        return this.category;
    }

    public String getUuid() {
        return this.uuid;
    }

    public VersionOption getVersion() {
        return this.version;
    }

    public String getLabel() {
        return this.label;
    }

}
