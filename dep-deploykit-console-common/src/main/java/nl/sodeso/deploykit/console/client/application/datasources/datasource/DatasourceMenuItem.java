package nl.sodeso.deploykit.console.client.application.datasources.datasource;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.DATASOURCE_I18N;

/**
 * @author Ronald Mathies
 */
public class DatasourceMenuItem extends MenuItem {

    private static final String KEY = "mi-datasource";

    private static Datasources datasources = null;

    public DatasourceMenuItem() {
        super(KEY, DATASOURCE_I18N.datasourcesMenuItem(), arguments -> {
            if (datasources == null) {
                datasources = new Datasources();
            }
            datasources.setArguments(arguments);
            CenterController.instance().setWidget(datasources);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
