package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.deploykit.console.client.application.group.GroupMenuItem;
import nl.sodeso.deploykit.console.client.application.security.role.rpc.RoleRpcGateway;
import nl.sodeso.deploykit.console.client.application.tooling.delete.DeleteProcess;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsageWindow;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.button.UsedByButton;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.dialog.UnsavedChangesQuestionPanel;

import java.util.ArrayList;
import java.util.Map;

import static nl.sodeso.deploykit.console.client.application.Resources.ROLE_I18N;
import static nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper.*;

/**
 * @author Ronald Mathies
 */
public class Roles extends AbstractCenterPanel {

    private static final String KEY_TABLE = "table";
    private static final String KEY_DETAILS = "details";

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<RoleSummaryItem> table = null;

    private Role currentObject = new Role();

    private Map<String, String> arguments = null;

    private RemoveButton.WithLabel removeButton;
    private UsedByButton.WithLabel usedByButton;
    private RefreshButton.WithIcon refreshButton;
    private CloneButton.WithLabel cloneButton;
    private AddButton.WithIcon addButton;
    private SaveButton.WithLabel saveButton;
    private CancelButton.WithLabel cancelButton;

    private RoleRpcGateway gateway = GWT.create(RoleRpcGateway.class);

    public Roles() {
        super();

        setFullHeight(true);
    }

    public void setArguments(final Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public void beforeAdd(final Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            table = new SummaryTableField<>();
            table.setFullHeight(true);
            table.addSelectionChangeHandler(event -> loadDetails());

            refreshButton = new RefreshButton.WithIcon((event) -> refresh(null));
            addButton = new AddButton.WithIcon((event) -> add());

            final WindowPanel tableWindowPanel = new WindowPanel(KEY_TABLE, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .addToBody(table)
                    .addToToolbar(Align.LEFT, refreshButton)
                    .addToToolbar(Align.RIGHT, addButton);

            horizontalPanel.addWidget(tableWindowPanel, 20, Style.Unit.PCT);

            cloneButton = new CloneButton.WithLabel((event) -> prepareCopy());
            usedByButton = new UsedByButton.WithLabel((event) -> usedBy());
            removeButton = new RemoveButton.WithLabel((event) -> delete());
            saveButton = new SaveButton.WithLabel((event) -> {
                save(() -> loadDetails());
            });
            cancelButton = new CancelButton.WithLabel((event) -> {
                if (currentObject.getUuid() == null) {
                    table.first();
                } else {
                    detailWindowPanel.revert();
                }
            });

            detailWindowPanel = new WindowPanel(KEY_DETAILS, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .addToBody(currentObject.toEditForm())
                    .addToToolbar(Align.LEFT, cloneButton)
                    .addToToolbar(Align.RIGHT, usedByButton)
                    .addToFooter(Align.LEFT, removeButton)
                    .addToFooter(Align.RIGHT, saveButton, cancelButton);

            horizontalPanel.addWidget(detailWindowPanel, 80, Style.Unit.PCT);

            evaluateButtonStates();

            add(horizontalPanel);
        }


        Trigger preselect = () -> {
            if (arguments != null && arguments.containsKey(ARG_UUID)) {
                final String uuid = arguments.get(ARG_UUID);
                if (table.getSelected() != null && table.getSelected().getUuid().equals(uuid)) {
                    loadDetails();
                } else {
                    for (RoleSummaryItem summary : table.allItems()) {
                        if (summary.getUuid().equals(uuid)) {
                            table.select(summary);

                            break;
                        }
                    }
                }
            } else {
                if (table.getSelected() == null) {
                    table.first();
                }
            }

            trigger.fire();
        };

        if (table.allItems().isEmpty()) {
            refresh(() -> {
                preselect.fire();
            });
        } else {
            preselect.fire();
        }
    }

    private void prepareCopy() {
        saveBeforeAction(this::copy);
    }

    private void copy() {
        gateway.duplicate(currentObject.getUuid(), new DefaultAsyncCallback<RoleSummaryItem>() {
            @Override
            public void success(RoleSummaryItem summary) {
                table.add(summary);
                table.select(summary);
            }
        });
    }

    public void refresh(final Trigger trigger) {
        gateway.findSummaries(new DefaultAsyncCallback<ArrayList<RoleSummaryItem>>() {
            @Override
            public void success(ArrayList<RoleSummaryItem> result) {
                table.replaceAll(result);

                if (trigger != null) {
                    trigger.fire();
                }
            }
        });
    }

    private void delete() {
        new DeleteProcess(gateway, currentObject.getUuid(), null, () -> table.remove(table.getSelected()), () -> usedBy()).start();
    }

    private void evaluateButtonStates() {
        boolean isFirst = table.getListDataProvider().getList().isEmpty();

        cancelButton.setEnabled(!isFirst);
        removeButton.setEnabled(!isFirst);
        usedByButton.setEnabled(!isFirst);
        cloneButton.setEnabled(!isFirst);
    }

    public void saveBeforeAction(final Trigger trigger) {
        if (currentObject.hasChanges()) {
            UnsavedChangesQuestionPanel questionPanel = new UnsavedChangesQuestionPanel(() -> save(trigger));
            questionPanel.center(true, true);
        } else {
            trigger.fire();
        }
    }

    private void save(final Trigger afterSaveTrigger) {
        if (currentObject.hasChanges()) {
            ValidationUtil.validate(result -> {
                if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                    gateway.save(currentObject, new DefaultAsyncCallback<RoleSummaryItem>() {
                        @Override
                        public void success(RoleSummaryItem summary) {
                            if (currentObject.getUuid() == null) {
                                table.add(summary);
                                table.select(summary);
                            } else {
                                table.replace(table.getSelected(), summary);
                            }

                            if (afterSaveTrigger != null) {
                                afterSaveTrigger.fire();
                            }
                        }
                    });
                }
            }, detailWindowPanel);
        }
    }

    private void add() {
        table.deselect();
        detailWindowPanel.clearBody();

        Scheduler.get().scheduleDeferred(() -> {
            currentObject = new Role();
            detailWindowPanel.addToBody(currentObject.toEditForm());

            evaluateButtonStates();
        });
    }

    private void usedBy() {
        gateway.usage(currentObject.getUuid(), new DefaultAsyncCallback<ArrayList<UsedBy>>() {
            @Override
            public void success(ArrayList<UsedBy> result) {
                UsageWindow usageWindow = new UsageWindow(result, ROLE_I18N.roleUsedByTitle(currentObject.getLabel().getValue()));
                usageWindow.setUsedByClickHandler(usedBy -> GroupMenuItem.click(usedBy.getUuid()));
                usageWindow.center(false, false);
            }
        });
    }

    private void loadDetails() {
        RoleSummaryItem item = table.getSelected();
        if (item != null) {
            String uuid = item.getUuid();
            gateway.findSingle(uuid, new DefaultAsyncCallback<Role>() {
                @Override
                public void success(Role result) {
                    currentObject = result;

                    detailWindowPanel.clearBody();
                    detailWindowPanel.addToBody(currentObject.toEditForm());

                    evaluateButtonStates();
                }
            });
        }
    }
}