package nl.sodeso.deploykit.console.client.application.services.webservice;

/**
 * @author Ronald Mathies
 */
public class WebservicePlaceHolderSuffix {

    public static final String WS_PROTOCOL = "-ws-protocol";
    public static final String WS_DOMAIN = "-ws-domain";
    public static final String WS_PORT = "-ws-port";
    public static final String WS_CONTEXT = "-ws-port";
    public static final String WS_PATH = "-ws-path";
    public static final String WS_TIMEOUT = "-ws-timeout";

    public static final String WS_WSA_FROM = "-ws-wsa-from";
    public static final String WS_WSA_TO = "-ws-wsa-to";
    public static final String WS_WSA_ACTION = "-ws-wsa-action";
    public static final String WS_WSA_REPLYTO = "-ws-wsa-replyto";
    public static final String WS_WSA_FAULTTO = "-ws-wsa-faultto";

    public static final String WS_WSSEC_USERNAME = "-ws-wssec-username";
    public static final String WS_WSSEC_PASSWORD = "-ws-wssec-password";

    public static String WS_PROTOCOL(String prefix) {
        return prefix + WS_PROTOCOL;
    }

    public static String WS_DOMAIN(String prefix) {
        return prefix + WS_DOMAIN;
    }

    public static String WS_PORT(String prefix) {
        return prefix + WS_PORT;
    }

    public static String WS_CONTEXT(String prefix) {
        return prefix + WS_CONTEXT;
    }

    public static String WS_PATH(String prefix) {
        return prefix + WS_PATH;
    }

    public static String WS_TIMEOUT(String prefix) {
        return prefix + WS_TIMEOUT;
    }

    public static String WS_WSA_FROM(String prefix) {
        return prefix + WS_WSA_FROM;
    }

    public static String WS_WSA_TO(String prefix) {
        return prefix + WS_WSA_TO;
    }

    public static String WS_WSA_ACTION(String prefix) {
        return prefix + WS_WSA_ACTION;
    }

    public static String WS_WSA_REPLYTO(String prefix) {
        return prefix + WS_WSA_REPLYTO;
    }

    public static String WS_WSA_FAULTTO(String prefix) {
        return prefix + WS_WSA_FAULTTO;
    }

    public static String WS_WSSEC_USERNAME(String prefix) {
        return prefix + WS_WSSEC_USERNAME;
    }

    public static String WS_WSSEC_PASSWORD(String prefix) {
        return prefix + WS_WSSEC_PASSWORD;
    }


}
