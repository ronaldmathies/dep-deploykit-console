package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.OkButton;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.KeyboardUtil;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;

/**
 * @author Ronald Mathies
 */
public class CheckQuestionPanel extends PopupWindowPanel {

    private UsernameSpecifiedHandler usernameSpecifiedHandler = null;
    private StringType username = new StringType();

    private TextField usernameField = null;

    public CheckQuestionPanel(final UsernameSpecifiedHandler usernameSpecifiedHandler) {
        super("check-access-control-popup", ACCESS_CONTROL_I18N.accessControlCheckQuestionTitle(), WindowPanel.Style.INFO);
        this.usernameSpecifiedHandler = usernameSpecifiedHandler;

        this.setWidth("400px");

        usernameField = new TextField<>("username", username);
        usernameField.addValidationRule(new MandatoryValidationRule("mandatory", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return username.getValue();
            }
        });
        usernameField.addKeyUpHandler(event -> {
            if (KeyboardUtil.isEnter(event)) {
                yes();
            }
        });

        addToBody(
            new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(null, ACCESS_CONTROL_I18N.accessControlCheckQuestionDocumentation()),
                    new EntryWithLabelAndWidget(null, ACCESS_CONTROL_I18N.usernameField(), usernameField)));

        addToFooter(Align.RIGHT,
            new OkButton.WithLabel((event) -> yes()),
            new CancelButton.WithLabel((event) -> close()));

        usernameField.setFocus();
    }

    private void yes() {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                close();
                usernameSpecifiedHandler.ok(username);
            }
        }, usernameField);

    }

}
