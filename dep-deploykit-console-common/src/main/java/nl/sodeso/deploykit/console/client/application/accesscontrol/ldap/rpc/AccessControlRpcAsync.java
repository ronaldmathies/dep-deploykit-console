package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlSummaryItem;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface AccessControlRpcAsync extends OptionRpcAsync, VersioningRpcAsync, UsedByRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<AccessControlSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<AccessControl> result);

    void save(AccessControl accessControl, AsyncCallback<AccessControlSummaryItem> result);
    void check(String uuid, VersionOption version, String username, AsyncCallback<CheckResult> result);

    void duplicate(String uuid, VersionOption version, AsyncCallback<AccessControlSummaryItem> result);

}
