package nl.sodeso.deploykit.console.client.application.general.credential.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.general.credential.Credential;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-credential")
public interface CredentialRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, DeleteRpc, LabelUniqueRpc, UsedByRpc {

    ArrayList<CredentialSummaryItem> findSummaries();
    Credential findSingle(String uuid, VersionOption version);

    CredentialSummaryItem save(Credential credential) throws RemoteException;
    CredentialSummaryItem duplicate(String uuid, VersionOption version);

}
