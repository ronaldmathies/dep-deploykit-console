package nl.sodeso.deploykit.console.client.application.security.user;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialPlaceHolderSuffix;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.PasswordField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.rules.PasswordMatchValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CREDENTIAL_I18N;
import static nl.sodeso.deploykit.console.client.application.Resources.USER_I18N;

/**
 * @author Ronald Mathies
 */
public class User implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_LABEL = "label";
    private static final String KEY_USER = "user";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_PASSWORD_VERIFY = "password-verify";

    private static final String KEY_USER_ROLES = "user-roles";


    private String uuid = null;

    private StringType label = new StringType();
    private StringType username = new StringType();
    private StringType password = new StringType();
    private StringType passwordVerify = new StringType();
    private UserRoles userRoles = new UserRoles();

    private transient EntryForm entryForm = null;
    private transient TextField labelTextField = null;
    private transient TextField usernameTextField = null;
    private transient PasswordField passwordField = null;
    private transient PasswordField passwordVerifyField = null;
    private transient LegendPanel userPanel = null;
    private transient LegendPanel userRolesPanel = null;

    public User() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        labelTextField = new TextField<>(KEY_LABEL, this.label);
        labelTextField.addValidationRule(new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return labelTextField.getValue();
            }
        });

        usernameTextField = new TextField<>(KEY_USERNAME, this.username);
        usernameTextField.addValidationRule(new MandatoryValidationRule(KEY_USERNAME, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return usernameTextField.getValue();
            }
        });

        PasswordMatchValidationRule rule = new PasswordMatchValidationRule(KEY_PASSWORD, ValidationMessage.Level.ERROR) {
            @Override
            public String getPasswordValue() {
                return password.getValue();
            }

            @Override
            public String getPasswordVerifyValue() {
                return passwordVerify.getValue();
            }
        };
        this.passwordField = new PasswordField(KEY_PASSWORD, this.password);
        this.passwordField.addValidationRule(
                new MandatoryValidationRule(KEY_PASSWORD, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return passwordField.getValue();
                    }
                });
        this.passwordField.addValidationRule(rule);

        this.passwordVerifyField = new PasswordField(KEY_PASSWORD_VERIFY, this.passwordVerify);
        this.passwordVerifyField.addValidationRule(
                new MandatoryValidationRule(KEY_PASSWORD_VERIFY, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return passwordVerifyField.getValue();
                    }
                });
        this.passwordVerifyField.addValidationRule(rule);

        userPanel = new LegendPanel(KEY_USER, USER_I18N.userPanel());
        userPanel.add(
            new EntryForm(KEY_USER)
                .addEntries(
                    new EntryWithLabelAndWidget(KEY_LABEL, USER_I18N.labelField(), labelTextField),
                    new EntryWithDocumentation(KEY_USERNAME, USER_I18N.usernameDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USERNAME, USER_I18N.usernameField(), usernameTextField),
                    new EntryWithLabelAndWidget(KEY_PASSWORD, USER_I18N.passwordField(), passwordField),
                    new EntryWithLabelAndWidget(KEY_PASSWORD_VERIFY, USER_I18N.passwordVerifyField(), passwordVerifyField)));

        userRolesPanel = new LegendPanel(KEY_USER_ROLES, USER_I18N.rolesPanel());
        userRolesPanel.add(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, USER_I18N.rolesDocumentation())));
        userRolesPanel.add(userRoles.toEditForm());

        entryForm = new EntryForm(null)
            .addEntries(
                    new EntryWithContainer(null, userPanel),
                    new EntryWithContainer(null, userRolesPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getUsername() {
        return username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() {
        return this.password;
    }

    public void setPassword(StringType password) {
        this.password = password;
    }

    public void setPasswordVerify(StringType passwordVerify) {
        this.passwordVerify = passwordVerify;
    }

    public UserRoles getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(UserRoles userRoles) {
        this.userRoles = userRoles;
    }

    public int hashCode() {
        return uuid.hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof User && ((User) obj).getUuid().equals(uuid);
    }
}
