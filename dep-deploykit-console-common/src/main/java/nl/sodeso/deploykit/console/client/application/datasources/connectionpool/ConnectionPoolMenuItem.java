package nl.sodeso.deploykit.console.client.application.datasources.connectionpool;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.CONNECTION_POOL_I18N;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolMenuItem extends MenuItem {

    private static final String KEY = "mi-connectionpool";

    private static ConnectionPools connectionPools = null;

    public ConnectionPoolMenuItem() {
        super(KEY, CONNECTION_POOL_I18N.connectionPoolMenuItem(), arguments -> {
            if (connectionPools == null) {
                connectionPools = new ConnectionPools();
            }
            connectionPools.setArguments(arguments);
            CenterController.instance().setWidget(connectionPools);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
