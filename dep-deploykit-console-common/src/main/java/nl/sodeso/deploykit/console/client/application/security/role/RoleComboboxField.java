package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import nl.sodeso.deploykit.console.client.application.security.role.rpc.RoleRpcGateway;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class RoleComboboxField extends ComboboxField<DefaultOption> {

    public RoleComboboxField(OptionType<DefaultOption> optionValue) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                Window.alert("Role Combobox Attached");

                ((RoleRpcGateway) GWT.create(RoleRpcGateway.class)).asOptions(new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                    @Override
                    public void success(ArrayList<DefaultOption> result) {
                        getItems().clear();
                        addItems(result);
                    }
                });
            }
        });
    }
}
