package nl.sodeso.deploykit.console.client.application.tooling.usage;

/**
 * @author Ronald Mathies
 */
public interface UsedByClickHandler {

    void click(UsedBy usedBy);

}
