package nl.sodeso.deploykit.console.client.application.services.webservice.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.services.webservice.Webservice;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface WebserviceRpcAsync extends OptionRpcAsync, VersioningRpcAsync, UsedByRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<WebserviceSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<Webservice> result);

    void save(Webservice webservice, AsyncCallback<WebserviceSummaryItem> result);

    void check(String uuid, VersionOption version, AsyncCallback<String> result);

    void duplicate(String uuid, VersionOption version, AsyncCallback<WebserviceSummaryItem> result);

}
