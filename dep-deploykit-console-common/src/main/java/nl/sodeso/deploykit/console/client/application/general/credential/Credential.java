package nl.sodeso.deploykit.console.client.application.general.credential;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.PasswordField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.rules.PasswordMatchValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CREDENTIAL_I18N;

/**
 * @author Ronald Mathies
 */
public class Credential extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_PLACEHOLDER_PREFIX = "placeholder-prefix";
    private static final String KEY_CREDENTIAL = "credential";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_PASSWORD_VERIFY = "password-verify";

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private StringType placeholderPrefix = new StringType();

    private StringType username = new StringType();
    private StringType password = new StringType();
    private StringType passwordVerify = new StringType();

    private transient LegendPanel credentialLegendPanel;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;
    private transient TextField usernameTextField = null;
    private transient TextField placeholderPrefixField = null;
    private transient PasswordField passwordField = null;
    private transient PasswordField passwordVerifyField = null;

    public Credential() {
    }

    public Widget toEditForm() {
        if (this.credentialLegendPanel != null) {
            return this.credentialLegendPanel;
        }

        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        placeholderPrefixField = new TextField<>(KEY_PLACEHOLDER_PREFIX, this.placeholderPrefix);
        placeholderPrefixField.addValidationRule(
            new MandatoryValidationRule(KEY_PLACEHOLDER_PREFIX, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                return placeholderPrefix.getValue();
            }
            });

        usernameTextField = new TextField<>(KEY_USERNAME, this.username);

        PasswordMatchValidationRule rule = new PasswordMatchValidationRule(KEY_PASSWORD, ValidationMessage.Level.ERROR) {
            @Override
            public String getPasswordValue() {
                return password.getValue();
            }

            @Override
            public String getPasswordVerifyValue() {
                return passwordVerify.getValue();
            }
        };
        this.passwordField = new PasswordField(KEY_PASSWORD, this.password);
        this.passwordField.addValidationRule(
            new MandatoryValidationRule(KEY_PASSWORD, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return passwordField.getValue();
                }
            });
        this.passwordField.addValidationRule(rule);

        this.passwordVerifyField = new PasswordField(KEY_PASSWORD_VERIFY, this.passwordVerify);
        this.passwordVerifyField.addValidationRule(
            new MandatoryValidationRule(KEY_PASSWORD_VERIFY, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return passwordVerifyField.getValue();
                }
            });
        this.passwordVerifyField.addValidationRule(rule);

        EntryForm credentialForm = new EntryForm(null)
            .addEntries(
                    new EntryWithDocumentation(KEY_CREDENTIAL, CREDENTIAL_I18N.credentialDocumentation()),
                    new EntryWithLabelAndWidget(KEY_GROUP, CREDENTIAL_I18N.groupField(), groupField),
                    new EntryWithLabelAndWidget(KEY_LABEL, CREDENTIAL_I18N.labelField(), labelTextField)
                        .addValidationRules(
                            new LabelUniqueValidationRule(GWT.create(CredentialRpcGateway.class)) {
                                @Override
                                public String getUuid() {
                                    return Credential.this.getUuid();
                                }

                                @Override
                                public StringType getLabel() {
                                    return Credential.this.getLabel();
                                }
                            }
                        ),
                    new EntryWithLabelAndWidget(KEY_PLACEHOLDER_PREFIX, CREDENTIAL_I18N.placeHolderPrefixField(), placeholderPrefixField),
                    new EntryWithLabelAndWidget(KEY_USERNAME, CREDENTIAL_I18N.usernameField(),
                        new VerticalPanel(
                            usernameTextField,
                            new PlaceholderSpanField(KEY_USERNAME, placeholderPrefix, CredentialPlaceHolderSuffix.USERNAME))
                        )
                        .addValidatableWidget(usernameTextField),
                    new EntryWithLabelAndWidget(KEY_PASSWORD, CREDENTIAL_I18N.passwordField(),
                        new VerticalPanel(
                            passwordField,
                            new PlaceholderSpanField(KEY_PASSWORD, placeholderPrefix, CredentialPlaceHolderSuffix.PASSWORD))
                        )
                        .addValidatableWidget(passwordField),
                    new EntryWithLabelAndWidget(KEY_PASSWORD_VERIFY, CREDENTIAL_I18N.passwordVerifyField(), passwordVerifyField));

        this.credentialLegendPanel = new LegendPanel(null, CREDENTIAL_I18N.credentialPanel());
        this.credentialLegendPanel.add(credentialForm);

        this.labelTextField.setFocus();

        return this.credentialLegendPanel;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.credentialLegendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.credentialLegendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, credentialLegendPanel);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getUsername() {
        return this.username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() {
        return this.password;
    }

    public void setPassword(StringType password) {
        this.password = password;
    }

    public void setPasswordVerify(StringType passwordVerify) {
        this.passwordVerify = passwordVerify;
    }

}
