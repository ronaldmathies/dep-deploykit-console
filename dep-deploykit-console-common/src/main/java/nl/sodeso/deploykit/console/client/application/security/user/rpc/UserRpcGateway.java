package nl.sodeso.deploykit.console.client.application.security.user.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class UserRpcGateway implements RpcGateway<UserRpcAsync>, UserRpcAsync {

    /**
     * {@inheritDoc}
     */
    public UserRpcAsync getRpcAsync() {
        return GWT.create(UserRpc.class);
    }
}
