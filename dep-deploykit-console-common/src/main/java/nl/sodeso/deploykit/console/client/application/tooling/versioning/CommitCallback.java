package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

/**
 * @author Ronald Mathies
 */
public interface CommitCallback {

    public void onSuccess(VersionOption versionOption);

}
