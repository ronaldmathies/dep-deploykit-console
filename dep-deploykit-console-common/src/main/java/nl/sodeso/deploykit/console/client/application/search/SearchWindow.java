package nl.sodeso.deploykit.console.client.application.search;

import com.google.gwt.core.client.GWT;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPoolMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceMenuItem;
import nl.sodeso.deploykit.console.client.application.profile.ProfileMenuItem;
import nl.sodeso.deploykit.console.client.application.search.rpc.SearchRpcAsync;
import nl.sodeso.deploykit.console.client.application.search.rpc.SearchRpcGateway;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceMenuItem;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceMenuItem;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.table.TablePagerField;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.util.KeyboardUtil;

import java.util.ArrayList;

import static nl.sodeso.deploykit.console.client.application.Resources.SEARCH_I18N;

/**
 * @author Ronald Mathies
 */
public class SearchWindow extends PopupWindowPanel {

    private static SearchWindow instance = null;

    private ResultTable table = null;

    private StringType searchQuery = new StringType();
    private TextField<StringType> searchField = null;

    public SearchWindow() {
        super("search", SEARCH_I18N.searchTitle(), Style.INFO);
        showCollapseButton(true);
        setWidth("650px");

        table = new ResultTable();
        table.setFullHeight(true);
        final SingleSelectionModel<SearchResult> singleSelectionModel = new SingleSelectionModel<>();
        table.setSelectionModel(singleSelectionModel);
        singleSelectionModel.addSelectionChangeHandler(event -> {
            if (table.getSelected() != null) {
                SearchResult selectedSearchResult = table.getSelected();
                switch (selectedSearchResult.getType()) {
                    case HTTP:
                        HttpMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case HTTPS:
                        HttpsMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case DATASOURCE:
                        DatasourceMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case WEBSERVICE:
                        WebserviceMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case LDAP_SERVICE:
                        LdapServiceMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case LDAP_ACCESS_CONTROL:
                        AccessControlMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case JBOSSCONNECTIONPOOL:
                    case JETTYCONNECTIONPOOL:
                        ConnectionPoolMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                        break;
                    case PROFILE:
                        ProfileMenuItem.click(selectedSearchResult.getUuid(), selectedSearchResult.getVersion());
                }

                table.deselect();
            }
        });

        TablePagerField tablePagerField = new TablePagerField();
        tablePagerField.setDisplay(table);

        searchField = new TextField<>("search", searchQuery).setPlaceholder(SEARCH_I18N.enterSearchCriteriaMessage());
        searchField.addKeyUpHandler(event -> {
            if (KeyboardUtil.isEnter(event)) {
                search();
            }
        });

        addToBody(
            new EntryForm("search-form")
                .addEntries(
                        new EntryWithLabelAndWidget("search", SEARCH_I18N.searchField(), searchField),
                        new EntryWithContainer("search-result", table)));

        addToFooter(Align.LEFT,
                new CloseButton.WithLabel((event) -> close()),
                new SearchButton.WithLabel((event) -> search()));

        addToFooter(Align.RIGHT, tablePagerField);
    }

    public static SearchWindow instance() {
        if (instance == null) {
            instance = new SearchWindow();
        }

        return instance;
    }

    private void search() {
        ((SearchRpcAsync)GWT.create(SearchRpcGateway.class)).search(searchQuery.getValue(), new DefaultAsyncCallback<ArrayList<SearchResult>>() {
            @Override
            public void success(ArrayList<SearchResult> result) {
                table.setItems(result);
            }
        });
    }

}
