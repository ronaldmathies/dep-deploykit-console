package nl.sodeso.deploykit.console.client.application.ui.options.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ReferenceTablesRpcAsync {

    void javaServerProviders(AsyncCallback<ArrayList<DefaultOption>> result);
    void accessControlTypes(AsyncCallback<ArrayList<DefaultOption>> result);

}
