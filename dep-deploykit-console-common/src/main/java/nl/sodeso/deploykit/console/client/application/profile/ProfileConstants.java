package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ProfileConstants extends Messages {

    @DefaultMessage("Profiles")
    String profilesMenuItem();

    @DefaultMessage("label")
    String label();

    @DefaultMessage("Profile")
    String profilePanel();

    @DefaultMessage("Group")
    String groupField();



    @DefaultMessage("Datasources ({0})")
    String datasourcesPanel(int count);

    @DefaultMessage("Webservices ({0})")
    String webservicesPanel(int count);

    @DefaultMessage("LDAP Services ({0})")
    String ldapServicesPanel(int count);

    @DefaultMessage("Credentials ({0})")
    String credentialsPanel(int count);

    @DefaultMessage("Access Control")
    String accessControlPanel();

    @DefaultMessage("SSL/HTTPS Connector")
    String sslHttpsPanel();

    @DefaultMessage("Configuration")
    String sslHttpConfigurationField();

    @DefaultMessage("Use HTTP")
    String sslHttpUseField();

    @DefaultMessage("Enables SSL/HTTPS configuration on the nodes.")
    String sslHttpDocumentation();

    @DefaultMessage("HTTP Connector")
    String httpPanel();

    @DefaultMessage("Configuration")
    String httpConfigurationField();

    @DefaultMessage("Use HTTP")
    String httpUseField();

    @DefaultMessage("Enables HTTP configuration on the nodes.")
    String httpDocumentation();

    @DefaultMessage("Nodes ({0})")
    String nodesPanel(int count);

    @DefaultMessage("Custom Properties ({0})")
    String propertiesPanel(int count);

    @DefaultMessage("Add additional properties that are specific to this profile and cannot be placed anywhere else. The key of the property is the placeholder which will be used during the processing of the application resources.")
    String propertiesDocumentation();
}