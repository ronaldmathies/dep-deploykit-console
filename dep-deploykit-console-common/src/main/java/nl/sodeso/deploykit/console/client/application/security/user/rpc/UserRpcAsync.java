package nl.sodeso.deploykit.console.client.application.security.user.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.security.user.User;
import nl.sodeso.deploykit.console.client.application.security.user.UserSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface UserRpcAsync extends DeleteRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<UserSummaryItem>> result);
    void findSingle(String uuid, AsyncCallback<User> result);
    void asOptions(AsyncCallback<ArrayList<DefaultOption>> options);

    void usage(String uuid, AsyncCallback<ArrayList<UsedBy>> result);
    void save(User user, AsyncCallback<UserSummaryItem> result);
    void duplicate(String uuid, AsyncCallback<UserSummaryItem> result);

}
