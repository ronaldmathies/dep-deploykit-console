package nl.sodeso.deploykit.console.client.application.security.role;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class RoleGroupPermissions extends WidgetCollectionField<RoleGroupPermission> {

    @Override
    public WidgetSupplier<RoleGroupPermission> createWidgetSupplier() {
        return callback -> callback.created(new RoleGroupPermission());
    }
}
