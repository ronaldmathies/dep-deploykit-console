package nl.sodeso.deploykit.console.client.application.tooling.delete;

import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.button.UsedByButton;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.panel.ValidationResultPanel;
import nl.sodeso.gwt.ui.client.dialog.DeleteQuestionPanel;

/**
 * @author Ronald Mathies
 */
public class DeleteProcess {

    private DeleteRpcAsync deleteRpcAsync;
    private String uuid;
    private VersionOption version;
    private DeleteCallback callback;
    private Trigger openUsedBy;

    public DeleteProcess(DeleteRpcAsync deleteRpcAsync, String uuid, VersionOption version, DeleteCallback callback, Trigger openUsedBy) {
        this.deleteRpcAsync = deleteRpcAsync;
        this.uuid = uuid;
        this.version = version;
        this.callback = callback;
        this.openUsedBy = openUsedBy;
    }

    public void start() {
        prepareDelete();
    }

    private void prepareDelete() {
        deleteRpcAsync.canBeDeletedSafely(uuid, version, new DefaultAsyncCallback<ValidationResult>() {
            @Override
            public void success(ValidationResult result) {
                if (result.hasMessages()) {
                    ValidationResultPanel validationResultPanel = new ValidationResultPanel(result, "Cannot Delete", "Please fix the following issues before trying to delete this version.");

                    if (result.containsMessageWithKey("in-use")) {
                        UsedByButton.WithLabel usedByButton = new UsedByButton.WithLabel();
                        usedByButton.addClickHandler((event) -> {
                            validationResultPanel.close();
                            openUsedBy.fire();
                        });
                        validationResultPanel.addToFooter(Align.RIGHT, usedByButton);
                    }

                    validationResultPanel.center(true, true);
                } else {
                    DeleteQuestionPanel questionPanel = new DeleteQuestionPanel("This entry is not in use by any component, are you sure you want to delete this entry permanently?", () -> delete());
                    questionPanel.center(true, true);
                }
            }
        });
    }

    private void delete() {
       deleteRpcAsync.delete(uuid, version, new DefaultAsyncCallback<Void>() {
           @Override
           public void success(Void result) {
               callback.onSuccess();
           }
       });
    }
}
