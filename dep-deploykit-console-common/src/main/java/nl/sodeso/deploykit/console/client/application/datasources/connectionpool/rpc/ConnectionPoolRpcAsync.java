package nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionpoolSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface ConnectionPoolRpcAsync extends OptionRpcAsync, VersioningRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync, UsedByRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<ConnectionpoolSummaryItem>> result);
    void findSingle(String uuid, VersionOption version, AsyncCallback<ConnectionPool> result);

    void save(ConnectionPool connectionPool, AsyncCallback<ConnectionpoolSummaryItem> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<ConnectionpoolSummaryItem> result);

}
