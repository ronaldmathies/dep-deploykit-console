package nl.sodeso.deploykit.console.client.application.general.credential;

/**
 * @author Ronald Mathies
 */
public class CredentialPlaceHolderSuffix {

    public static final String USERNAME = "-username";
    public static final String PASSWORD = "-password";

    public static String USERNAME(String prefix) {
        return prefix + USERNAME;
    }

    public static String PASSWORD(String prefix) {
        return prefix + PASSWORD;
    }
}
