package nl.sodeso.deploykit.console.client.application.general.credential.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.general.credential.Credential;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface CredentialRpcAsync extends OptionRpcAsync, VersioningRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync, UsedByRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<CredentialSummaryItem>> result);
    void findSingle(String uuid, VersionOption version, AsyncCallback<Credential> result);

    void save(Credential credential, AsyncCallback<CredentialSummaryItem> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<CredentialSummaryItem> result);


}
