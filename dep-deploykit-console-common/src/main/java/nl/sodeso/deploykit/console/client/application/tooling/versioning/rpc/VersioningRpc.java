package nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public interface VersioningRpc {

    /**
     * Check to see if it is allowed to create a branch based on the given <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param fromVersion the version.
     *
     * @return validation results.
     */
    ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion);

    /**
     * Creates a branch based on the given <code>uuid</code> and <code>version</code>.
     * @param uuid the uuid.
     * @param fromVersion the version.
     *
     * @return the new version created.
     */
    VersionOption branch(String uuid, VersionOption fromVersion);

    /**
     * Returns a list of all the versions available for the given <code>uuid</code>.
     *
     * @param uuid the uuid.
     *
     * @return a list containing all the versions.
     */
    List<VersionOption> versions(String uuid);

    /**
     * Check to see if it is allowed to commit the object based on the given <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param fromVersion the version.
     *
     * @return validation results.
     */
    ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion);

    /**
     * Commits the state of an object based on the given <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param fromVersion the version.
     *
     * @return the new version created.
     */
    VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion);

}
