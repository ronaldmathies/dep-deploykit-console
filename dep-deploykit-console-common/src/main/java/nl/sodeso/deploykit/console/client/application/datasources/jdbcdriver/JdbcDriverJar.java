package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.client.application.connectors.https.FileUploadForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadField;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.JDBC_DRIVER_I18N;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverJar implements WidgetCollectionFieldWidget, Serializable {


    private String uuid = null;
    private FileUploadType fileUploadType = new FileUploadType();

    private transient FileUploadForm fileUploadForm = null;
    private transient FileUploadField fileUploadField = null;

    private transient EntryWithWidgetAndWidget entry = null;

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        this.fileUploadField = new FileUploadField("jarUpload", fileUploadType);
        this.fileUploadField.addValidationRule(new MandatoryValidationRule("jarUpload", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return fileUploadField.getValue();
            }
        });
        this.fileUploadForm = new FileUploadForm(fileUploadField, "/deploykitconsole/jdbcdriver/upload");
        this.entry = new EntryWithLabelAndWidget("jar", JDBC_DRIVER_I18N.javaArchiveField(), this.fileUploadForm);

        return entry;
    }

    protected void upload(Trigger completeTrigger) {
        this.fileUploadForm.setFileUploadCompleteTrigger(completeTrigger);

        if (getFileUploadType().isChanged()) {
            this.fileUploadForm.submit();
        } else {
            completeTrigger.fire();
        }
    }

    public FileUploadType getFileUploadType() {
        return this.fileUploadType;
    }

    public void setFileUploadType(FileUploadType fileUploadType) {
        this.fileUploadType = fileUploadType;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

}

