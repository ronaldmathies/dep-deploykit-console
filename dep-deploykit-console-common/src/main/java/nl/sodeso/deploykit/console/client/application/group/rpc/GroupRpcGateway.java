package nl.sodeso.deploykit.console.client.application.group.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class GroupRpcGateway implements RpcGateway<GroupRpcAsync>, GroupRpcAsync {

    /**
     * {@inheritDoc}
     */
    public GroupRpcAsync getRpcAsync() {
        return GWT.create(GroupRpc.class);
    }
}
