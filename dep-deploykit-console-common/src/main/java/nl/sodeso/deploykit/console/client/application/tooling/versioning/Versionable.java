package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class Versionable implements Serializable {

    private String uuid = null;
    private VersionOptionType version = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));

    public Versionable() {}

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public VersionOptionType getVersion() {
        return this.version;
    }

    public void setVersion(VersionOptionType version) {
        this.version = version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Versionable) {
            return ((Versionable)obj).getUuid().equals(uuid);
        }

        return false;
    }
}
