package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc.JdbcDriverRpcGateway;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverClassComboboxField extends ComboboxField<DefaultOption> {

    public JdbcDriverClassComboboxField(OptionType<DefaultOption> optionValue, final String uuid, final VersionOptionType versionOptionType) {
        super(optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                ((JdbcDriverRpcGateway) GWT.create(JdbcDriverRpcGateway.class)).classes(uuid, versionOptionType.getValue(), new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                    @Override
                    public void success(ArrayList<DefaultOption> result) {
                        replaceItemsRetainSelection(result);
                    }
                });
            }
        });
    }
}
