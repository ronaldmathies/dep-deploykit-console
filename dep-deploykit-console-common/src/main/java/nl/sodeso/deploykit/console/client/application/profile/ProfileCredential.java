package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ProfileCredential implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> credentialOptionType = new OptionType<>();
    private VersionOptionType credentialVersionType = new VersionOptionType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient VersionSelectionField<CredentialRpcGateway> credentialField = null;

    public ProfileCredential() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        credentialField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), credentialOptionType, credentialVersionType);
        credentialField.listenToGroup(groupOptionType);

        entry = new EntryWithWidgetAndWidget("credential", null,
            new HorizontalPanel()
                .addWidget(this.credentialField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(credentialOptionType.getValue().getKey(), credentialVersionType.getValue()))));

        entry.addValidationRule(
            new ValidationCriteria.or(
                    new ComboboxMandatoryValidationRule("credential-option", ValidationMessage.Level.ERROR, groupOptionType),
                    new ComboboxMandatoryValidationRule("credential-version", ValidationMessage.Level.ERROR, credentialOptionType)
            )
        );

//        entry.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                credentialField.refresh(groupOptionType);
//            }
//        });

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getCredential() {
        return this.credentialOptionType;
    }

    public void setCredential(OptionType<DefaultOption> node) {
        this.credentialOptionType = node;
    }

    public VersionOptionType getCredentialVersion() {
        return credentialVersionType;
    }

    public void setCredentialVersion(VersionOptionType credentialVersionType) {
        this.credentialVersionType = credentialVersionType;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
