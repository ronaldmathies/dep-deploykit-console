package nl.sodeso.deploykit.console.client.application.datasources.datasource;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */

public class DatasourceConnectionPools extends WidgetCollectionField<DatasourceConnectionPool> {

    private OptionType<DefaultOption> groupOptionType = null;

    private DatasourceConnectionPools() {}

    public DatasourceConnectionPools(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    @Override
    public WidgetSupplier<DatasourceConnectionPool> createWidgetSupplier() {
        return callback -> {
            DatasourceConnectionPool datasourceConnectionPool = new DatasourceConnectionPool();
            datasourceConnectionPool.setGroup(groupOptionType);
            callback.created(datasourceConnectionPool);
        };
    }

}
