package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public interface VersionSpecifiedHandler {

    void ok(StringType version);

}
