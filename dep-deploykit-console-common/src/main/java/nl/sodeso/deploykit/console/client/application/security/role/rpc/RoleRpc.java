package nl.sodeso.deploykit.console.client.application.security.role.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.security.role.Role;
import nl.sodeso.deploykit.console.client.application.security.role.RoleSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-role")
public interface RoleRpc extends XsrfProtectedService, DeleteRpc {

    ArrayList<RoleSummaryItem> findSummaries();
    Role findSingle(String uuid);
    ArrayList<DefaultOption> asOptions();

    RoleSummaryItem save(Role role);
    ArrayList<UsedBy> usage(String uuid);
    RoleSummaryItem duplicate(String uuid);

}
