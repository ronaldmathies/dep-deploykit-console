package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.PROFILE_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class ProfileHttps implements Serializable, IsValidationContainer, IsRevertable  {

    private BooleanType useHttps = new BooleanType(false);
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> httpsOptionType = new OptionType<>();
    private VersionOptionType httpsVersionType = new VersionOptionType();

    private transient EntryForm editForm = null;
//    private transient GroupComboboxField groupField = null;
    private transient RadioButtonGroupField useHttpsGroupField = null;
    private transient VersionSelectionField<HttpsRpcGateway> httpsField = null;


    public Widget toEditForm() {
        if (editForm != null) {
            return editForm;
        }

        editForm = new EntryForm("profile-https");

        useHttpsGroupField = new RadioButtonGroupField("usehttps", this.useHttps);
        useHttpsGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useHttpsGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useHttpsGroupField.setAlignment(Align.HORIZONTAL);
        useHttpsGroupField.addClickHandler(event -> {
            boolean visible = useHttps.getValue();
            editForm.setEntriesVisible(visible, "configuration"); // "group", 

            if (!visible) {
//                groupOptionType.setValue(null);
                httpsOptionType.setValue(null);
            }
        });
        editForm.addEntry(new EntryWithDocumentation("usehttps", PROFILE_I18N.sslHttpDocumentation()));
        editForm.addEntry(new EntryWithLabelAndWidget("usehttps", PROFILE_I18N.sslHttpUseField(), this.useHttpsGroupField));

//        groupField = new GroupComboboxField("group", groupOptionType);
//        editForm.addEntry(new EntryWithLabelAndWidget("group", PROFILE_I18N.groupField(), groupField));

        httpsField = new VersionSelectionField<>(GWT.create(HttpsRpcGateway.class), httpsOptionType, httpsVersionType);
        httpsField.listenToGroup(groupOptionType);

        EntryWithLabelAndWidget entry = new EntryWithLabelAndWidget("configuration", PROFILE_I18N.sslHttpConfigurationField(),
                new HorizontalPanel()
                    .addWidget(httpsField, 100.0, Style.Unit.PCT)
                    .addWidget(new SearchButton.WithLabel((event) -> HttpsMenuItem.click(httpsOptionType.getValue().getKey(), httpsVersionType.getValue()))));

        entry.addValidationRule(
//                new ValidationCriteria.or(new ComboboxMandatoryValidationRule("https-group", ValidationMessage.Level.ERROR, groupOptionType),
                        new ValidationCriteria.or(
                                new ComboboxMandatoryValidationRule("https-option", ValidationMessage.Level.ERROR, httpsOptionType),
                                new ComboboxMandatoryValidationRule("https-version", ValidationMessage.Level.ERROR, httpsVersionType)
                        )
        //        )
                {

                    @Override
                    public boolean isApplicable() {
                        return useHttps.getValue();
                    }
                }
        );

        editForm.addEntry(entry);

        editForm.addAttachHandler(event -> {
            if (event.isAttached()) {
                useHttpsGroupField.fireClickEvent();
                httpsField.refreshOptionList();
            }
        });

        return editForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public BooleanType getUseHttps() {
        return useHttps;
    }

    public void setUseHttps(BooleanType useHttps) {
        this.useHttps = useHttps;
    }

    public OptionType<DefaultOption> getGroup() {
        return groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public OptionType<DefaultOption> getHttpsOptionType() {
        return httpsOptionType;
    }

    public void setHttpsOptionType(OptionType<DefaultOption> httpsOptionType) {
        this.httpsOptionType = httpsOptionType;
    }

    public VersionOptionType getHttpsVersionType() {
        return httpsVersionType;
    }

    public void setHttpsVersionType(VersionOptionType httpsVersionType) {
        this.httpsVersionType = httpsVersionType;
    }
}
