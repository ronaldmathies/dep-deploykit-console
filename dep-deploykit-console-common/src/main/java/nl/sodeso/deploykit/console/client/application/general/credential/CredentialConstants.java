package nl.sodeso.deploykit.console.client.application.general.credential;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface CredentialConstants extends Messages {

    @DefaultMessage("Credentials")
    String credentialMenuItem();

    @DefaultMessage("Credential")
    String credentialPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Placeholder Prefix")
    String placeHolderPrefixField();

    @DefaultMessage("Username")
    String usernameField();

    @DefaultMessage("Password")
    String passwordField();

    @DefaultMessage("Password Verify")
    String passwordVerifyField();

    @DefaultMessage("Credential {0} Used By")
    String credentialUsedByTitle(String name);

    @DefaultMessage("No Credentials")
    String noCredentialsEmptyTable();

    @DefaultMessage("Cannot Delete Credential")
    String cannotDeleteCredentialTitle();

    @DefaultMessage("Cannot delete this credential while it is in use.")
    String cannotDeleteCredentialMessage();

    @DefaultMessage("Credentials are used in various resource like data sources and SSL/HTTPS configurations. The username field is optional since it is used in a data source, SSL/HTTPS certificates however don&apos;t use username so in this case you can leave the field blank.")
    String credentialDocumentation();
}