package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverJars extends WidgetCollectionField<JdbcDriverJar> {

    @Override
    public WidgetSupplier<JdbcDriverJar> createWidgetSupplier() {
        return callback -> callback.created(new JdbcDriverJar());
    }
}