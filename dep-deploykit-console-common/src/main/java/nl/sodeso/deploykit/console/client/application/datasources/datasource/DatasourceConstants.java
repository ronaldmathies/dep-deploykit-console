package nl.sodeso.deploykit.console.client.application.datasources.datasource;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface DatasourceConstants extends Messages {

    @DefaultMessage("Datasources")
    String datasourcesMenuItem();

    @DefaultMessage("Datasource")
    String datasourcePanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Placeholder Prefix")
    String placeholderPrefixField();

    @DefaultMessage("Connection Information")
    String connectionInformationPanel();

    @DefaultMessage("JNDI")
    String jndiField();

    @DefaultMessage("URL")
    String urlField();

    @DefaultMessage("JDBC Driver")
    String jdbcDriverField();

    @DefaultMessage("Credential")
    String credentialField();

    @DefaultMessage("Connection Pools")
    String connectionPoolsPanel();

    @DefaultMessage("Custom Properties")
    String customPropertiesPanel();

    @DefaultMessage("The connection properties allow you to pass in arbitrary connection properties to the Driver.connection(url, props) method.")
    String customPropertiesDocumentation();

    @DefaultMessage("Checking if data source configuration is correct, this process may take a while so please do not close the browser.")
    String checkDatasourceDocumentation();

    @DefaultMessage("Checking Data Source Connection")
    String checkDatasourceTitle();

    @DefaultMessage("Datasource Connection")
    String checkDatasourceResultTitle();

    @DefaultMessage("Datasource {0} Used By")
    String datasourceUsedByTitle(String name);
}