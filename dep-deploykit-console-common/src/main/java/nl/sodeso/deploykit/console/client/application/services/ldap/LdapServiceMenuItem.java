package nl.sodeso.deploykit.console.client.application.services.ldap;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.LDAP_SERVICE_I18N;

/**
 * @author Ronald Mathies
 */
public class LdapServiceMenuItem extends MenuItem {

    private static final String KEY = "mi-ldap-service";

    public static final String ARG_UUID = "uuid";
    public static final String ARG_VERSION = "version";
    public static final String ARG_TYPE = "type";

    private static LdapServices ldapServices = null;

    public LdapServiceMenuItem() {
        super(KEY, LDAP_SERVICE_I18N.ldapServicesMenuItem(), arguments -> {
            if (ldapServices == null) {
                ldapServices = new LdapServices();
            }
            ldapServices.setArguments(arguments);
            CenterController.instance().setWidget(ldapServices);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
