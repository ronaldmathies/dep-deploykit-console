package nl.sodeso.deploykit.console.client.application.services.webservice.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class WebserviceRpcGateway implements RpcGateway<WebserviceRpcAsync>, WebserviceRpcAsync {

    /**
     * {@inheritDoc}
     */
    public WebserviceRpcAsync getRpcAsync() {
        return GWT.create(WebserviceRpc.class);
    }
}
