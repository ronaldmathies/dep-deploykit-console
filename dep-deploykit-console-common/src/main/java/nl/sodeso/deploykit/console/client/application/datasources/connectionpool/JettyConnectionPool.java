package nl.sodeso.deploykit.console.client.application.datasources.connectionpool;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.IntegerRangeValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.CONNECTION_POOL_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class JettyConnectionPool implements Serializable, IsValidationContainer, IsRevertable {

    private transient static final String KEY_JETTY = "jetty";
    private transient static final String KEY_INITIAL_SIZE = "initial-size";
    private transient static final String KEY_MAX_TOTAL = "max-total";
    private transient static final String KEY_MAX_IDLE = "max-idle";
    private transient static final String KEY_MIN_IDLE = "min-idle";
    private transient static final String KEY_MAX_WAIT_MILLIS = "max-wait-millis";
    private transient static final String KEY_VALIDATION_QUERY = "validation-query";
    private transient static final String KEY_TEST_ON_CREATE = "test-on-create";
    private transient static final String KEY_TEST_ON_RETURN = "test-on-return";
    private transient static final String KEY_TEST_ON_BORROW = "test-on-borrow";
    private transient static final String KEY_TEST_WHILE_IDLE = "test-while-idle";
    private transient static final String KEY_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "time-between-eviction-runs-millis";
    private transient static final String KEY_NUM_TESTS_PER_EVICTION_RUN = "num-tests-per-eviction-run";
    private transient static final String KEY_MIN_EVICTABLE_IDLE_TIME_MILLIS = "min-evictable-idle-time-millis";
    private transient static final String KEY_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS = "soft-min-evictable-idle-time-millis";
    private transient static final String KEY_NAX_CONN_LIFETIME_MILLIS = "max-conn-lifetime-millis";
    private transient static final String KEY_LOG_EXPIRED_CONNECTIONS= "log-expired-connections";
    private transient static final String KEY_CONNECTION_INIT_SQLS = "connection-init-sqls";
    private transient static final String KEY_LIFO = "lifo";
    private transient static final String KEY_DEFAULT_AUTO_COMMIT = "default-autocommit";
    private transient static final String KEY_DEFAULT_READ_ONLY = "default-readonly";
    private transient static final String KEY_DEFAULT_CATALOG = "default-catalog";
    private transient static final String KEY_CACHE_STATE = "cache-state";
    private transient static final String KEY_DEFAULT_TRANSACTION_ISOLATION = "default-transaction-isolation";

    private IntType initialSize = new IntType(0);
    private IntType maxTotal = new IntType(8);
    private IntType maxIdle = new IntType(8);
    private IntType minIdle = new IntType(0);
    private IntType maxWaitMillis = new IntType(-1);

    private StringType validationQuery = new StringType();
    private BooleanType testOnCreate = new BooleanType(false);
    private BooleanType testOnBorrow = new BooleanType(true);
    private BooleanType testOnReturn = new BooleanType(false);
    private BooleanType testWhileIdle = new BooleanType(false);
    private IntType timeBetweenEvictionRunsMillis = new IntType(-1);
    private IntType numTestsPerEvictionRun = new IntType(3);
    private IntType minEvictableIdleTimeMillis = new IntType(1000 * 60 * 30);
    private IntType softMiniEvictableIdleTimeMillis = new IntType(-1);
    private IntType maxConnLifetimeMillis = new IntType(-1);
    private BooleanType logExpiredConnections = new BooleanType(true);
    private StringType connectionInitSqls = new StringType();
    private BooleanType lifo = new BooleanType(true);

    private IntType defaultAutoCommit = new IntType(2);
    private IntType defaultReadOnly = new IntType(2);
    private OptionType<DefaultOption> defaultTransactionIsolation = new OptionType<>(new DefaultOption("default", "default", "DRIVER_DEFAULT"));
    private StringType defaultCatalog = new StringType();
    private BooleanType cacheState = new BooleanType(true);

    private transient EntryForm entryForm = null;

    private transient LegendPanel connectionPoolLegendPanel = null;
    private transient EntryForm connectionPoolEntryForm = null;
    private transient TextField initialSizeField = null;
    private transient TextField maxTotalField = null;
    private transient TextField maxIdleField = null;
    private transient TextField minIdleField = null;
    private transient TextField maxWaitMillisField = null;

    private transient LegendPanel connectionLegendPanel = null;
    private transient EntryForm connectionEntryForm = null;
    private transient TextField validationQueryField = null;
    private transient RadioButtonGroupField testOnCreateGroupField = null;
    private transient RadioButtonGroupField testOnBorrowGroupField = null;
    private transient RadioButtonGroupField testOnReturnGroupField = null;
    private transient RadioButtonGroupField testWhileIdleGroupField = null;
    private transient TextField timeBetweenEvictionRunsMillisField = null;
    private transient TextField numTestsPerEvictionRunField = null;
    private transient TextField minEvictableIdleTimeMillisField = null;
    private transient TextField softMiniEvictableIdleTimeMillisField = null;
    private transient TextField maxConnLifetimeMillisField = null;
    private transient RadioButtonGroupField logExpiredConnectionsGroupField = null;
    private transient TextField connectionInitSqlsField = null;
    private transient RadioButtonGroupField lifoGroupField = null;

    private transient LegendPanel transactionLegendPanel = null;
    private transient EntryForm transactionEntryForm = null;
    private transient RadioButtonGroupField defaultAutoCommitGroupField = null;
    private transient RadioButtonGroupField defaultReadOnlyGroupField = null;
    private transient ComboboxField<DefaultOption> defaultTransactionIsolationField = null;
    private transient TextField defaultCatalogField = null;
    private transient RadioButtonGroupField cacheStateGroupField = null;

    public JettyConnectionPool() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupConnectionPoolSettings();
        setupConnectionSetttings();
        setupTransactionConfiguration();

        this.entryForm = new EntryForm(KEY_JETTY);
        this.entryForm.addEntry(new EntryWithContainer(null, this.connectionPoolLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.connectionLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.transactionLegendPanel));

        this.initialSizeField.setFocus();

        return this.entryForm;
    }

    private void setupConnectionPoolSettings() {
        this.initialSizeField = new TextField<>(KEY_INITIAL_SIZE, this.initialSize).addFilter(new NumericInputFilter(false));
        this.maxTotalField = new TextField<>(KEY_MAX_TOTAL, this.maxTotal).addFilter(new NumericInputFilter(false));
        this.maxIdleField = new TextField<>(KEY_MAX_IDLE, this.maxIdle).addFilter(new NumericInputFilter(false));
        this.minIdleField = new TextField<>(KEY_MIN_IDLE, this.minIdle).addFilter(new NumericInputFilter(false));
        this.maxWaitMillisField = new TextField<>(KEY_MAX_WAIT_MILLIS, this.maxWaitMillis).addFilter(new NumericInputFilter(true));

        this.connectionPoolEntryForm = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_INITIAL_SIZE, CONNECTION_POOL_I18N.initialPoolSizeDocumentation()),
                        new EntryWithLabelAndWidget(KEY_INITIAL_SIZE, CONNECTION_POOL_I18N.initialPoolSizeField(), this.initialSizeField),
                        new EntryWithDocumentation(KEY_MAX_TOTAL, CONNECTION_POOL_I18N.maximumPoolSizeDocumentationJetty()),
                        new EntryWithLabelAndWidget(KEY_MAX_TOTAL, CONNECTION_POOL_I18N.maximumPoolSizeField(), this.maxTotalField),
                        new EntryWithDocumentation(KEY_MAX_IDLE, CONNECTION_POOL_I18N.maximumIdleDocumentation()),
                        new EntryWithLabelAndWidget(KEY_MAX_IDLE, CONNECTION_POOL_I18N.maximumIdleField(), this.maxIdleField),
                        new EntryWithDocumentation(KEY_MIN_IDLE, CONNECTION_POOL_I18N.minimumIdleDocumentation()),
                        new EntryWithLabelAndWidget(KEY_MIN_IDLE, CONNECTION_POOL_I18N.minimumIdleField(), this.minIdleField),
                        new EntryWithDocumentation(KEY_MAX_WAIT_MILLIS, CONNECTION_POOL_I18N.maximumWaitDocumentation()),
                        new EntryWithLabelAndWidget(KEY_MAX_WAIT_MILLIS, CONNECTION_POOL_I18N.maximumWaitField(), this.maxWaitMillisField)
                                .setHint(CONNECTION_POOL_I18N.maximumWaitFieldHint()));

        this.connectionPoolLegendPanel = new LegendPanel(null, CONNECTION_POOL_I18N.connectionPoolPropertiesPanel());
        this.connectionPoolLegendPanel.add(this.connectionPoolEntryForm);
    }

    private void setupConnectionSetttings() {
        this.validationQueryField = new TextField<>(KEY_VALIDATION_QUERY, this.validationQuery);

        this.testOnCreateGroupField = new RadioButtonGroupField(KEY_TEST_ON_CREATE, testOnCreate);
        this.testOnCreateGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.testOnCreateGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.testOnCreateGroupField.setAlignment(Align.HORIZONTAL);

        this.testOnBorrowGroupField = new RadioButtonGroupField(KEY_TEST_ON_BORROW, testOnBorrow);
        this.testOnBorrowGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.testOnBorrowGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.testOnBorrowGroupField.setAlignment(Align.HORIZONTAL);

        this.testOnReturnGroupField = new RadioButtonGroupField(KEY_TEST_ON_RETURN, testOnReturn);
        this.testOnReturnGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.testOnReturnGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.testOnReturnGroupField.setAlignment(Align.HORIZONTAL);

        this.testWhileIdleGroupField = new RadioButtonGroupField(KEY_TEST_WHILE_IDLE, testWhileIdle);
        this.testWhileIdleGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.testWhileIdleGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.testWhileIdleGroupField.setAlignment(Align.HORIZONTAL);

        this.timeBetweenEvictionRunsMillisField = new TextField<>(KEY_TIME_BETWEEN_EVICTION_RUNS_MILLIS, this.timeBetweenEvictionRunsMillis)
                .addFilter(new NumericInputFilter(true));
        this.timeBetweenEvictionRunsMillisField.addValidationRule(
                new IntegerRangeValidationRule(KEY_TIME_BETWEEN_EVICTION_RUNS_MILLIS, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return timeBetweenEvictionRunsMillisField.getValue();
                    }
                }.setMinimum(-1));
        this.numTestsPerEvictionRunField = new TextField<>(KEY_NUM_TESTS_PER_EVICTION_RUN, this.numTestsPerEvictionRun)
                .addFilter(new NumericInputFilter(false));
        this.minEvictableIdleTimeMillisField = new TextField<>(KEY_MIN_EVICTABLE_IDLE_TIME_MILLIS, this.minEvictableIdleTimeMillis)
                .addFilter(new NumericInputFilter(false));
        this.softMiniEvictableIdleTimeMillisField = new TextField<>(KEY_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS, this.softMiniEvictableIdleTimeMillis)
                .addFilter(new NumericInputFilter(true))
                .addValidationRule(
                    new IntegerRangeValidationRule(KEY_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS, ValidationMessage.Level.ERROR) {
                        @Override
                        public String getValue() {
                            return softMiniEvictableIdleTimeMillisField.getValue();
                        }
                    }.setMinimum(-1));
        this.maxConnLifetimeMillisField = new TextField<>(KEY_NAX_CONN_LIFETIME_MILLIS, this.maxConnLifetimeMillis)
                .addFilter(new NumericInputFilter(true))
                .addValidationRule(
                    new IntegerRangeValidationRule(KEY_NAX_CONN_LIFETIME_MILLIS, ValidationMessage.Level.ERROR) {
                        @Override
                        public String getValue() {
                            return maxConnLifetimeMillisField.getValue();
                        }
                    }.setMinimum(-1));

        this.logExpiredConnectionsGroupField = new RadioButtonGroupField(KEY_LOG_EXPIRED_CONNECTIONS, logExpiredConnections);
        this.logExpiredConnectionsGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.logExpiredConnectionsGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.logExpiredConnectionsGroupField.setAlignment(Align.HORIZONTAL);
        this.connectionInitSqlsField = new TextField<>(KEY_CONNECTION_INIT_SQLS, this.connectionInitSqls);

        this.lifoGroupField = new RadioButtonGroupField(KEY_LIFO, lifo);
        this.lifoGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.lifoGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.lifoGroupField.setAlignment(Align.HORIZONTAL);

        this.connectionEntryForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_VALIDATION_QUERY, CONNECTION_POOL_I18N.validationQueryDocumentationJetty()),
                    new EntryWithLabelAndWidget(KEY_VALIDATION_QUERY, CONNECTION_POOL_I18N.validationQueryFieldJetty(), this.validationQueryField),
                    new EntryWithDocumentation(KEY_TEST_ON_CREATE, CONNECTION_POOL_I18N.testOnCreateDocumentation()),
                    new EntryWithLabelAndWidget(KEY_TEST_ON_CREATE, CONNECTION_POOL_I18N.testOnCreateField(), this.testOnCreateGroupField),
                    new EntryWithDocumentation(KEY_TEST_ON_BORROW, CONNECTION_POOL_I18N.testOnBorrowDocumentation()),
                    new EntryWithLabelAndWidget(KEY_TEST_ON_BORROW, CONNECTION_POOL_I18N.testOnBorrowField(), this.testOnBorrowGroupField),
                    new EntryWithDocumentation(KEY_TEST_ON_RETURN, CONNECTION_POOL_I18N.testOnReturnDocumentation()),
                    new EntryWithLabelAndWidget(KEY_TEST_ON_RETURN, CONNECTION_POOL_I18N.testOnReturnField(), this.testOnReturnGroupField),
                    new EntryWithDocumentation(KEY_TEST_WHILE_IDLE, CONNECTION_POOL_I18N.testWhileIdleDocumentation()),
                    new EntryWithLabelAndWidget(KEY_TEST_WHILE_IDLE, CONNECTION_POOL_I18N.testWhileIdleField(), this.testWhileIdleGroupField),
                    new EntryWithDocumentation(KEY_TIME_BETWEEN_EVICTION_RUNS_MILLIS, CONNECTION_POOL_I18N.timeBetweenEvictionRunsDocumentation()),
                    new EntryWithLabelAndWidget(KEY_TIME_BETWEEN_EVICTION_RUNS_MILLIS, CONNECTION_POOL_I18N.timeBetweenEvictionRunsField(), this.timeBetweenEvictionRunsMillisField).setHint("Milliseconds"),
                    new EntryWithDocumentation(KEY_NUM_TESTS_PER_EVICTION_RUN, CONNECTION_POOL_I18N.runTestBetweenEvictionRunDocumentation()),
                    new EntryWithLabelAndWidget(KEY_NUM_TESTS_PER_EVICTION_RUN, CONNECTION_POOL_I18N.runTestBetweenEvictionRunField(), this.numTestsPerEvictionRunField),
                    new EntryWithDocumentation(KEY_MIN_EVICTABLE_IDLE_TIME_MILLIS, CONNECTION_POOL_I18N.minEvictableIdleTimeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_MIN_EVICTABLE_IDLE_TIME_MILLIS, CONNECTION_POOL_I18N.minEvictableIdleTimeField(), this.minEvictableIdleTimeMillisField).setHint("Milliseconds"),
                    new EntryWithDocumentation(KEY_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS, CONNECTION_POOL_I18N.softMinEvictableIdleTimeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS, CONNECTION_POOL_I18N.softMinEvictableIdleTimeField(), this.softMiniEvictableIdleTimeMillisField).setHint("Milliseconds"),
                    new EntryWithDocumentation(KEY_NAX_CONN_LIFETIME_MILLIS, CONNECTION_POOL_I18N.maxConnectionLifetimeDocumentation()),
                    new EntryWithLabelAndWidget(KEY_NAX_CONN_LIFETIME_MILLIS, CONNECTION_POOL_I18N.maxConnectionLifetimeField(), this.maxConnLifetimeMillisField).setHint("Milliseconds"),
                    new EntryWithDocumentation(KEY_LOG_EXPIRED_CONNECTIONS, CONNECTION_POOL_I18N.logExpiredConnectionsDocumentation()),
                    new EntryWithLabelAndWidget(KEY_LOG_EXPIRED_CONNECTIONS, CONNECTION_POOL_I18N.logExpiredConnectionsField(), this.logExpiredConnectionsGroupField),
                    new EntryWithDocumentation(KEY_CONNECTION_INIT_SQLS, CONNECTION_POOL_I18N.initSqlStatementsDocumentation()),
                    new EntryWithLabelAndWidget(KEY_CONNECTION_INIT_SQLS, CONNECTION_POOL_I18N.initSqlStatementsField(), this.connectionInitSqlsField),
                    new EntryWithDocumentation(KEY_LIFO, CONNECTION_POOL_I18N.lifoDocumentation()),
                    new EntryWithLabelAndWidget(KEY_LIFO, CONNECTION_POOL_I18N.lifoField(), this.lifoGroupField));

        this.connectionLegendPanel = new LegendPanel(null, CONNECTION_POOL_I18N.connectionPropertiesPanel());
        this.connectionLegendPanel.add(this.connectionEntryForm);
    }

    private void setupTransactionConfiguration() {
        this.defaultAutoCommitGroupField = new RadioButtonGroupField(KEY_DEFAULT_AUTO_COMMIT, this.defaultAutoCommit);
        this.defaultAutoCommitGroupField.addRadioButton("1", GENERAL_I18N.Yes());
        this.defaultAutoCommitGroupField.addRadioButton("0", GENERAL_I18N.No());
        this.defaultAutoCommitGroupField.addRadioButton("2", "Driver Default");
        this.defaultAutoCommitGroupField.setAlignment(Align.HORIZONTAL);

        this.defaultReadOnlyGroupField = new RadioButtonGroupField(KEY_DEFAULT_READ_ONLY, this.defaultReadOnly);
        this.defaultReadOnlyGroupField.addRadioButton("1", GENERAL_I18N.Yes());
        this.defaultReadOnlyGroupField.addRadioButton("0", GENERAL_I18N.No());
        this.defaultReadOnlyGroupField.addRadioButton("2", "Driver Default");
        this.defaultReadOnlyGroupField.setAlignment(Align.HORIZONTAL);

        this.defaultTransactionIsolationField = new ComboboxField<>(KEY_DEFAULT_TRANSACTION_ISOLATION, defaultTransactionIsolation);
        this.defaultTransactionIsolationField.addItems(
                new DefaultOption("default", "default", "DRIVER_DEFAULT"),
                new DefaultOption("none", "none", "NONE"),
                new DefaultOption("readcommited", "readcommited", "READ_COMMITED"),
                new DefaultOption("readuncommited", "readuncommited", "READ_UNCOMMITTED"),
                new DefaultOption("repeatableread", "repeatableread", "REPEATABLE_READ"),
                new DefaultOption("serializable", "serializable", "SERIALIZABLE"));

        this.defaultCatalogField = new TextField<>(KEY_DEFAULT_CATALOG, this.defaultCatalog);

        this.cacheStateGroupField = new RadioButtonGroupField(KEY_CACHE_STATE, this.cacheState);
        this.cacheStateGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        this.cacheStateGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        this.cacheStateGroupField.setAlignment(Align.HORIZONTAL);

        this.transactionEntryForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_DEFAULT_AUTO_COMMIT, CONNECTION_POOL_I18N.defaultAutoCommitDocumentation()),
                    new EntryWithLabelAndWidget(KEY_DEFAULT_AUTO_COMMIT, CONNECTION_POOL_I18N.defaultAutoCommitField(), this.defaultAutoCommitGroupField),
                    new EntryWithDocumentation(KEY_DEFAULT_READ_ONLY, CONNECTION_POOL_I18N.defaultReadOnlyDocumentation()),
                    new EntryWithLabelAndWidget(KEY_DEFAULT_READ_ONLY, CONNECTION_POOL_I18N.defaultReadOnlyField(), this.defaultReadOnlyGroupField),
                    new EntryWithDocumentation(KEY_DEFAULT_TRANSACTION_ISOLATION, CONNECTION_POOL_I18N.defaultTransactionIsolationDocumentation()),
                    new EntryWithLabelAndWidget(KEY_DEFAULT_TRANSACTION_ISOLATION, CONNECTION_POOL_I18N.defaultTransactionIsolationField(), defaultTransactionIsolationField),
                    new EntryWithDocumentation(KEY_DEFAULT_CATALOG, CONNECTION_POOL_I18N.defaultCatalogDocumentation()),
                    new EntryWithLabelAndWidget(KEY_DEFAULT_CATALOG, CONNECTION_POOL_I18N.defaultCatalogField(), this.defaultCatalogField),
                    new EntryWithDocumentation(KEY_CACHE_STATE, CONNECTION_POOL_I18N.cacheStateDocumentation()),
                    new EntryWithLabelAndWidget(KEY_CACHE_STATE, CONNECTION_POOL_I18N.cacheStateField(), this.cacheStateGroupField));

        this.transactionLegendPanel = new LegendPanel(null, CONNECTION_POOL_I18N.transactionPropertiesPanel());
        this.transactionLegendPanel.add(this.transactionEntryForm);
    }


    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public IntType getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(IntType initialSize) {
        this.initialSize = initialSize;
    }

    public IntType getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(IntType maxTotal) {
        this.maxTotal = maxTotal;
    }

    public IntType getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(IntType maxIdle) {
        this.maxIdle = maxIdle;
    }

    public IntType getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(IntType minIdle) {
        this.minIdle = minIdle;
    }

    public IntType getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(IntType maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public IntType getDefaultAutoCommit() {
        return defaultAutoCommit;
    }

    public void setDefaultAutoCommit(IntType defaultAutoCommit) {
        this.defaultAutoCommit = defaultAutoCommit;
    }

    public IntType getDefaultReadOnly() {
        return defaultReadOnly;
    }

    public void setDefaultReadOnly(IntType defaultReadOnly) {
        this.defaultReadOnly = defaultReadOnly;
    }

    public OptionType<DefaultOption> getDefaultTransactionIsolation() {
        return defaultTransactionIsolation;
    }

    public void setDefaultTransactionIsolation(OptionType<DefaultOption> defaultTransactionIsolation) {
        this.defaultTransactionIsolation = defaultTransactionIsolation;
    }

    public StringType getDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(StringType defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public BooleanType getCacheState() {
        return cacheState;
    }

    public void setCacheState(BooleanType cacheState) {
        this.cacheState = cacheState;
    }

    public StringType getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(StringType validationQuery) {
        this.validationQuery = validationQuery;
    }

    public BooleanType getTestOnCreate() {
        return testOnCreate;
    }

    public void setTestOnCreate(BooleanType testOnCreate) {
        this.testOnCreate = testOnCreate;
    }

    public BooleanType getTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(BooleanType testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public BooleanType getTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(BooleanType testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public BooleanType getTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(BooleanType testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public IntType getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(IntType timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public IntType getNumTestsPerEvictionRun() {
        return numTestsPerEvictionRun;
    }

    public void setNumTestsPerEvictionRun(IntType numTestsPerEvictionRun) {
        this.numTestsPerEvictionRun = numTestsPerEvictionRun;
    }

    public IntType getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(IntType minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public IntType getSoftMiniEvictableIdleTimeMillis() {
        return softMiniEvictableIdleTimeMillis;
    }

    public void setSoftMiniEvictableIdleTimeMillis(IntType softMiniEvictableIdleTimeMillis) {
        this.softMiniEvictableIdleTimeMillis = softMiniEvictableIdleTimeMillis;
    }

    public IntType getMaxConnLifetimeMillis() {
        return maxConnLifetimeMillis;
    }

    public void setMaxConnLifetimeMillis(IntType maxConnLifetimeMillis) {
        this.maxConnLifetimeMillis = maxConnLifetimeMillis;
    }

    public BooleanType getLogExpiredConnections() {
        return logExpiredConnections;
    }

    public void setLogExpiredConnections(BooleanType logExpiredConnections) {
        this.logExpiredConnections = logExpiredConnections;
    }

    public StringType getConnectionInitSqls() {
        return connectionInitSqls;
    }

    public void setConnectionInitSqls(StringType connectionInitSqls) {
        this.connectionInitSqls = connectionInitSqls;
    }

    public BooleanType getLifo() {
        return lifo;
    }

    public void setLifo(BooleanType lifo) {
        this.lifo = lifo;
    }

}
