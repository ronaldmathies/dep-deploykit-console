package nl.sodeso.deploykit.console.client.application.security.role;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.ROLE_I18N;

/**
 * @author Ronald Mathies
 */
public class RoleMenuItem extends MenuItem {

    private static final String KEY = "mi-roles";

    private static Roles roles = null;

    public RoleMenuItem() {
        super(KEY, ROLE_I18N.roleMenuItem(), arguments -> {
            if (roles == null) {
                roles = new Roles();
            }

            roles.setArguments(arguments);
            CenterController.instance().setWidget(roles);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }
}
