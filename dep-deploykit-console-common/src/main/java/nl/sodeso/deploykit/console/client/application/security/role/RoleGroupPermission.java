package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.group.GroupMenuItem;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ROLE_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class RoleGroupPermission implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();

    private BooleanType allowToRead = new BooleanType(true);
    private BooleanType allowToUse = new BooleanType(false);
    private BooleanType allowToEdit = new BooleanType(false);
    private BooleanType allowToDeploy = new BooleanType(false);

    private transient EntryWithSingleWidget entry = null;
    private transient GroupComboboxField groupField = null;
    private transient RadioButtonGroupField allowToViewField = null;
    private transient RadioButtonGroupField allowToUseField = null;
    private transient RadioButtonGroupField allowToEditField = null;
    private transient RadioButtonGroupField allowToDeployField = null;

    public RoleGroupPermission() {
    }

    public EntryWithSingleWidget toEntry() {
        if (entry != null) {
            return entry;
        }

        groupField = new GroupComboboxField("group", groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule("group-mandatory", ValidationMessage.Level.ERROR, groupOptionType));

        allowToViewField = new RadioButtonGroupField("allow-to-read#" + Document.get().createUniqueId(), this.allowToRead);
        allowToViewField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToViewField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToViewField.setAlignment(Align.HORIZONTAL);

        allowToUseField = new RadioButtonGroupField("allow-to-use#" + Document.get().createUniqueId(), this.allowToUse);
        allowToUseField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToUseField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToUseField.setAlignment(Align.HORIZONTAL);

        allowToEditField = new RadioButtonGroupField("allow-to-edit#" + Document.get().createUniqueId(), this.allowToEdit);
        allowToEditField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToEditField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToEditField.setAlignment(Align.HORIZONTAL);

        allowToDeployField = new RadioButtonGroupField("allow-to-deploy#" + Document.get().createUniqueId(), this.allowToDeploy);
        allowToDeployField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        allowToDeployField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        allowToDeployField.setAlignment(Align.HORIZONTAL);

        entry = new EntryWithSingleWidget(null,
            new EntryForm("permissions")
                .addEntries(
                    new EntryWithWidgetAndWidget(null, null,
                        new HorizontalPanel()
                            .addWidget(groupField, 100.0, Style.Unit.PCT)
                            .addWidget(new SearchButton.WithIcon((event) -> GroupMenuItem.click(groupOptionType.getValue().getKey())))),
                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToViewField(), allowToViewField),
                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToUseField(), allowToUseField),
                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToEditField(), allowToEditField),
                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToDeployField(), allowToDeployField)
//                    new EntryWithWidgetAndWidget(null, null,
//                            new EntryForm(null)
//                                .addEntries(
//                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToViewField(), allowToViewField),
//                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToUseField(), allowToUseField),
//                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToEditField(), allowToEditField),
//                                    new EntryWithLabelAndWidget(null, ROLE_I18N.allowToDeployField(), allowToDeployField)
//                                )
//                        )
                )
            );


        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public BooleanType getAllowToRead() {
        return allowToRead;
    }

    public void setAllowToRead(BooleanType allowToRead) {
        this.allowToRead = allowToRead;
    }

    public BooleanType getAllowToUse() {
        return allowToUse;
    }

    public void setAllowToUse(BooleanType allowToUse) {
        this.allowToUse = allowToUse;
    }

    public BooleanType getAllowToEdit() {
        return allowToEdit;
    }

    public void setAllowToEdit(BooleanType allowToEdit) {
        this.allowToEdit = allowToEdit;
    }

    public BooleanType getAllowToDeploy() {
        return allowToDeploy;
    }

    public void setAllowToDeploy(BooleanType allowToDeploy) {
        this.allowToDeploy = allowToDeploy;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
