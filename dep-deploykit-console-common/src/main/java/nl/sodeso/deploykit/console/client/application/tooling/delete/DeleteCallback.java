package nl.sodeso.deploykit.console.client.application.tooling.delete;

/**
 * @author Ronald Mathies
 */
public interface DeleteCallback {

    public void onSuccess();

}
