package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc.DatasourceRpcGateway;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.gwt.ui.client.form.*;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;

/**
 * @author Ronald Mathies
 */
public class DatasourceAccessControl implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_SERVER = "server";
    private static final String KEY_DATASOURCE = "datasource";
    private static final String KEY_USER_TABLE = "user-table";
    private static final String KEY_USER_COLUMN = "user-column";
    private static final String KEY_CREDENTIAL_COLUMN = "credential-column";

    private static final String KEY_ROLE_TABLE = "role-table";
    private static final String KEY_ROLE_USER_FIELD = "role-user-field";
    private static final String KEY_ROLE_ROLE_COLUMN = "role-role-column";

    private OptionType<DefaultOption> datasourceOptionType = new OptionType<>();
    private VersionOptionType datasourceVersionType = new VersionOptionType();

    private StringType userTable = new StringType();
    private StringType userColumn = new StringType();
    private StringType credentialColumn = new StringType();
    private StringType roleTable = new StringType();
    private StringType roleUserColumn = new StringType();
    private StringType roleRoleColumn = new StringType();

    private transient AccessControl accessControl;

    private transient EntryForm entryForm = null;

    private transient LegendPanel serverLegendPanel = null;
    private transient VersionSelectionField<DatasourceRpcGateway> datasourceSelectionField = null;

    private transient EntryForm datasourceSettingsForm = null;
    private transient TextField userTableTextField = null;
    private transient TextField userColumnTextField = null;
    private transient TextField credentialColumnTextField = null;
    private transient TextField roleTableTextField = null;
    private transient TextField roleUserColumnTextField = null;
    private transient TextField roleRoleColumnTextField = null;

    public DatasourceAccessControl() {}

    public Widget toEditForm(AccessControl accessControl) {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        this.accessControl = accessControl;

        setupServerSettings();
        setupDatasourceSettings();

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntry(new EntryWithContainer(null, this.serverLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.datasourceSettingsForm));

        return this.entryForm;
    }

    private void setupServerSettings() {
        EntryForm form = new EntryForm(null);

        form.addEntry(new EntryWithDocumentation(KEY_SERVER, ACCESS_CONTROL_I18N.datasourceDocumentation()));
        this.datasourceSelectionField = new VersionSelectionField<>(GWT.create(DatasourceRpcGateway.class), datasourceOptionType, datasourceVersionType);
        this.datasourceSelectionField.listenToGroup(accessControl.getGroup());
        HorizontalPanel horizontalPanel = new HorizontalPanel()
            .addWidget(datasourceSelectionField, 100.0, Style.Unit.PCT)
            .addWidget(new SearchButton.WithIcon((event) -> DatasourceMenuItem.click(datasourceOptionType.getValue().getKey(), datasourceVersionType.getValue())));
        EntryWithWidgetAndWidget entry = new EntryWithLabelAndWidget(KEY_DATASOURCE, ACCESS_CONTROL_I18N.datasourceField(), horizontalPanel);
        entry.addValidationRule(
            new ValidationCriteria.or(
                    new ComboboxMandatoryValidationRule(KEY_DATASOURCE, ValidationMessage.Level.ERROR, datasourceOptionType),
                    new ComboboxMandatoryValidationRule(KEY_DATASOURCE, ValidationMessage.Level.ERROR, datasourceVersionType)
            )
        );
        form.addEntry(entry);

        this.serverLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.datasourcePanel());
        this.serverLegendPanel.add(form);
    }

    private void setupDatasourceSettings() {
        datasourceSettingsForm = new EntryForm(null);

        {
            this.userTableTextField = new TextField<>(KEY_USER_TABLE, this.userTable);
            this.userTableTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_TABLE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userTableTextField.getValue();
                }
            });

            this.userColumnTextField = new TextField<>(KEY_USER_COLUMN, this.userColumn);
            this.userColumnTextField.addValidationRule(new MandatoryValidationRule(KEY_USER_COLUMN, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return userColumnTextField.getValue();
                }
            });

            this.credentialColumnTextField = new TextField<>(KEY_CREDENTIAL_COLUMN, this.credentialColumn);
            this.credentialColumnTextField.addValidationRule(new MandatoryValidationRule(KEY_CREDENTIAL_COLUMN, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return credentialColumnTextField.getValue();
                }
            });


            EntryForm userForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_USER_TABLE, ACCESS_CONTROL_I18N.userTableDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USER_TABLE, ACCESS_CONTROL_I18N.userTableField(), new VerticalPanel(userTableTextField,
                        new PlaceholderSpanField(KEY_USER_TABLE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_USER_TABLE)))
                        .setHint(ACCESS_CONTROL_I18N.userTableFieldHint())
                        .addValidatableWidget(userTableTextField),
                    new EntryWithLabelAndWidget(KEY_USER_COLUMN, ACCESS_CONTROL_I18N.userColumnField(), new VerticalPanel(userColumnTextField,
                        new PlaceholderSpanField(KEY_USER_COLUMN, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_USER_USER_COLUMN)))
                        .setHint(ACCESS_CONTROL_I18N.userColumnFieldHint())
                        .addValidatableWidget(userColumnTextField),
                    new EntryWithLabelAndWidget(KEY_CREDENTIAL_COLUMN, ACCESS_CONTROL_I18N.credentialColumnField(), new VerticalPanel(credentialColumnTextField,
                        new PlaceholderSpanField(KEY_CREDENTIAL_COLUMN, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_USER_CREDENTIAL_COLUMN)))
                        .setHint(ACCESS_CONTROL_I18N.credentialColumnFieldHint())
                        .addValidatableWidget(credentialColumnTextField));

            LegendPanel userLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.userConfigurationPanel());
            userLegendPanel.add(userForm);
            datasourceSettingsForm.addEntry(new EntryWithContainer(null, userLegendPanel));
        }

        {
            this.roleTableTextField = new TextField<>(KEY_ROLE_TABLE, this.roleTable);
            this.roleTableTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_TABLE, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleTableTextField.getValue();
                }
            });

            this.roleUserColumnTextField = new TextField<>(KEY_ROLE_USER_FIELD, this.roleUserColumn);
            this.roleUserColumnTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_USER_FIELD, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleUserColumnTextField.getValue();
                }
            });

            this.roleRoleColumnTextField = new TextField<>(KEY_ROLE_ROLE_COLUMN, this.roleRoleColumn);
            this.roleRoleColumnTextField.addValidationRule(new MandatoryValidationRule(KEY_ROLE_ROLE_COLUMN, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return roleRoleColumnTextField.getValue();
                }
            });

            EntryForm roleForm = new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(KEY_ROLE_TABLE, ACCESS_CONTROL_I18N.roleTableDocumentation()),
                    new EntryWithLabelAndWidget(KEY_ROLE_TABLE, ACCESS_CONTROL_I18N.roleTableField(), new VerticalPanel(roleTableTextField,
                        new PlaceholderSpanField(KEY_ROLE_TABLE, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_ROLE_TABLE)))
                        .setHint(ACCESS_CONTROL_I18N.roleTableFieldHint())
                        .addValidatableWidget(roleTableTextField),
                    new EntryWithLabelAndWidget(KEY_ROLE_USER_FIELD, ACCESS_CONTROL_I18N.roleUserColumnField(), new VerticalPanel(roleUserColumnTextField,
                        new PlaceholderSpanField(KEY_ROLE_USER_FIELD, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_ROLE_USER_COLUMN)))
                        .setHint(ACCESS_CONTROL_I18N.roleUserColumnFieldHint())
                        .addValidatableWidget(roleUserColumnTextField),
                    new EntryWithLabelAndWidget(KEY_ROLE_ROLE_COLUMN, ACCESS_CONTROL_I18N.roleColumnField(), new VerticalPanel(roleRoleColumnTextField,
                        new PlaceholderSpanField(KEY_ROLE_ROLE_COLUMN, accessControl.getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_DS_ROLE_ROLE_COLUMN)))
                        .setHint(ACCESS_CONTROL_I18N.roleColumnFieldHint())
                        .addValidatableWidget(roleRoleColumnTextField));

            LegendPanel roleLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.roleConfigurationPanel());
            roleLegendPanel.add(roleForm);
            datasourceSettingsForm.addEntry(new EntryWithContainer(null, roleLegendPanel));
        }
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getDatasource() {
        return datasourceOptionType;
    }

    public void setDatasource(OptionType<DefaultOption> datasourceOptionType) {
        this.datasourceOptionType = datasourceOptionType;
    }

    public VersionOptionType getDatasourceVersion() {
        return datasourceVersionType;
    }

    public void setDatasourceVersion(VersionOptionType datasourceVersionType) {
        this.datasourceVersionType = datasourceVersionType;
    }

    public StringType getUserTable() {
        return userTable;
    }

    public void setUserTable(StringType userTable) {
        this.userTable = userTable;
    }

    public StringType getUserColumn() {
        return userColumn;
    }

    public void setUserColumn(StringType userColumn) {
        this.userColumn = userColumn;
    }

    public StringType getCredentialColumn() {
        return credentialColumn;
    }

    public void setCredentialColumn(StringType credentialColumn) {
        this.credentialColumn = credentialColumn;
    }

    public StringType getRoleTable() {
        return roleTable;
    }

    public void setRoleTable(StringType roleTable) {
        this.roleTable = roleTable;
    }

    public StringType getRoleUserColumn() {
        return roleUserColumn;
    }

    public void setRoleUserColumn(StringType roleUserColumn) {
        this.roleUserColumn = roleUserColumn;
    }

    public StringType getRoleRoleColumn() {
        return roleRoleColumn;
    }

    public void setRoleRoleColumn(StringType roleRoleColumn) {
        this.roleRoleColumn = roleRoleColumn;
    }

}
