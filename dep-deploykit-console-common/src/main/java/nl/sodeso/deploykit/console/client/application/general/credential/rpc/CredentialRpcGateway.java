package nl.sodeso.deploykit.console.client.application.general.credential.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class CredentialRpcGateway implements RpcGateway<CredentialRpcAsync>, CredentialRpcAsync {

    /**
     * {@inheritDoc}
     */
    public CredentialRpcAsync getRpcAsync() {
        return GWT.create(CredentialRpc.class);
    }
}
