package nl.sodeso.deploykit.console.client.application.guide.steps;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;

/**
 * @author Ronald Mathies
 */
public class SetupGroupsStep extends AbstractWizardStep {

    private SetupCredentialsStep setupCredentialsStep;

    public SetupGroupsStep(AbstractWizardStep callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep getNextStep() {
        if (setupCredentialsStep == null) {
            setupCredentialsStep = new SetupCredentialsStep(this);
        }

        return setupCredentialsStep;
    }

    @Override
    public Widget getBodyWidget() {
        LegendPanel legendPanel = new LegendPanel("setup-groups-step", "Setup Groups");
        EntryForm entryForm = new EntryForm("form");

        entryForm.addEntry(new EntryWithDocumentation(null,
                "First we need to define a group which will be used to associate the various<br/>" +
                "components in a profile together. Groups can be defined in various ways.<br/>" +
                "<ul>" +
                "   <li>A group can be used to define an environment like development, test,<br/>" +
                        "acceptance or production.</li>" +
                "   <li>A group can be used to define teams, in this way certain resources are <br/>" +
                        "for example only available to a development team.</li>" +
                "   <li>A group can finally be used to your own liking, it is however advisably to <br/>" +
                        "make a coherent setup so it is clear to all parties how the setup is done.</li>" +
                "</ul>"));

        legendPanel.add(entryForm);

//        legendPanel.addAttachHandler(event -> GroupMenuItem.click(null));

        return legendPanel;
    }
}
