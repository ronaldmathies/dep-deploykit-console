package nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionpoolSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-pool")
public interface ConnectionPoolRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, DeleteRpc, LabelUniqueRpc, UsedByRpc {

    ArrayList<ConnectionpoolSummaryItem> findSummaries();
    ConnectionPool findSingle(String uuid, VersionOption version);

    ConnectionpoolSummaryItem save(ConnectionPool connectionPool) throws RemoteException;
    ConnectionpoolSummaryItem duplicate(String uuid, VersionOption version);

}
