package nl.sodeso.deploykit.console.client.application.ui.options;

import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class VersionSelectionField<X extends OptionRpcAsync & VersioningRpcAsync> extends HorizontalPanel {

    private OptionType<DefaultOption> groupOptionType = null;
    private OptionType<DefaultOption> optionType = null;

    private ComboboxField<DefaultOption> optionField = null;
    private ComboboxField<VersionOption> versionField = null;

    private X gateway = null;

    public VersionSelectionField(X gateway, final OptionType<DefaultOption> optionType, final VersionOptionType versionOptionType) {
        this.gateway = gateway;
        this.optionType = optionType;

        optionField = new ComboboxField<>(optionType);
        optionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                refreshVersionsList();
            }
        });

        versionField = new ComboboxField<>(versionOptionType);
        addWidgets(optionField, versionField);

        addAttachHandler(event -> {
            if (event.isAttached()) {
                refreshOptionList();
            }
        });
    }

    /**
     * To which <code>group</code> the datasource selection should listen.
     * @param groupOptionType the group option type.
     */
    public void listenToGroup(final OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
        this.groupOptionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                refreshOptionList();
            }
        });
    }

    /**
     * Refreshes the datasource list based on the selected group.
     */
    public void refreshOptionList() {
        if (this.groupOptionType != null && this.groupOptionType.getValue() != null) {
            gateway.asOptions(this.groupOptionType.getValue(), new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
                @Override
                public void success(ArrayList<DefaultOption> result) {
                    optionField.replaceItemsRetainSelection(result);

                    refreshVersionsList();
                }
            });
        } else {
            optionField.removeAllItems();
        }
    }

    /**
     * Refreshes the version list based on the selected group.
     */
    public void refreshVersionsList() {
        if (optionType != null && optionType.getValue() != null) {
            gateway.versions(optionType.getValue().getKey(), new DefaultAsyncCallback<List<VersionOption>>() {
                @Override
                public void success(List<VersionOption> result) {
                    versionField.replaceItemsRetainSelection(result);
                }
            });
        } else {
            versionField.removeAllItems();
        }
    }
}
