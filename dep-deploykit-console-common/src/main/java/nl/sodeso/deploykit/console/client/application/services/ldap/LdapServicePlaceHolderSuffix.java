package nl.sodeso.deploykit.console.client.application.services.ldap;

/**
 * @author Ronald Mathies
 */
public class LdapServicePlaceHolderSuffix {

    public static final String LDAP_DOMAIN = "-ldap-domain";
    public static final String LDAP_PORT = "-ldap-port";
    public static final String LDAP_USERNAME = "-ldap-username";
    public static final String LDAP_PASSWORD = "-ldap-password";

    public static String LDAP_DOMAIN(String prefix) {
        return prefix + LDAP_DOMAIN;
    }

    public static String LDAP_PORT(String prefix) {
        return prefix + LDAP_PORT;
    }

    public static String LDAP_USERNAME(String prefix) {
        return prefix + LDAP_USERNAME;
    }

    public static String LDAP_PASSWORD(String prefix) {
        return prefix + LDAP_PASSWORD;
    }
}
