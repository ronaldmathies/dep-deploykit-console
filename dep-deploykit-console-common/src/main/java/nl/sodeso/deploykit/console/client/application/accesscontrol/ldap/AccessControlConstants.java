package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface AccessControlConstants extends Messages {

    @DefaultMessage("Access Control")
    String accessControlMenuItem();

    @DefaultMessage(
            "Access Controls allow you to define JAAS (Java Access and Authorization Service) services, these services" +
            "are maintained by the JEE container and can be used by the various components running within a container." +
            "When defining a new configuration a choice has to be made as to what the source is used from where the users, credentials " +
            "and roles are to be retrieved. This can be either an LDAP service or a database.")
    String generalDocumentation();

    @DefaultMessage("Access Control")
    String accessControlPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Placeholder Prefix")
    String placeHolderPrefixField();

    @DefaultMessage("Realm Name")
    String realmNameField();

    @DefaultMessage("The realm name is used for associating this security configuration with the web-application. The realm name defined here should be the same as the &lt;realm-name&gt; within the &lt;login-config&gt; in the web.xml deployment descriptor.")
    String realmNameDocumentation();

    @DefaultMessage("Access Control Type")
    String accessControlTypeField();

    @DefaultMessage("LDAP Service")
    String ldapServicePanel();

    @DefaultMessage("Please specify the LDAP service to use for the authentication / authorization of the users.")
    String ldapServiceDocumentation();

    @DefaultMessage("LDAP Service")
    String ldapServiceField();

    @DefaultMessage("User Configuration")
    String userConfigurationPanel();

    @DefaultMessage("The base distinguished name from where the users are to be searched from.")
    String userBaseDnDocumentation();

    @DefaultMessage("User Base DN")
    String userBaseDnField();

    @DefaultMessage("ie. ou=users,dc=organization,dc=com")
    String userBaseDnFieldHint();

    @DefaultMessage("The attribute that specifies the user / principal.")
    String userAttributeDocumentation();

    @DefaultMessage("User Attribute")
    String userAttributeField();

    @DefaultMessage("ie. uid")
    String userAttributeFieldHint();

    @DefaultMessage("The attribute name of the user, although this field is mandatory it is only used by the Jetty container.")
    String passwordAttributeDocumentation();

    @DefaultMessage("Password Attribute")
    String passwordAttributeField();

    @DefaultMessage("ie. userPassword")
    String passwordAttributeFieldHint();

    @DefaultMessage("Object class of a user, although this field is mandatory it is only used by the Jetty container.")
    String userObjectClassDocumentation();

    @DefaultMessage("User Object Class")
    String userObjectClassField();

    @DefaultMessage("ie. person")
    String userObjectClassFieldHint();

    @DefaultMessage("Role Configuration")
    String roleConfigurationPanel();

    @DefaultMessage("The base distinguished name from where the roles are to be searched from.")
    String roleBaseDnDocumentation();

    @DefaultMessage("Role Base DN")
    String roleBaseDnField();

    @DefaultMessage("ie. ou=roles,dc=organization,dc=com")
    String roleBaseDnFieldHint();

    @DefaultMessage("Type of the attribute in the role object that identifies member user ids. This is used to locate the users roles")
    String roleMemberAttributeDocumentation();

    @DefaultMessage("Role Member Attribute")
    String roleMemberAttributeField();

    @DefaultMessage("ie. member")
    String roleMemberAttributeFieldHint();

    @DefaultMessage("The name of the attribute that a role would be stored under.")
    String roleNameAttributeDocumentation();

    @DefaultMessage("Role Name Attribute")
    String roleNameAttributeField();

    @DefaultMessage("ie. cn")
    String roleNameAttributeFieldHint();

    @DefaultMessage("Object class of a role, although the field is mandatory it is only used by the Jetty container.")
    String roleObjectClassDocumentation();

    @DefaultMessage("Role Object Class")
    String roleObjectClassField();

    @DefaultMessage("ie. groupOfNames")
    String roleObjectClassFieldHint();

    @DefaultMessage("Datasource")
    String datasourcePanel();

    @DefaultMessage("Please specify the data source to use for the authentication / authorization of the users.")
    String datasourceDocumentation();

    @DefaultMessage("Datasource")
    String datasourceField();

    @DefaultMessage("The user table contains the users that are allowed to login. The query being performed is &quot;select &lt;Credential Column&gt; from &lt;User Table&gt; where &lt;User Column&gt; =?&quot;")
    String userTableDocumentation();

    @DefaultMessage("User Table")
    String userTableField();

    @DefaultMessage("ie. t_user")
    String userTableFieldHint();

    @DefaultMessage("User Column")
    String userColumnField();

    @DefaultMessage("ie. username")
    String userColumnFieldHint();

    @DefaultMessage("Credential Column")
    String credentialColumnField();

    @DefaultMessage("ie. userpassword")
    String credentialColumnFieldHint();

    @DefaultMessage("The role table contains the roles associated with the users. The query being performed is &quot;select &lt;Role Column&gt; from &lt;Role Table&gt; where &lt;User Column&gt; =?&quot;")
    String roleTableDocumentation();

    @DefaultMessage("Role Table")
    String roleTableField();

    @DefaultMessage("ie. t_roles")
    String roleTableFieldHint();

    @DefaultMessage("User Column")
    String roleUserColumnField();

    @DefaultMessage("ie. username")
    String roleUserColumnFieldHint();

    @DefaultMessage("Role Column")
    String roleColumnField();

    @DefaultMessage("ie. role")
    String roleColumnFieldHint();

    @DefaultMessage("Access Control {0} Used By")
    String accessControlUsedByTitle(String name);

    @DefaultMessage("Access Control Check")
    String accessControlCheckTitle();

    @DefaultMessage("Checking if access control configuration is correct, this process may take a while so please do not close the browser.")
    String accessControlCheckDocumentation();

    @DefaultMessage("Access Control Check Result")
    String accessControlCheckResultTitle();

    @DefaultMessage("Check Access Control")
    String accessControlCheckQuestionTitle();

    @DefaultMessage("Please enter a username that can be used to check the entered information, this user should have at least one role.")
    String accessControlCheckQuestionDocumentation();

    @DefaultMessage("Username")
    String usernameField();
}