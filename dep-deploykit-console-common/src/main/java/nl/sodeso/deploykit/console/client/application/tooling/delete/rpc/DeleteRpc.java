package nl.sodeso.deploykit.console.client.application.tooling.delete.rpc;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

/**
 * @author Ronald Mathies
 */
public interface DeleteRpc {

    /**
     * Check to see if it is allowed to delete an object based on the given <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return validation results.
     */
    ValidationResult canBeDeletedSafely(String uuid, VersionOption version);

    /**
     * Deletes the object based on the given <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     */
    void delete(String uuid, VersionOption version);

}
