package nl.sodeso.deploykit.console.client.application.profile.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.profile.Profile;
import nl.sodeso.deploykit.console.client.application.profile.ProfileSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-profile")
public interface ProfileRpc
        extends XsrfProtectedService, VersioningRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<ProfileSummaryItem> findSummaries();

    Profile findSingle(String uuid, VersionOption version);

    ProfileSummaryItem save(Profile profile) throws RemoteException;
    CheckResult check(String uuid, VersionOption version);

    ProfileSummaryItem duplicate(String uuid, VersionOption version);

}
