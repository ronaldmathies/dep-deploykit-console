package nl.sodeso.deploykit.console.client.application.search;

/**
 * @author Ronald Mathies
 */
public enum Type {
    HTTP,
    HTTPS,
    WEBSERVICE,
    JBOSSCONNECTIONPOOL,
    JETTYCONNECTIONPOOL,
    DATASOURCE,
    LDAP_SERVICE,
    LDAP_ACCESS_CONTROL,
    DATASOURCE_ACCESS_CONTROL,
    PROFILE
}
