package nl.sodeso.deploykit.console.client.application.ui.validation.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

/**
 * @author Ronald Mathies
 */
public interface LabelUniqueRpcAsync {

    void isLabelUnique(String uuid, String label, AsyncCallback<ValidationResult> result);


}
