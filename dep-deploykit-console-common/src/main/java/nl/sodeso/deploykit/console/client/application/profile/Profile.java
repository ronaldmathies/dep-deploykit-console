package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.profile.rpc.ProfileRpcGateway;
import nl.sodeso.deploykit.console.client.application.profile.service.ldap.ProfileLdapServices;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;
import static nl.sodeso.deploykit.console.client.application.Resources.PROFILE_I18N;

/**
 * @author Ronald Mathies
 */
public class Profile extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private transient static final String KEY_GROUP = "group";
    private transient static final String KEY_LABEL = "label";
    private transient static final String KEY_DETAILS = "details";
    private transient static final String KEY_DATABASES = "databases";
    private transient static final String KEY_WEBSERVICES = "webservices";
    private transient static final String KEY_LDAP_SERVICES = "ldap-services";
    private transient static final String KEY_CREDENTIALS = "credentials";
    private transient static final String KEY_ACCESS_CONTROLS = "access-controls";
    private transient static final String KEY_HTTPS = "https";
    private transient static final String KEY_HTTP = "http";
    private transient static final String KEY_NODES = "nodes";
    private transient static final String KEY_PROPERTIES = "properties";
    private transient static final String KEY_PROFILE = "profile";

    private StringType label = new StringType();
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();

    private ProfileDatasources profileDatasources = new ProfileDatasources(groupOptionType);
    private ProfileWebservices profileWebservices = new ProfileWebservices(groupOptionType);
    private ProfileLdapServices profileLdapServices = new ProfileLdapServices(groupOptionType);
    private ProfileCredentials profileCredentials = new ProfileCredentials(groupOptionType);
    private ProfileAccessControl profileAccessControls = new ProfileAccessControl(groupOptionType);
    private ProfileHttps profileHttps = new ProfileHttps();
    private ProfileHttp profileHttp = new ProfileHttp();
    private ProfileNodes profileNodes = new ProfileNodes();
    private KeyValueProperties properties = new KeyValueProperties();

    private transient EntryForm profileForm = null;
    private transient TextField labelTextField = null;
    private transient GroupComboboxField groupField = null;
    private transient LegendPanel profileDatabasesLegendPanel = null;
    private transient LegendPanel profileWebservicesLegendPanel = null;
    private transient LegendPanel profileLdapServicesLegendPanel = null;
    private transient LegendPanel profileCredentialsLegendPanel = null;
    private transient LegendPanel profileLdapAccessControlsLegendPanel = null;
    private transient LegendPanel profileHttpsLegendPanel = null;
    private transient LegendPanel profileHttpLegendPanel = null;
    private transient LegendPanel profileNodesLegendPanel = null;
    private transient LegendPanel profilePropertiesLegendPanel = null;

    public Profile() {}

    public Widget toEditForm() {
        if (this.profileForm != null) {
            return this.profileForm;
        }

        this.groupField = new GroupComboboxField(KEY_GROUP, groupOptionType)
                .addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        this.groupField.setEnabled(getUuid() == null);

        // Profile Details
        this.labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        EntryForm profileDetailsForm = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, ACCESS_CONTROL_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, PROFILE_I18N.label(), labelTextField)
                    .addValidationRules(
                        new LabelUniqueValidationRule(GWT.create(HttpsRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return Profile.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return Profile.this.getLabel();
                            }
                        }
                    ));
        LegendPanel profileLegendPanel = new LegendPanel(KEY_DETAILS, PROFILE_I18N.profilePanel());
        profileLegendPanel.add(profileDetailsForm);

        profileDatabasesLegendPanel = new LegendPanel(KEY_DATABASES, PROFILE_I18N.datasourcesPanel(profileDatasources.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        profileDatabasesLegendPanel.add(profileDatasources.toEditForm());
        profileDatasources
            .addWidgetAddedEventHandler(event -> profileDatabasesLegendPanel.setTitle(PROFILE_I18N.datasourcesPanel(profileDatasources.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> profileDatabasesLegendPanel.setTitle(PROFILE_I18N.datasourcesPanel(profileDatasources.getWidgets().size())));

        profileWebservicesLegendPanel = new LegendPanel(KEY_WEBSERVICES, PROFILE_I18N.webservicesPanel(profileWebservices.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        profileWebservicesLegendPanel.add(profileWebservices.toEditForm());
        profileWebservices
            .addWidgetAddedEventHandler(event -> profileWebservicesLegendPanel.setTitle(PROFILE_I18N.webservicesPanel(profileWebservices.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> profileWebservicesLegendPanel.setTitle(PROFILE_I18N.webservicesPanel(profileWebservices.getWidgets().size())));

        profileLdapServicesLegendPanel = new LegendPanel(KEY_LDAP_SERVICES, PROFILE_I18N.ldapServicesPanel(profileLdapServices.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        profileLdapServicesLegendPanel.add(profileLdapServices.toEditForm());
        profileLdapServices
            .addWidgetAddedEventHandler(event -> profileLdapServicesLegendPanel.setTitle(PROFILE_I18N.ldapServicesPanel(profileLdapServices.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> profileLdapServicesLegendPanel.setTitle(PROFILE_I18N.ldapServicesPanel(profileLdapServices.getWidgets().size())));

        profileCredentialsLegendPanel = new LegendPanel(KEY_CREDENTIALS, PROFILE_I18N.credentialsPanel(profileCredentials.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        profileCredentialsLegendPanel.add(profileCredentials.toEditForm());
        profileCredentials
            .addWidgetAddedEventHandler(event -> profileCredentialsLegendPanel.setTitle(PROFILE_I18N.credentialsPanel(profileCredentials.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> profileCredentialsLegendPanel.setTitle(PROFILE_I18N.credentialsPanel(profileCredentials.getWidgets().size())));

        profileLdapAccessControlsLegendPanel = new LegendPanel(KEY_ACCESS_CONTROLS,PROFILE_I18N.accessControlPanel()).showCollapseButton(true).setCollapsed(true);
        profileLdapAccessControlsLegendPanel.add(profileAccessControls.toEditForm());

        profileHttpsLegendPanel = new LegendPanel(KEY_HTTPS, PROFILE_I18N.sslHttpsPanel()).showCollapseButton(true).setCollapsed(true);
        profileHttpsLegendPanel.add(profileHttps.toEditForm());

        profileHttpLegendPanel = new LegendPanel(KEY_HTTP, PROFILE_I18N.httpPanel()).showCollapseButton(true).setCollapsed(true);
        profileHttpLegendPanel.add(profileHttp.toEditForm());

        profileNodesLegendPanel = new LegendPanel(KEY_NODES, PROFILE_I18N.nodesPanel(profileNodes.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        profileNodesLegendPanel.add(profileNodes.toEditForm());
        profileNodes.addWidgetAddedEventHandler(event -> profileNodesLegendPanel.setTitle(PROFILE_I18N.nodesPanel(profileNodes.getWidgets().size())));
        profileNodes.addWidgetRemovedEventHandler(event -> profileNodesLegendPanel.setTitle(PROFILE_I18N.nodesPanel(profileNodes.getWidgets().size())));

        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                    new EntryWithDocumentation(KEY_PROPERTIES, PROFILE_I18N.propertiesDocumentation(), Align.LEFT),
                    new EntryWithContainer(null, properties.toEditForm()));
        profilePropertiesLegendPanel = new LegendPanel(KEY_PROPERTIES, PROFILE_I18N.propertiesPanel(properties.getWidgets().size())).showCollapseButton(true).setCollapsed(true);
        properties
            .addWidgetAddedEventHandler(event -> profilePropertiesLegendPanel.setTitle(PROFILE_I18N.propertiesPanel(properties.getWidgets().size())))
            .addWidgetRemovedEventHandler(event -> profilePropertiesLegendPanel.setTitle(PROFILE_I18N.propertiesPanel(properties.getWidgets().size())));
        profilePropertiesLegendPanel.add(entryForm);

        this.profileForm = new EntryForm(KEY_PROFILE)
                .addEntries(
                        new EntryWithContainer(null, profileLegendPanel),
                        new EntryWithContainer(null, profileDatabasesLegendPanel),
                        new EntryWithContainer(null, profileWebservicesLegendPanel),
                        new EntryWithContainer(null, profileLdapServicesLegendPanel),
                        new EntryWithContainer(null, profileCredentialsLegendPanel),
                        new EntryWithContainer(null, profileLdapAccessControlsLegendPanel),
                        new EntryWithContainer(null, profileHttpsLegendPanel),
                        new EntryWithContainer(null, profileHttpLegendPanel),
                        new EntryWithContainer(null, profileNodesLegendPanel),
                        new EntryWithContainer(null, profilePropertiesLegendPanel));


        this.labelTextField.setFocus();

        return this.profileForm;
    }

    public class ProfileUIState {
        public boolean[] legendPanelStates;
    }

    public ProfileUIState saveState() {
        ProfileUIState state = new ProfileUIState();
        state.legendPanelStates = new boolean[] {
            profileDatabasesLegendPanel.isCollapsed(),
            profileWebservicesLegendPanel.isCollapsed(),
            profileLdapServicesLegendPanel.isCollapsed(),
            profileCredentialsLegendPanel.isCollapsed(),
            profileLdapAccessControlsLegendPanel.isCollapsed(),
            profileHttpsLegendPanel.isCollapsed(),
            profileHttpLegendPanel.isCollapsed(),
            profileNodesLegendPanel.isCollapsed(),
            profilePropertiesLegendPanel.isCollapsed()
        };
        return state;
    }

    public void restoreState(ProfileUIState state) {
        profileDatabasesLegendPanel.setCollapsed(state.legendPanelStates[0]);
        profileWebservicesLegendPanel.setCollapsed(state.legendPanelStates[1]);
        profileLdapServicesLegendPanel.setCollapsed(state.legendPanelStates[2]);
        profileCredentialsLegendPanel.setCollapsed(state.legendPanelStates[3]);
        profileLdapAccessControlsLegendPanel.setCollapsed(state.legendPanelStates[4]);
        profileHttpsLegendPanel.setCollapsed(state.legendPanelStates[5]);
        profileHttpLegendPanel.setCollapsed(state.legendPanelStates[6]);
        profileNodesLegendPanel.setCollapsed(state.legendPanelStates[7]);
        profilePropertiesLegendPanel.setCollapsed(state.legendPanelStates[8]);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.profileForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.profileForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.profileForm);
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public ProfileDatasources getProfileDatasources() {
        return this.profileDatasources;
    }

    public ProfileWebservices getProfileWebservices() {
        return this.profileWebservices;
    }

    public ProfileLdapServices getProfileLdapServices() {
        return this.profileLdapServices;
    }

    public ProfileCredentials getProfileCredentials() {
        return this.profileCredentials;
    }

    public ProfileNodes getProfileNodes() {
        return this.profileNodes;
    }

    public ProfileHttps getProfileHttps() {
        return this.profileHttps;
    }

    public void setProfileHttps(ProfileHttps profileHttps) {
        this.profileHttps = profileHttps;
    }

    public ProfileHttp getProfileHttp() {
        return this.profileHttp;
    }

    public void setProfileHttp(ProfileHttp profileHttp) {
        this.profileHttp = profileHttp;
    }

    public ProfileAccessControl getProfileAccessControl() {
        return this.profileAccessControls;
    }

    public void setProfileAccessControl(ProfileAccessControl profileAccessControls) {
        this.profileAccessControls = profileAccessControls;
    }

    public KeyValueProperties getProperties() {
        return this.properties;
    }

}
