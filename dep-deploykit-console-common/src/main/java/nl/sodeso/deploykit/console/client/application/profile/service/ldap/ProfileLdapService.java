package nl.sodeso.deploykit.console.client.application.profile.service.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceMenuItem;
import nl.sodeso.deploykit.console.client.application.services.ldap.rpc.LdapServiceRpcGateway;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ProfileLdapService implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> ldapServiceOptionType = new OptionType<>();
    private VersionOptionType ldapServiceVersionType = new VersionOptionType();

    private transient EntryWithWidgetAndWidget entry = null;
//    private transient GroupComboboxField groupField = null;
    private transient VersionSelectionField<LdapServiceRpcGateway> ldapServiceField = null;

    public ProfileLdapService() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (entry != null) {
            return entry;
        }

//        groupField = new GroupComboboxField("group", groupOptionType);
//        groupField.addValidationRule(new ComboboxMandatoryValidationRule("group", ValidationMessage.Level.ERROR, groupOptionType));

        ldapServiceField = new VersionSelectionField<>(GWT.create(LdapServiceRpcGateway.class), ldapServiceOptionType, ldapServiceVersionType);
        ldapServiceField.listenToGroup(groupOptionType);

        entry = new EntryWithWidgetAndWidget("ldap-service", null, // groupField
            new HorizontalPanel()
                .addWidget(ldapServiceField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> LdapServiceMenuItem.click(ldapServiceOptionType.getValue().getKey(), ldapServiceVersionType.getValue()))));
        entry.addValidationRule(new ComboboxMandatoryValidationRule("ldap-service", ValidationMessage.Level.ERROR, ldapServiceOptionType));

//        entry.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                ldapServiceField.refresh(groupOptionType);
//            }
//        });

        return entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getLdapService() {
        return this.ldapServiceOptionType;
    }

    public void setLdapService(OptionType<DefaultOption> node) {
        this.ldapServiceOptionType = node;
    }

    public OptionType<DefaultOption> getLdapServiceOption() {
        return ldapServiceOptionType;
    }

    public void setLdapServiceOption(OptionType<DefaultOption> ldapServiceOptionType) {
        this.ldapServiceOptionType = ldapServiceOptionType;
    }

    public VersionOptionType getLdapServiceVersion() {
        return ldapServiceVersionType;
    }

    public void setLdapServiceVersion(VersionOptionType ldapServiceVersionType) {
        this.ldapServiceVersionType = ldapServiceVersionType;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
