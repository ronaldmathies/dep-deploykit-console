package nl.sodeso.deploykit.console.client.application.group;

import com.google.gwt.i18n.client.Messages;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface GroupConstants extends Messages {

    @DefaultMessage("Groups")
    String groupMenuItem();

    @DefaultMessage("Group")
    String groupPanel();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Group {0} Used By")
    String groupUsedByTitle(String name);

    @DefaultMessage("Add roles to the group to define which groups of users are allowed to perform what kind of action using the components defined with the groups.")
    String rolesDocumentation();

    @DefaultMessage("Roles")
    String rolesPanel();

    @DefaultMessage("Roles & Permissions")
    String roleField();

}