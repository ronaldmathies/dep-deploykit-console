package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.dialog.SaveChangesHandler;

/**
 * @author Ronald Mathies
 */
public class CreateBranchQuestionPanel extends PopupWindowPanel {

    private static final String KEY_BRANCH_CONFIRMATION = "branch-confirmation";

    private SaveChangesHandler handler = null;

    public CreateBranchQuestionPanel(final SaveChangesHandler handler) {
        super(KEY_BRANCH_CONFIRMATION, "Branch", WindowPanel.Style.INFO);

        this.handler = handler;

        addToBody(
            new EntryForm(null)
                .addEntry(new EntryWithDocumentation(null, "This will create a new branch of the selected version, are you sure you want to continue?")));

        addToFooter(Align.RIGHT,
                new YesButton.WithLabel((event) -> yes()),
                new CancelButton.WithLabel((event) -> close()));
    }

    private void yes() {
        close();
        handler.confirmed();
    }

}
