package nl.sodeso.deploykit.console.client.application.node.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class NodeRpcGateway implements RpcGateway<NodeRpcAsync>, NodeRpcAsync {

    /**
     * {@inheritDoc}
     */
    public NodeRpcAsync getRpcAsync() {
        return GWT.create(NodeRpc.class);
    }
}
