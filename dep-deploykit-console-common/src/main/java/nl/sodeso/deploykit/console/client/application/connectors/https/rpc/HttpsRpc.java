package nl.sodeso.deploykit.console.client.application.connectors.https.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.connectors.https.Https;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-https")
public interface HttpsRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, UsedByRpc, DeleteRpc, LabelUniqueRpc {

    ArrayList<HttpsSummaryItem> findSummaries();

    Https findSingle(String uuid, VersionOption version);

    HttpsSummaryItem save(Https https) throws RemoteException;
    HttpsSummaryItem duplicate(String uuid, VersionOption version);

}
