package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface JdbcDriverConstants extends Messages {

    @DefaultMessage("JDBC Drivers")
    String jdbcDriverMenuItem();

    @DefaultMessage("JDBC Driver")
    String jdbcDriverPanel();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Java Archive Files")
    String javaArchiveFilesPanel();

    @DefaultMessage("Add all Java Archive Files that are necessary for creating a JDBC connection with the database.")
    String javaArchiveFilesDocumentation();

    @DefaultMessage("Java Archive")
    String javaArchiveField();

    @DefaultMessage("JDBC Driver Classes")
    String jdbcDriverClassesPanel();

    @DefaultMessage("Below is a listing of all JDBC driver classes found in the given Java archives, please select the correct JDBC driver class which will be used to create the connections.")
    String jdbcDriverClassesDocumentation();

    @DefaultMessage("JDBC Driver Class")
    String jdbcDriverClassField();

    @DefaultMessage("JDBC Driver {0} Used By")
    String jdbcDriverUsedByTitle(String name);

}