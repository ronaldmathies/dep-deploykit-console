package nl.sodeso.deploykit.console.client.application.group;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import nl.sodeso.deploykit.console.client.application.group.rpc.GroupRpcGateway;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public class GroupComboboxField extends ComboboxField<DefaultOption> {

    public GroupComboboxField(String key, OptionType<DefaultOption> optionValue) {
        super(key, optionValue);

        this.addAttachHandler(event -> {
            if (event.isAttached()) {
                if (isEnabled()) {
                    refresh();
                }
            }
        });
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (isAttached()) {
            if (isEnabled && getItems().isEmpty()) {
                refresh();
            }
        }
    }

    private void refresh() {
        ((GroupRpcGateway) GWT.create(GroupRpcGateway.class)).asOptions(new DefaultAsyncCallback<ArrayList<DefaultOption>>() {
            @Override
            public void success(ArrayList<DefaultOption> result) {
                getItems().clear();
                addItems(result);
            }
        });
    }
}
