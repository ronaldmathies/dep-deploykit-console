package nl.sodeso.deploykit.console.client.application.tooling.usage.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface UsedByRpcAsync {

    void findUsages(String uuid, VersionOption version, AsyncCallback<ArrayList<UsedBy>> result);

}
