package nl.sodeso.deploykit.console.client.application.services.ldap.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapService;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpc;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpc;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpc;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-ldapservice")
public interface LdapServiceRpc extends XsrfProtectedService, OptionRpc, VersioningRpc, LabelUniqueRpc, DeleteRpc, UsedByRpc {

    ArrayList<LdapServiceSummaryItem> findSummaries();
    LdapService findSingle(String uuid, VersionOption version);

    LdapServiceSummaryItem save(LdapService ldapService) throws RemoteException;
    CheckResult check(String uuid, VersionOption version);
    LdapServiceSummaryItem duplicate(String uuid, VersionOption version);

}
