package nl.sodeso.deploykit.console.client.application.security.role.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class RoleRpcGateway implements RpcGateway<RoleRpcAsync>, RoleRpcAsync {

    /**
     * {@inheritDoc}
     */
    public RoleRpcAsync getRpcAsync() {
        return GWT.create(RoleRpc.class);
    }
}
