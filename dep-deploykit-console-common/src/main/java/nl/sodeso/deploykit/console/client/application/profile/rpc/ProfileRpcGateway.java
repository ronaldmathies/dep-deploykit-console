package nl.sodeso.deploykit.console.client.application.profile.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ProfileRpcGateway implements RpcGateway<ProfileRpcAsync>, ProfileRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ProfileRpcAsync getRpcAsync() {
        return GWT.create(ProfileRpc.class);
    }
}
