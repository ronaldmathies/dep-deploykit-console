package nl.sodeso.deploykit.console.client.application.tooling.usage;

import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.gwt.ui.client.form.button.CloseButton;
import nl.sodeso.gwt.ui.client.form.table.TablePagerField;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.util.Align;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class UsageWindow extends PopupWindowPanel {

    private UsageTable table = null;

    private UsedByClickHandler usedByClickHandler = null;

    public UsageWindow(List<UsedBy> list, String title) {
        super("usage", title, WindowPanel.Style.INFO);

        showCollapseButton(true);
        setWidth("500px");

        table = new UsageTable();
        table.setFullHeight(true);
        final SingleSelectionModel<UsedBy> singleSelectionModel = new SingleSelectionModel<>();
        table.setSelectionModel(singleSelectionModel);
        singleSelectionModel.addSelectionChangeHandler(event -> {
            UsedBy usedBy = singleSelectionModel.getSelectedObject();
            if (usedByClickHandler != null) {
                usedByClickHandler.click(usedBy);

                // Directly deselect the selected row.
                table.deselect();
            }
        });

        TablePagerField tablePagerField = new TablePagerField();
        tablePagerField.setDisplay(table);
        addToFooter(Align.RIGHT, tablePagerField);

        addToFooter(Align.LEFT, new CloseButton.WithLabel((event) -> close()));

        addToBody(table);

        this.table.replaceAll(list);
    }

    public void setUsedByClickHandler(UsedByClickHandler usedByClickHandler) {
        this.usedByClickHandler = usedByClickHandler;
    }

}
