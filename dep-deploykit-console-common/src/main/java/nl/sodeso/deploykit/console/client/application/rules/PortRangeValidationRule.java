package nl.sodeso.deploykit.console.client.application.rules;

import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.rules.IntegerRangeValidationRule;
import nl.sodeso.gwt.ui.client.types.IntType;

/**
 * @author Ronald Mathies
 */
public class PortRangeValidationRule extends IntegerRangeValidationRule {

    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 65535;

    private IntType valueType;

    public PortRangeValidationRule(String key, IntType valueType) {
        super(key, ValidationMessage.Level.ERROR);
        this.valueType = valueType;

        this.setMinimum(MIN_VALUE);
        this.setMaximum(MAX_VALUE);
    }

    @Override
    public String getValue() {
        return valueType.getValueAsString();
    }

}
