package nl.sodeso.deploykit.console.client.application.connectors.https;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.http.rpc.HttpRpcGateway;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.rules.PortRangeValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadField;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static nl.sodeso.deploykit.console.client.application.Resources.HTTPS_I18N;

/**
 * @author Ronald Mathies
 */
public class Https extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_KEYSTORE_UPLOAD = "keystoreUpload";
    private static final String KEY_KEYSTORE_PASSWORD = "keystore-password";
    private static final String KEY_TRUSTSTORE_PASSWORD = "truststore-password";
    private static final String KEY_KEYMANAGER_PASSWORD = "keymanager-password";
    private static final String KEY_CERTIFICATES = "certificates";
    private static final String KEY_PORT = "port";
    private static final String KEY_TIMEOUT = "timeout";


    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private OptionType<DefaultOption> keyStorePasswordOptionType = new OptionType<>();
    private VersionOptionType keyStoreVersionType = new VersionOptionType();
    private OptionType<DefaultOption> trustStorePasswordOptionType = new OptionType<>();
    private VersionOptionType trustStoreVersionType = new VersionOptionType();
    private OptionType<DefaultOption> keyManagerPasswordOptionType = new OptionType<>();
    private VersionOptionType keyManagerVersionType = new VersionOptionType();
    private StringType host = new StringType();
    private IntType port = new IntType(8443);
    private IntType timeout = new IntType(30000);

    private FileUploadType fileUploadType = new FileUploadType();

    private List<Certificate> certificates = new ArrayList<>();

    private transient LegendPanel legendPanel;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;

    private transient VersionSelectionField<CredentialRpcGateway> keyStorePasswordField = null;
    private transient VersionSelectionField<CredentialRpcGateway> trustStorePasswordField = null;
    private transient VersionSelectionField<CredentialRpcGateway> keyManagerPasswordField = null;

    private transient FileUploadForm fileUploadForm = null;
    private transient FileUploadField keystoreUploadField = null;
    private transient TextField portField = null;
    private transient TextField timeoutField = null;

    public Https() {
    }

    public Widget toEditForm() {
        if (this.legendPanel != null) {
            return this.legendPanel;
        }

        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
        .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        keyStorePasswordField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), keyStorePasswordOptionType, keyStoreVersionType);
        keyStorePasswordField.listenToGroup(groupOptionType);

        trustStorePasswordField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), trustStorePasswordOptionType, trustStoreVersionType);
        trustStorePasswordField.listenToGroup(groupOptionType);

        keyManagerPasswordField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), keyManagerPasswordOptionType, keyManagerVersionType);
        keyManagerPasswordField.listenToGroup(groupOptionType);

        keystoreUploadField = new FileUploadField(KEY_KEYSTORE_UPLOAD, fileUploadType);
        keystoreUploadField.addValidationRule(new MandatoryValidationRule(KEY_KEYSTORE_UPLOAD, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return keystoreUploadField.getValue();
            }
        });
        fileUploadForm = new FileUploadForm(keystoreUploadField, "/deploykitconsole/keystore/upload");

        portField = new TextField<>(KEY_PORT, this.port);
        portField.addValidationRule(new PortRangeValidationRule(KEY_PORT, port));
        portField.addFilter(new NumericInputFilter(false));

        timeoutField = new TextField<>(KEY_TIMEOUT, this.timeout)
            .addFilter(new NumericInputFilter(false));

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, HTTPS_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, HTTPS_I18N.labelField(), labelTextField)
                        .addValidationRule(
                            new LabelUniqueValidationRule(GWT.create(HttpsRpcGateway.class)) {
                                @Override
                                public String getUuid() {
                                    return Https.this.getUuid();
                                }

                                @Override
                                public StringType getLabel() {
                                    return Https.this.getLabel();
                                }
                            }
                        ),
                new EntryWithLabelAndWidget(KEY_KEYSTORE_PASSWORD, HTTPS_I18N.keystorePasswordField(),
                    new HorizontalPanel()
                        .addWidget(keyStorePasswordField, 100.0, Style.Unit.PCT)
                        .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(keyStorePasswordOptionType.getValue().getKey(), keyStoreVersionType.getValue()))))
                    .addValidationRules(
                            new ValidationCriteria.or(
                                    new ComboboxMandatoryValidationRule(KEY_KEYSTORE_PASSWORD, ValidationMessage.Level.ERROR, keyStorePasswordOptionType),
                                    new ComboboxMandatoryValidationRule(KEY_KEYSTORE_PASSWORD, ValidationMessage.Level.ERROR, keyStoreVersionType)
                            )
                    ),
                new EntryWithLabelAndWidget(KEY_TRUSTSTORE_PASSWORD, HTTPS_I18N.trustStorePasswordField(),
                    new HorizontalPanel()
                        .addWidget(trustStorePasswordField, 100.0, Style.Unit.PCT)
                        .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(trustStorePasswordOptionType.getValue().getKey(), trustStoreVersionType.getValue()))))
                    .addValidationRules(
                            new ValidationCriteria.or(
                                    new ComboboxMandatoryValidationRule(KEY_TRUSTSTORE_PASSWORD, ValidationMessage.Level.ERROR, trustStorePasswordOptionType),
                                    new ComboboxMandatoryValidationRule(KEY_TRUSTSTORE_PASSWORD, ValidationMessage.Level.ERROR, trustStoreVersionType)
                            )
                    ),
                new EntryWithLabelAndWidget(KEY_KEYMANAGER_PASSWORD, HTTPS_I18N.keyManagerPasswordField(),
                    new HorizontalPanel()
                        .addWidget(keyManagerPasswordField, 100.0, Style.Unit.PCT)
                        .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(keyManagerPasswordOptionType.getValue().getKey(), keyManagerVersionType.getValue()))))
                    .addValidationRules(
                            new ValidationCriteria.or(
                                    new ComboboxMandatoryValidationRule(KEY_KEYMANAGER_PASSWORD, ValidationMessage.Level.ERROR, keyManagerPasswordOptionType),
                                    new ComboboxMandatoryValidationRule(KEY_KEYMANAGER_PASSWORD, ValidationMessage.Level.ERROR, keyManagerVersionType)
                            )
                    ),
                new EntryWithLabelAndWidget(KEY_KEYSTORE_UPLOAD, HTTPS_I18N.keystoreField(), this.fileUploadForm),
                new EntryWithDocumentation(KEY_PORT, HTTPS_I18N.portFieldDocumentation()),
                new EntryWithLabelAndWidget(KEY_PORT, HTTPS_I18N.portField(), portField),
                new EntryWithLabelAndWidget(KEY_TIMEOUT, HTTPS_I18N.timeoutField(), timeoutField).setHint(HTTPS_I18N.timeoutFieldHint()));

        if (!this.certificates.isEmpty()) {
            LegendPanel certificateLegendPanel = new LegendPanel(null, HTTPS_I18N.certificates());
            CertificatesTable certificatesTable = new CertificatesTable();
            certificatesTable.setCertificates(certificates);

            EntryForm certificateEntryForm = new EntryForm(null)
                .addEntries(
                        new EntryWithDocumentation(KEY_CERTIFICATES, HTTPS_I18N.certificatesDocumentation()),
                        new EntryWithContainer(KEY_CERTIFICATES, certificatesTable));
            certificateLegendPanel.add(certificateEntryForm);

            form.addEntry(new EntryWithContainer(null, certificateLegendPanel));
        }

        legendPanel = new LegendPanel(null, HTTPS_I18N.httpsPanel());
        legendPanel.add(form);

//        legendPanel.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                keyStorePasswordField.refresh(groupOptionType);
//                trustStorePasswordField.refresh(groupOptionType);
//                keyManagerPasswordField.refresh(groupOptionType);
//            }
//        });

        labelTextField.setFocus();

        return legendPanel;
    }

    protected void upload(Trigger completeTrigger) {
        this.fileUploadForm.setFileUploadCompleteTrigger(completeTrigger);

        if (getFileUploadType().isChanged()) {
            this.fileUploadForm.submit();
        } else {
            completeTrigger.fire();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, legendPanel);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.legendPanel);
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public OptionType<DefaultOption> getGroup() {
        return groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public OptionType<DefaultOption> getKeyStorePassword() {
        return keyStorePasswordOptionType;
    }

    public void setKeyStorePassword(OptionType<DefaultOption> keyStorePasswordOptionType) {
        this.keyStorePasswordOptionType = keyStorePasswordOptionType;
    }

    public VersionOptionType getKeyStoreVersion() {
        return keyStoreVersionType;
    }

    public void setKeyStoreVersion(VersionOptionType keyStoreVersionType) {
        this.keyStoreVersionType = keyStoreVersionType;
    }

    public OptionType<DefaultOption> getTrustStorePassword() {
        return trustStorePasswordOptionType;
    }

    public void setTrustStorePassword(OptionType<DefaultOption> trustStorePasswordOptionType) {
        this.trustStorePasswordOptionType = trustStorePasswordOptionType;
    }

    public VersionOptionType getTrustStoreVersion() {
        return trustStoreVersionType;
    }

    public void setTrustStoreVersion(VersionOptionType trustStoreVersionType) {
        this.trustStoreVersionType = trustStoreVersionType;
    }

    public OptionType<DefaultOption> getKeyManagerPassword() {
        return keyManagerPasswordOptionType;
    }

    public void setKeyManagerPasswordOptionType(OptionType<DefaultOption> keyManagerPasswordOptionType) {
        this.keyManagerPasswordOptionType = keyManagerPasswordOptionType;
    }

    public VersionOptionType getKeyManagerVersion() {
        return keyManagerVersionType;
    }

    public void setKeyManagerVersion(VersionOptionType keyManagerVersionType) {
        this.keyManagerVersionType = keyManagerVersionType;
    }

    public FileUploadType getFileUploadType() {
        return fileUploadType;
    }

    public void setFileUploadType(FileUploadType fileUploadType) {
        this.fileUploadType = fileUploadType;
    }

    public StringType getHost() {
        return host;
    }

    public void setHost(StringType host) {
        this.host = host;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

}
