package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.ui.FormPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.AddButton;
import nl.sodeso.gwt.ui.client.form.button.RemoveButton;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.trigger.Trigger;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public abstract class MultipleFileUpload extends FormPanel implements FormPanel.SubmitHandler, FormPanel.SubmitCompleteHandler {

    private transient EntryForm entryForm = null;
    private transient EntryForm fileUploadsForm = null;

    private transient ArrayList<FileUploadField> fileUploadFields = null;

    private transient Trigger fileUploadCompleteTrigger = null;

    public MultipleFileUpload(final ArrayList<FileUploadField> fileUploadFields, String action) {
        super();

        setAction(action);

        this.fileUploadFields = fileUploadFields;

        this.addSubmitHandler(this);
        this.addSubmitCompleteHandler(this);

        this.setEncoding(FormPanel.ENCODING_MULTIPART);
        this.setMethod(FormPanel.METHOD_POST);

        // The form that will contain the file upload elements.
        this.fileUploadsForm = new EntryForm(null);

        for (FileUploadField fileUploadField : fileUploadFields) {
            addFileUploadFieldToForm(fileUploadField);
        }

        AddButton.WithIcon addButton = new AddButton.WithIcon((event) -> {
            FileUploadField fileUploadField = createFileUploadField();
            fileUploadFields.add(fileUploadField);
            addFileUploadFieldToForm(fileUploadField);
        });

        this.entryForm = new EntryForm(null)
                .addEntries(
                        new EntryWithContainer(null, this.fileUploadsForm),
                        new EntryWithWidgetAndWidget("add-java-archive-entry", null, null).setButton(addButton));

        this.add(entryForm);
    }

    public abstract FileUploadField createFileUploadField();



    private void addFileUploadFieldToForm(FileUploadField fileUploadField) {
        final EntryWithLabelAndWidget entry = new EntryWithLabelAndWidget(null, null, fileUploadField);
        entry.setButton(new RemoveButton.WithIcon((event) -> fileUploadsForm.removeEntry(entry)));
        this.fileUploadsForm.addEntry(entry);
    }

    @Override
    public void onSubmit(SubmitEvent event) {
        entryForm.validateContainer(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.ERROR) {
                event.cancel();
            }
        });

    }

    public void onSubmitComplete(SubmitCompleteEvent event) {
        JSONObject response = (JSONObject) JSONParser.parseStrict(event.getResults());
//        String uuid = ((JSONString) response.get("uuid")).stringValue();
//        fileUploadField.getFileUploadType().getValue().setUuid(uuid);

        if (this.fileUploadCompleteTrigger != null) {
            this.fileUploadCompleteTrigger.fire();
        }
    }

    public void setFileUploadCompleteTrigger(Trigger fileUploadCompleteTrigger) {
        this.fileUploadCompleteTrigger = fileUploadCompleteTrigger;
    }

}
