package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;

import nl.sodeso.gwt.ui.client.panel.ValidationResultPanel;

/**
 * @author Ronald Mathies
 */
public class BranchProcess {

    private VersioningRpcAsync commitRpcAsync;
    private Versionable versionable;
    private CommitCallback callback;

    public BranchProcess(VersioningRpcAsync commitRpcAsync, Versionable versionable, CommitCallback callback) {
        this.commitRpcAsync = commitRpcAsync;
        this.versionable = versionable;
        this.callback = callback;
    }

    public void start() {
        commitAllowedCheck();
    }

    private void commitAllowedCheck() {
        commitRpcAsync.isBranchAllowed(versionable.getUuid(), versionable.getVersion().getValue(),
            new DefaultAsyncCallback<ValidationResult>() {
                @Override
                public void success(ValidationResult result) {
                    if (result.hasMessages()) {
                        new ValidationResultPanel(result, "Cannot Create Branch", "Please fix the following issues before trying to branch this version.").center(true, true);
                    } else {
                        confirmBranchAction();
                    }
                }
            });
    }

    private void confirmBranchAction() {
        CreateBranchQuestionPanel questionPanel = new CreateBranchQuestionPanel(() -> commitRpcAsync.branch(versionable.getUuid(), versionable.getVersion().getValue(), new DefaultAsyncCallback<VersionOption>() {
            @Override
            public void success(VersionOption version) {
                callback.onSuccess(version);
            }

        }));

        questionPanel.center(true, true);
    }

}
