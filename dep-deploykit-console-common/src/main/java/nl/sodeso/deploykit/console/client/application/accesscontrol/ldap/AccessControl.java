package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc.AccessControlRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.AccessControlTypeComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.SwitchPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;

/**
 * @author Ronald Mathies
 */
public class AccessControl extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private transient static final String KEY_GROUP = "group";
    private transient static final String KEY_LABEL = "label";
    private transient static final String KEY_PLACEHOLDER_PREFIX = "placeholder-prefix";
    private transient static final String KEY_REALM = "realm";
    private transient static final String KEY_ACCESS_CONTROL_TYPE = "access-control-type";

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private StringType placeholderPrefix = new StringType();

    private StringType realm = new StringType();

    private OptionType<DefaultOption> accessControlTypeOptionType = new OptionType<>();
    private DatasourceAccessControl datasourceAccessControl = new DatasourceAccessControl();
    private LdapAccessControl ldapAccessControl = new LdapAccessControl();

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient GroupComboboxField groupField = null;
    private transient TextField realmTextField = null;
    private transient TextField labelTextField = null;
    private transient TextField placeholderPrefixField = null;

    private transient AccessControlTypeComboboxField accessControlTypeComboboxField = null;
    private transient SwitchPanel switchPanel = null;

    public AccessControl() {
    }

    /**
     * {@inheritDoc}
     */
    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGeneralSettings();

        this.switchPanel = new SwitchPanel(null, true)
            .addWidgets(
                    datasourceAccessControl.toEditForm(this),
                    ldapAccessControl.toEditForm(this));

        if (this.accessControlTypeOptionType.getValue() != null) {
            this.switchPanel.switchTo(this.accessControlTypeOptionType.getValue().getKey());
        }
        this.entryForm = new EntryForm(null)
            .addEntries(
                    new EntryWithContainer(null, this.generalLegendPanel),
                    new EntryWithContainer(null, this.switchPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupGeneralSettings() {
        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType)
            .addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                    new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                        @Override
                        public String getValue() {
                            return labelTextField.getValue();
                        }
                    },
                    new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                        @Override
                        public String getValue() {
                            return labelTextField.getValue();
                        }
                    }
            );
        placeholderPrefixField = new TextField<>(KEY_PLACEHOLDER_PREFIX, this.placeholderPrefix)
            .addValidationRule(new MandatoryValidationRule(KEY_PLACEHOLDER_PREFIX, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return placeholderPrefixField.getValue();
                }
            });

        realmTextField = new TextField<>(KEY_REALM, this.realm)
            .addValidationRule(new MandatoryValidationRule(KEY_REALM, ValidationMessage.Level.ERROR) {
                @Override
                public String getValue() {
                    return realmTextField.getValue();
                }
            });

        accessControlTypeComboboxField = new AccessControlTypeComboboxField(KEY_ACCESS_CONTROL_TYPE, accessControlTypeOptionType);
        accessControlTypeOptionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(accessControlTypeComboboxField) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                switchPanel.switchTo(accessControlTypeOptionType.getValue().getKey());
            }
        });

        EntryForm form = new EntryForm(null)
            .addEntries(
                new EntryWithDocumentation(null, ACCESS_CONTROL_I18N.generalDocumentation()),
                new EntryWithLabelAndWidget(KEY_GROUP, ACCESS_CONTROL_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, ACCESS_CONTROL_I18N.labelField(), labelTextField)
                    .addValidationRule(
                        new LabelUniqueValidationRule(GWT.create(AccessControlRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return AccessControl.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return AccessControl.this.getLabel();
                            }
                        }
                    ),
                new EntryWithLabelAndWidget(KEY_PLACEHOLDER_PREFIX, ACCESS_CONTROL_I18N.placeHolderPrefixField(), placeholderPrefixField),
                new EntryWithDocumentation(KEY_REALM, ACCESS_CONTROL_I18N.realmNameDocumentation()),
                new EntryWithLabelAndWidget(KEY_REALM, ACCESS_CONTROL_I18N.realmNameField(),
                    new VerticalPanel(
                        this.realmTextField,
                        new PlaceholderSpanField(KEY_REALM, getPlaceholderPrefix(), AccessControlPlaceHolderSuffix.AC_REALM))
                    )
                    .setHint(ACCESS_CONTROL_I18N.roleColumnFieldHint())
                    .addValidatableWidget(this.realmTextField),
                new EntryWithLabelAndWidget(KEY_ACCESS_CONTROL_TYPE, ACCESS_CONTROL_I18N.accessControlTypeField(), accessControlTypeComboboxField));

        this.generalLegendPanel = new LegendPanel(null, ACCESS_CONTROL_I18N.accessControlPanel());
        this.generalLegendPanel.add(form);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getRealm() {
        return realm;
    }

    public void setRealm(StringType realm) {
        this.realm = realm;
    }

    public OptionType<DefaultOption> getAccessControlType() {
        return accessControlTypeOptionType;
    }

    public void setAccessControlType(OptionType<DefaultOption> accessControlTypeOptionType) {
        this.accessControlTypeOptionType = accessControlTypeOptionType;
    }

    public DatasourceAccessControl getDatasourceAccessControl() {
        return datasourceAccessControl;
    }

    public void setDatasourceAccessControl(DatasourceAccessControl datasourceAccessControl) {
        this.datasourceAccessControl = datasourceAccessControl;
    }

    public LdapAccessControl getLdapAccessControl() {
        return ldapAccessControl;
    }

    public void setLdapAccessControl(LdapAccessControl ldapAccessControl) {
        this.ldapAccessControl = ldapAccessControl;
    }
}
