package nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class ConnectionPoolRpcGateway implements RpcGateway<ConnectionPoolRpcAsync>, ConnectionPoolRpcAsync {

    /**
     * {@inheritDoc}
     */
    public ConnectionPoolRpcAsync getRpcAsync() {
        return GWT.create(ConnectionPoolRpc.class);
    }
}
