package nl.sodeso.deploykit.console.client.application.search.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.search.SearchResult;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface SearchRpcAsync {

    public void search(String query, AsyncCallback<ArrayList<SearchResult>> result);
}
