package nl.sodeso.deploykit.console.client.application.profile.service.ldap;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplierCallback;

/**
 * @author Ronald Mathies
 */
public class ProfileLdapServices extends WidgetCollectionField<ProfileLdapService> {

    private OptionType<DefaultOption> groupOptionType = null;

    private ProfileLdapServices() {}

    public ProfileLdapServices(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    @Override
    public WidgetSupplier<ProfileLdapService> createWidgetSupplier() {
        return callback -> {
            ProfileLdapService profileLdapService = new ProfileLdapService();;
            profileLdapService.setGroup(groupOptionType);
            callback.created(profileLdapService);
        };
    }
}
