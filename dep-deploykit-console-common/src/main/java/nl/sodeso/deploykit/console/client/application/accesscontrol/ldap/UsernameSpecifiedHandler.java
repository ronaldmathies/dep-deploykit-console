package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @author Ronald Mathies
 */
public interface UsernameSpecifiedHandler {

    void ok(StringType username);

}
