package nl.sodeso.deploykit.console.client.application.node;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.NODE_I18N;

/**
 * @author Ronald Mathies
 */
public class NodeMenuItem extends MenuItem {

    private static final String KEY = "mi-node";

    private static Nodes nodes = null;

    public NodeMenuItem() {
        super(KEY, NODE_I18N.nodeMenuItem(), arguments -> {
            if (nodes == null) {
                nodes = new Nodes();
            }
            nodes.setArguments(arguments);
            CenterController.instance().setWidget(nodes);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }

}
