package nl.sodeso.deploykit.console.client.application.guide.steps;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;

/**
 * @author Ronald Mathies
 */
public class WelcomeStep extends AbstractWizardStep {

    private AbstractWizardStep setupGroupStep = null;

    public WelcomeStep(AbstractWizardStep callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep getNextStep() {
        if (setupGroupStep == null) {
            setupGroupStep = new SetupGroupsStep(this);
        }

        return setupGroupStep;
    }

    @Override
    public Widget getBodyWidget() {
        LegendPanel legendPanel = new LegendPanel("welcome-step", "Welcome");
        EntryForm entryForm = new EntryForm("form");

        entryForm.addEntry(new EntryWithDocumentation(null,
                "This wizard will guide you to setting up the DeployKit environment and create <br/>" +
                "a profile which can be used to provision a server. By clicking on Next we will " +
                "take you to the first step, if a step is not necessary simple click on the Next<br/>" +
                "button to skip to the following step.<br/>" +
                "<br/>" +
                "If at anytime you would like to create some space to do your work then<br/>" +
                "use the minimize button in the top right corner to create some<br/>" +
                "additional space."));

        legendPanel.add(entryForm);
        return legendPanel;
    }
}
