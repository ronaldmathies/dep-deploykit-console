package nl.sodeso.deploykit.console.client.application.tooling.versioning;

import com.google.gwt.event.dom.client.ClickEvent;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.CancelButton;
import nl.sodeso.gwt.ui.client.form.button.YesButton;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.validation.rules.VersionValidationRule;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class SpecifyVersionQuestionPanel extends PopupWindowPanel {

    private static final String KEY_SPECIFY_VERSION = "specify-version";

    private VersionSpecifiedHandler versionSpecifiedHandler = null;
    private StringType version = new StringType();

    private TextField versionField = null;

    public SpecifyVersionQuestionPanel(final VersionSpecifiedHandler versionSpecifiedHandler) {
        super(KEY_SPECIFY_VERSION, "Commit", WindowPanel.Style.INFO);
        this.versionSpecifiedHandler = versionSpecifiedHandler;

        this.setWidth("650px");

        versionField = new TextField<>("version", version);
        versionField.addValidationRule(new MandatoryValidationRule("mandatory", ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return version.getValue();
                    }
                });
        versionField.addValidationRule(new VersionValidationRule("format", ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return version.getValue();
            }
        });

        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation(null,
                "Please enter a version number, the version number should have the formatting, " +
                        "<i>One to three digits, dot, one to three digits, and so forth</i>. For example:<br/><ul>" +
                        "<li>1.0</li>" +
                        "<li>1.10.10</li>" +
                        "<li>10.100.10.1</li>" +
                        "</ul>"));
        entryForm.addEntry(new EntryWithLabelAndWidget(null, "Version", versionField));
        addToBody(entryForm);


        addToFooter(Align.RIGHT,
                new YesButton.WithLabel(this::yes),
                new CancelButton.WithLabel((event) -> close()));

        versionField.setFocus();
    }

    private void yes(ClickEvent event) {
        ValidationUtil.validate(result -> {
            if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                SpecifyVersionQuestionPanel.this.close();

                versionSpecifiedHandler.ok(version);
            }
        }, versionField);

    }

}
