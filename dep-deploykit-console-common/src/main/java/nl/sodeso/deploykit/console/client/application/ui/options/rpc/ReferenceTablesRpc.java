package nl.sodeso.deploykit.console.client.application.ui.options.rpc;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.user.client.rpc.XsrfProtectedService;
import com.google.gwt.user.server.rpc.XsrfProtect;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@XsrfProtect
@RemoteServiceRelativePath("deploykitconsole/endpoint.deploykitconsole-reference")
public interface ReferenceTablesRpc extends XsrfProtectedService {

    ArrayList<DefaultOption> javaServerProviders();
    ArrayList<DefaultOption> accessControlTypes();
}
