package nl.sodeso.deploykit.console.client.application.security.role;

import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class RoleUsers extends WidgetCollectionField<RoleUser> {

    @Override
    public WidgetSupplier<RoleUser> createWidgetSupplier() {
        return callback -> callback.created(new RoleUser());
    }
}
