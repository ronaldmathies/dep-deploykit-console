package nl.sodeso.deploykit.console.client.application.group;

import nl.sodeso.gwt.ui.client.form.table.summary.AbstractSummaryItem;
import nl.sodeso.gwt.ui.client.types.StringType;

/**
 * @auhor Ronald Mathies
 */
public class GroupSummaryItem extends AbstractSummaryItem {

    private String uuid;
    private StringType label;

    public GroupSummaryItem() {}

    public GroupSummaryItem(String uuid, StringType label) {
        this.uuid = uuid;
        this.label = label;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    @Override
    public String getLabel() {
        return this.label.getValue();
    }
}
