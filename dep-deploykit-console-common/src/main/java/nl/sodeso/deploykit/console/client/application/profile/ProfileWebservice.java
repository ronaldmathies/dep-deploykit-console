package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceMenuItem;
import nl.sodeso.deploykit.console.client.application.services.webservice.rpc.WebserviceRpcGateway;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class ProfileWebservice implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private OptionType<DefaultOption> webserviceOptionType = new OptionType<>();
    private VersionOptionType webserviceVersionType = new VersionOptionType();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient VersionSelectionField<WebserviceRpcGateway> webserviceField = null;

    public ProfileWebservice() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        webserviceField = new VersionSelectionField<>(GWT.create(WebserviceRpcGateway.class), webserviceOptionType, webserviceVersionType);
        webserviceField.listenToGroup(groupOptionType);

        entry = new EntryWithWidgetAndWidget("webservice", null,
            new HorizontalPanel()
                .addWidget(webserviceField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> WebserviceMenuItem.click(webserviceOptionType.getValue().getKey(), webserviceVersionType.getValue()))));

        entry.addValidationRule(
            new ValidationCriteria.or(
                new ComboboxMandatoryValidationRule("webservice-option", ValidationMessage.Level.ERROR, webserviceOptionType),
                new ComboboxMandatoryValidationRule("webservice-version", ValidationMessage.Level.ERROR, webserviceVersionType)
            )
        );

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> group) {
        this.groupOptionType = group;
    }

    public OptionType<DefaultOption> getWebservice() {
        return this.webserviceOptionType;
    }

    public void setWebservice(OptionType<DefaultOption> webservice) {
        this.webserviceOptionType = webservice;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

    public VersionOptionType getWebserviceVersion() {
        return webserviceVersionType;
    }

    public void setWebserviceVersion(VersionOptionType webserviceVersionType) {
        this.webserviceVersionType = webserviceVersionType;
    }
}
