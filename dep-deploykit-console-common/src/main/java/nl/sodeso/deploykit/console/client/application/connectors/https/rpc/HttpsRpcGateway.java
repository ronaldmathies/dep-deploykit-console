package nl.sodeso.deploykit.console.client.application.connectors.https.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class HttpsRpcGateway implements RpcGateway<HttpsRpcAsync>, HttpsRpcAsync {

    /**
     * {@inheritDoc}
     */
    public HttpsRpcAsync getRpcAsync() {
        return GWT.create(HttpsRpc.class);
    }
}
