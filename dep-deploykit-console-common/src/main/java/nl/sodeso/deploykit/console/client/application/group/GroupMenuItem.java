package nl.sodeso.deploykit.console.client.application.group;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.GROUP_I18N;

/**
 * @author Ronald Mathies
 */
public class GroupMenuItem extends MenuItem {

    private static final String KEY = "mi-groups";

    private static Groups groups = null;

    public GroupMenuItem() {
        super(KEY, GROUP_I18N.groupMenuItem(), arguments -> {
            if (groups == null) {
                groups = new Groups();
            }

            groups.setArguments(arguments);
            CenterController.instance().setWidget(groups);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }

}
