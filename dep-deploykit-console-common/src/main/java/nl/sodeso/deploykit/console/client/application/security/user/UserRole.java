package nl.sodeso.deploykit.console.client.application.security.user;

import com.google.gwt.dom.client.Style;
import nl.sodeso.deploykit.console.client.application.security.role.RoleComboboxField;
import nl.sodeso.deploykit.console.client.application.security.role.RoleMenuItem;
import nl.sodeso.gwt.ui.client.form.EntryWithWidgetAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionFieldWidget;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class UserRole implements WidgetCollectionFieldWidget, Serializable, IsValidationContainer {

    private String uuid = null;
    private OptionType<DefaultOption> roleOptionType = new OptionType<>();

    private transient EntryWithWidgetAndWidget entry = null;
    private transient RoleComboboxField roleField = null;

    public UserRole() {
    }

    public EntryWithWidgetAndWidget toEntry() {
        if (this.entry != null) {
            return this.entry;
        }

        this.roleField = new RoleComboboxField(roleOptionType);
        this.roleField.addValidationRule(new ComboboxMandatoryValidationRule("role-mandatory", ValidationMessage.Level.ERROR, roleOptionType));

        entry = new EntryWithWidgetAndWidget("ldap-service", null,
            new HorizontalPanel()
                .addWidget(this.roleField, 100.0, Style.Unit.PCT)
                .addWidget(new SearchButton.WithIcon((event) -> RoleMenuItem.click(roleOptionType.getValue().getKey()))));

        return this.entry;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public OptionType<DefaultOption> getRole() {
        return this.roleOptionType;
    }

    public void setRole(OptionType<DefaultOption> node) {
        this.roleOptionType = node;
    }

    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entry);
    }

}
