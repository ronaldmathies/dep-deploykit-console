package nl.sodeso.deploykit.console.client.application.connectors.http.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.connectors.http.Http;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.rpc.UsedByRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.rpc.VersioningRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.OptionRpcAsync;
import nl.sodeso.deploykit.console.client.application.ui.validation.rpc.LabelUniqueRpcAsync;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface HttpRpcAsync extends VersioningRpcAsync, OptionRpcAsync, UsedByRpcAsync, DeleteRpcAsync, LabelUniqueRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<HttpSummaryItem>> result);

    void findSingle(String uuid, VersionOption version, AsyncCallback<Http> result);

    void save(Http http, AsyncCallback<HttpSummaryItem> result);
    void duplicate(String uuid, VersionOption version, AsyncCallback<HttpSummaryItem> result);

}
