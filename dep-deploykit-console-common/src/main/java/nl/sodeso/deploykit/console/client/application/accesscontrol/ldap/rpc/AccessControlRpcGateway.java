package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class AccessControlRpcGateway implements RpcGateway<AccessControlRpcAsync>, AccessControlRpcAsync {

    /**
     * {@inheritDoc}
     */
    public AccessControlRpcAsync getRpcAsync() {
        return GWT.create(AccessControlRpc.class);
    }
}
