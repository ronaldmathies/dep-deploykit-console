package nl.sodeso.deploykit.console.client.application.accesscontrol.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.view.client.SingleSelectionModel;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc.AccessControlRpcGateway;
import nl.sodeso.deploykit.console.client.application.profile.ProfileMenuItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.DeleteProcess;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsageWindow;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.BranchProcess;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.CommitProcess;
import nl.sodeso.deploykit.console.client.application.ui.button.BranchButton;
import nl.sodeso.deploykit.console.client.application.ui.button.CheckButton;
import nl.sodeso.deploykit.console.client.application.ui.button.CommitButton;
import nl.sodeso.deploykit.console.client.application.ui.button.UsedByButton;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractCenterPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.button.*;
import nl.sodeso.gwt.ui.client.form.combobox.ComboboxField;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.table.summary.SummaryTableField;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.PopupWindowPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.rpc.DefaultAsyncCallback;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.types.ValueType;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEvent;
import nl.sodeso.gwt.ui.client.types.event.ValueChangedEventHandler;
import nl.sodeso.gwt.ui.client.util.Align;
import nl.sodeso.gwt.ui.client.dialog.UnsavedChangesQuestionPanel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static nl.sodeso.deploykit.console.client.application.Resources.ACCESS_CONTROL_I18N;
import static nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper.*;

/**
 * @author Ronald Mathies
 */
public class AccessControls extends AbstractCenterPanel {

    private static final String KEY_TABLE= "table";
    private static final String KEY_DETAILS = "details";
    private static final String KEY_CHECK_QUESTION = "check-question";
    private static final String KEY_CHECK_RESULT = "check-result";

    private HorizontalPanel horizontalPanel = null;
    private WindowPanel detailWindowPanel = null;

    private SummaryTableField<AccessControlSummaryItem> table = null;

    private AccessControl currentObject = new AccessControl();

    private Map<String, String> arguments = null;

    private RemoveButton.WithLabel removeButton;
    private UsedByButton.WithLabel usedByButton;
    private RefreshButton.WithIcon refreshButton;
    private CloneButton.WithLabel cloneButton;
    private AddButton.WithIcon addButton;
    private SaveButton.WithLabel saveButton;
    private CancelButton.WithLabel cancelButton;
    private CheckButton.WithLabel checkButton;
    private CommitButton.WithLabel commitButton;
    private BranchButton.WithLabel branchButton;

    private OptionType<VersionOption> versionOptionType = new OptionType<>();
    private ComboboxField<VersionOption> versionsField = null;

    public AccessControls() {
        super();

        setFullHeight(true);
    }

    public void setArguments(final Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public void beforeAdd(final Trigger trigger) {
        if (horizontalPanel == null) {
            horizontalPanel = new HorizontalPanel();
            horizontalPanel.setFullHeight(true);

            table = new SummaryTableField<>();
            table.setFullHeight(true);
            final SingleSelectionModel<AccessControlSummaryItem> singleSelectionModel = new SingleSelectionModel<>();
            table.setSelectionModel(singleSelectionModel);
            singleSelectionModel.addSelectionChangeHandler(event -> loadVersions(() -> {
                if (table.getSelected() != null) {
                    selectVersion();
                }
            }));

            refreshButton = new RefreshButton.WithIcon((event) -> refresh(null));
            addButton = new AddButton.WithIcon((event) -> add());

            final WindowPanel tableWindowPanel = new WindowPanel(KEY_TABLE, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .setBodyPadding(false)
                    .addToBody(table)
                    .addToToolbar(Align.LEFT, refreshButton)
                    .addToToolbar(Align.RIGHT, addButton);

            horizontalPanel.addWidget(tableWindowPanel, 20, Style.Unit.PCT);

            cloneButton = new CloneButton.WithLabel((event) -> prepareCopy());
            checkButton = new CheckButton.WithLabel((event) -> prepareCheck());
            usedByButton = new UsedByButton.WithLabel((event) -> usedBy());
            removeButton = new RemoveButton.WithLabel((event) -> delete());
            saveButton = new SaveButton.WithLabel((event) -> {
                save(() -> loadVersionDetails());
            });
            cancelButton = new CancelButton.WithLabel((event) -> {
                if (currentObject.getUuid() == null) {
                    table.first();
                } else {
                    detailWindowPanel.revert();
                }
            });

            detailWindowPanel = new WindowPanel(KEY_DETAILS, null, WindowPanel.Style.INFO)
                    .setFullHeight(true)
                    .addToBody(currentObject.toEditForm())
                    .addToToolbar(Align.LEFT, cloneButton)
                    .addToToolbar(Align.RIGHT, checkButton, usedByButton)
                    .addToFooter(Align.LEFT, removeButton)
                    .addToFooter(Align.RIGHT, saveButton, cancelButton);

            createVersioningComponents();

            horizontalPanel.addWidget(detailWindowPanel, 80, Style.Unit.PCT);

            evaluateButtonStates();

            add(horizontalPanel);
        }


        Trigger preselect = () -> {
            if (arguments != null && arguments.containsKey(ARG_UUID)) {
                final String uuid = arguments.get(ARG_UUID);
                if (table.getSelected() != null && table.getSelected().getUuid().equals(uuid)) {
                    selectVersion();
                } else {
                    for (AccessControlSummaryItem summary : table.allItems()) {
                        if (summary.getUuid().equals(uuid)) {
                            table.select(summary);

                            break;
                        }
                    }
                }
            } else {
                if (table.getSelected() == null) {
                    table.first();
                }
            }

            trigger.fire();
        };

        if (table.allItems().isEmpty()) {
            refresh(() -> {
                preselect.fire();
            });
        } else {
            preselect.fire();
        }
    }

    private void selectVersion() {
        String version = "SNAPSHOT";
        VersionType type = VersionType.SNAPSHOT;
        if (arguments != null && arguments.containsKey(ARG_VERSION)) {
            version = arguments.get(ARG_VERSION);
            type = VersionType.valueOf(arguments.get(ARG_TYPE));
        }
        arguments = null;
        versionsField.setSelection(new VersionOption(version, type));
    }

    private void createVersioningComponents() {
        commitButton = new CommitButton.WithLabel((event) -> saveBeforeCommitCheck());
        branchButton = new BranchButton.WithLabel((event) -> saveBeforeBranchCheck());

        versionsField = new ComboboxField<>(versionOptionType);
        versionOptionType.addValueChangedEventHandler(new ValueChangedEventHandler<ValueType>(this) {
            @Override
            public void onValueCanged(ValueChangedEvent<ValueType> event) {
                loadVersionDetails();
            }
        });
        versionsField.setWidth("200px");

        detailWindowPanel.addToToolbar(Align.LEFT, versionsField, commitButton, branchButton);
    }

    private void evaluateButtonStates() {
        boolean isSnapshotOrBranch = versionOptionType.getValue() != null && !versionOptionType.getValue().getType().isRelease();
        boolean isCreating = versionOptionType.getValue() == null && currentObject != null;
        boolean isFirst = table.getListDataProvider().getList().isEmpty();

        commitButton.setEnabled(isSnapshotOrBranch);
        saveButton.setEnabled(isSnapshotOrBranch || isCreating);
        cancelButton.setEnabled((isSnapshotOrBranch || isCreating) && !isFirst);
        removeButton.setEnabled(isSnapshotOrBranch || !isCreating);
        usedByButton.setEnabled(!isCreating);
        branchButton.setEnabled(!isSnapshotOrBranch && !isCreating);
        cloneButton.setEnabled(!isCreating);
    }

    private void saveBeforeCommitCheck() {
        saveBeforeAction(this::commit);
    }

    private void commit() {
        new CommitProcess(GWT.create(AccessControlRpcGateway.class), currentObject, toVersion ->
                loadVersions(() -> versionsField.setSelection(toVersion))).start();
    }

    private void saveBeforeBranchCheck() {
        saveBeforeAction(this::branch);
    }

    private void branch() {
        new BranchProcess(GWT.create(AccessControlRpcGateway.class), currentObject, toVersion ->
                loadVersions(() -> versionsField.setSelection(toVersion))).start();
    }

    private void prepareCopy() {
        saveBeforeAction(this::copy);
    }

    private void copy() {
        ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).duplicate(currentObject.getUuid(), currentObject.getVersion().getValue(), new DefaultAsyncCallback<AccessControlSummaryItem>() {
            @Override
            public void success(AccessControlSummaryItem summary) {
                table.add(summary);
                table.select(summary);
            }
        });
    }

    private void prepareCheck() {
        saveBeforeAction(this::check);
    }

    private void check() {
        CheckQuestionPanel checkQuestionPanel = new CheckQuestionPanel(username -> {
            PopupWindowPanel checkStartPanel = new PopupWindowPanel(KEY_CHECK_QUESTION, ACCESS_CONTROL_I18N.accessControlCheckTitle(), WindowPanel.Style.INFO);
            EntryForm entryForm = new EntryForm(null);
            entryForm.addEntry(new EntryWithDocumentation(null, ACCESS_CONTROL_I18N.accessControlCheckDocumentation()));
            checkStartPanel.addToBody(entryForm);

            OkButton.WithLabel okButton = new OkButton.WithLabel((event) -> {
                ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).check(currentObject.getUuid(), currentObject.getVersion().getValue(), username.getValue(), new DefaultAsyncCallback<CheckResult>() {
                    @Override
                    public void success(CheckResult result) {
                        checkStartPanel.close();

                        PopupWindowPanel popupWindowPanel = new PopupWindowPanel(KEY_CHECK_RESULT, ACCESS_CONTROL_I18N.accessControlCheckResultTitle(), result.isSuccess() ? WindowPanel.Style.INFO : WindowPanel.Style.ALERT);
                        EntryForm entryForm = new EntryForm(null);
                        entryForm.addEntry(new EntryWithDocumentation(null, result.getMessage()));
                        popupWindowPanel.addToBody(entryForm);

                        OkButton.WithLabel okButton = new OkButton.WithLabel((event) -> popupWindowPanel.close());
                        popupWindowPanel.addToFooter(Align.RIGHT, okButton);

                        popupWindowPanel.center(true, false);
                    }
                });
            });

            checkStartPanel.addToFooter(Align.RIGHT, okButton);
            checkStartPanel.center(true, false);
        });

        checkQuestionPanel.center(true, true);
    }

    public void refresh(final Trigger trigger) {
        ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).findSummaries(new DefaultAsyncCallback<ArrayList<AccessControlSummaryItem>>() {
            @Override
            public void success(ArrayList<AccessControlSummaryItem> result) {
                table.replaceAll(result);

                if (trigger != null) {
                    trigger.fire();
                }
            }
        });
    }

    private void delete() {
        new DeleteProcess(GWT.create(AccessControlRpcGateway.class), currentObject.getUuid(), currentObject.getVersion().getValue(), () -> {
            loadVersions(() -> {
                if (versionsField.getItems().isEmpty()) {
                    table.remove(table.getSelected());
                } else {
                    versionsField.setSelection(versionsField.getItems().get(0));
                }
            });
        }, () -> usedBy()).start();
    }

    public void saveBeforeAction(final Trigger trigger) {
        if (currentObject.hasChanges()) {
            UnsavedChangesQuestionPanel questionPanel = new UnsavedChangesQuestionPanel(() -> save(trigger));
            questionPanel.center(true, true);
        } else {
            trigger.fire();
        }
    }

    private void save(final Trigger afterSaveTrigger) {
        if (currentObject.hasChanges()) {
            ValidationUtil.validate(result -> {
                if (ValidationUtil.findWorstLevel(result) == ValidationMessage.Level.INFO) {
                    ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).save(currentObject, new DefaultAsyncCallback<AccessControlSummaryItem>() {
                        @Override
                        public void success(AccessControlSummaryItem summary) {
                            if (currentObject.getUuid() == null) {
                                table.add(summary);
                                table.select(summary);
                            } else {
                                table.replace(table.getSelected(), summary);
                            }

                            if (afterSaveTrigger != null) {
                                afterSaveTrigger.fire();
                            }
                        }
                    });
                }
            }, detailWindowPanel);
        }
    }

    private void add() {
        table.deselect();
        detailWindowPanel.clearBody();

        Scheduler.get().scheduleDeferred(() -> {
            currentObject = new AccessControl();
            detailWindowPanel.addToBody(currentObject.toEditForm());

            evaluateButtonStates();
        });
    }

    private void usedBy() {
        ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).findUsages(currentObject.getUuid(), currentObject.getVersion().getValue(), new DefaultAsyncCallback<ArrayList<UsedBy>>() {
            @Override
            public void success(ArrayList<UsedBy> result) {
                UsageWindow usageWindow = new UsageWindow(result, ACCESS_CONTROL_I18N.accessControlUsedByTitle(currentObject.getLabel().getValue()));
                usageWindow.setUsedByClickHandler(usedBy -> ProfileMenuItem.click(usedBy.getUuid(), usedBy.getVersion()));
                usageWindow.center(false, false);
            }
        });
    }

    private void loadVersions(final Trigger versionsLoadedTrigger) {
        AccessControlSummaryItem item = table.getSelected();
        if (item != null) {
            ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).versions(item.getUuid(), new DefaultAsyncCallback<List<VersionOption>>() {
                @Override
                public void success(List<VersionOption> result) {
                    versionsField.removeAllItems();
                    versionsField.addItems(result);
                    versionsLoadedTrigger.fire();
                }
            });
        } else {
            versionsField.removeAllItems();
            versionsLoadedTrigger.fire();
        }
    }

    private void loadVersionDetails() {
        VersionOption selectedVersion = versionsField.getSelectedItem();
        if (selectedVersion != null) {
            String uuid = table.getSelected().getUuid();

            ((AccessControlRpcGateway) GWT.create(AccessControlRpcGateway.class)).findSingle(uuid, selectedVersion, new DefaultAsyncCallback<AccessControl>() {
                @Override
                public void success(AccessControl result) {
                    currentObject = result;

                    detailWindowPanel.clearBody();
                    detailWindowPanel.addToBody(currentObject.toEditForm());

                    evaluateButtonStates();
                }
            });
        } else {
            detailWindowPanel.clearBody();
            evaluateButtonStates();
        }
    }

}