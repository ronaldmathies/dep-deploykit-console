package nl.sodeso.deploykit.console.client.application.security.role;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.IsValidationContainer;
import nl.sodeso.gwt.ui.client.form.validation.ValidationCompletedHandler;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationUtil;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.ROLE_I18N;

/**
 * @author Ronald Mathies
 */
public class Role implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_LABEL = "label";
    private static final String KEY_ROLE = "role";
    private static final String KEY_ROLE_GROUPS = "role-groups";
    private static final String KEY_ROLE_USERS = "role-users";

    private String uuid = null;

    private StringType label = new StringType();
//    private Permission permission = new Permission();

    private RoleGroupPermissions roleGroupPermissions = new RoleGroupPermissions();
    private RoleUsers roleUsers = new RoleUsers();

    private transient EntryForm entryForm = null;
    private transient TextField labelTextField = null;
    private transient LegendPanel roleGroupsPanel = null;
    private transient LegendPanel roleUsersPanel = null;

    public Role() {
    }

    public Widget toEditForm() {
        if (entryForm != null) {
            return entryForm;
        }

        labelTextField = new TextField<>(KEY_LABEL, this.label);
        labelTextField.addValidationRule(new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return labelTextField.getValue();
            }
        });

        LegendPanel rolePanel = new LegendPanel(KEY_ROLE, ROLE_I18N.rolePanel());
        rolePanel.add(
            new EntryForm(KEY_ROLE)
                .addEntries(
                    new EntryWithDocumentation(null, ROLE_I18N.roleDocumentation()),
                    new EntryWithLabelAndWidget(KEY_LABEL, ROLE_I18N.labelField(), labelTextField)));

        roleGroupsPanel = new LegendPanel(KEY_ROLE_GROUPS, ROLE_I18N.groupsAndPemissionsPanel());
        roleGroupsPanel.add(
            new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(null, ROLE_I18N.roleGroupsDocumentation()),
                    new EntryWithContainer(null, roleGroupPermissions.toEditForm())));

        roleUsersPanel = new LegendPanel(KEY_ROLE_USERS, ROLE_I18N.usersPanel());
        roleUsersPanel.add(
            new EntryForm(null)
                .addEntries(
                    new EntryWithDocumentation(null, ROLE_I18N.roleUsersDocumentation()),
                    new EntryWithContainer(null, roleUsers.toEditForm())));

        entryForm = new EntryForm(null)
            .addEntries(
                new EntryWithContainer(null, rolePanel),
                new EntryWithContainer(null, roleGroupsPanel),
                new EntryWithContainer(null, roleUsersPanel));

        labelTextField.setFocus();

        return entryForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, entryForm);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

//    public Permission getPermission() {
//        return permission;
//    }
//
//    public void setPermission(Permission permission) {
//        this.permission = permission;
//    }

    public RoleGroupPermissions getRoleGroupPermissions() {
        return roleGroupPermissions;
    }

    public void setRoleGroupPermissions(RoleGroupPermissions roleGroupPermissions) {
        this.roleGroupPermissions = roleGroupPermissions;
    }

    public RoleUsers getRoleUsers() {
        return roleUsers;
    }

    public void setRoleUsers(RoleUsers roleUsers) {
        this.roleUsers = roleUsers;
    }

    public int hashCode() {
        return uuid.hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof Role && ((Role) obj).getUuid().equals(uuid);
    }
}
