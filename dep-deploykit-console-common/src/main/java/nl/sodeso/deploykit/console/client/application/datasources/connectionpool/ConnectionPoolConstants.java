package nl.sodeso.deploykit.console.client.application.datasources.connectionpool;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface ConnectionPoolConstants extends Messages {

    @DefaultMessage("Connection Pools")
    String connectionPoolMenuItem();

    @DefaultMessage("Connection Pool")
    String connectionPoolPanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Java Server Type")
    String javaServerTypeField();

    @DefaultMessage("Connection Pool Properties")
    String connectionPoolPropertiesPanel();

    @DefaultMessage("Indicates the minimum number of connections a pool should hold. These are not created until a Subject is known from a request for a connection. (default 0)")
    String minimumPoolSizeDocumentation();

    @DefaultMessage("Minimum Pool Size")
    String minimumPoolSizeField();

    @DefaultMessage("Indicates the maximum number of connections for a pool. No more than connections will be created in each sub-pool. (default 20)")
    String maximumPoolSizeDocumentationJboss();

    @DefaultMessage("The maximum number of active connections that can be allocated from this pool at the same time, or negative for no limit. (default 8)")
    String maximumPoolSizeDocumentationJetty();

    @DefaultMessage("Maximum Pool Size")
    String maximumPoolSizeField();

    @DefaultMessage("The initial number of connections that are created when the pool is started. (default 0)")
    String initialPoolSizeDocumentation();

    @DefaultMessage("Initial Pool Size")
    String initialPoolSizeField();

    @DefaultMessage("Whether to attempt to prefill the connection pool. (default no)")
    String prefillDocumentation();

    @DefaultMessage("Prefill")
    String prefillField();

    @DefaultMessage("The default TransactionIsolation state of connections created by this pool. One of the following: (see <a target=&quot;_blank&quit; href=&quot;http://java.sun.com/j2se/1.4.2/docs/api/java/sql/Connection.html#field_summary&quot;>javadoc</a>)")
    String transactionIsolationDocumentation();

    @DefaultMessage("Default Transaction Isolation")
    String transactionIsolationField();

    @DefaultMessage("A sql statement that is executed before the connection is checked out from the pool to make sure it is still valid. If the sql fails, the connection is closed and a new one created. Also it will be used by the background validation.")
    String validationQueryDocumentationJboss();

    @DefaultMessage("Validation query")
    String validationQueryFieldJboss();

    @DefaultMessage("A sql statement that is executed against each new connection. This can be used to set the connection schema, etc.")
    String initSqlStatementDocumentation();

    @DefaultMessage("Init SQL statement")
    String initSqlStatementField();

    @DefaultMessage("Timeout Settings")
    String timeoutSettingsPanel();

    @DefaultMessage("Indicates the maximum time in milliseconds to block while waiting for a connection before throwing an exception. Note that this blocks only while waiting for a permit for a connection, and will never throw an exception if creating a new connection takes an inordinately long time. (default 30000)")
    String blockingTimeoutMillisecondsDocumentation();

    @DefaultMessage("Blocking Timeout Milliseconds")
    String blockingTimeoutMillisecondsField();

    @DefaultMessage("Indicates the maximum time in minutes a connection may be idle before being closed. The actual maximum time depends also on the Idle Remover scan time, which is 1/2 the smallest Idle Timeout Milliseconds of any pool. (default empty)")
    String idleTimeoutMinutesDocumentation();

    @DefaultMessage("Idle Timeout Minutes")
    String idleTimeoutMinutesField();

    @DefaultMessage("Indicates the number of times that allocating a connection should be tried before throwing an exception. (default 0)")
    String allocationRetryAttemtsDocumentation();

    @DefaultMessage("Allocation Retry Attempts")
    String allocationRetryAttemtsField();

    @DefaultMessage("The allocation retry wait millis element indicates the time in milliseconds to wait between retrying to allocate a connection. (default 5000)")
    String allocationRetryWaitDocumentation();

    @DefaultMessage("Allocation Retry Wait Milliseconds")
    String allocationRetryWaitField();

    @DefaultMessage("The maximum number of connections that can remain idle in the pool, without extra ones being released, or negative for no limit. (default 8)<br/><br/><b>NOTE:</b> If maxIdle is set too low on heavily loaded systems it is possible you will see connections being closed and almost immediately new connections being opened. This is a result of the active threads momentarily closing connections faster than they are opening them, causing the number of idle connections to rise above maxIdle. The best value for maxIdle for heavily loaded system will vary but the default is a good starting point.")
    String maximumIdleDocumentation();

    @DefaultMessage("Maximum Idle")
    String maximumIdleField();

    @DefaultMessage("The minimum number of connections that can remain idle in the pool, without extra ones being created, or zero to create none. (default 8)")
    String minimumIdleDocumentation();

    @DefaultMessage("Minimum Idle")
    String minimumIdleField();

    @DefaultMessage("The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception, or -1 to wait indefinitely. (default -1)")
    String maximumWaitDocumentation();

    @DefaultMessage("Maximum Wait")
    String maximumWaitField();

    @DefaultMessage("Milliseconds")
    String maximumWaitFieldHint();

    @DefaultMessage("Connection Properties")
    String connectionPropertiesPanel();

    @DefaultMessage("The SQL query that will be used to validate connections from this pool before returning them to the caller. If specified, this query <b>MUST</b> be an SQL SELECT statement that returns at least one row. If not specified, connections will be validation by calling the isValid() method.")
    String validationQueryDocumentationJetty();

    @DefaultMessage("Validation query")
    String validationQueryFieldJetty();

    @DefaultMessage("The indication of whether objects will be validated after creation. If the object fails to validate, the borrow attempt that triggered the object creation will fail. (default no)")
    String testOnCreateDocumentation();

    @DefaultMessage("Test On Create")
    String testOnCreateField();

    @DefaultMessage("The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and we will attempt to borrow another. (default yes)")
    String testOnBorrowDocumentation();

    @DefaultMessage("Test On Borrow")
    String testOnBorrowField();

    @DefaultMessage("The indication of whether objects will be validated before being returned to the pool. (default no)")
    String testOnReturnDocumentation();

    @DefaultMessage("Test On Return")
    String testOnReturnField();

    @DefaultMessage("The indication of whether objects will be validated by the idle object evictor (if any). If an object fails to validate, it will be dropped from the pool. (default no)")
    String testWhileIdleDocumentation();

    @DefaultMessage("Test While Idle")
    String testWhileIdleField();

    @DefaultMessage("The number of milliseconds to sleep between runs of the idle object evictor thread. When non-positive, no idle object evictor thread will be run. (default -1)")
    String timeBetweenEvictionRunsDocumentation();

    @DefaultMessage("Time Between Eviction Runs")
    String timeBetweenEvictionRunsField();

    @DefaultMessage("The number of objects to examine during each run of the idle object evictor thread (if any). (default 3)")
    String runTestBetweenEvictionRunDocumentation();

    @DefaultMessage("Run Tests Between Eviction Run")
    String runTestBetweenEvictionRunField();

    @DefaultMessage("The minimum amount of time an object may sit idle in the pool before it is eligable for eviction by the idle object evictor (if any). (default 1800000)")
    String minEvictableIdleTimeDocumentation();

    @DefaultMessage("Min Evictable Idle Time")
    String minEvictableIdleTimeField();

    @DefaultMessage("The minimum amount of time a connection may sit idle in the pool before it is eligible for eviction by the idle connection evictor, with the extra condition that at least &quit;Minimum Idle&quot; connections remain in the pool. When Min Evictable Idle Time Millis  is set to a positive value, Min Evictable Idle Time Millis is examined first by the idle connection evictor - i.e. when idle connections are visited by the evictor, idle time is first compared against Min Evictable Idle Time Millis (without considering the number of idle connections in the pool) and then against Soft Min Evictable Idle Time Millis, including the minIdle constraint. (Default -1)")
    String softMinEvictableIdleTimeDocumentation();

    @DefaultMessage("Soft Mini Evictable Idle Time")
    String softMinEvictableIdleTimeField();

    @DefaultMessage("The maximum lifetime in milliseconds of a connection. After this time is exceeded the connection will fail the next activation, passivation or validation test. A value of zero or less means the connection has an infinite lifetime. (Default -1)")
    String maxConnectionLifetimeDocumentation();

    @DefaultMessage("Max Connection Lifetime")
    String maxConnectionLifetimeField();

    @DefaultMessage("Indicator to log a message indicating that a connection is being closed by the pool due to maxConnLifetimeMillis exceeded. Set this property to false to suppress expired connection logging that is turned on by default.")
    String logExpiredConnectionsDocumentation();

    @DefaultMessage("Log Expired Connections")
    String logExpiredConnectionsField();

    @DefaultMessage("A Collection of SQL statements that will be used to initialize physical connections when they are first created. These statements are executed only once - when the configured connection factory creates the connection.")
    String initSqlStatementsDocumentation();

    @DefaultMessage("Init SQL statements")
    String initSqlStatementsField();

    @DefaultMessage("True means that borrowObject returns the most recently used (&quit;last in&quot;) connection in the pool (if there are idle connections available). False means that the pool behaves as a FIFO queue - connections are taken from the idle instance pool in the order that they are returned to the pool.")
    String lifoDocumentation();

    @DefaultMessage("Lifo")
    String lifoField();

    @DefaultMessage("Transaction Properties")
    String transactionPropertiesPanel();

    @DefaultMessage("The default auto-commit state of connections created by this pool. If not set then the setAutoCommit method will not be called.")
    String defaultAutoCommitDocumentation();

    @DefaultMessage("Default Auto Commit")
    String defaultAutoCommitField();

    @DefaultMessage("The default read-only state of connections created by this pool. If not set then the setReadOnly method will not be called. (Some drivers don&apos;t support read only mode, ex: Informix)")
    String defaultReadOnlyDocumentation();

    @DefaultMessage("Default Read Only")
    String defaultReadOnlyField();

    @DefaultMessage("The default TransactionIsolation state of connections created by this pool. One of the following: (see <a target=&quot;_blank&quot; href=&quot;http://java.sun.com/j2se/1.4.2/docs/api/java/sql/Connection.html#field_summary&quot;>javadoc</a>)")
    String defaultTransactionIsolationDocumentation();

    @DefaultMessage("Default Transaction Isolation")
    String defaultTransactionIsolationField();

    @DefaultMessage("The default catalog of connections created by this pool.")
    String defaultCatalogDocumentation();

    @DefaultMessage("Default Catalog")
    String defaultCatalogField();

    @DefaultMessage("If true, the pooled connection will cache the current readOnly and autoCommit settings when first read or written and on all subsequent writes. This removes the need for additional database queries for any further calls to the getter. If the underlying connection is accessed directly and the readOnly and/or autoCommit settings changed the cached values will not reflect the current state. In this case, caching should be disabled by setting this attribute to false.")
    String cacheStateDocumentation();

    @DefaultMessage("Cache State")
    String cacheStateField();

    @DefaultMessage("Connection Pool {0} Used By")
    String connectionPoolUsedByTitle(String name);
}