package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.input.WidgetCollectionField;
import nl.sodeso.gwt.ui.client.form.input.WidgetSupplier;

/**
 * @author Ronald Mathies
 */
public class ProfileWebservices extends WidgetCollectionField<ProfileWebservice> {

    private OptionType<DefaultOption> groupOptionType = null;

    private ProfileWebservices() {}

    public ProfileWebservices(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    @Override
    public WidgetSupplier<ProfileWebservice> createWidgetSupplier() {
        return callback -> {
            ProfileWebservice profileWebservice = new ProfileWebservice();
            profileWebservice.setGroup(groupOptionType);
            callback.created(profileWebservice);
        };
    }
}
