package nl.sodeso.deploykit.console.client.application.profile;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.resources.Icon;

import static nl.sodeso.deploykit.console.client.application.Resources.PROFILE_I18N;

/**
 * @author Ronald Mathies
 */
public class ProfileMenuItem extends MenuItem {

    private static final String KEY = "mi-profile";

    private static Profiles profiles = null;

    public ProfileMenuItem() {
        super(KEY, Icon.Tasks, PROFILE_I18N.profilesMenuItem(), arguments -> {
            if (profiles == null) {
                profiles = new Profiles();
            }

            profiles.setArguments(arguments);
            CenterController.instance().setWidget(profiles);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
