package nl.sodeso.deploykit.console.client.application.guide.steps;

import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.wizard.AbstractWizardStep;
/**
 * @author Ronald Mathies
 */
public class SetupCredentialsStep extends AbstractWizardStep {

    public SetupCredentialsStep(AbstractWizardStep callingStep) {
        super(callingStep);
    }

    @Override
    public AbstractWizardStep getPreviousStep() {
        return getCallingStep();
    }

    @Override
    public AbstractWizardStep getNextStep() {
        return null;
    }

    @Override
    public Widget getBodyWidget() {
        LegendPanel legendPanel = new LegendPanel("setup-credentials-step", "Setup LdapServices");
        EntryForm entryForm = new EntryForm("form");

        entryForm.addEntry(new EntryWithDocumentation(null,
                "In the credentials configuration you can register all the credentials that<br/>" +
                "can be used for various components like:<br/>" +
                "<ul>" +
                "   <li>SSL Configuration like (keystore, truststore and key manager passwords)</li>" +
                "   <li>LdapServices for data sources</li>" +
                "   <li>WS-Security (WSSEC) credentials.</li>" +
                "   <li>Resource files (property configuration files) using placeholders.</li>" +
                "</ul>" +
                "The <i>Placeholder Prefix</i> is used to create the various placeholders which<br/>" +
                "can be used during the provisioning to replace the same placeholder within<br/>" +
                "the configuration files."));

        legendPanel.add(entryForm);

        legendPanel.addAttachHandler(event -> CredentialMenuItem.click(null, null));

        return legendPanel;
    }
}
