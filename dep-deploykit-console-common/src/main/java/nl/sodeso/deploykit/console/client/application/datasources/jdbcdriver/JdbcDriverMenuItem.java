package nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.JDBC_DRIVER_I18N;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverMenuItem extends MenuItem {

    private static final String KEY = "mi-jdbcdriver";

    private static JdbcDrivers jdbcDrivers = null;

    public JdbcDriverMenuItem() {
        super(KEY, JDBC_DRIVER_I18N.jdbcDriverMenuItem(), arguments -> {
            if (jdbcDrivers == null) {
                jdbcDrivers = new JdbcDrivers();
            }

            jdbcDrivers.setArguments(arguments);
            CenterController.instance().setWidget(jdbcDrivers);
        });
    }

    public static void click(String uuid, VersionOption option) {
        HistoryHelper.click(KEY, uuid, option);
    }

}
