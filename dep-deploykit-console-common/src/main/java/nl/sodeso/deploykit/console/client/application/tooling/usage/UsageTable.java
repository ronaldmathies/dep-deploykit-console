package nl.sodeso.deploykit.console.client.application.tooling.usage;

import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.TextColumn;
import nl.sodeso.gwt.ui.client.form.table.CellTableField;
import nl.sodeso.gwt.ui.client.form.table.DefaultEmptyTableMessage;
import nl.sodeso.gwt.ui.client.form.table.DefaultTableCellGroupHeader;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class UsageTable extends CellTableField<UsedBy> {

    public UsageTable() {
        super("usedby-table", 10);

        this.setTableBuilder(new DefaultTableCellGroupHeader<UsedBy>(this, getListDataProvider()) {
            @Override
            public String getHeaderText(UsedBy object) {
                return object.getCategory();
            }
        });

        // Add a column for displaying the labels.
        TextColumn<UsedBy> column = new TextColumn<UsedBy>() {
            @Override
            public String getValue(UsedBy object) {
                return object.getLabel() + (object.getVersion() != null ? " " + object.getVersion().getDisplayDescription() : "");
            }
        };
        this.addColumn(column);

        // Create a sort handler.
        ColumnSortEvent.ListHandler<UsedBy> listHandler = new ColumnSortEvent.ListHandler<>(getListDataProvider().getList());
        listHandler.setComparator(column, (o1, o2) -> o1.getCategory().compareTo(o2.getCategory()));
        this.addColumnSortHandler(listHandler);

        // Set the default sorting to ascending.
        column.setDefaultSortAscending(true);

        // Set the sorting order of the cell list to the first and only column.
        ColumnSortList.ColumnSortInfo columnSortInfo = new ColumnSortList.ColumnSortInfo(column, true);
        this.getColumnSortList().push(columnSortInfo);

        this.setColumnWidth(0, 100, com.google.gwt.dom.client.Style.Unit.PCT);
        this.setEmptyTableWidget(new DefaultEmptyTableMessage("not-used", "Not in use by any component."));
    }

    public void setItems(List<UsedBy> list) {
        this.getListDataProvider().setList(list);
    }

}
