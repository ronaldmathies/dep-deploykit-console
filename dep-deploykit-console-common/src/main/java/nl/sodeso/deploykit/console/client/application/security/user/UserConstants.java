package nl.sodeso.deploykit.console.client.application.security.user;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface UserConstants extends Messages {

    @DefaultMessage("Users")
    String userMenuItem();

    @DefaultMessage("User")
    String userPanel();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("The username should be the same as the username that the users uses to login into the DeployKit application.")
    String usernameDocumentation();

    @DefaultMessage("Username")
    String usernameField();

    @DefaultMessage("Password")
    String passwordField();

    @DefaultMessage("Password Verify")
    String passwordVerifyField();

    @DefaultMessage("User {0} Used By")
    String userUsedByTitle(String name);

    @DefaultMessage("Roles")
    String rolesPanel();

    @DefaultMessage("Add roles for which this user is allowed to perform actions on, a user can have as many roles as needed, if there is any overlap within the roles, for example, different roles defined for the same group with different permissions then the permissions are combined where each individual permission is checked for being positive.")
    String rolesDocumentation();
}