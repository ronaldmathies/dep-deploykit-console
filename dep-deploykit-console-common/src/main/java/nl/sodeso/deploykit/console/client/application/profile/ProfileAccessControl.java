package nl.sodeso.deploykit.console.client.application.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlMenuItem;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc.AccessControlRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class ProfileAccessControl implements Serializable, IsValidationContainer, IsRevertable  {

    private BooleanType useAccessControl = new BooleanType(false);
    private OptionType<DefaultOption> groupOptionType = null;
    private OptionType<DefaultOption> accessControlOptionType = new OptionType<>();
    private VersionOptionType accessControlVersionType = new VersionOptionType();

    private transient EntryForm editForm = null;
//    private transient GroupComboboxField groupField = null;
    private transient RadioButtonGroupField useAccessControlGroupField = null;
    private transient VersionSelectionField<AccessControlRpcGateway> accessControlSelectionField = null;

    private ProfileAccessControl() {}

    public ProfileAccessControl(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public Widget toEditForm() {
        if (this.editForm != null) {
            return this.editForm;
        }

        useAccessControlGroupField = new RadioButtonGroupField("use-access-control", this.useAccessControl);
        useAccessControlGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useAccessControlGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useAccessControlGroupField.setAlignment(Align.HORIZONTAL);
        useAccessControlGroupField.addClickHandler(event -> {
            boolean visible = useAccessControl.getValue();
            editForm.setEntriesVisible(visible, "group", "configuration");

            if (!visible) {
                groupOptionType.setValue(null);
                accessControlOptionType.setValue(null);
            }
        });

//        groupField = new GroupComboboxField("group", groupOptionType);

        accessControlSelectionField = new VersionSelectionField<>(GWT.create(AccessControlRpcGateway.class), accessControlOptionType, accessControlVersionType);
        accessControlSelectionField.listenToGroup(groupOptionType);

        EntryWithLabelAndWidget configurationEntry = new EntryWithLabelAndWidget("configuration", "Configuration",
                new HorizontalPanel()
                        .addWidget(accessControlSelectionField, 100.0, Style.Unit.PCT)
                        .addWidget(new SearchButton.WithIcon((event) -> AccessControlMenuItem.click(accessControlOptionType.getValue().getKey(), accessControlVersionType.getValue()))));

        configurationEntry.addValidationRule(
                new ValidationCriteria.or(new ComboboxMandatoryValidationRule("access-group", ValidationMessage.Level.ERROR, groupOptionType),
                        new ValidationCriteria.or(
                                new ComboboxMandatoryValidationRule("access-option", ValidationMessage.Level.ERROR, accessControlOptionType),
                                new ComboboxMandatoryValidationRule("access-version", ValidationMessage.Level.ERROR, accessControlVersionType)
                        )
                ) {
                    @Override
                    public boolean isApplicable() {
                        return useAccessControl.getValue();
                    }
                }
        );


        this.editForm = new EntryForm("profile-ldap-access-control-form")
            .addEntries(
                    new EntryWithDocumentation("use-access-control", "Enables Access Control configuration for the application."),
                    new EntryWithLabelAndWidget(null, "Use Access Control", this.useAccessControlGroupField),
//                    new EntryWithLabelAndWidget("group", "Group", groupField),
                    configurationEntry);

        this.editForm.addAttachHandler(event -> {
            if (event.isAttached()) {
                useAccessControlGroupField.fireClickEvent();
            }
        });

        return this.editForm;
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.editForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, editForm);
    }

    public BooleanType getUseAccessControl() {
        return useAccessControl;
    }

    public void setUseAccessControl(BooleanType useAccessControl) {
        this.useAccessControl = useAccessControl;
    }

    public OptionType<DefaultOption> getGroup() {
        return groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> groupOptionType) {
        this.groupOptionType = groupOptionType;
    }

    public OptionType<DefaultOption> getAccessControlOption() {
        return accessControlOptionType;
    }

    public void setAccessControlOption(OptionType<DefaultOption> ldapAccessControlOptionType) {
        this.accessControlOptionType = ldapAccessControlOptionType;
    }

    public VersionOptionType getAccessControlVersion() {
        return accessControlVersionType;
    }

    public void setAccessControlVersion(VersionOptionType ldapAccessControlVersionType) {
        this.accessControlVersionType = ldapAccessControlVersionType;
    }
}
