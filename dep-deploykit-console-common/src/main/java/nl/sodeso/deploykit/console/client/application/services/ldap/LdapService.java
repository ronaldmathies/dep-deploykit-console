package nl.sodeso.deploykit.console.client.application.services.ldap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.rules.PortRangeValidationRule;
import nl.sodeso.deploykit.console.client.application.services.ldap.rpc.LdapServiceRpcGateway;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.LDAP_SERVICE_I18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class LdapService extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GENERAL = "general";
    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_PLACEHOLDER_PREFIX = "placeholder-prefix";

    private static final String KEY_SERVER_SETTINGS = "server-settings";
    private static final String KEY_DOMAIN = "domain";
    private static final String KEY_PORT = "port";

    private static final String KEY_SECURITY_SETTINGS = "security-settings";
    private static final String KEY_USE_SECURITY = "use-security";
    private static final String KEY_CREDENTIALS = "credentials";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";


    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private StringType placeholderPrefix = new StringType();

    private StringType domain = new StringType();
    private IntType port = new IntType(389);

    private BooleanType useCredentials = new BooleanType(false);
    private OptionType<DefaultOption> credentialOptionType = new OptionType<>();
    private VersionOptionType credentialVersionType = new VersionOptionType();

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;
    private transient TextField placeholderPrefixField = null;

    private transient LegendPanel serverLegendPanel = null;
    private transient TextField domainTextField = null;
    private transient TextField portTextField = null;

    private transient LegendPanel securityLegendPanel = null;
    private transient RadioButtonGroupField useCredentialsGroupField = null;
    private transient VersionSelectionField<CredentialRpcGateway> credentialSelectionField = null;

    public LdapService() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupGeneralSettings();
        setupServerSettings();
        setupSecuritySettings();

        this.entryForm = new EntryForm(null);
        this.entryForm.addEntry(new EntryWithContainer(null, this.generalLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.serverLegendPanel));
        this.entryForm.addEntry(new EntryWithContainer(null, this.securityLegendPanel));

        this.labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupGeneralSettings() {

        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        placeholderPrefixField = new TextField<>(KEY_PLACEHOLDER_PREFIX, this.placeholderPrefix);
        placeholderPrefixField.addValidationRule(new MandatoryValidationRule(KEY_PLACEHOLDER_PREFIX, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return placeholderPrefixField.getValue();
            }
        });

        EntryForm form = new EntryForm(KEY_GENERAL)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, LDAP_SERVICE_I18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, LDAP_SERVICE_I18N.labelField(), labelTextField)
                    .addValidationRules(
                        new LabelUniqueValidationRule(GWT.create(HttpsRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return LdapService.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return LdapService.this.getLabel();
                            }
                        }
                    ),
                new EntryWithLabelAndWidget(KEY_PLACEHOLDER_PREFIX, LDAP_SERVICE_I18N.placeholderPrefixField(), placeholderPrefixField));


        this.generalLegendPanel = new LegendPanel(KEY_GENERAL, LDAP_SERVICE_I18N.ldapServicePanel());
        this.generalLegendPanel.add(form);
    }

    private void setupServerSettings() {

        domainTextField = new TextField<>(KEY_DOMAIN, this.domain);
        domainTextField.addValidationRule(new MandatoryValidationRule(KEY_DOMAIN, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return domainTextField.getValue();
            }
        });


        portTextField = new TextField<>(KEY_PORT, this.port);
        portTextField.addValidationRule(new PortRangeValidationRule(KEY_PORT, port));
        portTextField.addFilter(new NumericInputFilter(false));

        EntryForm form = new EntryForm(KEY_SERVER_SETTINGS)
                .addEntries(
                    new EntryWithDocumentation(KEY_DOMAIN, LDAP_SERVICE_I18N.domainNameDocumentation()),
                    new EntryWithLabelAndWidget(KEY_DOMAIN, LDAP_SERVICE_I18N.domainNameField(), new VerticalPanel(domainTextField,
                        new PlaceholderSpanField(KEY_DOMAIN, placeholderPrefix, LdapServicePlaceHolderSuffix.LDAP_DOMAIN)))
                        .setHint("ie. 'ldap.organization.com'")
                        .addValidatableWidget(domainTextField),
                    new EntryWithLabelAndWidget(KEY_PORT, LDAP_SERVICE_I18N.portField(), new VerticalPanel(portTextField,
                        new PlaceholderSpanField(KEY_PORT, placeholderPrefix, LdapServicePlaceHolderSuffix.LDAP_PORT)))
                        .setHint("1 - 65535 (default 389)")
                        .addValidatableWidget(portTextField));

        serverLegendPanel = new LegendPanel(KEY_SERVER_SETTINGS, LDAP_SERVICE_I18N.ldapServerPanel());
        serverLegendPanel.add(form);
    }

    private void setupSecuritySettings() {
        useCredentialsGroupField = new RadioButtonGroupField(KEY_USE_SECURITY, this.useCredentials);
        useCredentialsGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useCredentialsGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useCredentialsGroupField.setAlignment(Align.HORIZONTAL);

        credentialSelectionField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), credentialOptionType, credentialVersionType);
        credentialSelectionField.listenToGroup(groupOptionType);

        EntryWithLabelAndWidget entry = new EntryWithLabelAndWidget(KEY_CREDENTIALS, LDAP_SERVICE_I18N.credentialField(),
                new VerticalPanel(
                        new HorizontalPanel()
                                .addWidget(credentialSelectionField, 100.0, Style.Unit.PCT)
                                .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(credentialOptionType.getValue().getKey(), credentialVersionType.getValue()))),
                        new PlaceholderSpanField(KEY_USERNAME, placeholderPrefix, LdapServicePlaceHolderSuffix.LDAP_USERNAME),
                        new PlaceholderSpanField(KEY_PASSWORD, placeholderPrefix, LdapServicePlaceHolderSuffix.LDAP_PASSWORD)));
        entry.addValidationRule(
                new ValidationCriteria.or(
                        new ComboboxMandatoryValidationRule(KEY_CREDENTIALS, ValidationMessage.Level.ERROR, credentialOptionType),
                        new ComboboxMandatoryValidationRule(KEY_CREDENTIALS, ValidationMessage.Level.ERROR, credentialVersionType)
                ) {

                    @Override
                    public boolean isApplicable() {
                        if (useCredentials.getValue()) {
                            return super.isApplicable();
                        }

                        return false;
                    }
                }
        );

        EntryForm form = new EntryForm(KEY_SECURITY_SETTINGS)
            .addEntries(
                    new EntryWithDocumentation(KEY_USE_SECURITY, LDAP_SERVICE_I18N.useSecurityDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USE_SECURITY, LDAP_SERVICE_I18N.useSecurityField(), useCredentialsGroupField),
                    entry);

        useCredentialsGroupField.addClickHandler(event -> {
            boolean visible = useCredentials.getValue();
            form.setEntriesVisible(visible, KEY_CREDENTIALS);

            if (!visible) {
                credentialOptionType.setValue(null);
            }
        });

        securityLegendPanel = new LegendPanel(KEY_SECURITY_SETTINGS, LDAP_SERVICE_I18N.ldapServerSecurityPanel());
        securityLegendPanel.add(form);

//        securityLegendPanel.addAttachHandler(event -> {
//            if (event.isAttached()) {
//                credentialSelectionField.refresh(groupOptionType);
//            }
//        });

        form.addAttachHandler(event -> {
            if (event.isAttached()) {
                useCredentialsGroupField.fireClickEvent();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getDomain() {
        return domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public BooleanType getUseCredentials() {
        return useCredentials;
    }

    public void setUseCredentials(BooleanType useCredentials) {
        this.useCredentials = useCredentials;
    }

    public OptionType<DefaultOption> getCredential() {
        return credentialOptionType;
    }

    public void setCredential(OptionType<DefaultOption> credentialOptionType) {
        this.credentialOptionType = credentialOptionType;
    }

    public VersionOptionType getCredentialVersion() {
        return credentialVersionType;
    }

    public void setCredentialVersion(VersionOptionType credentialVersionType) {
        this.credentialVersionType = credentialVersionType;
    }
}