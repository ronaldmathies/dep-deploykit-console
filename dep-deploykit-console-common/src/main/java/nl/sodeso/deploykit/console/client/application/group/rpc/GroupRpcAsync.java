package nl.sodeso.deploykit.console.client.application.group.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import nl.sodeso.deploykit.console.client.application.group.Group;
import nl.sodeso.deploykit.console.client.application.group.GroupSummaryItem;
import nl.sodeso.deploykit.console.client.application.tooling.delete.rpc.DeleteRpcAsync;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
public interface GroupRpcAsync extends DeleteRpcAsync {

    void findSummaries(AsyncCallback<ArrayList<GroupSummaryItem>> result);
    void findSingle(String uuid, AsyncCallback<Group> result);
    void asOptions(AsyncCallback<ArrayList<DefaultOption>> options);

    void usage(String uuid, AsyncCallback<ArrayList<UsedBy>> result);
    void save(Group contact, AsyncCallback<GroupSummaryItem> result);
    void duplicate(String uuid, AsyncCallback<GroupSummaryItem> result);

}
