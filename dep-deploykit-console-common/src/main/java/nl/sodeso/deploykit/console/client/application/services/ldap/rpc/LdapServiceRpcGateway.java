package nl.sodeso.deploykit.console.client.application.services.ldap.rpc;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.rpc.RpcGateway;

/**
 * @author Ronald Mathies
 */
public abstract class LdapServiceRpcGateway implements RpcGateway<LdapServiceRpcAsync>, LdapServiceRpcAsync {

    /**
     * {@inheritDoc}
     */
    public LdapServiceRpcAsync getRpcAsync() {
        return GWT.create(LdapServiceRpc.class);
    }
}
