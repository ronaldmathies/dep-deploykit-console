package nl.sodeso.deploykit.console.client.application.ui.options;

import nl.sodeso.gwt.ui.client.form.combobox.OptionType;

/**
 * @author Ronald Mathies
 */
public class VersionOptionType extends OptionType<VersionOption> {

    public VersionOptionType() {}

    public VersionOptionType(VersionOption value) {
        super(value);
    }

    public boolean isSnapshot() {
        return getValue().getType().isSnapshot();
    }

    public boolean isBranch() {
        return getValue().getType().isBranch();
    }

    public boolean isRelease() {
        return getValue().getType().isRelease();
    }

}
