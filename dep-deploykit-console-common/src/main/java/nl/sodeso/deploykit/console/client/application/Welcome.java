package nl.sodeso.deploykit.console.client.application;

import nl.sodeso.deploykit.console.client.application.guide.GuideWindow;
import nl.sodeso.gwt.ui.client.controllers.center.AbstractWelcomeCenterPanel;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithSingleWidget;
import nl.sodeso.gwt.ui.client.form.button.SimpleButton;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.panel.WindowPanel;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.trigger.Trigger;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * @author Ronald Mathies
 */
public class Welcome extends AbstractWelcomeCenterPanel {

    @Override
    public void beforeAddWelcomePanel(Trigger trigger) {
        VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.addWidget(createWelcomePanel());

        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.addWidgets(createViewAndManageSettingsPanel(), createRecentChangesPanel());

        verticalPanel.addWidget(horizontalPanel);
        add(verticalPanel);

        trigger.fire();
    }

    private WindowPanel createWelcomePanel() {
        WindowPanel welcomeWindow = new WindowPanel("welcome", WindowPanel.Style.INFO);
        LegendPanel welcomeLegenPanel = new LegendPanel("welcome-legend", "Getting Started With DeployKit Console");
        EntryForm entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithDocumentation(null, "DeployKit Console allows you to configure application / application server settings in an organized and centralised manner to keep maintenance low and manageable. "));
        welcomeLegenPanel.add(entryForm);
        welcomeWindow.addToBody(welcomeLegenPanel);
        return welcomeWindow;
    }

    private WindowPanel createViewAndManageSettingsPanel() {
        WindowPanel quickSettingsWindow = new WindowPanel("view-and-manage-settings", WindowPanel.Style.INFO);
        EntryForm entryForm = new EntryForm(null);

        entryForm.addEntry(new EntryWithDocumentation(null, "To follow a step-by-step guide on how to get started simply use the following link. This will present you with a guided tour which you can follow to do an initial setup of the environment and to start creating a new profile which can be used to provision a server."));
        SimpleButton stepByStepButton = new SimpleButton("step-by-step", "Step By Step Guide", Icon.ArrowCircleRightO, SimpleButton.Style.NONE, Align.RIGHT);
        stepByStepButton.addClickHandler((event) -> {
            backToActiveMenuItem();
            GuideWindow.instance().resetWizard();
            GuideWindow.instance().setVisible(true);
        });
        entryForm.addEntry(new EntryWithSingleWidget(null, stepByStepButton));

        entryForm.addEntry(new EntryWithDocumentation(null, "To directly jump into the configuration in general then using link below."));
        SimpleButton showAllSettingsButton = new SimpleButton("show-all-settings", "Show All Settings", Icon.ArrowCircleRightO, SimpleButton.Style.NONE, Align.RIGHT);
        showAllSettingsButton.addClickHandler((event) -> backToActiveMenuItem());
        entryForm.addEntry(new EntryWithSingleWidget(null, showAllSettingsButton));

        LegendPanel quickSettingsLegendPanel = new LegendPanel("quick-settings", "Manage Configuration");
        quickSettingsLegendPanel.add(entryForm);
        quickSettingsWindow.addToBody(quickSettingsLegendPanel);
        return quickSettingsWindow;
    }

    private WindowPanel createRecentChangesPanel() {
        WindowPanel recentChangesWindow = new WindowPanel("recent-changes", WindowPanel.Style.INFO);
        EntryForm entryForm = new EntryForm(null);




        LegendPanel recentChangesPanel = new LegendPanel("recent-changes", "Recent Changes");
        recentChangesPanel.add(entryForm);

        recentChangesWindow.addToBody(recentChangesPanel);
        return recentChangesWindow;
    }

}
