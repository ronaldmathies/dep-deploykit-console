package nl.sodeso.deploykit.console.client.application.security.user;

import nl.sodeso.deploykit.console.client.application.ui.menu.HistoryHelper;
import nl.sodeso.gwt.ui.client.controllers.center.CenterController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;

import static nl.sodeso.deploykit.console.client.application.Resources.USER_I18N;

/**
 * @author Ronald Mathies
 */
public class UserMenuItem extends MenuItem {

    private static final String KEY = "mi-users";

    private static Users users = null;

    public UserMenuItem() {
        super(KEY, USER_I18N.userMenuItem(), arguments -> {
            if (users == null) {
                users = new Users();
            }

            users.setArguments(arguments);
            CenterController.instance().setWidget(users);
        });
    }

    public static void click(String uuid) {
        HistoryHelper.click(KEY, uuid);
    }
}
