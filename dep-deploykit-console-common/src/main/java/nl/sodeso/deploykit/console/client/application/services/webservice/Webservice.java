package nl.sodeso.deploykit.console.client.application.services.webservice;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.Widget;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpcGateway;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpcGateway;
import nl.sodeso.deploykit.console.client.application.group.GroupComboboxField;
import nl.sodeso.deploykit.console.client.application.rules.LabelChangeWarningValidationRule;
import nl.sodeso.deploykit.console.client.application.rules.PortRangeValidationRule;
import nl.sodeso.deploykit.console.client.application.services.webservice.rpc.WebserviceRpcGateway;
import nl.sodeso.deploykit.console.client.application.tooling.versioning.Versionable;
import nl.sodeso.deploykit.console.client.application.ui.options.UrlProtocolComboboxField;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionSelectionField;
import nl.sodeso.deploykit.console.client.application.ui.placeholder.PlaceholderSpanField;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.validation.LabelUniqueValidationRule;
import nl.sodeso.gwt.ui.client.form.EntryForm;
import nl.sodeso.gwt.ui.client.form.EntryWithContainer;
import nl.sodeso.gwt.ui.client.form.EntryWithDocumentation;
import nl.sodeso.gwt.ui.client.form.EntryWithLabelAndWidget;
import nl.sodeso.gwt.ui.client.form.button.SearchButton;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.form.combobox.rules.ComboboxMandatoryValidationRule;
import nl.sodeso.gwt.ui.client.form.input.RadioButtonGroupField;
import nl.sodeso.gwt.ui.client.form.input.TextField;
import nl.sodeso.gwt.ui.client.form.input.filters.NumericInputFilter;
import nl.sodeso.gwt.ui.client.form.revertable.IsRevertable;
import nl.sodeso.gwt.ui.client.form.revertable.RevertUtil;
import nl.sodeso.gwt.ui.client.form.validation.*;
import nl.sodeso.gwt.ui.client.form.validation.rules.MandatoryValidationRule;
import nl.sodeso.gwt.ui.client.panel.HorizontalPanel;
import nl.sodeso.gwt.ui.client.panel.LegendPanel;
import nl.sodeso.gwt.ui.client.panel.VerticalPanel;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.gwt.ui.client.util.Align;

import java.io.Serializable;

import static nl.sodeso.deploykit.console.client.application.Resources.WEBSERVICE_L18N;
import static nl.sodeso.gwt.ui.client.resources.Resources.GENERAL_I18N;

/**
 * @author Ronald Mathies
 */
public class Webservice extends Versionable implements Serializable, IsValidationContainer, IsRevertable {

    private static final String KEY_GENERAL = "general";
    private static final String KEY_GROUP = "group";
    private static final String KEY_LABEL = "label";
    private static final String KEY_PLACEHOLDER_PREFIX = "placeholder-prefix";

    private static final String KEY_PROTOCOL = "protocol";
    private static final String KEY_DOMAIN = "domain";
    private static final String KEY_PORT = "port";
    private static final String KEY_CONTEXT = "context";
    private static final String KEY_PATH = "path";
    private static final String KEY_TIMEOUT = "timeout";
    private static final String KEY_WEBSERVICE_URL = "webservice-url";
    private static final String KEY_URL = "url";

    private static final String KEY_FROM = "from";
    private static final String KEY_TO = "to";
    private static final String KEY_ACTION = "action";
    private static final String KEY_REPLYTO = "replyto";
    private static final String KEY_FAULTTO = "faultto";
    private static final String KEY_WS_ADDRESSING = "ws-addressing";
    private static final String KEY_USE_WS_ADDRESSING = "use-ws-addressing";

    private static final String KEY_USE_WS_SECURITY = "use-ws-security";
    private static final String KEY_WS_SECURITY = "ws-security";
    private static final String KEY_CREDENTIAL = "credential";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_CREDENTIAL_OPTION = "credential-option";
    private static final String KEY_CREDENTIAL_VERSION = "credential-version";
    private static final String KEY_PROPERTIES = "properties";

    private static final int DEFAULT_PORT = 8080;
    private static final int DEFAULT_TIMEOUT = 30000;

    private OptionType<DefaultOption> groupOptionType = new OptionType<>();
    private StringType label = new StringType();
    private StringType placeholderPrefix = new StringType();

    private OptionType<DefaultOption> protocol = new OptionType<>();
    private StringType domain = new StringType();
    private StringType context = new StringType();
    private StringType path = new StringType();
    private IntType port = new IntType(DEFAULT_PORT);
    private IntType timeout = new IntType(DEFAULT_TIMEOUT);

    private BooleanType useWsAddressing = new BooleanType(false);
    private StringType from = new StringType();
    private StringType to = new StringType();
    private StringType action = new StringType();
    private StringType replyTo = new StringType();
    private StringType faultTo = new StringType();

    private BooleanType useWsSecurity = new BooleanType(false);
    private OptionType<DefaultOption> credentialOptionType = new OptionType<>();
    private VersionOptionType credentialVersionType = new VersionOptionType();

    private KeyValueProperties properties = new KeyValueProperties();

    private transient EntryForm entryForm = null;

    private transient LegendPanel generalLegendPanel = null;
    private transient GroupComboboxField groupField = null;
    private transient TextField labelTextField = null;
    private transient TextField placeholderPrefixField = null;

    private transient LegendPanel webserviceUrlLegendPanel = null;
    private transient UrlProtocolComboboxField urlProtocolField = null;
    private transient TextField domainTextField = null;
    private transient TextField portField = null;
    private transient TextField contextTextField = null;
    private transient TextField pathTextField = null;
    private transient TextField timeoutTextField = null;

    private transient LegendPanel wsAddressingLegendPanel = null;
    private transient RadioButtonGroupField useWsAddressingGroupField = null;
    private transient TextField fromTextField = null;
    private transient TextField toTextField = null;
    private transient TextField actionTextField = null;
    private transient TextField replyToTextField = null;
    private transient TextField faultToTextField = null;

    private transient LegendPanel wsSecurityLegendPanel = null;
    private transient RadioButtonGroupField useWsSecurityGroupField = null;
    private transient VersionSelectionField<CredentialRpcGateway> credentialField = null;

    private transient LegendPanel propertiesLegendPanel = null;

    public Webservice() {
    }

    public Widget toEditForm() {
        if (this.entryForm != null) {
            return this.entryForm;
        }

        setupWebserviceGeneral();
        setupWebserviceUrl();
        setupWsAddressing();
        setupWsSecurity();
        setupProperties();

        entryForm = new EntryForm(null);
        entryForm.addEntry(new EntryWithContainer(null, this.generalLegendPanel));
        entryForm.addEntry(new EntryWithContainer(null, this.webserviceUrlLegendPanel));
        entryForm.addEntry(new EntryWithContainer(null, this.wsAddressingLegendPanel));
        entryForm.addEntry(new EntryWithContainer(null, this.wsSecurityLegendPanel));
        entryForm.addEntry(new EntryWithContainer(null, this.propertiesLegendPanel));

        labelTextField.setFocus();

        return this.entryForm;
    }

    private void setupWebserviceGeneral() {
        groupField = new GroupComboboxField(KEY_GROUP, groupOptionType);
        groupField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_GROUP, ValidationMessage.Level.ERROR, groupOptionType));
        groupField.setEnabled(getUuid() == null);

        labelTextField = new TextField<>(KEY_LABEL, this.label)
            .addValidationRules(
                new LabelChangeWarningValidationRule(KEY_LABEL, this.label) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                },
                new MandatoryValidationRule(KEY_LABEL, ValidationMessage.Level.ERROR) {
                    @Override
                    public String getValue() {
                        return labelTextField.getValue();
                    }
                }
            );

        placeholderPrefixField = new TextField<>(KEY_PLACEHOLDER_PREFIX, this.placeholderPrefix);
        placeholderPrefixField.addValidationRule(new MandatoryValidationRule(KEY_PLACEHOLDER_PREFIX, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return placeholderPrefixField.getValue();
            }
        });

        EntryForm form = new EntryForm(KEY_GENERAL)
            .addEntries(
                new EntryWithLabelAndWidget(KEY_GROUP, WEBSERVICE_L18N.groupField(), groupField),
                new EntryWithLabelAndWidget(KEY_LABEL, WEBSERVICE_L18N.labelField(), labelTextField)
                    .addValidationRules(
                        new LabelUniqueValidationRule(GWT.create(WebserviceRpcGateway.class)) {
                            @Override
                            public String getUuid() {
                                return Webservice.this.getUuid();
                            }

                            @Override
                            public StringType getLabel() {
                                return Webservice.this.getLabel();
                            }
                        }
                    ),
                new EntryWithLabelAndWidget(KEY_PLACEHOLDER_PREFIX, WEBSERVICE_L18N.placeholderPrefix(), placeholderPrefixField));

        generalLegendPanel = new LegendPanel(KEY_GENERAL, WEBSERVICE_L18N.webservicesPanel());
        generalLegendPanel.add(form);
    }

    private void setupWebserviceUrl() {
        urlProtocolField = new UrlProtocolComboboxField(protocol);
        urlProtocolField.addValidationRule(new ComboboxMandatoryValidationRule(KEY_PROTOCOL, ValidationMessage.Level.ERROR, protocol));

        domainTextField = new TextField<>(KEY_DOMAIN, this.domain);
        domainTextField.addValidationRule(new MandatoryValidationRule(KEY_DOMAIN, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return domainTextField.getValue();
            }
        });

        portField = new TextField<>(KEY_PORT, this.port);
        portField.addValidationRule(new PortRangeValidationRule(KEY_PORT, port));
        portField.addFilter(new NumericInputFilter(false));

        contextTextField = new TextField<>(KEY_CONTEXT, this.context);
        contextTextField.addValidationRule(new SlashValidationRule() {
            @Override
            public String getValue() {
                return context.getValue();
            }
        });

        pathTextField = new TextField<>(KEY_PATH, this.path);
        pathTextField.addValidationRule(new SlashValidationRule() {
            @Override
            public String getValue() {
                return path.getValue();
            }
        });

        timeoutTextField = new TextField<>(KEY_TIMEOUT, this.timeout)
                .addFilter(new NumericInputFilter(false));

        EntryForm form = new EntryForm(KEY_WEBSERVICE_URL)
            .addEntries(
                new EntryWithDocumentation(KEY_URL, WEBSERVICE_L18N.urlDocumentation()),
                new EntryWithLabelAndWidget(KEY_PROTOCOL, WEBSERVICE_L18N.protocolField(), new VerticalPanel(urlProtocolField,
                        new PlaceholderSpanField(KEY_PROTOCOL, placeholderPrefix, WebservicePlaceHolderSuffix.WS_PROTOCOL))),
                new EntryWithDocumentation(KEY_DOMAIN, WEBSERVICE_L18N.domainDocumentation()),
                new EntryWithLabelAndWidget(KEY_DOMAIN, WEBSERVICE_L18N.domainNameField(), new VerticalPanel(domainTextField,
                        new PlaceholderSpanField(KEY_DOMAIN, placeholderPrefix, WebservicePlaceHolderSuffix.WS_DOMAIN)))
                        .setHint(WEBSERVICE_L18N.domainHint())
                        .addValidatableWidget(domainTextField),
                new EntryWithLabelAndWidget(KEY_PORT, WEBSERVICE_L18N.portField(), new VerticalPanel(portField,
                        new PlaceholderSpanField(KEY_PORT, placeholderPrefix, WebservicePlaceHolderSuffix.WS_PORT)))
                        .setHint(WEBSERVICE_L18N.portHint())
                        .addValidatableWidget(portField),
                new EntryWithDocumentation(KEY_CONTEXT, WEBSERVICE_L18N.contextDocumentation()),
                new EntryWithLabelAndWidget(KEY_CONTEXT, WEBSERVICE_L18N.contextField(), new VerticalPanel(contextTextField,
                        new PlaceholderSpanField(KEY_CONTEXT, placeholderPrefix, WebservicePlaceHolderSuffix.WS_CONTEXT)))
                        .setHint(WEBSERVICE_L18N.contextHint())
                        .addValidatableWidget(contextTextField),
                new EntryWithDocumentation(KEY_PATH, WEBSERVICE_L18N.pathDocumentation()),
                new EntryWithLabelAndWidget(KEY_PATH, WEBSERVICE_L18N.pathField(), new VerticalPanel(pathTextField,
                        new PlaceholderSpanField(KEY_PATH, placeholderPrefix, WebservicePlaceHolderSuffix.WS_PATH)))
                        .setHint(WEBSERVICE_L18N.pathHint())
                        .addValidatableWidget(pathTextField),
                new EntryWithLabelAndWidget(KEY_TIMEOUT, WEBSERVICE_L18N.timeoutField(), new VerticalPanel(timeoutTextField,
                        new PlaceholderSpanField(KEY_TIMEOUT, placeholderPrefix, WebservicePlaceHolderSuffix.WS_TIMEOUT)))
                        .setHint(WEBSERVICE_L18N.timeoutFieldHint())
                        .addValidatableWidget(timeoutTextField));

        webserviceUrlLegendPanel = new LegendPanel(KEY_URL, WEBSERVICE_L18N.webserviceUrlDestinationPanel());
        webserviceUrlLegendPanel.add(form);
    }

    private void setupWsAddressing() {
        useWsAddressingGroupField = new RadioButtonGroupField(KEY_USE_WS_ADDRESSING, this.useWsAddressing);
        useWsAddressingGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useWsAddressingGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useWsAddressingGroupField.setAlignment(Align.HORIZONTAL);

        fromTextField = new TextField<>(KEY_FROM, this.from);
        toTextField = new TextField<>(KEY_TO, this.to);
        actionTextField = new TextField<>(KEY_ACTION, this.action);
        actionTextField.addValidationRule(new MandatoryValidationRule(KEY_ACTION, ValidationMessage.Level.ERROR) {
            @Override
            public String getValue() {
                return actionTextField.getValue();
            }

            @Override
            public boolean isApplicable() {
                return useWsAddressing.getValue();
            }
        });

        replyToTextField = new TextField<>(KEY_REPLYTO, this.replyTo);
        faultToTextField = new TextField<>(KEY_FAULTTO, this.faultTo);

        EntryForm form = new EntryForm(KEY_WS_ADDRESSING)
            .addEntries(
                    new EntryWithDocumentation(KEY_USE_WS_ADDRESSING, WEBSERVICE_L18N.useWsAddressingDocumentation()),
                    new EntryWithLabelAndWidget(KEY_USE_WS_ADDRESSING, WEBSERVICE_L18N.useWsAddressingField(), this.useWsAddressingGroupField),
                    new EntryWithLabelAndWidget(KEY_FROM, WEBSERVICE_L18N.fromField(), new VerticalPanel(fromTextField,
                            new PlaceholderSpanField(KEY_FROM, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSA_FROM))),
                    new EntryWithLabelAndWidget(KEY_TO, WEBSERVICE_L18N.toField(), new VerticalPanel(toTextField,
                            new PlaceholderSpanField(KEY_TO, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSA_TO))),
                    new EntryWithLabelAndWidget(KEY_ACTION, WEBSERVICE_L18N.actionField(), new VerticalPanel(actionTextField,
                            new PlaceholderSpanField(KEY_ACTION, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSA_ACTION)))
                            .addValidatableWidget(actionTextField),
                    new EntryWithDocumentation(KEY_REPLYTO, WEBSERVICE_L18N.replyToDocumentation()),
                    new EntryWithLabelAndWidget(KEY_REPLYTO, WEBSERVICE_L18N.replyToField(), new VerticalPanel(replyToTextField,
                            new PlaceholderSpanField(KEY_REPLYTO, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSA_REPLYTO))),
                    new EntryWithDocumentation(KEY_FAULTTO, WEBSERVICE_L18N.faultToDocumentation()),
                    new EntryWithLabelAndWidget(KEY_FAULTTO, WEBSERVICE_L18N.faultToField(), new VerticalPanel(faultToTextField,
                            new PlaceholderSpanField(KEY_FAULTTO, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSA_FAULTTO))));

        wsAddressingLegendPanel = new LegendPanel(KEY_WS_ADDRESSING, WEBSERVICE_L18N.wsAddressingPanel());
        wsAddressingLegendPanel.add(form);

        useWsAddressingGroupField.addClickHandler(event -> {
            boolean visible = useWsAddressing.getValue();
            form.setEntriesVisible(visible, KEY_FROM, KEY_TO, KEY_ACTION, KEY_REPLYTO, KEY_FAULTTO, KEY_FAULTTO);

            if (!visible) {
                fromTextField.setValue(null);
                toTextField.setValue(null);
                actionTextField.setValue(null);
                replyToTextField.setValue(null);
                faultToTextField.setValue(null);
            }
        });

        form.addAttachHandler(event -> {
            if (event.isAttached()) {
                useWsAddressingGroupField.fireClickEvent();
            }
        });
    }

    private void setupWsSecurity() {
        useWsSecurityGroupField = new RadioButtonGroupField(KEY_USE_WS_SECURITY, this.useWsSecurity);
        useWsSecurityGroupField.addRadioButton(Boolean.TRUE.toString(), GENERAL_I18N.Yes());
        useWsSecurityGroupField.addRadioButton(Boolean.FALSE.toString(), GENERAL_I18N.No());
        useWsSecurityGroupField.setAlignment(Align.HORIZONTAL);

        credentialField = new VersionSelectionField<>(GWT.create(CredentialRpcGateway.class), credentialOptionType, credentialVersionType);
        credentialField.listenToGroup(groupOptionType);

        EntryForm form = new EntryForm(KEY_WS_SECURITY);
        form.addEntry(new EntryWithDocumentation(KEY_USE_WS_SECURITY, WEBSERVICE_L18N.useWsSecurityDocumentation()));
        form.addEntry(new EntryWithLabelAndWidget(KEY_USE_WS_SECURITY, WEBSERVICE_L18N.useWsSecurityField(), this.useWsSecurityGroupField));
        form.addEntry(new EntryWithLabelAndWidget(KEY_CREDENTIAL, WEBSERVICE_L18N.credentialField(),
                new VerticalPanel(
                        new HorizontalPanel()
                            .addWidget(credentialField, 100.0, Style.Unit.PCT)
                            .addWidget(new SearchButton.WithIcon((event) -> CredentialMenuItem.click(credentialOptionType.getValue().getKey(), credentialVersionType.getValue()))),
                        new PlaceholderSpanField(KEY_USERNAME, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSSEC_USERNAME),
                        new PlaceholderSpanField(KEY_PASSWORD, placeholderPrefix, WebservicePlaceHolderSuffix.WS_WSSEC_PASSWORD)
                ))
                .addValidationRule(
                        new ValidationCriteria.or(
                                new ComboboxMandatoryValidationRule(KEY_CREDENTIAL_OPTION, ValidationMessage.Level.ERROR, credentialOptionType),
                                new ComboboxMandatoryValidationRule(KEY_CREDENTIAL_VERSION, ValidationMessage.Level.ERROR, credentialVersionType)
                        ) {

                            @Override
                            public boolean isApplicable() {
                                if (useWsSecurity.getValue()) {
                                    return super.isApplicable();
                                }

                                return false;
                            }
                        }
                ));

        useWsSecurityGroupField.addClickHandler(event -> {
            boolean visible = useWsSecurity.getValue();
            form.setEntriesVisible(visible, KEY_CREDENTIAL);

            if (!visible) {
                credentialOptionType.setValue(null);
            }
        });

        wsSecurityLegendPanel = new LegendPanel(KEY_WS_SECURITY, WEBSERVICE_L18N.wsSecurityPanel());
        wsSecurityLegendPanel.add(form);

        form.addAttachHandler(event -> {
            if (event.isAttached()) {
                useWsSecurityGroupField.fireClickEvent();
            }
        });
    }

    private void setupProperties() {
        EntryForm entryForm = new EntryForm(null)
            .addEntries(
                    new EntryWithDocumentation(KEY_PROPERTIES, WEBSERVICE_L18N.customPropertiesDocumentation(), Align.LEFT),
                    new EntryWithContainer(null, properties.toEditForm()));

        propertiesLegendPanel = new LegendPanel(KEY_PROPERTIES, WEBSERVICE_L18N.customPropertiesPanel());
        propertiesLegendPanel.add(entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void revert() {
        RevertUtil.revert(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasChanges() {
        return RevertUtil.hasChanges(this.entryForm);
    }

    /**
     * {@inheritDoc}
     */
    public void validateContainer(ValidationCompletedHandler handler) {
        ValidationUtil.validate(handler, this.entryForm);
    }

    public OptionType<DefaultOption> getGroup() {
        return this.groupOptionType;
    }

    public void setGroup(OptionType<DefaultOption> optionType) {
        this.groupOptionType = optionType;
    }

    public StringType getLabel() {
        return this.label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public OptionType<DefaultOption> getProtocol() {
        return protocol;
    }

    public void setProtocol(OptionType<DefaultOption> protocol) {
        this.protocol = protocol;
    }

    public StringType getDomain() {
        return domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public StringType getContext() {
        return context;
    }

    public void setContext(StringType context) {
        this.context = context;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

    public BooleanType getUseWsAddressing() {
        return useWsAddressing;
    }

    public void setUseWsAddressing(BooleanType useWsAddressing) {
        this.useWsAddressing = useWsAddressing;
    }

    public StringType getFrom() {
        return from;
    }

    public void setFrom(StringType from) {
        this.from = from;
    }

    public StringType getTo() {
        return to;
    }

    public void setTo(StringType to) {
        this.to = to;
    }

    public StringType getAction() {
        return action;
    }

    public void setAction(StringType action) {
        this.action = action;
    }

    public StringType getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(StringType replyTo) {
        this.replyTo = replyTo;
    }

    public StringType getFaultTo() {
        return faultTo;
    }

    public void setFaultTo(StringType faultTo) {
        this.faultTo = faultTo;
    }

    public BooleanType getUseWsSecurity() {
        return useWsSecurity;
    }

    public void setUseWsSecurity(BooleanType useWsSecurity) {
        this.useWsSecurity = useWsSecurity;
    }

    public OptionType<DefaultOption> getCredential() {
        return credentialOptionType;
    }

    public void setCredential(OptionType<DefaultOption> credentialOptionType) {
        this.credentialOptionType = credentialOptionType;
    }

    public VersionOptionType getCredentialVersion() {
        return credentialVersionType;
    }

    public void setCredentialVersion(VersionOptionType credentialVersionType) {
        this.credentialVersionType = credentialVersionType;
    }

    public KeyValueProperties getProperties() {
        return this.properties;
    }

}
