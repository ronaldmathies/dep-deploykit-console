package nl.sodeso.deploykit.console.client.application.services.ldap;

import com.google.gwt.i18n.client.Messages;

/**
 * @author Ronald Mathies
 */
public interface LdapServiceConstants extends Messages {

    @DefaultMessage("LDAP Services")
    String ldapServicesMenuItem();

    @DefaultMessage("LDAP Service")
    String ldapServicePanel();

    @DefaultMessage("Group")
    String groupField();

    @DefaultMessage("Label")
    String labelField();

    @DefaultMessage("Placeholder Prefix")
    String placeholderPrefixField();

    @DefaultMessage("LDAP Server")
    String ldapServerPanel();

    @DefaultMessage("Port")
    String portField();

    @DefaultMessage("Domain Name")
    String domainNameField();

    @DefaultMessage("Provide the fully qualified domain name of the LDAP server without any slashes, protocols or other information.")
    String domainNameDocumentation();

    @DefaultMessage("LDAP Server Security")
    String ldapServerSecurityPanel();

    @DefaultMessage("Credential")
    String credentialField();

    @DefaultMessage("Use Security")
    String useSecurityField();

    @DefaultMessage("Enable the following option when the LDAP server requires credentials to connect.")
    String useSecurityDocumentation();

    @DefaultMessage("Checking LDAP Service Connection")
    String checkTitle();

    @DefaultMessage("Checking if LDAP service configuration is correct, this process may take a while so please do not close the browser.")
    String checkMessage();

    @DefaultMessage("LDAP Service Connection")
    String checkResultTitle();

    @DefaultMessage("Connection was made successfully.")
    String checkSuccesfullMessage();

    @DefaultMessage("LDAP Service {0} Used By")
    String ldapServiceUsedByTitle(String name);

    @DefaultMessage("No LDAP Services")
    String noLdapServicesEmptyTable();
}