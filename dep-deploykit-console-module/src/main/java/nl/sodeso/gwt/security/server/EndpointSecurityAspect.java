package nl.sodeso.gwt.security.server;

import nl.sodeso.gwt.security.server.annotation.EndpointMethodSecurity;
import nl.sodeso.gwt.security.client.exception.PermissionDeniedException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author Ronald Mathies
 */
@Aspect
public class EndpointSecurityAspect {

    @Around("execution(@EndpointMethodSecurity * *.*(..)) && @annotation(endpointMethodSecurity)")
    public Object endpointMethodSecurity(ProceedingJoinPoint point, EndpointMethodSecurity endpointMethodSecurity) throws Throwable, PermissionDeniedException {
//        System.out.println("Called:" + endpointMethodSecurity.key());
//        Object result = point.proceed();

        throw new PermissionDeniedException(endpointMethodSecurity.key(), endpointMethodSecurity.checkFailedMessage());

//        return result;
    }

}
