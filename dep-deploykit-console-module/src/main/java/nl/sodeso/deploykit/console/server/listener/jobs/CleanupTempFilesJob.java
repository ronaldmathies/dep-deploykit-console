package nl.sodeso.deploykit.console.server.listener.jobs;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.commons.quartz.annotation.QuartzJob;
import nl.sodeso.commons.quartz.jobs.CleanupFilesConfig;
import nl.sodeso.commons.quartz.jobs.CleanupFilesJob;

/**
 * @author Ronald Mathies
 */
@QuartzJob(
        name = CleanupTempFilesJob.JOB_NAME,
        cron = "0 0/1 * 1/1 * ? *"
)
public class CleanupTempFilesJob extends CleanupFilesJob {

    public static final String JOB_NAME = "CleanupTempFiles";

    @Override
    public CleanupFilesConfig getCleanupFilesConfig() {
        CleanupFilesConfig config = new CleanupFilesConfig();
        config.setFolder(FileUtil.getSystemTempFolder());
        config.setExpirationSinceCreated(1000l * 60l * 30l);
        config.setExpression("\\.upl");
        return config;
    }
}
