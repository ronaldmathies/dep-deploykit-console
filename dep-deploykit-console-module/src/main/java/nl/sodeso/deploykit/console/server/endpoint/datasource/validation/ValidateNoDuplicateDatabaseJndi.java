package nl.sodeso.deploykit.console.server.endpoint.datasource.validation;

import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.server.endpoint.validation.ValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ValidateNoDuplicateDatabaseJndi implements ValidationCheck<DoDatasource> {

    public void validate(List<DoDatasource> datasources, ValidationResult result) {
        Map<StringType, List<DoDatasource>> jndiNames = new HashMap<>();
        for (DoDatasource doDatasource : datasources) {
            if (!jndiNames.containsKey(doDatasource.getJndi())) {
                jndiNames.put(doDatasource.getJndi(), new ArrayList<>());
            }

            List<DoDatasource> datasourcesForJndiName = jndiNames.get(doDatasource.getJndi());
            datasourcesForJndiName.add(doDatasource);
        }

        for (Map.Entry<StringType, List<DoDatasource>> jndiNameEntry : jndiNames.entrySet()) {
            if (jndiNameEntry.getValue().size() > 1) {

                StringBuilder message = new StringBuilder(String.format("The following data sources share the same JNDI name (%s), this causes conflicts when this profile is used.<ul>", jndiNameEntry.getKey().getValue()));
                for (DoDatasource doDatasource : jndiNameEntry.getValue()) {
                    message.append(String.format("<li>(%s) %s: %s</li>",
                            doDatasource.getGroup().getLabel().getValue(),
                            doDatasource.getLabel().getValue(),
                            doDatasource.getVersion().getValue().getDisplayDescription()));
                }
                message.append("</ul>");

                result.add(new ValidationMessage("datasource-jndi-duplicates", ValidationMessage.Level.ERROR, message.toString()));
            }
        }
    }

    public void validate(DoDatasource datasource, ValidationResult result) {
    }
}
