package nl.sodeso.deploykit.console.server.endpoint.datasource.validation;

import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateDatasourceIsReleased extends ReleasedValidationCheck<DoDatasource> {

    public void validate(List<DoDatasource> datasources, ValidationResult result) {
        for (DoDatasource doDatasource : datasources) {
            validate(doDatasource, result);
        }
    }

    public void validate(DoDatasource datasource, ValidationResult result) {
        if (datasource != null && !isReleased(datasource.getVersion())) {
            result.add(new ValidationMessage("datasource-version", ValidationMessage.Level.ERROR,
                    String.format("The selected datasource configuration (%s) %s: %s is not a released version.",
                            datasource.getGroup().getLabel().getValue(),
                            datasource.getLabel().getValue(),
                            datasource.getVersion().getValue().getDisplayDescription())));
        }
    }
}
