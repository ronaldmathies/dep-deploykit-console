package nl.sodeso.deploykit.console.server.endpoint.properties;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;
import nl.sodeso.deploykit.console.client.Constants;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = Constants.MODULE_ID
)
@FileResources(
    files = {
            @FileResource(
                    file= "/dep-deploykit-console-configuration.properties",
                    monitor=false
            )
    }
)
public class ModulePropertiesContainer extends FileContainer {
}
