package nl.sodeso.deploykit.console.server.endpoint.referencetables;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.ui.options.rpc.ReferenceTablesRpc;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-reference")
public class ReferenceTablesEndpoint extends XsrfProtectedServiceServlet implements ReferenceTablesRpc {

    public ArrayList<DefaultOption> javaServerProviders() {
        ArrayList<DefaultOption> providers = new ArrayList<>();
        providers.add(new DefaultOption("jetty", "jetty", "Jetty"));
        providers.add(new DefaultOption("jboss", "jboss", "JBoss"));
        return providers;
    }

    public ArrayList<DefaultOption> accessControlTypes() {
        ArrayList<DefaultOption> providers = new ArrayList<>();
        providers.add(new DefaultOption("ldap", "ldap", "LDAP"));
        providers.add(new DefaultOption("database", "database", "Database"));
        return providers;
    }

}
