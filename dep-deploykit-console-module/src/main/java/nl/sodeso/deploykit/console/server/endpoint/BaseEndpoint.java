package nl.sodeso.deploykit.console.server.endpoint;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.security.user.UserRepository;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;

/**
 * @author Ronald Mathies
 */
public class BaseEndpoint extends XsrfProtectedServiceServlet {

    protected String getPrincipalName() {
        return getThreadLocalRequest().getUserPrincipal().getName();
    }

    protected void validatePermissions(Permissions permissions, String forGroupUuid) throws RemoteException {
        boolean isAllowedToEdit = new UserRepository().isAllowedTo(permissions, forGroupUuid);
        if (!isAllowedToEdit) {
            throw new RemoteException("You do not have sufficient permissions to save these changes.");
        }
    }

    protected void validateNotReleased(VersionOptionType versionOptionType) throws RemoteException {
        if (versionOptionType.isRelease()) {
            throw new RemoteException("It is not allowed to make changes to a released version.");
        }
    }

}
