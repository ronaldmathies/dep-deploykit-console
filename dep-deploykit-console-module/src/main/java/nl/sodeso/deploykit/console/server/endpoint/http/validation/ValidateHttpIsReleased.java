package nl.sodeso.deploykit.console.server.endpoint.http.validation;

import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateHttpIsReleased extends ReleasedValidationCheck<DoHttp> {

    public void validate(List<DoHttp> httpes, ValidationResult result) {
        for (DoHttp doHttp : httpes) {
            validate(doHttp, result);
        }
    }

    public void validate(DoHttp http, ValidationResult result) {
        if (http != null && !isReleased(http.getVersion())) {
            result.add(new ValidationMessage("http-version", ValidationMessage.Level.ERROR,
                    String.format("The selected HTTP configuration '%s' does not contain a released version.", http.getLabel().getValue())));
        }
    }
}
