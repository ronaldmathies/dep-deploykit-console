package nl.sodeso.deploykit.console.server.endpoint.https.validation;

import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateHttpsIsReleased extends ReleasedValidationCheck<DoHttps> {

    public void validate(List<DoHttps> httpses, ValidationResult result) {
        for (DoHttps doHttps : httpses) {
            validate(doHttps, result);
        }
    }

    public void validate(DoHttps https, ValidationResult result) {
        if (https != null && !isReleased(https.getVersion())) {
            result.add(new ValidationMessage("https-version", ValidationMessage.Level.ERROR,
                    String.format("The selected HTTPS configuration '%s' does not contain a released version.", https.getLabel().getValue())));
        }
    }
}
