package nl.sodeso.deploykit.console.server.endpoint.credential;

import nl.sodeso.deploykit.console.client.application.general.credential.CredentialSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialConvertor;
import nl.sodeso.deploykit.console.client.application.general.credential.Credential;
import nl.sodeso.deploykit.console.client.application.general.credential.rpc.CredentialRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-credential")
public class CredentialEndpoint extends BaseEndpoint implements CredentialRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<CredentialSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoCredential> credentials = new CredentialRepository().find(new Permissions(username, Permissions.READ));
        return CredentialConvertor.toSummaries(credentials);
    }

    public Credential findSingle(String uuid, VersionOption version) {
        DoCredential doCredential = new CredentialRepository().findByUuidAndVersion(uuid, version);
        return CredentialConvertor.from(doCredential);
    }

    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoCredential doCredential = new CredentialRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoDatasource> datasources = doCredential.getDatasources();
        for (DoDatasource doDatasource : datasources) {
            usages.add(new UsedBy(doDatasource.getUuid(), doDatasource.getVersion().getValue(), "Datasources", doDatasource.getLabel().getValue()));
        }

        List<DoHttps> httpses = doCredential.getHttpsKeymanagers();
        for (DoHttps doHttps : httpses) {
            usages.add(new UsedBy(doHttps.getUuid(), doHttps.getVersion().getValue(), "HTTPS Keymanagers", doHttps.getLabel().getValue()));
        }

        httpses = doCredential.getHttpsKeystores();
        for (DoHttps doHttps : httpses) {
            usages.add(new UsedBy(doHttps.getUuid(), doHttps.getVersion().getValue(), "HTTPS Keystores", doHttps.getLabel().getValue()));
        }

        httpses = doCredential.getHttpsTruststores();
        for (DoHttps doHttps : httpses) {
            usages.add(new UsedBy(doHttps.getUuid(), doHttps.getVersion().getValue(), "HTTPS Truststores", doHttps.getLabel().getValue()));
        }

        List<DoProfile> profiles = doCredential.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }

        List<DoLdapService> ldapServices = doCredential.getLdapServices();
        for (DoLdapService doLdapService : ldapServices) {
            usages.add(new UsedBy(doLdapService.getUuid(), doLdapService.getVersion().getValue(), "LDAP Services", doLdapService.getLabel().getValue()));
        }

        List<DoWebservice> webservices = doCredential.getWebservices();
        for (DoWebservice doWebservice : webservices) {
            usages.add(new UsedBy(doWebservice.getUuid(), doWebservice.getVersion().getValue(), "Web Services", doWebservice.getLabel().getValue()));
        }

        return usages;
    }

    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoCredential> objects = new CredentialRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoCredential object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        CredentialRepository credentialRepository = new CredentialRepository();
        DoCredential doCredential = credentialRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doCredential != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doCredential.getGroup().getLabel().getValue(), doCredential.getLabel().getValue())));
        }

        return result;
    }

    public CredentialSummaryItem save(Credential credential) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), credential.getGroup().getValue().getKey());

        DoCredential doCredential;
        boolean labelChanged = false;


        if (credential.getUuid() != null) {
            doCredential = new CredentialRepository().findByUuidAndVersion(credential.getUuid(), credential.getVersion().getValue());
            labelChanged = !doCredential.getLabel().getValue().equals(doCredential.getLabel().getValue());

            CredentialConvertor.to(doCredential, credential);
        } else {
            doCredential = CredentialConvertor.to(null, credential);
        }

        new CredentialRepository().save(doCredential);

        if (labelChanged) {
            new CredentialRepository().updateLabel(doCredential.getId(), doCredential.getUuid(), credential.getLabel());
        }

        session.flush();
        session.evict(doCredential);

        DoCredential doCredentialReloaded = new CredentialRepository().findByUuidAndVersion(doCredential.getUuid(), doCredential.getVersion().getValue());

        return CredentialConvertor.toSummary(doCredentialReloaded);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoCredential doCredentialToDuplicate = new CredentialRepository().findByUuidAndVersion(uuid, fromVersion);
        Credential duplicateCredential = CredentialConvertor.from(doCredentialToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateCredential.setVersion(version);
        DoCredential doDuplicateCredential = CredentialConvertor.to(null, duplicateCredential);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateCredential.setUuid(uuid);

        new CredentialRepository().save(doDuplicateCredential);

        session.flush();
        session.evict(doDuplicateCredential);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoCredential branchedVersion = new CredentialRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoCredential doCredentialToBranch = new CredentialRepository().findByUuidAndVersion(uuid, fromVersion);
        Credential branchCredential = CredentialConvertor.from(doCredentialToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchCredential.setVersion(version);
        DoCredential doDuplicateCredential = CredentialConvertor.to(null, branchCredential);

        // Keep the UUID the same, we are creating a branch here for the same webservice.
        doDuplicateCredential.setUuid(uuid);

        new CredentialRepository().save(doDuplicateCredential);

        session.flush();
        session.evict(doDuplicateCredential);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new CredentialRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public CredentialSummaryItem duplicate(String uuid, VersionOption version) {
        DoCredential doCredentialToDuplicate = new CredentialRepository().findByUuidAndVersion(uuid, version);
        Credential duplicateCredential = CredentialConvertor.from(doCredentialToDuplicate);

        duplicateCredential.setLabel(new StringType(duplicateCredential.getLabel().getValue() + " Copy"));
        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateCredential.setVersion(snapshot);

        DoCredential doDuplicateCredential = CredentialConvertor.to(null, duplicateCredential);

        new CredentialRepository().save(doDuplicateCredential);

        session.flush();
        session.evict(doDuplicateCredential);

        DoCredential doDuplicateCredentialReloaded = new CredentialRepository().findByUuidAndVersion(doDuplicateCredential.getUuid(), snapshot.getValue());

        return CredentialConvertor.toSummary(doDuplicateCredentialReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoCredential doCredential = new CredentialRepository().findByUuidAndVersion(uuid, version);
        if (!doCredential.getDatasources().isEmpty() ||
                !doCredential.getHttpsKeymanagers().isEmpty() ||
                !doCredential.getHttpsKeystores().isEmpty() ||
                !doCredential.getHttpsTruststores().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new CredentialRepository().remove(uuid, version);
    }
}
