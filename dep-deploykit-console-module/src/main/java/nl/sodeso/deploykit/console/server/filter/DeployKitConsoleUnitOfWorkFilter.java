package nl.sodeso.deploykit.console.server.filter;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.hibernate.filter.UnitOfWorkFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author Ronald Mathies
 */
@WebFilter(
    urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = UnitOfWorkFilter.INIT_PERSISTENCE_UNIT, value = Constants.PU),
        @WebInitParam(name = UnitOfWorkFilter.INIT_EXTRA_URL_PATTERN, value = ".*/deploykitconsole/.*"),
    }
)
public class DeployKitConsoleUnitOfWorkFilter extends UnitOfWorkFilter {
}
