package nl.sodeso.deploykit.console.server.endpoint.group;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.group.Group;
import nl.sodeso.deploykit.console.client.application.group.GroupSummaryItem;
import nl.sodeso.deploykit.console.client.application.group.rpc.GroupRpc;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.general.group.GroupConvertor;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.gwt.security.server.annotation.EndpointMethodSecurity;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-group")
public class GroupEndpoint extends XsrfProtectedServiceServlet implements GroupRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<GroupSummaryItem> findSummaries() {
        List<DoGroup> groups = new GroupRepository().find(null);
        return GroupConvertor.toSummaries(groups);
    }

    public Group findSingle(String uuid) {
        DoGroup doGroup = new GroupRepository().findByUuid(uuid);
        return GroupConvertor.from(doGroup);
    }

    public ArrayList<DefaultOption> asOptions() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoGroup> objects = new GroupRepository().find(new Permissions(username, Permissions.USE));
        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoGroup object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<UsedBy> usage(String uuid) {
        DoGroup doGroup = new GroupRepository().findByUuid(uuid);

        ArrayList<UsedBy> usages = new ArrayList<>();

        for (DoProfile doProfile : doGroup.getProfiles()) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }

        for (DoJdbcDriver doJdbcDriver : doGroup.getJdbcDrivers()) {
            usages.add(new UsedBy(doJdbcDriver.getUuid(), doJdbcDriver.getVersion().getValue(), "JDBC Drivers", doJdbcDriver.getLabel().getValue()));
        }

        for (DoDatasource doDatasource : doGroup.getDatasources()) {
            usages.add(new UsedBy(doDatasource.getUuid(), doDatasource.getVersion().getValue(), "Datasources", doDatasource.getLabel().getValue()));
        }

        for (DoHttps doHttps : doGroup.getHttps()) {
            usages.add(new UsedBy(doHttps.getUuid(), doHttps.getVersion().getValue(), "HTTPS", doHttps.getLabel().getValue()));
        }

        for (DoHttp http : doGroup.getHttp()) {
            usages.add(new UsedBy(http.getUuid(), http.getVersion().getValue(), "HTTP", http.getLabel().getValue()));
        }

        for (DoCredential credential : doGroup.getCredentials()) {
            usages.add(new UsedBy(credential.getUuid(), credential.getVersion().getValue(), "Credentials", credential.getLabel().getValue()));
        }

        for (DoLdapService ldapService : doGroup.getLdapServices()) {
            usages.add(new UsedBy(ldapService.getUuid(), ldapService.getVersion().getValue(), "LDAP Services", ldapService.getLabel().getValue()));
        }

        for (DoNode node : doGroup.getNodes()) {
            usages.add(new UsedBy(node.getUuid(), "Nodes", node.getLabel().getValue()));
        }

        return usages;
    }

    public GroupSummaryItem save(Group group) {
        DoGroup doGroup;

        if (group.getUuid() != null) {
            doGroup = new GroupRepository().findByUuid(group.getUuid());
            GroupConvertor.to(doGroup, group);
        } else {
            doGroup = GroupConvertor.to(null, group);
        }

        new GroupRepository().save(doGroup);

        session.flush();
        session.evict(doGroup);

        DoGroup doGroupReloaded = new GroupRepository().findByUuid(doGroup.getUuid());

        return GroupConvertor.toSummary(doGroupReloaded);
    }

    @EndpointMethodSecurity(key = "duplicate_group", description = "Duplicate Group", checkFailedMessage = "Insufficient permission to duplicate group.")
    public GroupSummaryItem duplicate(String uuid) {
        DoGroup doGroupToDuplicate = new GroupRepository().findByUuid(uuid);
        Group duplicateGroup = GroupConvertor.from(doGroupToDuplicate);

        duplicateGroup.setLabel(new StringType(duplicateGroup.getLabel().getValue() + " Copy"));

        duplicateGroup.setUuid(null);
        DoGroup doDuplicateGroup = GroupConvertor.to(null, duplicateGroup);

        new GroupRepository().save(doDuplicateGroup);

        session.flush();
        session.evict(doDuplicateGroup);

        DoGroup doDuplicateGroupReloaded = new GroupRepository().findByUuid(doDuplicateGroup.getUuid());

        return GroupConvertor.toSummary(doDuplicateGroupReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoGroup doGroup = new GroupRepository().findByUuid(uuid);

        if (!doGroup.getProfiles().isEmpty() ||
            !doGroup.getJdbcDrivers().isEmpty() ||
            !doGroup.getDatasources().isEmpty() ||
            !doGroup.getCredentials().isEmpty() ||
            !doGroup.getHttps().isEmpty() ||
            !doGroup.getHttp().isEmpty() ||
            !doGroup.getNodes().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        return result;

    }

    public void delete(String uuid, VersionOption version) {
        new GroupRepository().remove(uuid);
    }
}
