package nl.sodeso.deploykit.console.server.endpoint.security.user;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.security.user.User;
import nl.sodeso.deploykit.console.client.application.security.user.UserSummaryItem;
import nl.sodeso.deploykit.console.client.application.security.user.rpc.UserRpc;
import nl.sodeso.deploykit.console.domain.security.user.UserConvertor;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.deploykit.console.domain.security.user.UserRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-user")
public class UserEndpoint extends XsrfProtectedServiceServlet implements UserRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<UserSummaryItem> findSummaries() {
        List<DoUser> users = new UserRepository().find();
        return UserConvertor.toSummaries(users);
    }

    public User findSingle(String uuid) {
        DoUser doUser = new UserRepository().findByUuid(uuid);
        return UserConvertor.from(doUser);
    }

    public ArrayList<DefaultOption> asOptions() {
        List<DoUser> objects = new UserRepository().find();

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoUser object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<UsedBy> usage(String uuid) {
        DoUser doUser = new UserRepository().findByUuid(uuid);

        ArrayList<UsedBy> usages = new ArrayList<>();
        for (DoRole doRole : doUser.getRoles()) {
            usages.add(new UsedBy(doRole.getUuid(), "Role", doRole.getLabel().getValue()));
        }

        return usages;
    }

    public UserSummaryItem save(User user) {
        DoUser doUser;

        if (user.getUuid() != null) {
            doUser = new UserRepository().findByUuid(user.getUuid());
            UserConvertor.to(doUser, user);
        } else {
            doUser = UserConvertor.to(null, user);
        }

        new UserRepository().save(doUser);

        session.flush();
        session.clear();

        DoUser doUserReloaded = new UserRepository().findByUuid(doUser.getUuid());

        return UserConvertor.toSummary(doUserReloaded);
    }

    public UserSummaryItem duplicate(String uuid) {
        DoUser doUserToDuplicate = new UserRepository().findByUuid(uuid);
        User duplicateUser = UserConvertor.from(doUserToDuplicate);
        duplicateUser.setLabel(new StringType(duplicateUser.getLabel().getValue() + " Copy"));

        duplicateUser.setUuid(null);
        DoUser doDuplicateUser = UserConvertor.to(null, duplicateUser);

        new UserRepository().save(doDuplicateUser);

        session.flush();
        session.evict(doDuplicateUser);

        DoUser doDuplicateUserReloaded = new UserRepository().findByUuid(doDuplicateUser.getUuid());

        return UserConvertor.toSummary(doDuplicateUserReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoUser doUser = new UserRepository().findByUuid(uuid);

        if (!doUser.getRoles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not possible to delete this user since it is in use by other components, use the Used By button to see which components use this user."));
        }

        return result;

    }

    public void delete(String uuid, VersionOption version) {
        new UserRepository().remove(uuid);
    }
}
