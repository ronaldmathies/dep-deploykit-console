package nl.sodeso.deploykit.console.server.endpoint.https;

import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.connectors.https.HttpsConvertor;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.exception.UnableToReadKeystoreException;
import nl.sodeso.deploykit.console.client.application.connectors.https.Https;
import nl.sodeso.deploykit.console.client.application.connectors.https.rpc.HttpsRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.connectors.https.HttpsRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.deploykit.console.server.endpoint.credential.validation.ValidateCredentialIsReleased;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-https")
public class HttpsEndpoint extends BaseEndpoint implements HttpsRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<HttpsSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoHttps> items = new HttpsRepository().find(new Permissions(username, Permissions.READ));
        return HttpsConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public Https findSingle(String uuid, VersionOption version) {
        DoHttps doHttps = new HttpsRepository().findByUuidAndVersion(uuid, version);
        return HttpsConvertor.from(doHttps);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoHttps doHttps = new HttpsRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doHttps.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    /**
     * Returns a list of options
     * @param group
     * @return
     */
    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoHttps> objects = new HttpsRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoHttps object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        HttpsRepository httpsRepository = new HttpsRepository();
        DoHttps doHttps = httpsRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doHttps != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doHttps.getGroup().getLabel().getValue(), doHttps.getLabel().getValue())));
        }

        return result;
    }

    public HttpsSummaryItem save(Https https) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), https.getGroup().getValue().getKey());
        validateNotReleased(https.getVersion());

        try {
            HttpsRepository httpsRepository = new HttpsRepository();

            DoHttps doHttps;
            boolean labelChanged = false;

            if (https.getUuid() != null) {
                doHttps = httpsRepository.findByUuidAndVersion(https.getUuid(), https.getVersion().getValue());

                labelChanged = !doHttps.getLabel().getValue().equals(https.getLabel().getValue());

                HttpsConvertor.to(doHttps, https);
            } else {
                doHttps = HttpsConvertor.to(null, https);
            }

            httpsRepository.save(doHttps);

            session.flush();
            session.evict(doHttps);

            if (labelChanged) {
                httpsRepository.updateLabel(doHttps.getId(), doHttps.getUuid(), https.getLabel());
            }

            DoHttps doHttpsReloaded = httpsRepository.findByUuidAndVersion(doHttps.getUuid(), doHttps.getVersion().getValue());

            return HttpsConvertor.toSummary(doHttpsReloaded);
        } catch (UnableToReadKeystoreException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        DoHttps doHttpsToCommit = new HttpsRepository().findByUuidAndVersion(uuid, fromVersion);
        ValidateCredentialIsReleased validateCredentialIsReleased = new ValidateCredentialIsReleased();
        validateCredentialIsReleased.validate(doHttpsToCommit.getKeyManagerPassword(), result);
        validateCredentialIsReleased.validate(doHttpsToCommit.getKeyStorePassword(), result);
        validateCredentialIsReleased.validate(doHttpsToCommit.getTrustStorePassword(), result);

        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoHttps doHttpsToDuplicate = new HttpsRepository().findByUuidAndVersion(uuid, fromVersion);
        Https duplicateHttps = HttpsConvertor.from(doHttpsToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateHttps.setVersion(version);
        DoHttps doDuplicateHttps = HttpsConvertor.to(null, duplicateHttps);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateHttps.setUuid(uuid);

        new HttpsRepository().save(doDuplicateHttps);

        session.flush();
        session.evict(doDuplicateHttps);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoHttps branchedVersion = new HttpsRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoHttps doHttpsToBranch = new HttpsRepository().findByUuidAndVersion(uuid, fromVersion);
        Https branchHttps = HttpsConvertor.from(doHttpsToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchHttps.setVersion(version);
        DoHttps doDuplicateHttps = HttpsConvertor.to(null, branchHttps);

        // Keep the UUID the same, we are creating a branch here for the same https.
        doDuplicateHttps.setUuid(uuid);

        new HttpsRepository().save(doDuplicateHttps);

        session.flush();
        session.evict(doDuplicateHttps);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new HttpsRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public HttpsSummaryItem duplicate(String uuid, VersionOption version) {
        DoHttps doHttpsToDuplicate = new HttpsRepository().findByUuidAndVersion(uuid, version);
        Https duplicateHttps = HttpsConvertor.from(doHttpsToDuplicate);

        duplicateHttps.setLabel(new StringType(doHttpsToDuplicate.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateHttps.setVersion(snapshot);

        DoHttps doDuplicateHttps = HttpsConvertor.to(null, duplicateHttps);

        new HttpsRepository().save(doDuplicateHttps);

        session.flush();
        session.evict(doDuplicateHttps);

        DoHttps doDuplicateHttpsReloaded = new HttpsRepository().findByUuidAndVersion(doDuplicateHttps.getUuid(), snapshot.getValue());

        return HttpsConvertor.toSummary(doDuplicateHttpsReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoHttps doHttps = new HttpsRepository().findByUuidAndVersion(uuid, version);
        if (!doHttps.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new HttpsRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new HttpsRepository().remove(uuid, version);
        session.flush();
    }
}
