package nl.sodeso.deploykit.console.server.endpoint.services.webservice.validate;

import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateWebserviceIsReleased extends ReleasedValidationCheck<DoWebservice> {

    public void validate(List<DoWebservice> webservices, ValidationResult result) {
        for (DoWebservice doWebservice : webservices) {
            validate(doWebservice, result);
        }
    }

    public void validate(DoWebservice webservice, ValidationResult result) {
        if (webservice != null && !isReleased(webservice.getVersion())) {
            result.add(new ValidationMessage("webservice-version", ValidationMessage.Level.ERROR,
                    String.format("The selected webservice configuration '%s' does not contain a released version.", webservice.getLabel().getValue())));
        }
    }
}
