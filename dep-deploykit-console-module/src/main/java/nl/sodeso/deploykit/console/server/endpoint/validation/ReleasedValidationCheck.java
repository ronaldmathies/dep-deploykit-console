package nl.sodeso.deploykit.console.server.endpoint.validation;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;

/**
 * @author Ronald Mathies
 */
public abstract class ReleasedValidationCheck<T> implements ValidationCheck<T> {

    public boolean isReleased(VersionOptionType versionOptionType) {
        return versionOptionType.getValue().getType().isRelease();
    }

}
