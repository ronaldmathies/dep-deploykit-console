package nl.sodeso.deploykit.console.server.listener;

import nl.sodeso.deploykit.console.client.Constants;
import nl.sodeso.deploykit.console.server.endpoint.properties.DeployKitConsoleServerModuleProperties;
import nl.sodeso.gwt.ui.server.listener.AbstractModulePropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class DeployKitConsolePropertiesInitializerContextListener extends AbstractModulePropertiesInitializerContextListener<DeployKitConsoleServerModuleProperties> {

    @Override
    public Class<DeployKitConsoleServerModuleProperties> getServerModulePropertiesClass() {
        return DeployKitConsoleServerModuleProperties.class;
    }

    @Override
    public String getConfiguration() {
        return "dep-deploykit-console-configuration.properties";
    }

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
