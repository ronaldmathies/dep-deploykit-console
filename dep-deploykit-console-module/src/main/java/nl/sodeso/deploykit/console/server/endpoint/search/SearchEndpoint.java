package nl.sodeso.deploykit.console.server.endpoint.search;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.search.SearchResult;
import nl.sodeso.deploykit.console.client.application.search.rpc.SearchRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.elasticsearch.IndexedClasses;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import nl.sodeso.persistence.elasticsearch.serialization.Deserializer;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-search")
public class SearchEndpoint extends XsrfProtectedServiceServlet implements SearchRpc {

    private static final int MAX_SEARCH_RESULT_SIZE = 250;

    public ArrayList<SearchResult> search(String query) {
        ArrayList<SearchResult> results = new ArrayList<>();

        SearchRequestBuilder searchRequestBuilder = ElasticSearchClientFactory.getInstance().getClient(AnnotationUtil.getElasticSearchUnit(IndexEntry.class))
                .prepareSearch(IndexedClasses.getAllIndices())
                .setIndicesOptions(IndicesOptions.fromOptions(true, true, false, false))
                .setTypes(AnnotationUtil.getElasticSearchType(IndexEntry.class))
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.wildcardQuery("keywords", "*" + query + "*"))
                .addSort("type", SortOrder.ASC);

        searchRequestBuilder.setFrom(0).setSize(MAX_SEARCH_RESULT_SIZE).setExplain(false);

        SearchHits keywords = searchRequestBuilder.execute().actionGet().getHits();
        for (SearchHit hit : keywords.getHits()) {

            IndexEntry indexEntry = Deserializer.from(hit, IndexEntry.class);


            VersionOption version = null;
            if (indexEntry.getVersion() != null) {
                version = new VersionOption(
                        indexEntry.getVersion(), indexEntry.isRelease() ? VersionType.RELEASE : indexEntry.isBranch() ? VersionType.BRANCH : VersionType.SNAPSHOT);
            }

            SearchResult result = new SearchResult(indexEntry.getUuid(), version, indexEntry.getLabel(), indexEntry.getType());
            results.add(result);
        }

        return results;
    }
}
