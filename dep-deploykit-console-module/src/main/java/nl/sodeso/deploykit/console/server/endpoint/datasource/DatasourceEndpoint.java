package nl.sodeso.deploykit.console.server.endpoint.datasource;

import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceConvertor;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.Datasource;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.rpc.DatasourceRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.deploykit.console.server.endpoint.connectionpool.validation.ValidateConnectionPoolIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.credential.validation.ValidateCredentialIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.jdbcdriver.validate.ValidateJdbcDriverIsReleased;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-datasource")
public class DatasourceEndpoint extends BaseEndpoint implements DatasourceRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<DatasourceSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoDatasource> items = new DatasourceRepository().find(new Permissions(username, Permissions.READ));
        return DatasourceConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public Datasource findSingle(String uuid, VersionOption version) {
        DoDatasource doDatasource = new DatasourceRepository().findByUuidAndVersion(uuid, version);
        return DatasourceConvertor.from(doDatasource);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoDatasource doDatasource = new DatasourceRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doDatasource.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    /**
     * Returns a list of options
     * @param group
     * @return
     */
    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoDatasource> objects = new DatasourceRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoDatasource object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        DatasourceRepository datasourceRepository = new DatasourceRepository();
        DoDatasource doDatasource = datasourceRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doDatasource != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doDatasource.getGroup().getLabel().getValue(), doDatasource.getLabel().getValue())));
        }

        return result;
    }

    public DatasourceSummaryItem save(Datasource datasource) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), datasource.getGroup().getValue().getKey());
        validateNotReleased(datasource.getVersion());

        DatasourceRepository datasourceRepository = new DatasourceRepository();

        DoDatasource doDatasource;
        boolean labelChanged = false;

        if (datasource.getUuid() != null) {
            doDatasource = datasourceRepository.findByUuidAndVersion(datasource.getUuid(), datasource.getVersion().getValue());
            labelChanged = !doDatasource.getLabel().getValue().equals(datasource.getLabel().getValue());
            DatasourceConvertor.to(doDatasource, datasource);
        } else {
            doDatasource = DatasourceConvertor.to(null, datasource);
        }

        datasourceRepository.save(doDatasource);

        if (labelChanged) {
            datasourceRepository.updateLabel(doDatasource.getId(), doDatasource.getUuid(), datasource.getLabel());
        }

        session.flush();
        session.evict(doDatasource);

        DoDatasource doHttpReloaded = datasourceRepository.findByUuidAndVersion(doDatasource.getUuid(), doDatasource.getVersion().getValue());

        return DatasourceConvertor.toSummary(doHttpReloaded);
    }

    public CheckResult check(String uuid, VersionOption version) {
        return DatasourceCheck.check(uuid, version);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        DoDatasource doDatasourceToCommit = new DatasourceRepository().findByUuidAndVersion(uuid, fromVersion);

        new ValidateConnectionPoolIsReleased().validate(doDatasourceToCommit.getConnectionPools(), result);
        new ValidateCredentialIsReleased().validate(doDatasourceToCommit.getCredential(), result);
        new ValidateJdbcDriverIsReleased().validate(doDatasourceToCommit.getJdbcDriver(), result);

        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoDatasource doDatasourceToCommit = new DatasourceRepository().findByUuidAndVersion(uuid, fromVersion);
        Datasource committedDatasource = DatasourceConvertor.from(doDatasourceToCommit);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        committedDatasource.setVersion(version);
        DoDatasource doCommittedDatasource = DatasourceConvertor.to(null, committedDatasource);

        // Keep the UUID the same, we are creating a version here.
        doCommittedDatasource.setUuid(uuid);

        new DatasourceRepository().save(doCommittedDatasource);

        session.flush();
        session.evict(doCommittedDatasource);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoDatasource branchedVersion = new DatasourceRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoDatasource doDatasourceToBranch = new DatasourceRepository().findByUuidAndVersion(uuid, fromVersion);
        Datasource branchDatasource = DatasourceConvertor.from(doDatasourceToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchDatasource.setVersion(version);
        DoDatasource doDuplicateDatasource = DatasourceConvertor.to(null, branchDatasource);

        // Keep the UUID the same, we are creating a branch here for the same datasource.
        doDuplicateDatasource.setUuid(uuid);

        new DatasourceRepository().save(doDuplicateDatasource);

        session.flush();
        session.evict(doDuplicateDatasource);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new DatasourceRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public DatasourceSummaryItem duplicate(String uuid, VersionOption version) {
        DoDatasource doDatasourceToDuplicate = new DatasourceRepository().findByUuidAndVersion(uuid, version);
        Datasource duplicateDatasource = DatasourceConvertor.from(doDatasourceToDuplicate);

        duplicateDatasource.setLabel(new StringType(doDatasourceToDuplicate.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateDatasource.setVersion(snapshot);

        DoDatasource doDuplicateDatasource = DatasourceConvertor.to(null, duplicateDatasource);

        new DatasourceRepository().save(doDuplicateDatasource);

        session.flush();
        session.evict(doDuplicateDatasource);

        DoDatasource doDuplicateDatasourceReloaded = new DatasourceRepository().findByUuidAndVersion(doDuplicateDatasource.getUuid(), snapshot.getValue());

        return DatasourceConvertor.toSummary(doDuplicateDatasourceReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoDatasource doDatasource = new DatasourceRepository().findByUuidAndVersion(uuid, version);
        if (!doDatasource.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new DatasourceRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }

        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new DatasourceRepository().remove(uuid, version);
        session.flush();
    }


}
