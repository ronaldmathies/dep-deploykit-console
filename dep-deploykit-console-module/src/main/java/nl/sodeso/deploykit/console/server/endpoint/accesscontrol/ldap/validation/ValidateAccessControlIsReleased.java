package nl.sodeso.deploykit.console.server.endpoint.accesscontrol.ldap.validation;

import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateAccessControlIsReleased extends ReleasedValidationCheck<DoAccessControl> {

    public void validate(List<DoAccessControl> accessControls, ValidationResult result) {
        for (DoAccessControl doAccessControl : accessControls) {
            validate(doAccessControl, result);
        }
    }

    public void validate(DoAccessControl accessControl, ValidationResult result) {
        if (accessControl != null && !isReleased(accessControl.getVersion())) {
            result.add(new ValidationMessage("access-control-version", ValidationMessage.Level.ERROR,
                    String.format("The selected Access Control configuration '%s' does not contain a released version.", accessControl.getLabel())));
        }
    }

}
