package nl.sodeso.deploykit.console.server.endpoint.node;

import nl.sodeso.deploykit.console.client.application.node.NodeSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.node.NodeConvertor;
import nl.sodeso.deploykit.console.client.application.node.Node;
import nl.sodeso.deploykit.console.client.application.node.rpc.NodeRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.deploykit.console.domain.node.NodeRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-node")
public class NodeEndpoint extends BaseEndpoint implements NodeRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<NodeSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoNode> contacts = new NodeRepository().find(new Permissions(username, Permissions.READ));
        return NodeConvertor.toSummaries(contacts);
    }

    public Node findSingle(String uuid) {
        DoNode doNode = new NodeRepository().findByUuid(uuid);
        return NodeConvertor.from(doNode);
    }

    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoNode> objects = new NodeRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoNode object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoNode doNode = new NodeRepository().findByUuid(uuid);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doNode.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        NodeRepository nodeRepository = new NodeRepository();
        DoNode doNode = nodeRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doNode != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for  %s, please change the label.", doNode.getLabel().getValue())));
        }

        return result;
    }

    public NodeSummaryItem save(Node node) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), node.getGroup().getValue().getKey());

        DoNode doNode;

        if (node.getUuid() != null) {
            doNode = new NodeRepository().findByUuid(node.getUuid());
            NodeConvertor.to(doNode, node);
        } else {
            doNode = NodeConvertor.to(null, node);
        }

        new NodeRepository().save(doNode);

        session.flush();
        session.evict(doNode);

        DoNode doNodeReloaded = new NodeRepository().findByUuid(doNode.getUuid());

        return NodeConvertor.toSummary(doNodeReloaded);
    }

    public NodeSummaryItem duplicate(String uuid) {
        DoNode doNodeToDuplicate = new NodeRepository().findByUuid(uuid);
        Node duplicateNode = NodeConvertor.from(doNodeToDuplicate);

        duplicateNode.setLabel(new StringType(duplicateNode.getLabel().getValue() + " Copy"));
        duplicateNode.setUuid(null);
        DoNode doDuplicateNode = NodeConvertor.to(null, duplicateNode);

        new NodeRepository().save(doDuplicateNode);

        session.flush();
        session.evict(doDuplicateNode);

        DoNode doDuplicateHttpReloaded = new NodeRepository().findByUuid(doDuplicateNode.getUuid());

        return NodeConvertor.toSummary(doDuplicateHttpReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {

        ValidationResult result = new ValidationResult();

        DoNode doNode = new NodeRepository().findByUuid(uuid);
        if (!doNode.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new NodeRepository().remove(uuid);
    }
}
