package nl.sodeso.deploykit.console.server.listener;

import nl.sodeso.commons.quartz.AbstractQuartzSchedulerContextListener;
import org.quartz.JobDataMap;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class DeployKitConsoleQuartzSchedulerContextListener extends AbstractQuartzSchedulerContextListener {

    @Override
    public JobDataMap getJobDataMap(String name) {
        return null;
    }
}
