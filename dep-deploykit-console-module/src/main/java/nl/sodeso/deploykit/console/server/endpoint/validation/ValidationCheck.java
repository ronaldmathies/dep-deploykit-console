package nl.sodeso.deploykit.console.server.endpoint.validation;

import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public interface ValidationCheck<T> {

    void validate(List<T> doObject, ValidationResult result);

    void validate(T doObject, ValidationResult result);

}
