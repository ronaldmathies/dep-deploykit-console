package nl.sodeso.deploykit.console.server.endpoint.services.ldap.validate;

import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateLdapServiceIsReleased extends ReleasedValidationCheck<DoLdapService> {

    public void validate(List<DoLdapService> ldapServices, ValidationResult result) {
        for (DoLdapService doLdapService : ldapServices) {
            validate(doLdapService, result);
        }
    }

    public void validate(DoLdapService ldapService, ValidationResult result) {
        if (ldapService != null && !isReleased(ldapService.getVersion())) {
            result.add(new ValidationMessage("ldap-service-version", ValidationMessage.Level.ERROR,
                    String.format("The selected LDAP Service configuration '%s' does not contain a released version.", ldapService.getLabel().getValue())));
        }
    }
}
