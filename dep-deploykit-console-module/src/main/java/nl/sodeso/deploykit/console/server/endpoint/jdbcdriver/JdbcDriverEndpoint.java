package nl.sodeso.deploykit.console.server.endpoint.jdbcdriver;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.JdbcDriverConvertor;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.rpc.JdbcDriverRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverClass;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.JdbcDriverRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-jdbcdriver")
public class JdbcDriverEndpoint extends XsrfProtectedServiceServlet implements JdbcDriverRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<JdbcDriverSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoJdbcDriver> jdbcDrivers = new JdbcDriverRepository().find(new Permissions(username, Permissions.READ));
        return JdbcDriverConvertor.toSummaries(jdbcDrivers);
    }

    public JdbcDriver findSingle(String uuid, VersionOption version) {
        DoJdbcDriver doJdbcDriver = new JdbcDriverRepository().findByUuidAndVersion(uuid, version);
        return JdbcDriverConvertor.from(doJdbcDriver);
    }

    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoJdbcDriver doJdbcDriver = new JdbcDriverRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoDatasource> datasources = doJdbcDriver.getDatasources();
        for (DoDatasource doDatasource : datasources) {
            usages.add(new UsedBy(doDatasource.getUuid(), doDatasource.getVersion().getValue(), "Datasources", doDatasource.getLabel().getValue()));
        }
        return usages;
    }

    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoJdbcDriver> objects = new JdbcDriverRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoJdbcDriver object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<DefaultOption> classes(String uuid, VersionOption version) {
        DoJdbcDriver doJdbcDriver = new JdbcDriverRepository().findByUuidAndVersion(uuid, version);

        ArrayList<DefaultOption> options = new ArrayList<>(doJdbcDriver.getJdbcDriverClasses().size());
        for (DoJdbcDriverClass object : doJdbcDriver.getJdbcDriverClasses()) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        JdbcDriverRepository jdbcDriverRepository = new JdbcDriverRepository();
        DoJdbcDriver doJdbcDriver = jdbcDriverRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doJdbcDriver != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s, please change the label.", doJdbcDriver.getLabel().getValue())));
        }

        return result;
    }

    public JdbcDriverSummaryItem save(JdbcDriver jdbcDriver) {
        DoJdbcDriver doJdbcDriver;

        int currentClassesValue = 0;
        if (jdbcDriver.getUuid() != null) {
            doJdbcDriver = new JdbcDriverRepository().findByUuidAndVersion(jdbcDriver.getUuid(), jdbcDriver.getVersion().getValue());
            currentClassesValue = getUnique(doJdbcDriver.getJdbcDriverClasses());

            JdbcDriverConvertor.to(doJdbcDriver, jdbcDriver);
        } else {
            doJdbcDriver = JdbcDriverConvertor.to(null, jdbcDriver);
        }

        new JdbcDriverRepository().save(doJdbcDriver);

        session.flush();
        session.evict(doJdbcDriver);

        DoJdbcDriver doJdbcDriverReloaded = new JdbcDriverRepository().findByUuidAndVersion(doJdbcDriver.getUuid(), doJdbcDriver.getVersion().getValue());
//        int newClassesValue = getUnique(doJdbcDriver.getJdbcDriverClasses());
//
//        JdbcDriver reloadedJdbcDriver = JdbcDriverConvertor.from(doJdbcDriverReloaded);
//        reloadedJdbcDriver.setJdbcDriverClassesListChanged(currentClassesValue != newClassesValue);

        return JdbcDriverConvertor.toSummary(doJdbcDriverReloaded);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoJdbcDriver doJdbcDriverToDuplicate = new JdbcDriverRepository().findByUuidAndVersion(uuid, fromVersion);
        JdbcDriver duplicateJdbcDriver = JdbcDriverConvertor.from(doJdbcDriverToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateJdbcDriver.setVersion(version);
        DoJdbcDriver doDuplicateJdbcDriver = JdbcDriverConvertor.to(null, duplicateJdbcDriver);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateJdbcDriver.setUuid(uuid);

        new JdbcDriverRepository().save(doDuplicateJdbcDriver);

        session.flush();
        session.evict(doDuplicateJdbcDriver);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoJdbcDriver branchedVersion = new JdbcDriverRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoJdbcDriver doJdbcDriverToBranch = new JdbcDriverRepository().findByUuidAndVersion(uuid, fromVersion);
        JdbcDriver branchJdbcDriver = JdbcDriverConvertor.from(doJdbcDriverToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchJdbcDriver.setVersion(version);
        DoJdbcDriver doDuplicateJdbcDriver = JdbcDriverConvertor.to(null, branchJdbcDriver);

        // Keep the UUID the same, we are creating a branch here for the same webservice.
        doDuplicateJdbcDriver.setUuid(uuid);

        new JdbcDriverRepository().save(doDuplicateJdbcDriver);

        session.flush();
        session.evict(doDuplicateJdbcDriver);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new JdbcDriverRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public int getUnique(List<DoJdbcDriverClass> doJdbcDriverClasses) {
        int hashcode = 0;
        for (DoJdbcDriverClass doJdbcDriverClass : doJdbcDriverClasses) {
            hashcode += doJdbcDriverClass.getUuid().hashCode();
        }
        return hashcode;
    }

    public JdbcDriverSummaryItem duplicate(String uuid, VersionOption version) {
        DoJdbcDriver doJdbcDriverToDuplicate = new JdbcDriverRepository().findByUuidAndVersion(uuid, version);
        JdbcDriver duplicateJdbcDriver = JdbcDriverConvertor.from(doJdbcDriverToDuplicate);

        duplicateJdbcDriver.setLabel(new StringType(duplicateJdbcDriver.getLabel().getValue() + " Copy"));
        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateJdbcDriver.setVersion(snapshot);

        duplicateJdbcDriver.setUuid(null);
        DoJdbcDriver doDuplicateJdbcDriver = JdbcDriverConvertor.to(null, duplicateJdbcDriver);

        new JdbcDriverRepository().save(doDuplicateJdbcDriver);

        session.flush();
        session.evict(doDuplicateJdbcDriver);

        DoJdbcDriver doDuplicateCredentialReloaded = new JdbcDriverRepository().findByUuidAndVersion(doDuplicateJdbcDriver.getUuid(), snapshot.getValue());

        return JdbcDriverConvertor.toSummary(doDuplicateCredentialReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoJdbcDriver doJdbcDriver = new JdbcDriverRepository().findByUuidAndVersion(uuid, version);
        if (!doJdbcDriver.getDatasources().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new JdbcDriverRepository().remove(uuid, version);
    }
}
