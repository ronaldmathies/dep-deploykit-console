package nl.sodeso.deploykit.console.server.endpoint.accesscontrol.ldap;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.util.classloader.JdbcDriverClassloader;
import nl.sodeso.persistence.jdbc.SqlScriptExecutor;

import java.io.IOException;
import java.io.StringReader;
import java.sql.*;

/**
 * @author Ronald Mathies
 */
public class DatasourceAccessControlCheck {

    private static final String SELECT_USER = "SELECT %s, %s FROM %s WHERE %s = ?";
    private static final String SELECT_ROLES = "SELECT %s FROM %s WHERE %s = ?";

    public static CheckResult check(DoAccessControlDatasource doAccessControlDatasource, String username) {
        DoDatasource doDatasource = doAccessControlDatasource.getDatasource();

        Connection connection = null;
        try {
            JdbcDriverClassloader jdbcDriverClassloader = new JdbcDriverClassloader(doDatasource.getJdbcDriver().getJdbcDriverJars(), doDatasource.getClass().getClassLoader());
            jdbcDriverClassloader.loadClass(doDatasource.getJdbcDriver().getJdbcDriverClass().getClassname());

            DriverManager.setLoginTimeout(5);
            connection = DriverManager.getConnection(doDatasource.getUrl().getValue(), doDatasource.getCredential().getUsername().getValue(),
                    doDatasource.getCredential().getPassword().getValue());

            boolean catalogSet = false;
            for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
                if (doConnectionPool instanceof DoConnectionPoolJetty) {
                    DoConnectionPoolJetty doConnectionPoolJetty = (DoConnectionPoolJetty)doConnectionPool;

                    if (doConnectionPoolJetty.getDefaultCatalog() != null &&
                            doConnectionPoolJetty.getDefaultCatalog().getValue() != null &&
                            !doConnectionPoolJetty.getDefaultCatalog().getValue().isEmpty()) {
                        connection.setCatalog(doConnectionPoolJetty.getDefaultCatalog().getValue());
                        catalogSet = true;
                        break;
                    }
                }
            }

            if (!catalogSet) {
                for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
                    if (doConnectionPool.getConnectionInitSqls() != null &&
                            doConnectionPool.getConnectionInitSqls().getValue() != null &&
                            !doConnectionPool.getConnectionInitSqls().getValue().isEmpty()) {

                        SqlScriptExecutor.execute(connection, new StringReader(doConnectionPool.getConnectionInitSqls().getValue()));
                        break;
                    }
                }
            }

            StringBuilder message = new StringBuilder("Successfully connected with the LDAP server and resolved the following roles for the user '").append(username).append("'.</br><ul>");

            PreparedStatement preparedStatement = connection.prepareStatement(
                    String.format(SELECT_USER, doAccessControlDatasource.getUserColumn().getValue(), doAccessControlDatasource.getCredentialColumn().getValue(),
                            doAccessControlDatasource.getUserTable().getValue(), doAccessControlDatasource.getUserColumn().getValue()));
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.first()) {
                resultSet.close();

                preparedStatement = connection.prepareStatement(
                        String.format(SELECT_ROLES, doAccessControlDatasource.getRoleRoleColumn().getValue(),
                                doAccessControlDatasource.getRoleTable().getValue(), doAccessControlDatasource.getRoleUserColumn().getValue()));
                preparedStatement.setString(1, username);
                resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    String role = resultSet.getString(doAccessControlDatasource.getRoleRoleColumn().getValue());
                    message.append("<li>").append(role).append("</li>");
                }
                message.append("</ul>");

                resultSet.close();
            } else {
                return new CheckResult(false, String.format("User '%s' not found.", username));
            }

            jdbcDriverClassloader.close();


            return new CheckResult(true, message.toString());
        } catch (IOException | ClassNotFoundException | SQLException e) {
            return new CheckResult(false, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

}
