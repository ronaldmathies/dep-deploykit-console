package nl.sodeso.deploykit.console.server.servlet;

import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.KeystoreConvertor;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.deploykit.console.domain.connectors.https.KeystoreFactory;
import nl.sodeso.gwt.ui.client.util.ContentType;
import nl.sodeso.gwt.ui.server.servlet.AbstractFileUploadServlet;
import nl.sodeso.gwt.ui.server.servlet.UploadFile;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet(
        name = "Keystore File Upload Servlet",
        urlPatterns = {"/deploykitconsole/keystore/upload"}
)
@MultipartConfig(
        fileSizeThreshold=1024*1024*10,
        maxFileSize=1024*1024*50,
        maxRequestSize=1024*1024*100)
public class KeystoreFileUploadServlet extends AbstractFileUploadServlet {

    @Override
    public void upload(List<UploadFile> uploadedFiles, HttpServletResponse response) throws ServletException {
        try {
            UploadFile uploadFile = null;
            if (!uploadedFiles.isEmpty()) {
                uploadFile = uploadedFiles.get(0);
            }

            DoKeystore doKeystore = KeystoreConvertor.to(null, uploadFile);
            new KeystoreFactory().save(doKeystore);

            response.setContentType(ContentType.TEXT_HTML.getContentType());
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print("{ \"uuid\": \"" + doKeystore.getUuid() + "\" }");
        } catch (IOException e) {
            throw new ServletException("Failed to store keystore.", e);
        }
    }
}
