package nl.sodeso.deploykit.console.server.endpoint.accesscontrol.ldap;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.accesscontrol.AccessControlConvertor;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.rpc.AccessControlRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.AccessControlRepository;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.deploykit.console.server.endpoint.datasource.validation.ValidateDatasourceIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.services.ldap.validate.ValidateLdapServiceIsReleased;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-accesscontrol")
public class AccessControlEndpoint extends BaseEndpoint implements AccessControlRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<AccessControlSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoAccessControl> items = new AccessControlRepository().find(new Permissions(username, Permissions.READ));
        return AccessControlConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public AccessControl findSingle(String uuid, VersionOption version) {
        DoAccessControl doAccessControl = new AccessControlRepository().findByUuidAndVersion(uuid, version);
        return AccessControlConvertor.from(doAccessControl);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoAccessControl doLdapAccessControl = new AccessControlRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doLdapAccessControl.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    /**
     * Returns a list of options
     * @param group
     * @return
     */
    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoAccessControl> objects = new AccessControlRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoAccessControl object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        AccessControlRepository accessControlRepository = new AccessControlRepository();
        DoAccessControl doAccessControl = accessControlRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doAccessControl != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doAccessControl.getGroup().getLabel().getValue(), doAccessControl.getLabel().getValue())));
        }

        return result;
    }

    public AccessControlSummaryItem save(AccessControl accessControl) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), accessControl.getGroup().getValue().getKey());
        validateNotReleased(accessControl.getVersion());

        AccessControlRepository accessControlRepository = new AccessControlRepository();

        DoAccessControl doAccessControl;
        boolean labelChanged = false;

        if (accessControl.getUuid() != null) {
            doAccessControl = accessControlRepository.findByUuidAndVersion(accessControl.getUuid(), accessControl.getVersion().getValue());

            labelChanged = !doAccessControl.getLabel().getValue().equals(accessControl.getLabel().getValue());
            AccessControlConvertor.to(doAccessControl, accessControl);
        } else {
            doAccessControl = AccessControlConvertor.to(null, accessControl);
        }

        accessControlRepository.save(doAccessControl);

        if (labelChanged) {
            accessControlRepository.updateLabel(doAccessControl.getId(), doAccessControl.getUuid(), accessControl.getLabel());
        }

        session.flush();
        session.evict(doAccessControl);

        DoAccessControl doLdapAccessControlReloaded = accessControlRepository.findByUuidAndVersion(doAccessControl.getUuid(), doAccessControl.getVersion().getValue());

        return AccessControlConvertor.toSummary(doLdapAccessControlReloaded);
    }

    public CheckResult check(String uuid, VersionOption version, String username) {
        DoAccessControl doAccessControl = new AccessControlRepository().findByUuidAndVersion(uuid, version);
        if (doAccessControl instanceof DoAccessControlLdap) {
            return LdapAccessControlCheck.check((DoAccessControlLdap)doAccessControl, username);
        }

        return DatasourceAccessControlCheck.check((DoAccessControlDatasource)doAccessControl, username);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        DoAccessControl doAccessControl = new AccessControlRepository().findByUuidAndVersion(uuid, fromVersion);
        if (doAccessControl instanceof DoAccessControlLdap) {
            DoAccessControlLdap doAccessControlLdap = (DoAccessControlLdap)doAccessControl;
            new ValidateLdapServiceIsReleased().validate(doAccessControlLdap.getLdapService(), result);
        } else {
            DoAccessControlDatasource doAccessControlDatasource = (DoAccessControlDatasource)doAccessControl;
            new ValidateDatasourceIsReleased().validate(doAccessControlDatasource.getDatasource(), result);
        }

        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {


        DoAccessControl doAccessControlToDuplicate = new AccessControlRepository().findByUuidAndVersion(uuid, fromVersion);
        AccessControl duplicateAccessControl = AccessControlConvertor.from(doAccessControlToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateAccessControl.setVersion(version);
        DoAccessControl doDuplicateAccessControl = AccessControlConvertor.to(null, duplicateAccessControl);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateAccessControl.setUuid(uuid);

        new AccessControlRepository().save(doDuplicateAccessControl);

        session.flush();
        session.evict(doDuplicateAccessControl);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoAccessControl branchedVersion = new AccessControlRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoAccessControl doAccessControlToBranch = new AccessControlRepository().findByUuidAndVersion(uuid, fromVersion);
        AccessControl branchAccessControl = AccessControlConvertor.from(doAccessControlToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchAccessControl.setVersion(version);
        DoAccessControl doDuplicateAccessControl = AccessControlConvertor.to(null, branchAccessControl);

        // Keep the UUID the same, we are creating a branch here for the same webservice.
        doDuplicateAccessControl.setUuid(uuid);

        new AccessControlRepository().save(doDuplicateAccessControl);

        session.flush();
        session.evict(doDuplicateAccessControl);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new AccessControlRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public AccessControlSummaryItem duplicate(String uuid, VersionOption version) {
        DoAccessControl doAccessControlToDuplicate = new AccessControlRepository().findByUuidAndVersion(uuid, version);
        AccessControl duplicateAccessControl = AccessControlConvertor.from(doAccessControlToDuplicate);

        duplicateAccessControl.setLabel(new StringType(doAccessControlToDuplicate.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateAccessControl.setVersion(snapshot);

        DoAccessControl doDuplicateAccessControl = AccessControlConvertor.to(null, duplicateAccessControl);

        new AccessControlRepository().save(doDuplicateAccessControl);

        session.flush();
        session.evict(doDuplicateAccessControl);

        DoAccessControl doDuplicateAccessControlReloaded = new AccessControlRepository().findByUuidAndVersion(doDuplicateAccessControl.getUuid(), snapshot.getValue());

        return AccessControlConvertor.toSummary(doDuplicateAccessControlReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoAccessControl doAccessControl = new AccessControlRepository().findByUuidAndVersion(uuid, version);
        if (!doAccessControl.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new AccessControlRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new AccessControlRepository().remove(uuid, version);
        session.flush();
    }
}
