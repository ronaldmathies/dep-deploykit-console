package nl.sodeso.deploykit.console.server.endpoint.connectionpool;

import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionpoolSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolConvertor;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.rpc.ConnectionPoolRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolRepository;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-pool")
public class ConnectionPoolEndpoint extends BaseEndpoint implements ConnectionPoolRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<ConnectionpoolSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoConnectionPool> objects = new ConnectionPoolRepository().find(new Permissions(username, Permissions.READ));
        return ConnectionPoolConvertor.toSummaries(objects);
    }

    public ConnectionPool findSingle(String uuid, VersionOption version) {
        DoConnectionPool doConnectionPool = new ConnectionPoolRepository().findByUuidAndVersion(uuid, version);
        return ConnectionPoolConvertor.from(doConnectionPool);
    }

    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoConnectionPool> objects = new ConnectionPoolRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoConnectionPool object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoConnectionPool doConnectionPool = new ConnectionPoolRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoDatasource> datasources = doConnectionPool.getDatasources();
        for (DoDatasource doDatasource : datasources) {
            usages.add(new UsedBy(doDatasource.getUuid(), doDatasource.getVersion().getValue(), "Datasources", doDatasource.getLabel().getValue()));
        }
        return usages;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        ConnectionPoolRepository connectionPoolRepository = new ConnectionPoolRepository();
        DoConnectionPool doConnectionPool = connectionPoolRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doConnectionPool != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doConnectionPool.getGroup().getLabel().getValue(), doConnectionPool.getLabel().getValue())));
        }

        return result;
    }

    public ConnectionpoolSummaryItem save(ConnectionPool connectionPool) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), connectionPool.getGroup().getValue().getKey());
        validateNotReleased(connectionPool.getVersion());

        DoConnectionPool doConnectionPool;
        boolean labelChanged = false;

        if (connectionPool.getUuid() != null) {
            doConnectionPool = new ConnectionPoolRepository().findByUuidAndVersion(connectionPool.getUuid(), connectionPool.getVersion().getValue());
            labelChanged = !doConnectionPool.getLabel().getValue().equals(connectionPool.getLabel().getValue());

            if (connectionPool.getJavaServerProvider().getValue().getKey().equals("jetty")) {
                if (doConnectionPool instanceof DoConnectionPoolJBoss) {
                    new ConnectionPoolRepository().remove(connectionPool.getUuid(), connectionPool.getVersion().getValue());
                    ConnectionPoolConvertor.to(null, connectionPool);
                } else {
                    ConnectionPoolConvertor.to(doConnectionPool, connectionPool);
                }
            } else {
                if (doConnectionPool instanceof DoConnectionPoolJetty) {
                    new ConnectionPoolRepository().remove(connectionPool.getUuid(), connectionPool.getVersion().getValue());
                    ConnectionPoolConvertor.to(null, connectionPool);
                } else {
                    ConnectionPoolConvertor.to(doConnectionPool, connectionPool);
                }
            }


        } else {
            doConnectionPool = ConnectionPoolConvertor.to(null, connectionPool);
        }

        new ConnectionPoolRepository().save(doConnectionPool);

        if (labelChanged) {
            new ConnectionPoolRepository().updateLabel(doConnectionPool.getId(), doConnectionPool.getUuid(), connectionPool.getLabel());
        }

        session.flush();
        session.evict(doConnectionPool);

        DoConnectionPool doConnectionPoolReloaded = new ConnectionPoolRepository().findByUuidAndVersion(doConnectionPool.getUuid(), doConnectionPool.getVersion().getValue());

        return ConnectionPoolConvertor.toSummary(doConnectionPoolReloaded);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoConnectionPool doConnectionPoolToCommit = new ConnectionPoolRepository().findByUuidAndVersion(uuid, fromVersion);
        ConnectionPool committedConnectionPool = ConnectionPoolConvertor.from(doConnectionPoolToCommit);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        committedConnectionPool.setVersion(version);
        DoConnectionPool doCommittedConnectionPool = ConnectionPoolConvertor.to(null, committedConnectionPool);

        // Keep the UUID the same, we are creating a version here.
        doCommittedConnectionPool.setUuid(uuid);

        new ConnectionPoolRepository().save(doCommittedConnectionPool);

        session.flush();
        session.evict(doCommittedConnectionPool);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoConnectionPool branchedVersion = new ConnectionPoolRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoConnectionPool doConnectionPoolToBranch = new ConnectionPoolRepository().findByUuidAndVersion(uuid, fromVersion);
        ConnectionPool branchConnectionPool = ConnectionPoolConvertor.from(doConnectionPoolToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchConnectionPool.setVersion(version);
        DoConnectionPool doDuplicateConnectionPool = ConnectionPoolConvertor.to(null, branchConnectionPool);

        // Keep the UUID the same, we are creating a branch here for the same datasource.
        doDuplicateConnectionPool.setUuid(uuid);

        new ConnectionPoolRepository().save(doDuplicateConnectionPool);

        session.flush();
        session.evict(doDuplicateConnectionPool);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new ConnectionPoolRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public ConnectionpoolSummaryItem duplicate(String uuid, VersionOption version) {
        DoConnectionPool doConnectionPoolToDuplicate = new ConnectionPoolRepository().findByUuidAndVersion(uuid, version);
        ConnectionPool duplicateConnectionPool = ConnectionPoolConvertor.from(doConnectionPoolToDuplicate);

        duplicateConnectionPool.setLabel(new StringType(duplicateConnectionPool.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateConnectionPool.setVersion(snapshot);

        DoConnectionPool doDuplicateConnectionPool = ConnectionPoolConvertor.to(null, duplicateConnectionPool);

        new ConnectionPoolRepository().save(doDuplicateConnectionPool);

        session.flush();
        session.evict(doDuplicateConnectionPool);

        DoConnectionPool doDuplicateConnectionPoolReloaded = new ConnectionPoolRepository().findByUuidAndVersion(doDuplicateConnectionPool.getUuid(), snapshot.getValue());

        return ConnectionPoolConvertor.toSummary(doDuplicateConnectionPoolReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoConnectionPool doConnectionPool = new ConnectionPoolRepository().findByUuidAndVersion(uuid, version);
        if (!doConnectionPool.getDatasources().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new ConnectionPoolRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }

        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new ConnectionPoolRepository().remove(uuid, version);
    }

}
