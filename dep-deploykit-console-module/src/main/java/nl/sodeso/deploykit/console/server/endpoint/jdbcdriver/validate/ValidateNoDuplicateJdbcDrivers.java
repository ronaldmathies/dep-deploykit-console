package nl.sodeso.deploykit.console.server.endpoint.jdbcdriver.validate;

import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.server.endpoint.validation.ValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ValidateNoDuplicateJdbcDrivers implements ValidationCheck<DoDatasource> {

    public void validate(List<DoDatasource> datasoures, ValidationResult result) {
        Map<String, List<DoDatasource>> drivers = new HashMap<>();

        for (DoDatasource doDatasource : datasoures) {
            String jdbcDriverClass = doDatasource.getJdbcDriver().getJdbcDriverClass().getClassname();

            if (!drivers.containsKey(jdbcDriverClass)) {
                drivers.put(jdbcDriverClass, new ArrayList<>());
            }

            List<DoDatasource> driver = drivers.get(jdbcDriverClass);
            driver.add(doDatasource);
        }

        for (Map.Entry<String, List<DoDatasource>> driverEntry : drivers.entrySet()) {
            if (driverEntry.getValue().size() > 1) {
                StringBuilder message = new StringBuilder(String.format("The following data sources have different JDBC drivers but they all share a common driver implementation (%s), this causes conflicts when this profile is used.<ul>", driverEntry.getKey()));
                for (DoDatasource doDatasource : driverEntry.getValue()) {
                    message.append(String.format("<li>(%s) %s: %s</li>",
                            doDatasource.getGroup().getLabel().getValue(),
                            doDatasource.getLabel().getValue(),
                            doDatasource.getVersion().getValue().getDisplayDescription()));
                }
                message.append("</ul>");

                result.add(new ValidationMessage("datasource-jdbc-conflicts", ValidationMessage.Level.ERROR, message.toString()));

            }

        }
    }
    public void validate(DoDatasource doDatasource, ValidationResult result) {

    }
}
