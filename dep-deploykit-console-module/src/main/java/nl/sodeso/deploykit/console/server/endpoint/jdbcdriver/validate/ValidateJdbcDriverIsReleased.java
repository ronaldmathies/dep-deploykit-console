package nl.sodeso.deploykit.console.server.endpoint.jdbcdriver.validate;

import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateJdbcDriverIsReleased extends ReleasedValidationCheck<DoJdbcDriver> {

    public void validate(List<DoJdbcDriver> jdbcDrivers, ValidationResult result) {
        for (DoJdbcDriver doJdbcDriver : jdbcDrivers) {
            validate(doJdbcDriver, result);
        }
    }

    public void validate(DoJdbcDriver jdbcDriver, ValidationResult result) {
        if (jdbcDriver != null && !isReleased(jdbcDriver.getVersion())) {
            result.add(new ValidationMessage("jdbcdriver-version", ValidationMessage.Level.ERROR,
                    String.format("The selected JDBC driver configuration (%s) %s: %s is not a released version.",
                            jdbcDriver.getGroup().getLabel().getValue(),
                            jdbcDriver.getLabel().getValue(),
                            jdbcDriver.getVersion().getValue().getDisplayDescription())));
        }
    }
}
