package nl.sodeso.deploykit.console.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.ServerModuleProperties;

/**
 * @author Ronald Mathies
 */
public class DeployKitConsoleServerModuleProperties extends ServerModuleProperties {

    public DeployKitConsoleServerModuleProperties() {
        super();
    }
}
