package nl.sodeso.deploykit.console.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.deploykit.console.client.application.WelcomeLink;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpMenuItem;
import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPoolMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceMenuItem;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverMenuItem;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialMenuItem;
import nl.sodeso.deploykit.console.client.application.node.NodeMenuItem;
import nl.sodeso.deploykit.console.client.application.profile.ProfileMenuItem;
import nl.sodeso.deploykit.console.client.application.search.SearchWindow;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceMenuItem;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceMenuItem;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuController;
import nl.sodeso.gwt.ui.client.controllers.menu.MenuItem;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationButton;
import nl.sodeso.gwt.ui.client.controllers.navigation.NavigationController;
import nl.sodeso.gwt.ui.client.link.LinkFactory;
import nl.sodeso.gwt.ui.client.resources.Icon;
import nl.sodeso.gwt.ui.client.rpc.DefaultRunAsyncCallback;
import nl.sodeso.gwt.ui.client.util.Align;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class DeployKitConsoleEntryPoint extends ModuleEntryPoint {

    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    @Override
    public String getModuleName() {
        return "DeployKit Console";
    }

    @Override
    public Icon getModuleIcon() {
        return Icon.Tasks;
    }

    @Override
    public void onBeforeModuleLoad(BeforeModuleLoadFinishedTrigger trigger) {
        trigger.fire();
    }

    @Override
    public void onAfterModuleLoad(AfterModuleLoadFinishedTrigger trigger) {
        GWT.runAsync(new DefaultRunAsyncCallback() {

            public void success() {
                initMenu();

                if (!LinkFactory.hasLink(getWelcomeToken())) {
                    LinkFactory.addLink(new WelcomeLink());
                }

                trigger.fire();
            }

        });
    }

    private void initMenu() {
        MenuController.instance().setFullwidth(true);

        MenuController.instance().addMenuItems(
                new ProfileMenuItem(),
                new MenuItem("mi-general", Icon.Settings, "General", null)
                        .addChildren(
                                new CredentialMenuItem()
                        ),
                new MenuItem("mi-connectors", Icon.Bookmark, "Connectors", null)
                        .addChildren(
                                new HttpsMenuItem(),
                                new HttpMenuItem()
                        ),
                new MenuItem("mi-datasources", Icon.Database, "Datasources", null)
                        .addChildren(
                                new ConnectionPoolMenuItem(),
                                new JdbcDriverMenuItem(),
                                new DatasourceMenuItem()
                        ),
                new MenuItem("mi-accesscontrol", Icon.Key, "Access Control", null)
                        .addChildren(
                                new AccessControlMenuItem()
                        ),
                new MenuItem("mi-services", Icon.Book, "Services", null)
                        .addChildren(
                                new WebserviceMenuItem(),
                                new LdapServiceMenuItem()
                        ),
                new MenuItem("mi-resources", Icon.Server, "Resources", null)
                        .addChildren(
                                new NodeMenuItem()
                        )
        );

        NavigationButton searchButton = new NavigationButton("search", Icon.Search);
        searchButton.addClickHandler((event) -> SearchWindow.instance().center(false, false));
        NavigationController.instance().addButtons(Align.LEFT, searchButton);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWelcomeToken() {
        return WelcomeLink.TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public void activateModule(ActivateFinishedTrigger trigger) {
        trigger.fire();
    }

    /**
     * {@inheritDoc}
     */
    public void suspendModule(SuspendFinishedTrigger trigger) {
        trigger.fire();
    }
}
