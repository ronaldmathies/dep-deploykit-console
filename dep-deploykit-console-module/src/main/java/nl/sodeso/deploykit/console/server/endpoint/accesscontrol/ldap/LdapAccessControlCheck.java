package nl.sodeso.deploykit.console.server.endpoint.accesscontrol.ldap;

import nl.sodeso.commons.ldap.LdapClient;
import nl.sodeso.commons.ldap.Scope;
import nl.sodeso.commons.ldap.exception.*;
import nl.sodeso.commons.ldap.exception.LdapSearchException;
import nl.sodeso.commons.ldap.utils.LdapUtil;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;

import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Ronald Mathies
 */
public class LdapAccessControlCheck {

    public static CheckResult check(DoAccessControlLdap doAccessControlLdap, String username) {

        Properties configuration = new Properties();
        configuration.put(LdapClient.KEY_HOST, doAccessControlLdap.getLdapService().getDomain().getValue());
        configuration.put(LdapClient.KEY_PORT, doAccessControlLdap.getLdapService().getPort().getValueAsString());
        configuration.put(LdapClient.KEY_PRINCIPAL, doAccessControlLdap.getLdapService().getCredential().getUsername().getValue());
        configuration.put(LdapClient.KEY_CREDENTIALS, doAccessControlLdap.getLdapService().getCredential().getPassword().getValue());

        LdapClient ldapClient = new LdapClient(configuration);

        try {
            ldapClient.connect();

            String userAttr = doAccessControlLdap.getUserAttribute().getValue();
            String userBaseDn = doAccessControlLdap.getUserBaseDn().getValue();
            String userObjectClass = doAccessControlLdap.getUserObjectClass().getValueAsString();
            String passwordAttr = doAccessControlLdap.getUserPasswordAttribute().getValue();
            String roleBaseDn = doAccessControlLdap.getRoleBaseDn().getValue();
            String roleMemberAttr = doAccessControlLdap.getRoleMemberAttribute().getValue();
            String roleNameAttr = doAccessControlLdap.getRoleNameAttribute().getValue();
            String roleObjectClass = doAccessControlLdap.getRoleObjectClass().getValue();

            ArrayList<SearchResult> userResults = null;
            try {
                userResults = ldapClient.search(userBaseDn, "(&(objectClass=" + userObjectClass + ")(" + userAttr + "=" + username + "))", Scope.SUBTREE_SCOPE);
                if (userResults.isEmpty()) {
                    return new CheckResult(false,
                            "Failed to find the user, please check any of the following information:<br/>" +
                                    "<ul>" +
                                    "<li>Entered username</li>" +
                                    "<li>User Attribute</li>" +
                                    "<li>User Object Class" +
                                    "</ul>");
                }
            } catch (LdapSearchException e) {
                return new CheckResult(false,
                        "Failed to find the user, please check any of the following information:<br/>" +
                                "<ul><li>User Base DN</li></ul>");
            }

            SearchResult userResult = userResults.get(0);
            byte[] password = LdapUtil.getAttributeValue(userResult, passwordAttr);
            if (password == null || password.length == 0) {
                return new CheckResult(false,
                        "Found the user but failed to resolve the password, please check any of the following information:<br/>" +
                                "<ul>" +
                                "<li>Password Attribute</li>" +
                                "</ul>");
            }

            ArrayList<SearchResult> roleResults = null;
            try {
                roleResults = ldapClient.search(roleBaseDn, "(&(objectClass=" + roleObjectClass + ")(" + roleMemberAttr + "=" + userAttr + "=" + username + "," + userBaseDn + "))", Scope.SUBTREE_SCOPE);
                if (roleResults.isEmpty()) {
                    return new CheckResult(false,
                            "Failed to find the roles of the user, please check any of the following information:<br/>" +
                                    "<ul>" +
                                    "<li>Role Member Attribute</li>" +
                                    "<li>Role Name Attribute</li>" +
                                    "<li>Role Object Class" +
                                    "</ul>");                }
            } catch (LdapSearchException e) {
                return new CheckResult(false,
                        "Failed to find the roles of the user, please check any of the following information:<br/>" +
                                "<ul><li>Role Base DN</li></ul>");
            }

            StringBuilder message = new StringBuilder("Successfully connected with the LDAP server and resolved the following roles for the user '" + username + "'.</br><ul>");
            for (SearchResult roleResult : roleResults) {
                List<Object> allRoles = LdapUtil.getAllAttributeValues(roleResult, roleNameAttr);
                for (Object role : allRoles) {
                    message.append("<li>").append(role).append("</li>");
                }
            }
            message.append("</ul>");

            return new CheckResult(true, message.toString());
        } catch (LdapConnectException | LdapNotConnectedException e) {
            return new CheckResult(false, e.getMessage());
        } finally {
            try {
                ldapClient.disconnect();
            } catch (LdapDisconnectException e) {
                // Do nothing...
            }
        }
    }

}
