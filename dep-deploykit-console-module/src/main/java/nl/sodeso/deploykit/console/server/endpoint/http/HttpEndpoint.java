package nl.sodeso.deploykit.console.server.endpoint.http;

import nl.sodeso.deploykit.console.client.application.connectors.http.HttpSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.connectors.http.HttpConvertor;
import nl.sodeso.deploykit.console.client.application.connectors.http.Http;
import nl.sodeso.deploykit.console.client.application.connectors.http.rpc.HttpRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.http.HttpRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-http")
public class HttpEndpoint extends BaseEndpoint implements HttpRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<HttpSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoHttp> items = new HttpRepository().find(new Permissions(username, Permissions.READ), null);
        return HttpConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public Http findSingle(String uuid, VersionOption version) {
        DoHttp doHttp = new HttpRepository().findByUuidAndVersion(uuid, version);
        return HttpConvertor.from(doHttp);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoHttp doHttp = new HttpRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doHttp.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    /**
     * Returns a list of options
     * @param group
     * @return
     */
    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoHttp> objects = new HttpRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoHttp object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }


    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        HttpRepository httpRepository = new HttpRepository();
        DoHttp doHttp = httpRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doHttp != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doHttp.getGroup().getLabel().getValue(), doHttp.getLabel().getValue())));
        }

        return result;
    }

    public HttpSummaryItem save(Http http) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), http.getGroup().getValue().getKey());
        validateNotReleased(http.getVersion());

        HttpRepository httpRepository = new HttpRepository();

        DoHttp doHttp;
        boolean labelChanged = false;

        if (http.getUuid() != null) {
            doHttp = httpRepository.findByUuidAndVersion(http.getUuid(), http.getVersion().getValue());

            labelChanged = !doHttp.getLabel().getValue().equals(http.getLabel().getValue());
            HttpConvertor.to(doHttp, http);
        } else {
            doHttp = HttpConvertor.to(null, http);
        }

        httpRepository.save(doHttp);

        if (labelChanged) {
            httpRepository.updateLabel(doHttp.getId(), doHttp.getUuid(), http.getLabel());
        }

        session.flush();
        session.evict(doHttp);

        DoHttp doHttpReloaded = httpRepository.findByUuidAndVersion(doHttp.getUuid(), doHttp.getVersion().getValue());

        return HttpConvertor.toSummary(doHttpReloaded);
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoHttp doHttpToDuplicate = new HttpRepository().findByUuidAndVersion(uuid, fromVersion);
        Http duplicateHttp = HttpConvertor.from(doHttpToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateHttp.setVersion(version);
        DoHttp doDuplicateHttp = HttpConvertor.to(null, duplicateHttp);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateHttp.setUuid(uuid);

        new HttpRepository().save(doDuplicateHttp);

        session.flush();
        session.evict(doDuplicateHttp);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoHttp branchedVersion = new HttpRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoHttp doHttpToBranch = new HttpRepository().findByUuidAndVersion(uuid, fromVersion);
        Http branchHttp = HttpConvertor.from(doHttpToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchHttp.setVersion(version);
        DoHttp doDuplicateHttp = HttpConvertor.to(null, branchHttp);

        // Keep the UUID the same, we are creating a branch here for the same http.
        doDuplicateHttp.setUuid(uuid);

        new HttpRepository().save(doDuplicateHttp);

        session.flush();
        session.evict(doDuplicateHttp);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new HttpRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public HttpSummaryItem duplicate(String uuid, VersionOption version) {
        DoHttp doHttpToDuplicate = new HttpRepository().findByUuidAndVersion(uuid, version);
        Http duplicateHttp = HttpConvertor.from(doHttpToDuplicate);

        duplicateHttp.setLabel(new StringType(doHttpToDuplicate.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateHttp.setVersion(snapshot);

        DoHttp doDuplicateHttp = HttpConvertor.to(null, duplicateHttp);

        new HttpRepository().save(doDuplicateHttp);

        session.flush();
        session.evict(doDuplicateHttp);

        DoHttp doDuplicateHttpReloaded = new HttpRepository().findByUuidAndVersion(doDuplicateHttp.getUuid(), snapshot.getValue());

        return HttpConvertor.toSummary(doDuplicateHttpReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoHttp doHttp = new HttpRepository().findByUuidAndVersion(uuid, version);
        if (!doHttp.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new HttpRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new HttpRepository().remove(uuid, version);
        session.flush();
    }

}
