package nl.sodeso.deploykit.console.server.endpoint.connectionpool.validation;

import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateConnectionPoolIsReleased extends ReleasedValidationCheck<DoConnectionPool> {

    public void validate(List<DoConnectionPool> connectionPools, ValidationResult result) {
        for (DoConnectionPool doConnectionPool : connectionPools) {
            validate(doConnectionPool, result);
        }
    }

    public void validate(DoConnectionPool connectionPool, ValidationResult result) {
        if (connectionPool != null && !isReleased(connectionPool.getVersion())) {
            result.add(new ValidationMessage("connectionpool-version", ValidationMessage.Level.ERROR,
                    String.format("The selected connection pool configuration '%s' does not contain a released version.", connectionPool.getLabel().getValue())));
        }
    }
}
