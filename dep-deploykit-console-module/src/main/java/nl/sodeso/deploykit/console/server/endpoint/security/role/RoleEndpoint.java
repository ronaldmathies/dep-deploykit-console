package nl.sodeso.deploykit.console.server.endpoint.security.role;

import com.google.gwt.user.server.rpc.XsrfProtectedServiceServlet;
import nl.sodeso.deploykit.console.client.application.security.role.Role;
import nl.sodeso.deploykit.console.client.application.security.role.RoleSummaryItem;
import nl.sodeso.deploykit.console.client.application.security.role.rpc.RoleRpc;
import nl.sodeso.deploykit.console.domain.security.role.RoleConvertor;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.role.RoleRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-role")
public class RoleEndpoint extends XsrfProtectedServiceServlet implements RoleRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<RoleSummaryItem> findSummaries() {
        List<DoRole> roles = new RoleRepository().find();
        return RoleConvertor.toSummaries(roles);
    }

    public Role findSingle(String uuid) {
        DoRole doRole = new RoleRepository().findByUuid(uuid);
        return RoleConvertor.from(doRole);
    }

    public ArrayList<DefaultOption> asOptions() {
        List<DoRole> objects = new RoleRepository().find();

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoRole object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ArrayList<UsedBy> usage(String uuid) {
//        DoRole doRole = new RoleRepository().findByUuid(uuid);
//
        ArrayList<UsedBy> usages = new ArrayList<>();
//        for (DoGroup doGroup : doRole.getGroups()) {
//            usages.add(new UsedBy(doGroup.getUuid(), "Group", doGroup.getLabel().getValue()));
//        }
//
        return usages;
    }

    public RoleSummaryItem save(Role role) {
        DoRole doRole;

        if (role.getUuid() != null) {
            doRole = new RoleRepository().findByUuid(role.getUuid());
            RoleConvertor.to(doRole, role);
        } else {
            doRole = RoleConvertor.to(null, role);
        }

        new RoleRepository().save(doRole);

        session.flush();
        session.evict(doRole);

        DoRole doRoleReloaded = new RoleRepository().findByUuid(doRole.getUuid());

        return RoleConvertor.toSummary(doRoleReloaded);
    }

    public RoleSummaryItem duplicate(String uuid) {
        DoRole doRoleToDuplicate = new RoleRepository().findByUuid(uuid);
        Role duplicateRole = RoleConvertor.from(doRoleToDuplicate);
        duplicateRole.setLabel(new StringType(duplicateRole.getLabel().getValue() + " Copy"));

        duplicateRole.setUuid(null);
        DoRole doDuplicateRole = RoleConvertor.to(null, duplicateRole);

        new RoleRepository().save(doDuplicateRole);

        session.flush();
        session.evict(doDuplicateRole);

        DoRole doDuplicateRoleReloaded = new RoleRepository().findByUuid(doDuplicateRole.getUuid());

        return RoleConvertor.toSummary(doDuplicateRoleReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoRole doRole = new RoleRepository().findByUuid(uuid);

//        if (!doRole.getGroups().isEmpty()) {
//            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not possible to delete this role since it is in use by other components, use the Used By button to see which components use this role."));
//        }

        return result;

    }

    public void delete(String uuid, VersionOption version) {
        new RoleRepository().remove(uuid);
    }
}
