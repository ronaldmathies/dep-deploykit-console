package nl.sodeso.deploykit.console.server.endpoint.services.ldap;

import nl.sodeso.commons.ldap.LdapClient;
import nl.sodeso.commons.ldap.exception.LdapConnectException;
import nl.sodeso.commons.ldap.exception.LdapDisconnectException;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.services.ldap.LdapServiceConvertor;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapService;
import nl.sodeso.deploykit.console.client.application.services.ldap.rpc.LdapServiceRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.ldap.LdapServiceRepository;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-ldapservice")
public class LdapServiceEndpoint extends BaseEndpoint implements LdapServiceRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public ArrayList<LdapServiceSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoLdapService> contacts = new LdapServiceRepository().find(new Permissions(username, Permissions.READ));
        return LdapServiceConvertor.toSummaries(contacts);
    }

    public LdapService findSingle(String uuid, VersionOption version) {
        DoLdapService doLdapService = new LdapServiceRepository().findByUuidAndVersion(uuid, version);
        return LdapServiceConvertor.from(doLdapService);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoLdapService doLdapService = new LdapServiceRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoAccessControlLdap> ldapAccessControls = doLdapService.getLdapAccessControls();
        for (DoAccessControlLdap doAccessControlLdap : ldapAccessControls) {
            usages.add(new UsedBy(doAccessControlLdap.getUuid(), doAccessControlLdap.getVersion().getValue(), "LDAP Access Controls", doAccessControlLdap.getLabel().getValue()));
        }

        List<DoProfile> profiles = doLdapService.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));

        }
        return usages;
    }

    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoLdapService> objects = new LdapServiceRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoLdapService object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        LdapServiceRepository ldapServiceRepository = new LdapServiceRepository();
        DoLdapService doLdapService = ldapServiceRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doLdapService != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doLdapService.getGroup().getLabel().getValue(), doLdapService.getLabel().getValue())));
        }

        return result;
    }

    public LdapServiceSummaryItem save(LdapService ldapService) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), ldapService.getGroup().getValue().getKey());
        validateNotReleased(ldapService.getVersion());

        DoLdapService doLdapService;
        boolean labelChanged = false;

        if (ldapService.getUuid() != null) {
            doLdapService = new LdapServiceRepository().findByUuidAndVersion(ldapService.getUuid(), ldapService.getVersion().getValue());
            labelChanged = !doLdapService.getLabel().getValue().equals(ldapService.getLabel().getValue());

            LdapServiceConvertor.to(doLdapService, ldapService);
        } else {
            doLdapService = LdapServiceConvertor.to(null, ldapService);
        }

        new LdapServiceRepository().save(doLdapService);

        if (labelChanged) {
            new LdapServiceRepository().updateLabel(doLdapService.getId(), doLdapService.getUuid(), doLdapService.getLabel());
        }

        session.flush();
        session.evict(doLdapService);

        DoLdapService doLdapServiceReloaded = new LdapServiceRepository().findByUuidAndVersion(doLdapService.getUuid(), doLdapService.getVersion().getValue());

        return LdapServiceConvertor.toSummary(doLdapServiceReloaded);
    }

    public CheckResult check(String uuid, VersionOption version) {
        DoLdapService doLdapService = new LdapServiceRepository().findByUuidAndVersion(uuid, version);

        Properties configuration = new Properties();
        configuration.put(LdapClient.KEY_HOST, doLdapService.getDomain().getValue());
        configuration.put(LdapClient.KEY_PORT, doLdapService.getPort().getValueAsString());
        configuration.put(LdapClient.KEY_PRINCIPAL, doLdapService.getCredential().getUsername().getValue());
        configuration.put(LdapClient.KEY_CREDENTIALS, doLdapService.getCredential().getPassword().getValue());

        LdapClient ldapClient = new LdapClient(configuration);

        try {
            ldapClient.connect();
        } catch (LdapConnectException e) {
            return new CheckResult(false, e.getMessage());
        } finally {
            try {
                ldapClient.disconnect();
            } catch (LdapDisconnectException e) {
                // Do nothing...
            }
        }

        return new CheckResult(true, "Successfully connected with the LDAP Service.");
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoLdapService doLdapServiceToDuplicate = new LdapServiceRepository().findByUuidAndVersion(uuid, fromVersion);
        LdapService duplicateLdapService = LdapServiceConvertor.from(doLdapServiceToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateLdapService.setVersion(version);
        DoLdapService doDuplicateLdapService = LdapServiceConvertor.to(null, duplicateLdapService);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateLdapService.setUuid(uuid);

        new LdapServiceRepository().save(doDuplicateLdapService);

        session.flush();
        session.evict(doDuplicateLdapService);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoLdapService branchedVersion = new LdapServiceRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoLdapService doLdapServiceToBranch = new LdapServiceRepository().findByUuidAndVersion(uuid, fromVersion);
        LdapService branchLdapService = LdapServiceConvertor.from(doLdapServiceToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchLdapService.setVersion(version);
        DoLdapService doDuplicateLdapService = LdapServiceConvertor.to(null, branchLdapService);

        doDuplicateLdapService.setUuid(uuid);

        new LdapServiceRepository().save(doDuplicateLdapService);

        session.flush();
        session.evict(doDuplicateLdapService);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new LdapServiceRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public LdapServiceSummaryItem duplicate(String uuid, VersionOption version) {
        DoLdapService doLdapServiceToDuplicate = new LdapServiceRepository().findByUuidAndVersion(uuid, version);
        LdapService duplicateLdapService = LdapServiceConvertor.from(doLdapServiceToDuplicate);

        duplicateLdapService.setLabel(new StringType(duplicateLdapService.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateLdapService.setVersion(snapshot);

        DoLdapService doDuplicateLdapService = LdapServiceConvertor.to(null, duplicateLdapService);

        new LdapServiceRepository().save(doDuplicateLdapService);

        session.flush();
        session.evict(doDuplicateLdapService);

        DoLdapService doDuplicateLdapServiceReloaded = new LdapServiceRepository().findByUuidAndVersion(doDuplicateLdapService.getUuid(), snapshot.getValue());

        return LdapServiceConvertor.toSummary(doDuplicateLdapServiceReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoLdapService doLdapService = new LdapServiceRepository().findByUuidAndVersion(uuid, version);
        if (!doLdapService.getLdapAccessControls().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since it is in use by other components, use the Used By button to see which where it is in use."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new LdapServiceRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new LdapServiceRepository().remove(uuid, version);
    }
}
