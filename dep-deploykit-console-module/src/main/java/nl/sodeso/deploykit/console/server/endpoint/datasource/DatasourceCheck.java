package nl.sodeso.deploykit.console.server.endpoint.datasource;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceRepository;
import nl.sodeso.deploykit.console.domain.util.classloader.JdbcDriverClassloader;
import nl.sodeso.persistence.jdbc.SqlScriptExecutor;

import java.io.IOException;
import java.io.StringReader;
import java.sql.*;

/**
 * @author Ronald Mathies
 */
public class DatasourceCheck {

    public static CheckResult check(String uuid, VersionOption version) {
        DoDatasource doDatasource = new DatasourceRepository().findByUuidAndVersion(uuid, version);

        try {
            StringBuilder message = new StringBuilder("Successfully performed the following checks:<br/><ul>");

            JdbcDriverClassloader jdbcDriverClassloader = new JdbcDriverClassloader(doDatasource.getJdbcDriver().getJdbcDriverJars(), doDatasource.getClass().getClassLoader());
            jdbcDriverClassloader.loadClass(doDatasource.getJdbcDriver().getJdbcDriverClass().getClassname());

            DriverManager.setLoginTimeout(5);

            for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
                Connection connection = DriverManager.getConnection(doDatasource.getUrl().getValue(), doDatasource.getCredential().getUsername().getValue(),
                        doDatasource.getCredential().getPassword().getValue());

                message.append("<li>Successfully created a connection for connection pool '").append(doConnectionPool.getLabel().getValue()).append("'.</li>");

                if (doConnectionPool instanceof DoConnectionPoolJetty) {
                    DoConnectionPoolJetty doConnectionPoolJetty = (DoConnectionPoolJetty)doConnectionPool;
                    if (doConnectionPoolJetty.getDefaultCatalog() != null &&
                            doConnectionPoolJetty.getDefaultCatalog().getValue() != null &&
                            !doConnectionPoolJetty.getDefaultCatalog().getValue().isEmpty()) {

                        boolean found = false;
                        ResultSet resultSet = connection.getMetaData().getCatalogs();
                        while (resultSet.next()) {
                            String foundCatalog = resultSet.getString(1);
                            if (doConnectionPoolJetty.getDefaultCatalog().getValue().equals(foundCatalog)) {
                                message.append("<li>Found catalog '").append(doConnectionPoolJetty.getDefaultCatalog().getValue()).append("'.</li>");
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            message.append("<li>Catalog '").append(doConnectionPoolJetty.getDefaultCatalog().getValue()).append("' not found.</li>");
                        }
                    }

                }

                if (doConnectionPool.getConnectionInitSqls() != null &&
                        doConnectionPool.getConnectionInitSqls().getValue() != null &&
                        !doConnectionPool.getConnectionInitSqls().getValue().isEmpty()) {


                    SqlScriptExecutor.execute(connection, new StringReader(doConnectionPool.getConnectionInitSqls().getValue()));
                    message.append("<li>Connection Init Sqls for connection pool '").append(doConnectionPool.getLabel().getValue()).append("' successful.</li>");
                }

                if (doConnectionPool.getValidationQuery() != null &&
                        doConnectionPool.getValidationQuery().getValue() != null &&
                        !doConnectionPool.getValidationQuery().getValue().isEmpty()) {
                    Statement statement = connection.createStatement();
                    boolean execute = statement.execute(doConnectionPool.getValidationQuery().getValue());
                    if (execute) {
                        message.append("<li>Validation Query for connection pool '").append(doConnectionPool.getLabel().getValue()).append("' successful.</li>");
                    }
                }

                try {
                    connection.close();
                } catch (SQLException e) {
                    // ignore
                }

            }

            jdbcDriverClassloader.close();

            return new CheckResult(true, message.toString());
        } catch (IOException | ClassNotFoundException | SQLException e) {
            return new CheckResult(false, e.getMessage());
        }
    }

}
