package nl.sodeso.deploykit.console.server.endpoint.profile;

import nl.sodeso.deploykit.console.client.application.profile.ProfileSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.profile.ProfileConvertor;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.CheckResult;
import nl.sodeso.deploykit.console.client.application.profile.Profile;
import nl.sodeso.deploykit.console.client.application.profile.rpc.ProfileRpc;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.profile.ProfileRepository;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.deploykit.console.server.endpoint.accesscontrol.ldap.validation.ValidateAccessControlIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.credential.validation.ValidateCredentialIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.datasource.validation.ValidateDatasourceIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.datasource.validation.ValidateNoDuplicateDatabaseJndi;
import nl.sodeso.deploykit.console.server.endpoint.http.validation.ValidateHttpIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.https.validation.ValidateHttpsIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.jdbcdriver.validate.ValidateNoDuplicateJdbcDrivers;
import nl.sodeso.deploykit.console.server.endpoint.services.ldap.validate.ValidateLdapServiceIsReleased;
import nl.sodeso.deploykit.console.server.endpoint.services.webservice.validate.ValidateWebserviceIsReleased;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-profile")
public class ProfileEndpoint extends BaseEndpoint implements ProfileRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<ProfileSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoProfile> items = new ProfileRepository().find(new Permissions(username, Permissions.READ));
        return ProfileConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public Profile findSingle(String uuid, VersionOption version) {
        DoProfile item = new ProfileRepository().findByUuidAndVersion(uuid, version);
        return ProfileConvertor.from(item);
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        ProfileRepository profileRepository = new ProfileRepository();
        DoProfile doProfile = profileRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doProfile != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for  %s, please change the label.", doProfile.getLabel().getValue())));
        }

        return result;
    }

    public ProfileSummaryItem save(Profile profile) throws RemoteException {
        validateNotReleased(profile.getVersion());

        ProfileRepository factory = new ProfileRepository();

        DoProfile doProfile;
        boolean labelChanged = false;

        if (profile.getUuid() != null) {
            doProfile = factory.findByUuidAndVersion(profile.getUuid(), profile.getVersion().getValue());

            labelChanged = !doProfile.getLabel().getValue().equals(profile.getLabel().getValue());
            ProfileConvertor.to(doProfile, profile);
        } else {
            doProfile = ProfileConvertor.to(null, profile);
        }

        factory.save(doProfile);

        if (labelChanged) {
            factory.updateLabel(doProfile.getId(), doProfile.getUuid(), profile.getLabel());
        }

        session.flush();
        session.evict(doProfile);

        DoProfile doProfileReloaded = factory.findByUuidAndVersion(doProfile.getUuid(), profile.getVersion().getValue());

        return ProfileConvertor.toSummary(doProfileReloaded);
    }

    public CheckResult check(String uuid, VersionOption version) {
        return new CheckResult(true, "Fiets");
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        DoProfile doProfileToCommit = new ProfileRepository().findByUuidAndVersion(uuid, fromVersion);

        new ValidateDatasourceIsReleased().validate(doProfileToCommit.getDatasources(), result);
        new ValidateNoDuplicateDatabaseJndi().validate(doProfileToCommit.getDatasources(), result);
        new ValidateNoDuplicateJdbcDrivers().validate(doProfileToCommit.getDatasources(), result);

        new ValidateWebserviceIsReleased().validate(doProfileToCommit.getWebservices(), result);
        new ValidateHttpIsReleased().validate(doProfileToCommit.getHttp(), result);
        new ValidateHttpsIsReleased().validate(doProfileToCommit.getHttps(), result);
        new ValidateAccessControlIsReleased().validate(doProfileToCommit.getAccessControl(), result);
        new ValidateCredentialIsReleased().validate(doProfileToCommit.getCredentials(), result);
        new ValidateLdapServiceIsReleased().validate(doProfileToCommit.getLdapServices(), result);


        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoProfile doProfileToCommit = new ProfileRepository().findByUuidAndVersion(uuid, fromVersion);
        Profile profileToCommit = ProfileConvertor.from(doProfileToCommit);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        profileToCommit.setVersion(version);
        DoProfile doDuplicateProfile = ProfileConvertor.to(null, profileToCommit);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateProfile.setUuid(uuid);

        new ProfileRepository().save(doDuplicateProfile);

        session.flush();
        session.evict(doDuplicateProfile);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoProfile branchedVersion = new ProfileRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoProfile doProfileToDuplicate = new ProfileRepository().findByUuidAndVersion(uuid, fromVersion);
        Profile duplicateProfile = ProfileConvertor.from(doProfileToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        duplicateProfile.setVersion(version);
        DoProfile doDuplicateProfile = ProfileConvertor.to(null, duplicateProfile);

        // Keep the UUID the same, we are creating a branch here for the same profile.
        doDuplicateProfile.setUuid(uuid);

        new ProfileRepository().save(doDuplicateProfile);

        session.flush();
        session.evict(doDuplicateProfile);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new ProfileRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public ProfileSummaryItem duplicate(String uuid, VersionOption version) {
        DoProfile doProfileToDuplicate = new ProfileRepository().findByUuidAndVersion(uuid, version);
        Profile duplicateProfile = ProfileConvertor.from(doProfileToDuplicate);

        duplicateProfile.setLabel(new StringType(duplicateProfile.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateProfile.setVersion(snapshot);

        duplicateProfile.setUuid(null);
        DoProfile doDuplicateProfile = ProfileConvertor.to(null, duplicateProfile);

        new ProfileRepository().save(doDuplicateProfile);

        session.flush();
        session.evict(doDuplicateProfile);

        DoProfile doDuplicateHttpReloaded = new ProfileRepository().findByUuidAndVersion(doDuplicateProfile.getUuid(), snapshot.getValue());

        return ProfileConvertor.toSummary(doDuplicateHttpReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new ProfileRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new ProfileRepository().remove(uuid, version);
        session.flush();
    }
}
