package nl.sodeso.deploykit.console.server.servlet;

import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.JdbcDriverJarConvertor;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverJar;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.JdbcDriverJarRepository;
import nl.sodeso.gwt.ui.client.util.ContentType;
import nl.sodeso.gwt.ui.server.servlet.AbstractFileUploadServlet;
import nl.sodeso.gwt.ui.server.servlet.UploadFile;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet(
        name = "JDBC Driver File Upload Servlet",
        urlPatterns = {"/deploykitconsole/jdbcdriver/upload"}
)
@MultipartConfig(
        fileSizeThreshold=1024*1024*10,
        maxFileSize=1024*1024*50,
        maxRequestSize=1024*1024*100)
public class JdbcDriverFileUploadServlet extends AbstractFileUploadServlet {

    @Override
    public void upload(List<UploadFile> uploadedFiles, HttpServletResponse response) throws ServletException {
        try {
            UploadFile uploadFile = null;
            if (!uploadedFiles.isEmpty()) {
                uploadFile = uploadedFiles.get(0);
            }

            DoJdbcDriverJar doJdbcDriverJar = JdbcDriverJarConvertor.to(null, uploadFile);
            new JdbcDriverJarRepository().save(doJdbcDriverJar);

            response.setContentType(ContentType.TEXT_HTML.getContentType());
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print("{ \"uuid\": \"" + doJdbcDriverJar.getUuid() + "\" }");
        } catch (IOException e) {
            throw new ServletException("Failed to store keystore.", e);
        }
    }
}
