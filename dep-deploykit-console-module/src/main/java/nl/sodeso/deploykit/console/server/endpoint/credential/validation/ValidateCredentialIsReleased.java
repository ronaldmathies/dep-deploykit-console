package nl.sodeso.deploykit.console.server.endpoint.credential.validation;

import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.server.endpoint.validation.ReleasedValidationCheck;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidateCredentialIsReleased extends ReleasedValidationCheck<DoCredential> {

    public void validate(List<DoCredential> credentials, ValidationResult result) {
        for (DoCredential doCredential : credentials) {
            validate(doCredential, result);
        }
    }

    public void validate(DoCredential credential, ValidationResult result) {
        if (credential != null && !isReleased(credential.getVersion())) {
            result.add(new ValidationMessage("credential-version", ValidationMessage.Level.ERROR,
                    String.format("The selected credential configuration '%s' does not contain a released version.", credential.getLabel().getValue())));
        }
    }
}
