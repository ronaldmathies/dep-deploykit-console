package nl.sodeso.deploykit.console.server.endpoint.services.webservice;

import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceSummaryItem;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.services.webservice.WebserviceConvertor;
import nl.sodeso.deploykit.console.client.application.services.webservice.Webservice;
import nl.sodeso.deploykit.console.client.application.services.webservice.rpc.WebserviceRpc;
import nl.sodeso.deploykit.console.client.application.tooling.usage.UsedBy;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.webservice.WebserviceRepository;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.server.endpoint.BaseEndpoint;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.validation.ValidationMessage;
import nl.sodeso.gwt.ui.client.form.validation.ValidationResult;
import nl.sodeso.gwt.ui.client.rpc.RemoteException;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Session;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@WebServlet("*.deploykitconsole-webservice")
public class WebserviceEndpoint extends BaseEndpoint implements WebserviceRpc {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    /**
     * Find all the entities and transforms them to summary objects. The
     * entities are distinctly chosen so only one version per UUID is
     * selected.
     *
     * @return list of summaries.
     */
    public ArrayList<WebserviceSummaryItem> findSummaries() {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoWebservice> items = new WebserviceRepository().find(new Permissions(username, Permissions.READ));
        return WebserviceConvertor.toSummaries(items);
    }

    /**
     * Finds a single entity with the specified uuid and version.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return Returns a single entity.
     */
    public Webservice findSingle(String uuid, VersionOption version) {
        DoWebservice doWebservice = new WebserviceRepository().findByUuidAndVersion(uuid, version);
        return WebserviceConvertor.from(doWebservice);
    }

    /**
     * Returns a list of <code>UsedBy</code> objects which specify where the entity
     * with the <code>uuid</code> and <code>version</code>.
     *
     * @param uuid the uuid.
     * @param version the version.
     *
     * @return a list of <code>UsedBy</code> objects.
     */
    public ArrayList<UsedBy> findUsages(String uuid, VersionOption version) {
        DoWebservice doWebservice = new WebserviceRepository().findByUuidAndVersion(uuid, version);

        ArrayList<UsedBy> usages = new ArrayList<>();
        List<DoProfile> profiles = doWebservice.getProfiles();
        for (DoProfile doProfile : profiles) {
            usages.add(new UsedBy(doProfile.getUuid(), doProfile.getVersion().getValue(), "Profiles", doProfile.getLabel().getValue()));
        }
        return usages;
    }

    /**
     * Returns a list of options
     * @param group
     * @return
     */
    public ArrayList<DefaultOption> asOptions(DefaultOption group) {
        String username = getThreadLocalRequest().getUserPrincipal().getName();
        List<DoWebservice> objects = new WebserviceRepository().find(new Permissions(username, Permissions.USE), group.getKey());

        ArrayList<DefaultOption> options = new ArrayList<>(objects.size());
        for (DoWebservice object : objects) {
            options.add(object.getOption().getValue());
        }

        return options;
    }

    public ValidationResult isLabelUnique(String uuid, String label) {
        ValidationResult result = new ValidationResult();

        WebserviceRepository webserviceRepository = new WebserviceRepository();
        DoWebservice doWebservice = webserviceRepository.checkIfLabelAlreadyExists(uuid, label);
        if (doWebservice != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("The label already exists for %s: %s, please change the label.", doWebservice.getGroup().getLabel().getValue(), doWebservice.getLabel().getValue())));
        }

        return result;
    }

    public WebserviceSummaryItem save(Webservice webservice) throws RemoteException {
        validatePermissions(new Permissions(getPrincipalName(), Permissions.EDIT), webservice.getGroup().getValue().getKey());
        validateNotReleased(webservice.getVersion());

        WebserviceRepository webserviceRepository = new WebserviceRepository();

        DoWebservice doWebservice;
        boolean labelChanged = false;

        if (webservice.getUuid() != null) {
            doWebservice = webserviceRepository.findByUuidAndVersion(webservice.getUuid(), webservice.getVersion().getValue());
            labelChanged = !doWebservice.getLabel().getValue().equals(webservice.getLabel().getValue());
            WebserviceConvertor.to(doWebservice, webservice);
        } else {
            doWebservice = WebserviceConvertor.to(null, webservice);
        }

        webserviceRepository.save(doWebservice);

        if (labelChanged) {
            webserviceRepository.updateLabel(doWebservice.getId(), doWebservice.getUuid(), webservice.getLabel());
        }

        session.flush();
        session.evict(doWebservice);

        DoWebservice doWebserviceReloaded = webserviceRepository.findByUuidAndVersion(doWebservice.getUuid(), doWebservice.getVersion().getValue());

        return WebserviceConvertor.toSummary(doWebserviceReloaded);
    }

    public String check(String uuid, VersionOption version) {
        //DoWebservice doWebservice = new WebserviceRepository().findByUuidAndVersion(uuid, version);
        return "Fiets!";//doWebservice.check();
    }

    public ValidationResult isCommitAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public VersionOption commit(String uuid, VersionOption fromVersion, StringType toVersion) {
        DoWebservice doWebserviceToDuplicate = new WebserviceRepository().findByUuidAndVersion(uuid, fromVersion);
        Webservice duplicateWebservice = WebserviceConvertor.from(doWebserviceToDuplicate);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(toVersion.getValue(), VersionType.RELEASE));
        duplicateWebservice.setVersion(version);
        DoWebservice doDuplicateWebservice = WebserviceConvertor.to(null, duplicateWebservice);

        // Keep the UUID the same, we are creating a version here.
        doDuplicateWebservice.setUuid(uuid);

        new WebserviceRepository().save(doDuplicateWebservice);

        session.flush();
        session.evict(doDuplicateWebservice);

        return version.getValue();
    }

    public ValidationResult isBranchAllowed(String uuid, VersionOption fromVersion) {
        ValidationResult result = new ValidationResult();

        VersionOptionType toVersion = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        DoWebservice branchedVersion = new WebserviceRepository().findByUuidAndVersion(uuid, toVersion.getValue());

        if (branchedVersion != null) {
            result.add(new ValidationMessage("exists", ValidationMessage.Level.ERROR,
                    String.format("Unable to create a branch, branch already exists.")));
        }

        return result;
    }

    public VersionOption branch(String uuid, VersionOption fromVersion) {
        DoWebservice doWebserviceToBranch = new WebserviceRepository().findByUuidAndVersion(uuid, fromVersion);
        Webservice branchWebservice = WebserviceConvertor.from(doWebserviceToBranch);

        // Set the version.
        VersionOptionType version = new VersionOptionType(new VersionOption(fromVersion.getKey(), VersionType.BRANCH));
        branchWebservice.setVersion(version);
        DoWebservice doDuplicateWebservice = WebserviceConvertor.to(null, branchWebservice);

        // Keep the UUID the same, we are creating a branch here for the same webservice.
        doDuplicateWebservice.setUuid(uuid);

        new WebserviceRepository().save(doDuplicateWebservice);

        session.flush();
        session.evict(doDuplicateWebservice);

        return version.getValue();
    }

    public List<VersionOption> versions(String uuid) {
        List<VersionOptionType> versions = new WebserviceRepository().versions(uuid);

        ArrayList<VersionOption> options = new ArrayList<>();
        for (VersionOptionType version : versions) {
            options.add(version.getValue());
        }

        return options;
    }

    public WebserviceSummaryItem duplicate(String uuid, VersionOption version) {
        DoWebservice doWebserviceToDuplicate = new WebserviceRepository().findByUuidAndVersion(uuid, version);
        Webservice duplicateWebservice = WebserviceConvertor.from(doWebserviceToDuplicate);

        duplicateWebservice.setLabel(new StringType(doWebserviceToDuplicate.getLabel().getValue() + " Copy"));

        VersionOptionType snapshot = new VersionOptionType(new VersionOption("SNAPSHOT", VersionType.SNAPSHOT));
        duplicateWebservice.setVersion(snapshot);

        DoWebservice doDuplicateWebservice = WebserviceConvertor.to(null, duplicateWebservice);

        new WebserviceRepository().save(doDuplicateWebservice);

        session.flush();
        session.evict(doDuplicateWebservice);

        DoWebservice doDuplicateWebserviceReloaded = new WebserviceRepository().findByUuidAndVersion(doDuplicateWebservice.getUuid(), snapshot.getValue());

        return WebserviceConvertor.toSummary(doDuplicateWebserviceReloaded);
    }

    public ValidationResult canBeDeletedSafely(String uuid, VersionOption version) {
        ValidationResult result = new ValidationResult();

        DoWebservice doWebservice = new WebserviceRepository().findByUuidAndVersion(uuid, version);
        if (!doWebservice.getProfiles().isEmpty()) {
            result.add(new ValidationMessage("in-use", ValidationMessage.Level.ERROR, "It is not possible to delete this webservice configuration since it is in use by other components, use the Used By button to see which components use this webservice."));
        }

        if (version.getType().isSnapshot()) {
            List<VersionOptionType> versions = new WebserviceRepository().versions(uuid);
            for (VersionOptionType _version : versions) {
                if (!_version.getValue().getType().isSnapshot()) {
                    result.add(new ValidationMessage("other-versions", ValidationMessage.Level.ERROR, "It is not allowed to delete this version since there are non-snapshot versions, delete the non-snapshot versions first before trying to delete this version."));
                    break;
                }
            }
        }

        if (version.getType().isRelease()) {
            result.add(new ValidationMessage("released-version", ValidationMessage.Level.ERROR, "It is not allowed to delete a released version."));
        }

        return result;
    }

    public void delete(String uuid, VersionOption version) {
        new WebserviceRepository().remove(uuid, version);
        session.flush();
    }
}
