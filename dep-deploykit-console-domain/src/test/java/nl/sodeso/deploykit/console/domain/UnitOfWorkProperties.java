package nl.sodeso.deploykit.console.domain;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = Constants.PU
)
@FileResources(
        files = {
                @FileResource(file = "/dep-deploykit-console-persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}
