package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialBuilder;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class HttpsBuilder {


    private static HttpsRepository factory = new HttpsRepository();

    private DoHttps doHttps = new DoHttps();

    private DoGroup doGroup = null;
    private GroupBuilder groupBuilder = null;
    private CredentialBuilder keyManagerPasswordBuilder = null;
    private CredentialBuilder keyStorePasswordBuilder = null;
    private CredentialBuilder trustStorePasswordBuilder = null;

    public HttpsBuilder(String label, VersionOption version) {
        doHttps.setUuid(UuidUtils.generate());
        doHttps.setLabel(new StringType(label));
        doHttps.setVersion(new VersionOptionType(version));
        doHttps.setKeystore(new KeystoreBuilder().build());
    }

    public HttpsBuilder addDefaultValues() {
        doHttps.setHost(new StringType("127.0.0.1"));
        doHttps.setPort(new IntType(8080));
        doHttps.setTimeout(new IntType(30000));

        keyManagerPasswordBuilder = new CredentialBuilder("keymanager").addDefaultValues();
        keyStorePasswordBuilder = new CredentialBuilder("keystore").addDefaultValues();
        trustStorePasswordBuilder = new CredentialBuilder("truststore").addDefaultValues();

        return this;
    }

    public HttpsBuilder setGroup(DoGroup group) {
        this.doGroup = group;
        return this;
    }

    public HttpsBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }

    public HttpsBuilder setKeyManagerPassword(CredentialBuilder builder) {
        keyManagerPasswordBuilder = builder;
        return this;
    }

    public HttpsBuilder setKeyStorePassword(CredentialBuilder builder) {
        keyStorePasswordBuilder = builder;
        return this;
    }

    public HttpsBuilder setTrustStorePassword(CredentialBuilder builder) {
        trustStorePasswordBuilder = builder;
        return this;
    }

    public HttpsBuilder setUuid(String uuid) {
        doHttps.setUuid(uuid);
        return this;
    }

    public DoHttps build() {
        if (doGroup != null) {
            doHttps.setGroup(doGroup);
        } else {
            doHttps.setGroup(groupBuilder.build());
        }

        doHttps.setKeyManagerPassword(keyManagerPasswordBuilder.build());
        doHttps.setKeyStorePassword(keyStorePasswordBuilder.build());
        doHttps.setTrustStorePassword(trustStorePasswordBuilder.build());

        factory.save(doHttps);
        return doHttps;
    }

}
