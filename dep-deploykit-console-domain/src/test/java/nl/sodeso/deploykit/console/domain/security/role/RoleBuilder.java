package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.UserBuilder;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class RoleBuilder {

    private RoleRepository factory = new RoleRepository();

    private DoRole doRole = new DoRole();

    private List<UserBuilder> users = new ArrayList<>();
    private List<GroupPermissionBuilder> groupPermissionBuilders = new ArrayList<>();

    public RoleBuilder(String label) {
        doRole.setUuid(UuidUtils.generate());
        doRole.setLabel(new StringType(label));
    }

    public RoleBuilder addUser(UserBuilder builder) {
        users.add(builder);
        return this;
    }

    public RoleBuilder addGroupPermission(GroupPermissionBuilder builder) {
        groupPermissionBuilders.add(builder);
        return this;
    }


    public DoRole build() {
        for (UserBuilder builder : users) {
            this.doRole.add(builder.build());
        }

        for (GroupPermissionBuilder groupPermissionBuilder : groupPermissionBuilders) {
            DoGroupPermission doGroupPermission = groupPermissionBuilder.build();

            doRole.addGroupPermission(doGroupPermission);
        }

        factory.save(doRole);
        return this.doRole;
    }

}
