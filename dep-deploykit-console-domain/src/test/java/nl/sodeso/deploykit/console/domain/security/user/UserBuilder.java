package nl.sodeso.deploykit.console.domain.security.user;

import nl.sodeso.deploykit.console.domain.security.role.RoleBuilder;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class UserBuilder {

    private static UserRepository factory = new UserRepository();

    private DoUser doUser = new DoUser();

    private List<DoRole> roles = new ArrayList<>();
    private List<RoleBuilder> roleBuilders = new ArrayList<>();

    public UserBuilder(String label, String username, String password) {
        doUser.setUuid(UuidUtils.generate());
        doUser.setLabel(new StringType(label));
        doUser.setUsername(new StringType(username));
        doUser.setPassword(new StringType(password));
    }

    public UserBuilder addRole(DoRole role) {
        this.roles.add(role);
        return this;
    }

    public UserBuilder addRole(RoleBuilder builder) {
        roleBuilders.add(builder);
        return this;
    }

    public DoUser build() {
        for (DoRole doRole : roles) {
            this.doUser.add(doRole);
        }
        for (RoleBuilder builder : roleBuilders) {
            this.doUser.add(builder.build());
        }

        factory.save(doUser);
        return doUser;
    }
}
