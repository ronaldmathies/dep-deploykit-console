package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.RepositoryTest;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.UserBuilder;
import nl.sodeso.persistence.hibernate.HibernateTestUtils;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class RoleRepositoryTest extends RepositoryTest {

    @Before
    public void initDatabase() {
        super.initDatabase();
    }

    @Test
    public void testFind() throws Exception {
        List<DoRole> roles = roleRepository.find();
        assertNotNull(roles);
        assertEquals(6, roles.size());
    }

    @Test
    public void testFindByUsername() throws Exception {
        List<DoRole> roles = roleRepository.findByUsername(USERNAME_ADMINISTRATOR);
        assertNotNull(roles);
        assertEquals(1, roles.size());
    }

    @Test
    public void testFindByUuid() throws Exception {
        DoRole found = roleRepository.findByUuid(getAdministratorRole().getUuid());
        assertNotNull(found);
    }

    @Test
    public void testFindByLabel() throws Exception {
        DoRole found = roleRepository.findByLabel(getAdministratorRole().getLabel().getValue());
        assertNotNull(found);
    }


    @Test
    public void testRemove() throws Exception {
        DoRole found = roleRepository.findByLabel(getAdministratorRole().getLabel().getValue());
        assertNotNull(found);

        roleRepository.remove(found.getUuid());

        unitOfWork.commit();
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);

        DoRole notFound = roleRepository.findByUuid(getAdministratorRole().getUuid());
        assertNull(notFound);
    }
}