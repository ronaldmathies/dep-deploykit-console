package nl.sodeso.deploykit.console.domain.connectors.http;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.RepositoryTest;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.hibernate.util.UuidUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class HttpRepositoryTest extends RepositoryTest {

    private HttpRepository httpRepository = new HttpRepository();

    private DoHttp
            httpDevSnapshot,
            httpDevRelease1_0,
            httpDevBranch1_0,
            httpTstSnapshot,
            httpTstRelease1_0,
            httpTstBranch1_0;

    @Before
    public void initDatabase() {
        super.initDatabase();

        String httpDevUuid = UuidUtils.generate();
        String httpTstUuid = UuidUtils.generate();

        httpDevSnapshot = new HttpBuilder("Default", SNAPSHOT)
                .setUuid(httpDevUuid)
                .addDefaultValues()
                .setGroup(getGroupDev())
                .build();

        httpDevRelease1_0 = new HttpBuilder("Default", RELEASE_1_0)
                .setUuid(httpDevUuid)
                .addDefaultValues()
                .setGroup(getGroupDev())
                .build();

        httpDevBranch1_0 = new HttpBuilder("Default", BRANCH_1_0)
                .setUuid(httpDevUuid)
                .addDefaultValues()
                .setGroup(getGroupDev())
                .build();

        httpTstSnapshot = new HttpBuilder("Default", SNAPSHOT)
                .setUuid(httpTstUuid)
                .addDefaultValues()
                .setGroup(getGroupTst())
                .build();

        httpTstRelease1_0 = new HttpBuilder("Default", RELEASE_1_0)
                .setUuid(httpTstUuid)
                .addDefaultValues()
                .setGroup(getGroupTst())
                .build();

        httpTstBranch1_0 = new HttpBuilder("Default", BRANCH_1_0)
                .setUuid(httpTstUuid)
                .addDefaultValues()
                .setGroup(getGroupTst())
                .build();

        unitOfWork.commit();
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
    }

    @Test
    public void testFind() throws Exception {
        unitOfWork.commit();
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);

        List<DoHttp> result = httpRepository.find(new Permissions(USERNAME_ADMINISTRATOR, Permissions.READ));
        assertNotNull(result);
        assertEquals(2, result.size());

        result = httpRepository.find(new Permissions(USERNAME_DEVELOPER, Permissions.READ));
        assertNotNull(result);
        assertEquals(1, result.size());

        result = httpRepository.find(new Permissions(USERNAME_TESTER, Permissions.READ));
        assertNotNull(result);
        assertEquals(1, result.size());
    }

//    @Test
//    public void checkIfLabelAlreadyExists() throws Exception {
//        DoGroup groupDev = new GroupBuilder("DEV").build();
//        DoHttp stored = new HttpBuilder("Default", SNAPSHOT)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new UserBuilder("Ronald Mathies", "ronmat")
//            .addRole(new RoleBuilder("Administrator")
//                .addGroupPermission(
//                    new GroupPermissionBuilder(groupDev)
//                        .addDefaultValues()
//                        .setAllowedToRead(true)))
//            .build();
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        DoHttp notFound = httpRepository.checkIfLabelAlreadyExists(stored.getUuid(), "Default");
//        assertNull(notFound);
//
//        DoHttp found = httpRepository.checkIfLabelAlreadyExists("none", "Default");
//        assertNotNull(found);
//        assertEquals(found.getUuid(), stored.getUuid());
//
//        assertNotNull(found.getUuid());
//        assertNotNull(found.getLabel());
//        assertNotNull(found.getGroup());
//        assertNull(found.getVersion());
//        assertNull(found.getTimeout());
//        assertNull(found.getPort());
//        assertNull(found.getHost());
//    }
//
//    @Test
//    public void findByUuidAndVersion() throws Exception {
//        String uuid = UuidUtils.generate();
//
//        DoGroup groupDev = new GroupBuilder("DEV").build();
//
//        new HttpBuilder("Default", SNAPSHOT)
//            .setUuid(uuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new HttpBuilder("Default", RELEASE_1_0)
//            .setUuid(uuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new UserBuilder("Ronald Mathies", "ronmat")
//            .addRole(new RoleBuilder("Administrator")
//                .addGroupPermission(new GroupPermissionBuilder(groupDev)
//                    .addDefaultValues()
//                    .setAllowedToRead(true)))
//            .build();
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        DoHttp foundReleaseHttp = httpRepository.findByUuidAndVersion(uuid, RELEASE_1_0);
//        assertNotNull(foundReleaseHttp);
//        assertEquals(RELEASE_1_0, foundReleaseHttp.getVersion().getValue());
//        assertEquals(uuid, foundReleaseHttp.getUuid());
//
//        DoHttp foundSnapshot = httpRepository.findByUuidAndVersion(uuid, SNAPSHOT);
//        assertNotNull(foundSnapshot);
//        assertEquals(SNAPSHOT, foundSnapshot.getVersion().getValue());
//        assertEquals(uuid, foundSnapshot.getUuid());
//    }
//
//    @Test
//    public void versions() throws Exception {
//        String defaultUuid = UuidUtils.generate();
//
//        DoGroup groupDev = new GroupBuilder("DEV").build();
//
//        new HttpBuilder("Default", SNAPSHOT)
//            .setUuid(defaultUuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new HttpBuilder("Default", RELEASE_1_0)
//            .setUuid(defaultUuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new HttpBuilder("Other Default", SNAPSHOT)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new UserBuilder("Ronald Mathies", "ronmat")
//            .addRole(new RoleBuilder("Administrator")
//                .addGroupPermission(new GroupPermissionBuilder(groupDev)
//                    .addDefaultValues()
//                    .setAllowedToRead(true)))
//            .build();
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        List<VersionOptionType> foundVersions = httpRepository.versions(defaultUuid);
//        assertNotNull(foundVersions);
//        assertEquals(2, foundVersions.size());
//    }
//
//    @Test
//    public void updateLabel() throws Exception {
//        String defaultUuid = UuidUtils.generate();
//
//        DoGroup groupDev = new GroupBuilder("DEV").build();
//
//        DoHttp storedSnapshot = new HttpBuilder("Default", SNAPSHOT)
//            .setUuid(defaultUuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new HttpBuilder("Default", RELEASE_1_0)
//            .setUuid(defaultUuid)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new HttpBuilder("Other Default", SNAPSHOT)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new UserBuilder("Ronald Mathies", "ronmat")
//            .addRole(new RoleBuilder("Administrator")
//                .addGroupPermission(new GroupPermissionBuilder(groupDev)
//                    .addDefaultValues()
//                    .setAllowedToRead(true)))
//            .build();
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        httpRepository.updateLabel(storedSnapshot.getId(), storedSnapshot.getUuid(), new StringType("New Default"));
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        DoHttp foundRelease = httpRepository.findByUuidAndVersion(storedSnapshot.getUuid(), RELEASE_1_0);
//        assertNotNull(foundRelease);
//        assertEquals("New Default", foundRelease.getLabel().getValue());
//    }
//
//    @Test
//    public void remove() throws Exception {
//        DoGroup groupDev = new GroupBuilder("DEV").build();
//
//        DoHttp storedSnapshot = new HttpBuilder("Default", SNAPSHOT)
//            .addDefaultValues()
//            .setGroup(groupDev)
//            .build();
//
//        new UserBuilder("Ronald Mathies", "ronmat")
//            .addRole(new RoleBuilder("Administrator")
//                .addGroupPermission(new GroupPermissionBuilder(groupDev)
//                    .addDefaultValues()
//                    .setAllowedToRead(true)))
//            .build();
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        DoHttp found = httpRepository.findByUuidAndVersion(storedSnapshot.getUuid(), SNAPSHOT);
//        assertNotNull(found);
//
//        httpRepository.remove(storedSnapshot.getUuid(), SNAPSHOT);
//
//        unitOfWork.commit();
//        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
//
//        DoHttp notFound = httpRepository.findByUuidAndVersion(storedSnapshot.getUuid(), SNAPSHOT);
//        assertNull(notFound);
//    }
}