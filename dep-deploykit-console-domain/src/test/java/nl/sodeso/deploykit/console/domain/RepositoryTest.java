package nl.sodeso.deploykit.console.domain;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.security.role.GroupPermissionBuilder;
import nl.sodeso.deploykit.console.domain.security.role.RoleBuilder;
import nl.sodeso.deploykit.console.domain.security.role.RoleRepository;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.UserBuilder;
import nl.sodeso.deploykit.console.domain.security.user.UserRepository;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.persistence.hibernate.HibernateTestUtils;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;

/**
 * @author Ronald Mathies
 */
public class RepositoryTest {

    protected RoleRepository roleRepository = new RoleRepository();
    protected UserRepository userRepository = new UserRepository();
    protected GroupRepository groupRepository = new GroupRepository();

    public static VersionOption SNAPSHOT = new VersionOption(VersionType.SNAPSHOT);
    public static VersionOption RELEASE_1_0 = new VersionOption("1.0", VersionType.RELEASE);
    public static VersionOption RELEASE_2_0 = new VersionOption("1.0", VersionType.RELEASE);
    public static VersionOption BRANCH_1_0 = new VersionOption("1.0", VersionType.BRANCH);
    public static VersionOption BRANCH_2_0 = new VersionOption("2.0", VersionType.BRANCH);

    public static final String USERNAME_ADMINISTRATOR = "administrator";
    public static final String USERNAME_DEVELOPER = "developer";
    public static final String USERNAME_TESTER = "tester";
    public static final String USERNAME_DEPLOYER = "deployer";
    public static final String USERNAME_VIEWER = "viewer";
    public static final String USERNAME_CONFIGURER = "configurer";
    public static final String PASSWORD_ALL = "'NjIzMEMzNTNCMDAxRkQ2NTBDRjc0NTgzMEM3QTdGRkYxODU2MTA2Qzk5QUNDMzM5RDQ1OEY4RTJCMDI4NzA3Nw=='";

    protected static UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
    protected Session session = ThreadSafeSession.getSession(Constants.PU);

    private DoGroup groupDev, groupTst;
    private DoRole administratorRole, developerRole, testerRole, viewerRole, deployerRole, configurerRole;
    private DoUser administratorUser, developerUser, testerUser, viewerUser, deployerUser, configurerUser;

    @After
    public void cleanDatabase() {
        HibernateTestUtils.truncateAllTables(session);
    }

    public void initDatabase() {
        groupDev = new GroupBuilder("Group DEV")
                .build();

        groupTst = new GroupBuilder("Group TST")
                .build();

        administratorRole = new RoleBuilder("Administrator")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(true)
                                .setAllowedToUse(true))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(true)
                                .setAllowedToUse(true))
                .build();

        developerRole = new RoleBuilder("Developer")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(true)
                                .setAllowedToUse(true))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(false)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .build();

        testerRole = new RoleBuilder("Tester")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(false)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(true)
                                .setAllowedToUse(true))
                .build();

        viewerRole = new RoleBuilder("Viewer")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(false)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(true)
                                .setAllowedToDeploy(false)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .build();

        deployerRole = new RoleBuilder("Deployer")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(false))
                .build();

        configurerRole = new RoleBuilder("Configurer")
                .addGroupPermission(
                        new GroupPermissionBuilder(groupDev)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(false)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(true))
                .addGroupPermission(
                        new GroupPermissionBuilder(groupTst)
                                .setAllowedToRead(false)
                                .setAllowedToDeploy(true)
                                .setAllowedToEdit(false)
                                .setAllowedToUse(true))
                .build();


        administratorUser = new UserBuilder("Administrator", USERNAME_ADMINISTRATOR, PASSWORD_ALL)
                .addRole(administratorRole)
                .build();

        developerUser = new UserBuilder("Developer", USERNAME_DEVELOPER, PASSWORD_ALL)
                .addRole(developerRole)
                .build();

        testerUser = new UserBuilder("Tester", USERNAME_TESTER, PASSWORD_ALL)
                .addRole(testerRole)
                .build();

        viewerUser = new UserBuilder("Viewer", USERNAME_VIEWER, PASSWORD_ALL)
                .addRole(viewerRole)
                .build();

        deployerUser = new UserBuilder("Deployer", USERNAME_DEPLOYER, PASSWORD_ALL)
                .addRole(deployerRole)
                .build();

        configurerUser = new UserBuilder("Configurer", USERNAME_CONFIGURER, PASSWORD_ALL)
                .addRole(configurerRole)
                .build();

        unitOfWork.commit();
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);
    }

    public DoGroup getGroupDev() {
        return this.groupDev;
    }

    public DoGroup getGroupTst() {
        return this.groupTst;
    }

    public DoRole getAdministratorRole() {
        return administratorRole;
    }

    public DoRole getDeveloperRole() {
        return developerRole;
    }

    public DoRole getTesterRole() {
        return testerRole;
    }

    public DoRole getViewerRole() {
        return viewerRole;
    }

    public DoRole getDeployerRole() {
        return deployerRole;
    }

    public DoRole getConfigurerRole() {
        return configurerRole;
    }

    public DoUser getAdministratorUser() {
        return administratorUser;
    }

    public DoUser getDeveloperUser() {
        return developerUser;
    }

    public DoUser getTesterUser() {
        return testerUser;
    }

    public DoUser getViewerUser() {
        return viewerUser;
    }

    public DoUser getDeployerUser() {
        return deployerUser;
    }

    public DoUser getConfigurerUser() {
        return configurerUser;
    }
}
