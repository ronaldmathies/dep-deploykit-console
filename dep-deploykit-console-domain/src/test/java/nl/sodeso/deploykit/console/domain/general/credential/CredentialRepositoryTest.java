package nl.sodeso.deploykit.console.domain.general.credential;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.RepositoryTest;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.security.role.RoleBuilder;
import nl.sodeso.deploykit.console.domain.security.user.UserBuilder;
import nl.sodeso.persistence.hibernate.HibernateTestUtils;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.hibernate.util.UuidUtils;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class CredentialRepositoryTest extends RepositoryTest {

}