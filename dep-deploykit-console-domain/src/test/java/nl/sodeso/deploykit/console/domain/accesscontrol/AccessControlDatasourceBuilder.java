package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class AccessControlDatasourceBuilder {


    private static AccessControlRepository factory = new AccessControlRepository();

    private DoAccessControlDatasource doAccessControl = new DoAccessControlDatasource();

    private GroupBuilder groupBuilder = null;

    public AccessControlDatasourceBuilder(String label, VersionOption version) {
        doAccessControl.setUuid(UuidUtils.generate());
        doAccessControl.setLabel(new StringType(label));
        doAccessControl.setVersion(new VersionOptionType(version));
    }

    public AccessControlDatasourceBuilder addDefaultValues() {
        doAccessControl.setPlaceholderPrefix(new StringType("accesscontrol"));
        doAccessControl.setRealm(new StringType("realm"));

        doAccessControl.setRoleTable(new StringType("t_roles"));
        doAccessControl.setRoleUserColumn(new StringType("username"));
        doAccessControl.setRoleRoleColumn(new StringType("role"));

        doAccessControl.setUserTable(new StringType("t_users"));
        doAccessControl.setUserColumn(new StringType("username"));
        doAccessControl.setCredentialColumn(new StringType("password"));

        return this;
    }

    public AccessControlDatasourceBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }

    public AccessControlDatasourceBuilder setUuid(String uuid) {
        doAccessControl.setUuid(uuid);
        return this;
    }

    public DoAccessControl build() {
        this.doAccessControl.setGroup(groupBuilder.build());

        factory.save(doAccessControl);
        return doAccessControl;
    }

}
