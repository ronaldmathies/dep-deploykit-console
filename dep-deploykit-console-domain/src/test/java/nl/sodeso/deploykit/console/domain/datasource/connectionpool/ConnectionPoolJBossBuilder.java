package nl.sodeso.deploykit.console.domain.datasource.connectionpool;

import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolRepository;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolJBossBuilder {


    private static ConnectionPoolRepository factory = new ConnectionPoolRepository();

    private DoConnectionPoolJBoss doConnectionPool = new DoConnectionPoolJBoss();

    private DoGroup doGroup = null;
    private GroupBuilder groupBuilder = null;

    public ConnectionPoolJBossBuilder(String label) {
        doConnectionPool.setUuid(UuidUtils.generate());
        doConnectionPool.setLabel(new StringType(label));
    }

    public ConnectionPoolJBossBuilder addDefaultValues() {
        doConnectionPool.setAllocationRetry(new IntType(0));
        doConnectionPool.setAllocationRetryWaitMillis(new IntType(5000));
        doConnectionPool.setBlockingTimeoutMillis(new IntType(30000));
        doConnectionPool.setIdleTimeoutMinutes(new IntType(0));
        doConnectionPool.setInitialPoolSize(new IntType(0));
        doConnectionPool.setMinPoolSize(new IntType(0));
        doConnectionPool.setMaxPoolSize(new IntType(20));
        doConnectionPool.setPrefill(new BooleanType(false));
        doConnectionPool.setConnectionInitSqls(new StringType("connection_init_sql"));
        doConnectionPool.setValidationQuery(new StringType("validation_query"));
        doConnectionPool.setDefaultTransactionIsolation(new OptionType<>(new DefaultOption("default", "default", "DRIVER_DEFAULT")));
        return this;
    }

    public ConnectionPoolJBossBuilder setGroup(DoGroup group) {
        this.doGroup = group;
        return this;
    }

    public ConnectionPoolJBossBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }

    public ConnectionPoolJBossBuilder setUuid(String uuid) {
        doConnectionPool.setUuid(uuid);
        return this;
    }

    public DoConnectionPool build() {
        if (doGroup != null) {
            this.doConnectionPool.setGroup(doGroup);
        } else {
            this.doConnectionPool.setGroup(groupBuilder.build());
        }

        factory.save(doConnectionPool);
        return doConnectionPool;
    }

}
