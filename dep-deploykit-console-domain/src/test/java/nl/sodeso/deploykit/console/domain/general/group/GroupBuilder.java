package nl.sodeso.deploykit.console.domain.general.group;

import nl.sodeso.deploykit.console.domain.connectors.http.HttpBuilder;
import nl.sodeso.deploykit.console.domain.connectors.https.HttpsBuilder;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.security.role.RoleBuilder;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class GroupBuilder {

    private static GroupRepository factory = new GroupRepository();

    private DoGroup doGroup = new DoGroup();

    private List<CredentialBuilder> credentials = new ArrayList<>();
    private List<HttpBuilder> httpes = new ArrayList<>();
    private List<HttpsBuilder> httpses = new ArrayList<>();

    public GroupBuilder(String label) {
        doGroup.setUuid(UuidUtils.generate());
        doGroup.setLabel(new StringType(label));
    }

    public GroupBuilder setUuid(String uuid) {
        doGroup.setUuid(uuid);
        return this;
    }

    public GroupBuilder addCredential(CredentialBuilder builder) {
        credentials.add(builder);
        return this;
    }

    public GroupBuilder addHttp(HttpBuilder builder) {
        httpes.add(builder);
        return this;
    }

    public GroupBuilder addHttps(HttpsBuilder builder) {
        httpses.add(builder);
        return this;
    }

    public DoGroup build() {
        for (CredentialBuilder builder : credentials) {
            doGroup.addCredential(builder.build());
        }

        for (HttpBuilder builder : httpes) {
            doGroup.addHttp(builder.build());
        }

        for (HttpsBuilder builder : httpses) {
            doGroup.addHttps(builder.build());
        }

        factory.save(doGroup);

        return this.doGroup;
    }

}
