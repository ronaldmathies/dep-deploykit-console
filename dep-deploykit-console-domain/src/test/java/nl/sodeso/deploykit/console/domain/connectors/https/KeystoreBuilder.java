package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.deploykit.console.domain.security.role.RoleBuilder;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class KeystoreBuilder {

    private static final String KEYSTORE_FILENAME = "/keystore.keystore";

    private static KeystoreFactory factory = new KeystoreFactory();

    private DoKeystore doKeystore = new DoKeystore();

    private List<RoleBuilder> roles = new ArrayList<>();

    public KeystoreBuilder() {
        doKeystore.setUuid(UuidUtils.generate());
        doKeystore.setFilename(KEYSTORE_FILENAME);
        doKeystore.setKeystore(getClass().getResourceAsStream(KEYSTORE_FILENAME));
    }

    public DoKeystore build() {
        factory.save(doKeystore);
        return doKeystore;
    }
}
