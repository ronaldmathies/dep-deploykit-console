package nl.sodeso.deploykit.console.domain.connectors.http;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class HttpBuilder {

    private static HttpRepository factory = new HttpRepository();

    private DoHttp doHttp = new DoHttp();

    private DoGroup doGroup = null;
    private GroupBuilder groupBuilder = null;

    public HttpBuilder(String label, VersionOption version) {
        doHttp.setUuid(UuidUtils.generate());
        doHttp.setLabel(new StringType(label));
        doHttp.setVersion(new VersionOptionType(version));
    }

    public HttpBuilder addDefaultValues() {
        doHttp.setHost(new StringType("127.0.0.1"));
        doHttp.setPort(new IntType(8080));
        doHttp.setTimeout(new IntType(30000));
        return this;
    }

    public HttpBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }
    public HttpBuilder setGroup(DoGroup group) {
        this.doGroup = group;
        return this;
    }

    public HttpBuilder setUuid(String uuid) {
        doHttp.setUuid(uuid);
        return this;
    }

    public DoHttp build() {
        if (doGroup != null) {
            this.doHttp.setGroup(doGroup);
        } else {
            this.doHttp.setGroup(groupBuilder.build());
        }

        factory.save(doHttp);
        return doHttp;
    }

}
