package nl.sodeso.deploykit.console.domain.security.user;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.RepositoryTest;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.persistence.hibernate.HibernateTestUtils;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class UserRepositoryTest extends RepositoryTest {

    @Before
    public void initDatabase() {
        super.initDatabase();
    }

    @Test
    public void testFind() throws Exception {
        List<DoUser> users = userRepository.find();
        assertNotNull(users);
        assertEquals(6, users.size());
    }

    @Test
    public void testFindByUuid() throws Exception {
        DoUser found = userRepository.findByUuid(getAdministratorUser().getUuid());
        assertNotNull(found);
    }

    @Test
    public void testFindByLabel() throws Exception {
        DoUser found = userRepository.findByLabel(getAdministratorUser().getLabel().getValue());
        assertNotNull(found);
    }


    @Test
    public void testRemove() throws Exception {
        DoUser found = userRepository.findByLabel(getAdministratorUser().getLabel().getValue());
        assertNotNull(found);

        userRepository.remove(getAdministratorUser().getUuid());

        unitOfWork.commit();
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(Constants.PU);

        DoUser notFound = userRepository.findByUuid(getAdministratorUser().getUuid());
        assertNull(notFound);
    }
}