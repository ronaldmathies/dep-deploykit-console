package nl.sodeso.deploykit.console.domain.general.credential;

import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class CredentialBuilder {

    private CredentialRepository factory = new CredentialRepository();

    private DoCredential doCredential = new DoCredential();

    private GroupBuilder groupBuilder = null;

    public CredentialBuilder(String label) {
        doCredential.setUuid(UuidUtils.generate());
        doCredential.setLabel(new StringType(label));
    }

    public CredentialBuilder addDefaultValues() {
        doCredential.setUsername(new StringType("username"));
        doCredential.setPassword(new StringType("password"));
        doCredential.setPlaceholderPrefix(new StringType("credential"));
        return this;
    }

    public CredentialBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }

    public DoCredential build() {
        if (groupBuilder != null) {
            doCredential.setGroup(groupBuilder.build());
        }

        factory.save(doCredential);
        return this.doCredential;
    }

}
