package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class GroupPermissionBuilder {

    private GroupPermissionRepository groupPermissionRepository = new GroupPermissionRepository();

    private DoGroupPermission doGroupPermission = new DoGroupPermission();

    private DoGroup doGroup = null;
    private GroupBuilder groupBuilder = null;

    public GroupPermissionBuilder(GroupBuilder groupBuilder) {
        this.groupBuilder = groupBuilder;
        setUuid(UuidUtils.generate());
    }

    public GroupPermissionBuilder(DoGroup doGroup) {
        this.doGroup = doGroup;
        setUuid(UuidUtils.generate());
    }

    public GroupPermissionBuilder addDefaultValues() {
        setAllowedToEdit(false);
        setAllowedToRead(false);
        setAllowedToUse(false);
        setAllowedToDeploy(false);
        return this;
    }

    public GroupPermissionBuilder setUuid(String uuid) {
        doGroupPermission.setUuid(uuid);
        return this;
    }

    public GroupPermissionBuilder setAllowedToRead(Boolean allowedToRead) {
        doGroupPermission.setAllowedToRead(new BooleanType(allowedToRead));
        return this;
    }

    public GroupPermissionBuilder setAllowedToDeploy(Boolean allowedToDeploy) {
        doGroupPermission.setAllowedToDeploy(new BooleanType(allowedToDeploy));
        return this;
    }

    public GroupPermissionBuilder setAllowedToUse(Boolean allowedToUse) {
        doGroupPermission.setAllowedToUse(new BooleanType(allowedToUse));
        return this;
    }

    public GroupPermissionBuilder setAllowedToEdit(Boolean allowedToEdit) {
        doGroupPermission.setAllowedToEdit(new BooleanType(allowedToEdit));
        return this;
    }


    public DoGroupPermission build() {
        if (groupBuilder != null) {
            this.doGroupPermission.setGroup(groupBuilder.build());
        } else {
            this.doGroupPermission.setGroup(doGroup);
        }

        groupPermissionRepository.save(doGroupPermission);

        return this.doGroupPermission;
    }

}
