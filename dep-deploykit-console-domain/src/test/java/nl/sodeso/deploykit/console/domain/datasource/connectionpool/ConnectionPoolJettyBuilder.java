package nl.sodeso.deploykit.console.domain.datasource.connectionpool;

import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolRepository;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.general.group.GroupBuilder;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolJettyBuilder {


    private static ConnectionPoolRepository factory = new ConnectionPoolRepository();

    private DoConnectionPoolJetty doConnectionPool = new DoConnectionPoolJetty();

    private DoGroup doGroup = null;
    private GroupBuilder groupBuilder = null;

    public ConnectionPoolJettyBuilder(String label) {
        doConnectionPool.setUuid(UuidUtils.generate());
        doConnectionPool.setLabel(new StringType(label));
    }

    public ConnectionPoolJettyBuilder addDefaultValues() {
        doConnectionPool.setInitialSize(new IntType(8));
        doConnectionPool.setMaxTotal(new IntType(8));
        doConnectionPool.setMaxIdle(new IntType(8));
        doConnectionPool.setMinIdle(new IntType(0));
        doConnectionPool.setMaxWaitMillis(new IntType(-1));
        doConnectionPool.setValidationQuery(new StringType("validation_query"));
        doConnectionPool.setTestOnCreate(new BooleanType(false));
        doConnectionPool.setTestOnBorrow(new BooleanType(true));
        doConnectionPool.setTestOnReturn(new BooleanType(false));
        doConnectionPool.setTestWhileIdle(new BooleanType(false));
        doConnectionPool.setTimeBetweenEvictionRunsMillis(new IntType(-1));
        doConnectionPool.setNumTestsPerEvictionRun(new IntType(3));
        doConnectionPool.setMinEvictableIdleTimeMillis(new IntType(1000 * 60 * 30));
        doConnectionPool.setSoftMiniEvictableIdleTimeMillis(new IntType(-1));
        doConnectionPool.setMaxConnLifetimeMillis(new IntType(-1));
        doConnectionPool.setLogExpiredConnections(new BooleanType(true));
        doConnectionPool.setConnectionInitSqls(new StringType("connection_init_sql"));
        doConnectionPool.setLifo(new BooleanType(true));
        doConnectionPool.setDefaultAutoCommit(new IntType(2));
        doConnectionPool.setDefaultReadOnly(new IntType(2));
        doConnectionPool.setDefaultTransactionIsolation(new OptionType<>(new DefaultOption("default", "default", "DRIVER_DEFAULT")));
        doConnectionPool.setDefaultCatalog(new StringType("catalog"));
        doConnectionPool.setCacheState(new BooleanType(true));

        return this;
    }

    public ConnectionPoolJettyBuilder setGroup(DoGroup group) {
        this.doGroup = group;
        return this;
    }

    public ConnectionPoolJettyBuilder setGroup(GroupBuilder builder) {
        groupBuilder = builder;
        return this;
    }

    public ConnectionPoolJettyBuilder setUuid(String uuid) {
        doConnectionPool.setUuid(uuid);
        return this;
    }

    public DoConnectionPool build() {
        if (doGroup != null) {
            this.doConnectionPool.setGroup(doGroup);
        } else {
            this.doConnectionPool.setGroup(groupBuilder.build());
        }

        factory.save(doConnectionPool);
        return doConnectionPool;
    }

}
