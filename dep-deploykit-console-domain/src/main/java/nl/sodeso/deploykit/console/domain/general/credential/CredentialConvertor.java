package nl.sodeso.deploykit.console.domain.general.credential;

import nl.sodeso.deploykit.console.client.application.general.credential.Credential;
import nl.sodeso.deploykit.console.client.application.general.credential.CredentialSummaryItem;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CredentialConvertor {

    public static DoCredential to(DoCredential doCredential, Credential credential) {
        if (doCredential == null) {
            doCredential = new DoCredential();
            doCredential.setUuid(UuidUtils.generate());
        }

        doCredential.setGroup(new GroupRepository().findByUuid(credential.getGroup().getValue().getKey()));
        doCredential.setLabel(credential.getLabel());
        doCredential.setVersion(credential.getVersion());
        doCredential.setPlaceholderPrefix(credential.getPlaceholderPrefix());
        doCredential.setUsername(credential.getUsername());
        doCredential.setPassword(credential.getPassword());

        return doCredential;
    }

    public static Credential from(DoCredential doCredential) {
        Credential credential = new Credential();

        credential.setUuid(doCredential.getUuid());
        credential.setGroup(doCredential.getGroup().getOption());
        credential.setLabel(doCredential.getLabel());
        credential.setVersion(doCredential.getVersion());
        credential.setPlaceholderPrefix(doCredential.getPlaceholderPrefix());
        credential.setUsername(doCredential.getUsername());
        credential.setPassword(doCredential.getPassword());
        credential.setPasswordVerify(new StringType(doCredential.getPassword().getValue()));
        return credential;
    }


    public static ArrayList<CredentialSummaryItem> toSummaries(List<DoCredential> credentials) {
        ArrayList<CredentialSummaryItem> summaries = new ArrayList<>();
        for (DoCredential doCredential : credentials) {
            summaries.add(toSummary(doCredential));
        }

        return summaries;
    }

    public static CredentialSummaryItem toSummary(DoCredential doCredential) {
        CredentialSummaryItem summary = new CredentialSummaryItem();
        summary.setUuid(doCredential.getUuid());
        summary.setLabel(doCredential.getLabel());
        return summary;
    }
}
