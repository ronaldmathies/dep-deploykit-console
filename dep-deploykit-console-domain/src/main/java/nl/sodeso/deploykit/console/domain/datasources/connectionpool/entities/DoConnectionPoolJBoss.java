package nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolElasticSearchMapper;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = ConnectionPoolElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "connectionpoolidx")
@Entity
@DiscriminatorValue(value = "jboss")
@TypeDefs(
    value = {
            @TypeDef(name = "string", typeClass = StringTypeUserType.class),
            @TypeDef(name = "int", typeClass = IntTypeUserType.class),
            @TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)

    }
)
public class DoConnectionPoolJBoss extends DoConnectionPool {

    @Type(type = "int")
    @Column(name = "min_pool_size")
    private IntType minPoolSize = null;

    @Type(type = "int")
    @Column(name = "max_pool_size")
    private IntType maxPoolSize = null;

    @Type(type = "int")
    @Column(name = "initial_pool_size")
    private IntType initialPoolSize = null;

    @Type(type = "boolean")
    @Column(name = "prefill")
    private BooleanType prefill = null;

    @Type(type = "int")
    @Column(name = "blocking_timeout_millis")
    private IntType blockingTimeoutMillis = null;

    @Type(type = "int")
    @Column(name = "idle_timeout_minutes")
    private IntType idleTimeoutMinutes = null;

    @Type(type = "int")
    @Column(name = "allocation_retry")
    private IntType allocationRetry = null;

    @Type(type = "int")
    @Column(name = "allocation_retry_wait_millis")
    private IntType allocationRetryWaitMillis = null;

    public DoConnectionPoolJBoss() {}

    public IntType getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(IntType minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public IntType getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(IntType maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public IntType getInitialPoolSize() {
        return initialPoolSize;
    }

    public void setInitialPoolSize(IntType initialPoolSize) {
        this.initialPoolSize = initialPoolSize;
    }

    public BooleanType getPrefill() {
        return prefill;
    }

    public void setPrefill(BooleanType prefill) {
        this.prefill = prefill;
    }

    public IntType getBlockingTimeoutMillis() {
        return blockingTimeoutMillis;
    }

    public void setBlockingTimeoutMillis(IntType blockingTimeoutMillis) {
        this.blockingTimeoutMillis = blockingTimeoutMillis;
    }

    public IntType getIdleTimeoutMinutes() {
        return idleTimeoutMinutes;
    }

    public void setIdleTimeoutMinutes(IntType idleTimeoutMinutes) {
        this.idleTimeoutMinutes = idleTimeoutMinutes;
    }

    public IntType getAllocationRetry() {
        return allocationRetry;
    }

    public void setAllocationRetry(IntType allocationRetry) {
        this.allocationRetry = allocationRetry;
    }

    public IntType getAllocationRetryWaitMillis() {
        return allocationRetryWaitMillis;
    }

    public void setAllocationRetryWaitMillis(IntType allocationRetryWaitMillis) {
        this.allocationRetryWaitMillis = allocationRetryWaitMillis;
    }

}
