package nl.sodeso.deploykit.console.domain.services.ldap.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.LdapServiceElasticSearchMapper;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
    mapper = LdapServiceElasticSearchMapper.class,
    mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "ldapserviceidx")
@Entity
@Table(name = "t_ldap_service",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class),
        @TypeDef(name = "int", typeClass = IntTypeUserType.class)
    }
)
public class DoLdapService extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "placeholder_prefix", nullable = false, length = 255)
    private StringType placeholderPrefix;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_ldap_service_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group;

    @Type(type = "string")
    @Column(name = "domain", nullable = false, length = 255)
    private StringType domain = null;

    @Type(type = "int")
    @Column(name = "port")
    private IntType port = null;

    @JoinColumn(name="credential_id", foreignKey = @ForeignKey(name = "fk_ldap_service_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoCredential credential;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "ldapServices")
    private List<DoProfile> profiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "ldapService", fetch = FetchType.LAZY)
    private List<DoAccessControlLdap> ldapAccessControls = new ArrayList<>();

    public DoLdapService() {}

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public DoGroup getGroup() {
        return group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public StringType getDomain() {
        return domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public DoCredential getCredential() {
        return this.credential;
    }

    public void setCredential(DoCredential credential) {
        this.credential = credential;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public List<DoAccessControlLdap> getLdapAccessControls() {
        return this.ldapAccessControls;
    }

}
