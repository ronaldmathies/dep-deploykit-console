package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class AccessControlRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoAccessControl> find(Permissions permissions) {
        return BaseRepository.find(DoAccessControl.class, permissions);
    }

    public List<DoAccessControl> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoAccessControl.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoAccessControl.class, uuid);
    }

    public DoAccessControl findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoAccessControl.class, uuid, version);
    }

    public DoAccessControl checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoAccessControl.class, uuid, label);
    }

    public void save(DoAccessControl doAccessControl) {
        BaseRepository.save(doAccessControl);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoAccessControl.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoAccessControl> doAccessControls = criteria.list();
        for (DoAccessControl doAccessControl : doAccessControls) {
            doAccessControl.setLabel(label);
            session.update(doAccessControl);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoAccessControl.class, uuid, version);
    }

}
