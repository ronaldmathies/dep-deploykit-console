package nl.sodeso.deploykit.console.domain.security.role.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseEntity;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_role", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoRole extends DoBaseEntity {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "t_role_user", joinColumns = {
            @JoinColumn(name = "role_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_role_user_to_role")) },
            inverseJoinColumns = { @JoinColumn(name = "user_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_role_user_to_user")) })
    private List<DoUser> users = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "role")
    private List<DoGroupPermission> groupPermissions = new ArrayList<>();

    public DoRole() {}

    public List<DoUser> getUsers() {
        return users;
    }

    public void setUsers(List<DoUser> users) {
        this.users = users;
    }

    public void add(DoUser user) {
        this.users.add(user);
    }

    public void remove(DoUser user) {
        this.users.remove(user);
    }

    public void setGroupPermissions(List<DoGroupPermission> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    public List<DoGroupPermission> getGroupPermissions() {
        return this.groupPermissions;
    }

    public void addGroupPermission(DoGroupPermission groupPermission) {
        this.groupPermissions.add(groupPermission);
        groupPermission.setRole(this);
    }

}
