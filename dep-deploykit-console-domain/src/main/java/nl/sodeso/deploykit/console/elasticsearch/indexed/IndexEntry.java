package nl.sodeso.deploykit.console.elasticsearch.indexed;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchDocumentId;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchField;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@ElasticSearchUnit(unit = Constants.ELASTIC_SEARCH_UNIT)
@ElasticSearchType(type = "IndexEntry")
public class IndexEntry {

    @ElasticSearchDocumentId
    private String id;

    @ElasticSearchField
    private String uuid;

    @ElasticSearchField
    private String version;

    /**
     * Default true, some components don't have versioning, in those cases
     * we want the component to show up in all version forms.
     */
    @ElasticSearchField
    private Boolean isRelease;

    /**
     * Default true, some components don't have versioning, in those cases
     * we want the component to show up in all version forms.
     */
    @ElasticSearchField
    private Boolean isSnapshot;

    /**
     * Default true, some components don't have versioning, in those cases
     * we want the component to show up in all version forms.
     */
    @ElasticSearchField
    private Boolean isBranch;

    @ElasticSearchField
    private String label;

    @ElasticSearchField
    private Type type;

    @ElasticSearchField
    private List<String> keywords = new ArrayList<>();

    public IndexEntry() {}

    public IndexEntry(Long id, String uuid, VersionOption version, String label, Type type) {
        this.id = String.valueOf(id);
        this.uuid = uuid;

        if (version != null) {
            this.version = version.getKey();
            this.isRelease = version.getType().isRelease();
            this.isBranch = version.getType().isBranch();
            this.isSnapshot = version.getType().isSnapshot();
        }

        this.label = label;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getVersion() {
        return version;
    }

    public boolean isRelease() {
        return isRelease;
    }

    public boolean getIsRelease() {
        return isRelease;
    }

    public boolean isSnapshot() {
        return isSnapshot;
    }

    public boolean getIsSnapshot() {
        return isSnapshot;
    }

    public boolean isBranch() {
        return isBranch;
    }

    public boolean getIsBranch() {
        return isBranch;
    }

    public String getLabel() {
        return label;
    }

    public Type getType() {
        return type;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public IndexEntry addKeyword(String keyword) {
        this.keywords.add(keyword);
        return this;
    }

}
