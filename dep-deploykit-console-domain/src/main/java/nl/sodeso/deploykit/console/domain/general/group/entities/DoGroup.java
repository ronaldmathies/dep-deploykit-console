package nl.sodeso.deploykit.console.domain.general.group.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_group", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoGroup implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Type(type = "string")
    @Column(name = "label", nullable = false, length = 255)
    private StringType label;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoProfile> profiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoGroupPermission> groupPermissions = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoCredential> credentials = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoJdbcDriver> jdbcDrivers = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoDatasource> datasources = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoNode> nodes = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoHttps> httpses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoHttp> httpes = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group", orphanRemoval = false, fetch = FetchType.LAZY)
    private List<DoLdapService> ldapServices = new ArrayList<>();

    public DoGroup() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getLabel() {
        return label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public void addProfile(DoProfile profile) {
        this.profiles.add(profile);
        profile.setGroup(this);
    }

    public List<DoGroupPermission> getGroupPermissions() {
        return groupPermissions;
    }

    public void addGroupPermission(DoGroupPermission groupPermission) {
        this.groupPermissions.add(groupPermission);
        groupPermission.setGroup(this);
    }

    public List<DoCredential> getCredentials() {
        return this.credentials;
    }

    public void addCredential(DoCredential credential) {
        this.credentials.add(credential);
        credential.setGroup(this);
    }

    public List<DoJdbcDriver> getJdbcDrivers() {
        return this.jdbcDrivers;
    }

    public void addJdbcDriver(DoJdbcDriver jdbcDriver) {
        this.jdbcDrivers.add(jdbcDriver);
        jdbcDriver.setGroup(this);
    }

    public List<DoDatasource> getDatasources() {
        return this.datasources;
    }

    public void addDatasource(DoDatasource datasource) {
        this.datasources.add(datasource);
        datasource.setGroup(this);
    }

    public List<DoNode> getNodes() {
        return this.nodes;
    }

    public void addNode(DoNode node) {
        this.nodes.add(node);
        node.setGroup(this);
    }

    public List<DoHttps> getHttps() {
        return this.httpses;
    }

    public void addHttps(DoHttps https) {
        this.httpses.add(https);
        https.setGroup(this);
    }

    public List<DoHttp> getHttp() {
        return this.httpes;
    }

    public void addHttp(DoHttp http) {
        this.httpes.add(http);
        http.setGroup(this);
    }

    public List<DoLdapService> getLdapServices() {
        return this.ldapServices;
    }

    public void addLdapService(DoLdapService ldapService) {
        this.ldapServices.add(ldapService);
        ldapService.setGroup(this);
    }

    public OptionType<DefaultOption> getOption() {
        return new OptionType<>(new DefaultOption(uuid, label.getValue().toLowerCase(), label.getValue()));
    }
}
