package nl.sodeso.deploykit.console.domain.types;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A Hibernate user type to map an OptionType to three individual STRING types in the database.
 *
 * @author Ronald Mathies
 */
public class VersionOptionCompositeUserType implements CompositeUserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getPropertyNames() {
        return new String[] {"key", "code", "description", "type"};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Type[] getPropertyTypes() {
        return new Type[] {StringType.INSTANCE, StringType.INSTANCE, StringType.INSTANCE, StringType.INSTANCE};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getPropertyValue(Object component, int property) throws HibernateException {
        VersionOptionType optionType = (VersionOptionType)component;

        if (optionType.getValue() != null) {
            switch (property) {
                case 0:
                    return optionType.getValue().getKey();
                case 1:
                    return optionType.getValue().getCode();
                case 2:
                    return optionType.getValue().getDescription();
                case 3:
                    return optionType.getValue().getType();
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPropertyValue(Object component, int property, Object value) throws HibernateException {
        VersionOption option = new VersionOption();
        switch (property) {
            case 0:
                option.setKey((String) value);
                break;
            case 1:
                option.setCode((String) value);
                break;
            case 2:
                option.setDescription((String)value);
                break;
            case 3:
                option.setType(nl.sodeso.deploykit.console.client.application.ui.options.VersionType.valueOf((String) value));
                break;
        }
        VersionOptionType optionType = (VersionOptionType)component;
        optionType.setValue(option);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class returnedClass() {
        return VersionOptionType.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o1, Object o2) throws HibernateException {
        if (o1 == null || o2 == null) {
            return false;
        }

        VersionOption option1 = ((VersionOptionType) o1).getValue();
        VersionOption option2 = ((VersionOptionType) o2).getValue();

        return !(option1 != null && option2 != null) || option1.equals(option2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object value) throws HibernateException {
        return value.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        VersionOption option = null;
        String key = StringType.INSTANCE.nullSafeGet(resultSet, names[0], session);
        if (!resultSet.wasNull()) {
            option = new VersionOption();

            option.setKey(key);
            option.setCode(StringType.INSTANCE.nullSafeGet(resultSet, names[1], session));
            option.setDescription(StringType.INSTANCE.nullSafeGet(resultSet, names[2], session));
            option.setType(VersionType.valueOf(StringType.INSTANCE.nullSafeGet(resultSet, names[3], session)));
        }

        return new VersionOptionType(option);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int property, SessionImplementor session) throws HibernateException, SQLException {
        if (null == value || ((VersionOptionType)value).getValue() == null) {
            preparedStatement.setNull(property, StringType.INSTANCE.sqlType());
            preparedStatement.setNull(property + 1, StringType.INSTANCE.sqlType());
            preparedStatement.setNull(property + 2, StringType.INSTANCE.sqlType());
            preparedStatement.setNull(property + 3, StringType.INSTANCE.sqlType());
        } else {
            final VersionOptionType optionType = (VersionOptionType) value;

            StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getKey(), property, session);
            StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getCode(), property + 1, session);
            StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getDescription(), property + 2, session);
            StringType.INSTANCE.nullSafeSet(preparedStatement, optionType.getValue().getType().name(), property + 3, session);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        VersionOptionType optionType = (VersionOptionType) value;
        VersionOption option = optionType.getValue();
        return new VersionOptionType(option != null ? option.clone() : null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value, SessionImplementor session) throws HibernateException {
        return (Serializable) value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, SessionImplementor session, Object owner) throws HibernateException {
        return cached;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, SessionImplementor session, Object owner) throws HibernateException {
        return this.deepCopy(original);
    }

}
