package nl.sodeso.deploykit.console.domain;

/**
 * @author Ronald Mathies
 */
public class Permissions {

    public static final int READ = 1;
    public static final int USE = 2;
    public static final int EDIT = 4;
    public static final int DEPLOY = 8;

    private String username;
    private int permissions = 0;

    public Permissions(String username, int permissions) {
        this.username = username;
        this.permissions = permissions;
    }

    public String getUsername() {
        return this.username;
    }

    public boolean isPermissionRequired(int permission) {
        return (permissions & permission) == permission;
    }

}
