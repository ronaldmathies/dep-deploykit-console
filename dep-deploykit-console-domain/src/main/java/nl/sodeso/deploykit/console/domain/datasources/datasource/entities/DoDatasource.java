package nl.sodeso.deploykit.console.domain.datasources.datasource.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = DatasourceElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "datasourceidx")
@Entity
@Table(name = "t_datasource",
    indexes = {
            @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
            @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@TypeDefs(
    value = {
            @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
public class DoDatasource extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "placeholder_prefix", nullable = false, length = 255)
    private StringType placeholderPrefix = null;

    @Type(type = "string")
    @Column(name = "jndi", nullable = false, length = 255)
    private StringType jndi;

    @Type(type = "string")
    @Column(name = "url", nullable = false, length = 255)
    private StringType url;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_datasource_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group;

    @JoinColumn(name="credential_id", foreignKey = @ForeignKey(name = "fk_datasource_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoCredential credential;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "datasources")
    private List<DoProfile> profiles = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_datasource_connection_pool", joinColumns = {
            @JoinColumn(name = "datasource_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_datasource_connection_pool_to_datasource")) },
            inverseJoinColumns = { @JoinColumn(name = "connection_pool_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_datasource_connectionpool_to_connection_pool")) })
    private List<DoConnectionPool> connectionPools = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "datasource")
    private List<DoConnectionProperty> properties = new ArrayList<>();

    @JoinColumn(name="jdbc_driver_id", foreignKey = @ForeignKey(name = "fk_datasource_to_jdbc_driver"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoJdbcDriver jdbcDriver;

    public DoDatasource() {}

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getJndi() {
        return jndi;
    }

    public void setJndi(StringType jndi) {
        this.jndi = jndi;
    }

    public StringType getUrl() {
        return url;
    }

    public void setUrl(StringType url) {
        this.url = url;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public DoCredential getCredential() {
        return this.credential;
    }

    public void setCredential(DoCredential credential) {
        this.credential = credential;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public List<DoConnectionPool> getConnectionPools() {
        return this.connectionPools;
    }

    public List<DoConnectionProperty> getProperties() {
        return this.properties;
    }

    public DoJdbcDriver getJdbcDriver() {
        return this.jdbcDriver;
    }

    public void setJdbcDriver(DoJdbcDriver jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

}
