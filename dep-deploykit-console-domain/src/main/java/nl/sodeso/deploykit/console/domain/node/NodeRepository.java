package nl.sodeso.deploykit.console.domain.node;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class NodeRepository {

    public List<DoNode> find(Permissions permissions) {
        return BaseRepository.find(DoNode.class, permissions);
    }

    public List<DoNode> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoNode.class, permissions, groupUuid);
    }

    public DoNode findByUuid(String uuid) {
        return BaseRepository.findByUuid(DoNode.class, uuid);
    }

    public DoNode checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoNode.class, uuid, label);
    }

    public void save(DoNode doNode) {
        BaseRepository.save(doNode);
    }

    public void remove(String uuid) {
        BaseRepository.remove(DoNode.class, uuid);
    }

}
