package nl.sodeso.deploykit.console.domain.accesscontrol.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.accesscontrol.AccessControlLdapElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = AccessControlLdapElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "ldapaccesscontrolidx")
@Entity
@DiscriminatorValue(value = "ldap")
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class),
                @TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)

        }
)
public class DoAccessControlLdap extends DoAccessControl {

    /**
     * Jetty: userBaseDn
     *
     * Base DN where users are to be searched from.
     *
     * Wildfly: principalDNSuffix
     *
     * A prefix and suffix to add to the username when forming the user distinguished name. This is useful
     * if you prompt a user for a username and you don't want them to have to enter the fully distinguished
     * name. Using this property and principalDNSuffix the userDN will be formed as:
     *
     # String userDN = principalDNPrefix + username + principalDNSuffix;
     */
    @Type(type = "string")
    @Column(name = "user_base_dn", nullable = false, length = 128)
    private StringType userBaseDn = null;

    /**
     * Jetty: userIdAttribute
     * Jetty: userRdnAttribute (Not used by jetty but does influence the lookup, should be the same as userIdAttribute)
     *
     * Attribute that the principal is located.
     *
     * Wildfly: principalDNPrefix
     *
     * A prefix and suffix to add to the username when forming the user distiguished name. This is useful
     * if you prompt a user for a username and you don't want them to have to enter the fully distinguished
     * name. Using this property and principalDNSuffix the userDN will be formed as:
     *
     # String userDN = principalDNPrefix + username + principalDNSuffix;
     */
    @Type(type = "string")
    @Column(name = "user_attribute", nullable = false, length = 16)
    private StringType userAttribute = null;

    /**
     * Jetty: roleBaseDn
     *
     * Base DN where role memberschip is to be searched from.
     *
     * Wildfly: rolesCtxDN
     *
     * The fixed distinguished name to the context to search for user roles. Consider that this is not the
     * Distinguished Name of where the actual roles are; rather, this is the DN of where the objects containing
     * the user roles are (e.g. for active directory, this is the DN where the user account is)
     */
    @Type(type = "string")
    @Column(name = "role_base_dn", nullable = false, length = 128)
    private StringType roleBaseDn = null;

    /**
     * Jetty: roleMemberAttribute
     *
     * Name of the attribute that a username would be under a role class.
     *
     * Wildfly: uidAttributeID
     *
     * Type of the attribute in the role object that identifies member user ids. This is used to locate the user's
     * roles through an LDAP query. For example, the query might look like:
     *
     * "(" + uidAttributeID + "={0})"
     */
    @Type(type = "string")
    @Column(name = "role_member_attribute", nullable = false, length = 16)
    private StringType roleMemberAttribute = null;

    /**
     * Jetty: roleNameAttribute
     *
     * The name of the attribute that a role would be stored under.
     *
     * Wildfly: roleAttributeID
     *
     * The name of the attribute that a role would be stored under.
     */
    @Type(type = "string")
    @Column(name = "role_name_attribute", nullable = false, length = 16)
    private StringType roleNameAttribute = null;

    /**
     * Jetty Only:
     *
     * If the getUserInfo can pull a password off of the user then password comparison is an option for authn, to force
     * binding login checks, set this to true.
     *
     * Comment:
     *
     * By default we set this to true.
     */
    @Type(type = "boolean")
    @Column(name = "force_binding_login", nullable = false)
    private BooleanType forceBindingLogin = new BooleanType(true);

    @JoinColumn(name="ldap_service_id", foreignKey = @ForeignKey(name = "fk_access_control_to_ldap_service"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoLdapService ldapService;

    /**
     * Jetty: userPasswordAttribute
     *
     * Name of the attribute that a user password is stored under.
     */
    @Type(type = "string")
    @Column(name = "user_password_attribute", nullable = false, length = 16)
    private StringType userPasswordAttribute = null;

    /**
     * Jetty: userObjectClass
     *
     * Object class of a user.
     */
    @Type(type = "string")
    @Column(name = "user_object_class", nullable = false, length = 64)
    private StringType userObjectClass = null;

    /**
     * Jetty: roleObjectClass
     *
     * Object class of a role.
     */
    @Type(type = "string")
    @Column(name = "role_object_class", nullable = false, length = 64)
    private StringType roleObjectClass = null;

    public DoAccessControlLdap() {}

    public StringType getUserBaseDn() {
        return userBaseDn;
    }

    public void setUserBaseDn(StringType userBaseDn) {
        this.userBaseDn = userBaseDn;
    }

    public StringType getUserAttribute() {
        return userAttribute;
    }

    public void setUserAttribute(StringType userAttributeDn) {
        this.userAttribute = userAttributeDn;
    }

    public StringType getRoleBaseDn() {
        return roleBaseDn;
    }

    public void setRoleBaseDn(StringType roleBaseDn) {
        this.roleBaseDn = roleBaseDn;
    }

    public StringType getRoleMemberAttribute() {
        return roleMemberAttribute;
    }

    public void setRoleMemberAttribute(StringType roleMemberAttribute) {
        this.roleMemberAttribute = roleMemberAttribute;
    }

    public StringType getRoleNameAttribute() {
        return roleNameAttribute;
    }

    public void setRoleNameAttribute(StringType roleUserAttribute) {
        this.roleNameAttribute = roleUserAttribute;
    }

    public BooleanType getForceBindingLogin() {
        return forceBindingLogin;
    }

    public DoLdapService getLdapService() {
        return ldapService;
    }

    public void setLdapService(DoLdapService ldapService) {
        this.ldapService = ldapService;
    }

    public StringType getUserPasswordAttribute() {
        return userPasswordAttribute;
    }

    public void setUserPasswordAttribute(StringType userPasswordAttribute) {
        this.userPasswordAttribute = userPasswordAttribute;
    }

    public StringType getUserObjectClass() {
        return userObjectClass;
    }

    public void setUserObjectClass(StringType userObjectClass) {
        this.userObjectClass = userObjectClass;
    }

    public StringType getRoleObjectClass() {
        return roleObjectClass;
    }

    public void setRoleObjectClass(StringType roleObjectClass) {
        this.roleObjectClass = roleObjectClass;
    }

}
