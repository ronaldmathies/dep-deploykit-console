package nl.sodeso.deploykit.console.domain.connectors.https.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_certificate", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
public class DoCertificate {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "alias", nullable = false, length = 255)
    private String alias;

    @Column(name = "notbefore", nullable = false)
    private Date notBefore;

    @Column(name = "notafter", nullable = false)
    private Date notAfter;

    @Column(name = "subjectdn", nullable = false, length = 512)
    private String subjectDN;

    @Column(name = "issuerdn", nullable = false, length = 512)
    private String issuerDN;

    @Column(name = "sigalname", nullable = false, length = 64)
    private String sigAlgName;

    @Column(name = "serialnumber", nullable = false, length = 64)
    private String serialNumber;

    @JoinColumn(name="keystore_id", foreignKey = @ForeignKey(name = "fk_certificate_to_keystore"))
    @ManyToOne(fetch = FetchType.LAZY)
    private DoKeystore keystore = null;

    public DoCertificate() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public Date getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = notAfter;
    }

    public String getSubjectDN() {
        return subjectDN;
    }

    public void setSubjectDN(String subjectDN) {
        this.subjectDN = subjectDN;
    }

    public String getIssuerDN() {
        return issuerDN;
    }

    public void setIssuerDN(String issuerDN) {
        this.issuerDN = issuerDN;
    }

    public String getSigAlgName() {
        return sigAlgName;
    }

    public void setSigAlgName(String sigAlgName) {
        this.sigAlgName = sigAlgName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public DoKeystore getKeystore() {
        return keystore;
    }

    public void setKeystore(DoKeystore keystore) {
        this.keystore = keystore;
    }
}
