package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverJar;
import nl.sodeso.deploykit.console.client.application.datasources.jdbcdriver.JdbcDriverSummaryItem;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverClass;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverJar;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.util.classloader.JdbcDriverClassloader;
import nl.sodeso.gwt.ui.client.form.upload.FileUpload;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.io.IOException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverConvertor {

    public static DoJdbcDriver to(DoJdbcDriver doJdbcDriver, JdbcDriver jdbcDriver) {
        if (doJdbcDriver == null) {
            doJdbcDriver = new DoJdbcDriver();
            doJdbcDriver.setUuid(UuidUtils.generate());
        }

        doJdbcDriver.setGroup(new GroupRepository().findByUuid(jdbcDriver.getGroup().getValue().getKey()));
        doJdbcDriver.setVersion(jdbcDriver.getVersion());
        doJdbcDriver.setLabel(jdbcDriver.getLabel());

        handleJars(doJdbcDriver, jdbcDriver);
        handleJdbcDriverClasses(doJdbcDriver);


        if (jdbcDriver.getJdbcDriverClass() != null && jdbcDriver.getJdbcDriverClass().getValue() != null) {
            String jdbcDriverClassUuid = jdbcDriver.getJdbcDriverClass().getValue().getKey();

            DoJdbcDriverClass doJdbcDriverClass = null;
            if (jdbcDriverClassUuid != null) {
                doJdbcDriverClass = new JdbcDriverRepository().findClassByUuid(jdbcDriverClassUuid);
            }

            doJdbcDriver.setJdbcDriverClass(doJdbcDriverClass);
        }

        return doJdbcDriver;
    }

    private static void handleJars(DoJdbcDriver doJdbcDriver, JdbcDriver jdbcDriver) {
        Iterator<DoJdbcDriverJar> doJdbcDriverJarIterator = doJdbcDriver.getJdbcDriverJars().iterator();
        while (doJdbcDriverJarIterator.hasNext()) {
            DoJdbcDriverJar doJdbcDriverJar = doJdbcDriverJarIterator.next();

            for (JdbcDriverJar deletedJar : jdbcDriver.getJars().getDeletedWidgets()) {
                if (doJdbcDriverJar.getUuid().equals(deletedJar.getFileUploadType().getValue().getUuid())) {
                    doJdbcDriverJarIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (JdbcDriverJar jar : jdbcDriver.getJars().getWidgets()) {
            String jarUuid = jar.getFileUploadType().getValue().getUuid();

            boolean exists = false;
            for (DoJdbcDriverJar doJdbcDriverJar : doJdbcDriver.getJdbcDriverJars()) {
                if (doJdbcDriverJar.getUuid().equalsIgnoreCase(jarUuid)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doJdbcDriver.getJdbcDriverJars().add(new JdbcDriverJarRepository().findByUuid(jarUuid));
            }

        }
    }

    private static void handleJdbcDriverClasses(DoJdbcDriver doJdbcDriver) {
        List<DoJdbcDriverClass> jdbcDriverClasses = getJdbcDriverClasses(doJdbcDriver);

        for (DoJdbcDriverClass foundJdbcDriverClass : jdbcDriverClasses) {

            boolean found = false;
            for (DoJdbcDriverClass existingJdbcDriverClass : doJdbcDriver.getJdbcDriverClasses()) {
                if (foundJdbcDriverClass.getClassname().equals(existingJdbcDriverClass.getClassname())) {
                    found = true;

                    break;
                }
            }

            if (!found) {
                doJdbcDriver.getJdbcDriverClasses().add(foundJdbcDriverClass);
            }
        }

        for (DoJdbcDriverClass existingJdbcDriverClass : doJdbcDriver.getJdbcDriverClasses()) {

            boolean found = false;
            for (DoJdbcDriverClass foundJdbcDriverClass : jdbcDriverClasses) {
                if (existingJdbcDriverClass.getClassname().equals(foundJdbcDriverClass.getClassname())) {
                    found = true;
                }
            }

            if (!found) {
                doJdbcDriver.getJdbcDriverClasses().remove(existingJdbcDriverClass);

                if (doJdbcDriver.getJdbcDriverClass() != null &&
                        doJdbcDriver.getJdbcDriverClass().getUuid().equals(existingJdbcDriverClass.getUuid())) {
                    doJdbcDriver.setJdbcDriverClass(null);
                }
            }
        }
    }

    public static List<DoJdbcDriverClass> getJdbcDriverClasses(DoJdbcDriver doJdbcDriver) {
        List<DoJdbcDriverJar> jdbcDriverJars = doJdbcDriver.getJdbcDriverJars();

        try {
            ArrayList<DoJdbcDriverClass> doJdbcDriverClasses = new ArrayList<>();

            JdbcDriverClassloader jdbcDriverClassloader = new JdbcDriverClassloader(jdbcDriverJars, null);

            Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                    .setUrls(ClasspathHelper.forClassLoader(jdbcDriverClassloader))
            );

            for (Class<? extends Driver> driverClass : reflections.getSubTypesOf(Driver.class)) {
                DoJdbcDriverClass doJdbcDriverClass = new DoJdbcDriverClass();

                Driver driver = driverClass.newInstance();
                doJdbcDriverClass.setUuid(UuidUtils.generate());
                doJdbcDriverClass.setClassname(driverClass.getName());
                doJdbcDriverClass.setMajorVersion(driver.getMajorVersion());
                doJdbcDriverClass.setMinorVersion(driver.getMinorVersion());

                doJdbcDriverClasses.add(doJdbcDriverClass);
            }

            return doJdbcDriverClasses;
        } catch (IOException | IllegalAccessException | InstantiationException e) {

        }

        return new ArrayList<>();
    }

    public static JdbcDriver from(DoJdbcDriver doJdbcDriver) {
        JdbcDriver jdbcDriver = new JdbcDriver();


        List<DoJdbcDriverJar> jdbcDriverJars = doJdbcDriver.getJdbcDriverJars();
        for (DoJdbcDriverJar doJdbcDriverJar : jdbcDriverJars) {
            JdbcDriverJar jar = new JdbcDriverJar();
            jar.setUuid(doJdbcDriverJar.getUuid());
            jar.setFileUploadType(new FileUploadType(new FileUpload(doJdbcDriverJar.getUuid(), doJdbcDriverJar.getFilename(), doJdbcDriverJar.getFilesize())));
            jdbcDriver.getJars().getWidgets().add(jar);
        }

        jdbcDriver.setShowJdbcDriverClasses(!doJdbcDriver.getJdbcDriverClasses().isEmpty());

        if (doJdbcDriver.getJdbcDriverClass() != null) {
            jdbcDriver.setJdbcDriverClass(doJdbcDriver.getJdbcDriverClass().getOption());
        }

        jdbcDriver.setUuid(doJdbcDriver.getUuid());
        jdbcDriver.setLabel(doJdbcDriver.getLabel());
        jdbcDriver.setGroup(doJdbcDriver.getGroup().getOption());
        jdbcDriver.setVersion(doJdbcDriver.getVersion());

        return jdbcDriver;
    }

    public static ArrayList<JdbcDriverSummaryItem> toSummaries(List<DoJdbcDriver> jdbcDrivers) {
        ArrayList<JdbcDriverSummaryItem> summaries = new ArrayList<>();
        for (DoJdbcDriver doJdbcDriver : jdbcDrivers) {
            summaries.add(toSummary(doJdbcDriver));
        }

        return summaries;
    }

    public static JdbcDriverSummaryItem toSummary(DoJdbcDriver doJdbcDriver) {
        JdbcDriverSummaryItem summary = new JdbcDriverSummaryItem();
        summary.setUuid(doJdbcDriver.getUuid());
        summary.setLabel(doJdbcDriver.getLabel());
        return summary;
    }

}
