package nl.sodeso.deploykit.console.domain.node;

import nl.sodeso.deploykit.console.client.application.node.Node;
import nl.sodeso.deploykit.console.client.application.node.NodeSummaryItem;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class NodeConvertor {

    public static DoNode to(DoNode doNode, Node node) {
        if (doNode == null) {
            doNode = new DoNode();
            doNode.setUuid(UuidUtils.generate());
        }

        doNode.setLabel(node.getLabel());
        doNode.setGroup(new GroupRepository().findByUuid(node.getGroup().getValue().getKey()));
        doNode.setHostname(node.getHostname());
        doNode.setPort(node.getPort());
        doNode.setUsername(node.getUsername());
        doNode.setPassword(node.getPassword());

        return doNode;
    }

    public static Node from(DoNode doNode) {
        Node node = new Node();

        node.setUuid(doNode.getUuid());
        node.setLabel(doNode.getLabel());
        node.setGroup(doNode.getGroup().getOption());
        node.setHostname(doNode.getHostname());
        node.setPort(doNode.getPort());
        node.setUsername(doNode.getUsername());
        node.setPassword(doNode.getPassword());

        return node;
    }

    public static ArrayList<NodeSummaryItem> toSummaries(List<DoNode> nodes) {
        ArrayList<NodeSummaryItem> summaries = new ArrayList<>();
        for (DoNode doNode : nodes) {
            summaries.add(toSummary(doNode));
        }

        return summaries;
    }

    public static NodeSummaryItem toSummary(DoNode doNode) {
        NodeSummaryItem summary = new NodeSummaryItem();
        summary.setUuid(doNode.getUuid());
        summary.setLabel(doNode.getLabel());
        return summary;
    }
}
