package nl.sodeso.deploykit.console.domain;

import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
@MappedSuperclass
public abstract class DoBaseEntity implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Type(type = "string")
    @Column(name = "label", nullable = false, length = 255)
    private StringType label;

    public DoBaseEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getLabel() {
        return label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public OptionType<DefaultOption> getOption() {
        return new OptionType<>(new DefaultOption(uuid, label.getValue().toLowerCase(), label.getValue()));
    }
}
