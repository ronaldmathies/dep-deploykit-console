package nl.sodeso.deploykit.console.domain.datasources.connectionpool;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoConnectionPool> find(Permissions permissions) {
        return BaseRepository.find(DoConnectionPool.class, permissions);
    }

    public List<DoConnectionPool> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoConnectionPool.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoConnectionPool.class, uuid);
    }

    public DoConnectionPool findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoConnectionPool.class, uuid, version);
    }

    public DoConnectionPool checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoConnectionPool.class, uuid, label);
    }

    public void save(DoConnectionPool doConnectionPool) {
        BaseRepository.save(doConnectionPool);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoConnectionPool.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoConnectionPool> doConnectionPools = criteria.list();
        for (DoConnectionPool doConnectionPool : doConnectionPools) {
            doConnectionPool.setLabel(label);
            session.update(doConnectionPool);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoConnectionPool.class, uuid, version);
    }
}
