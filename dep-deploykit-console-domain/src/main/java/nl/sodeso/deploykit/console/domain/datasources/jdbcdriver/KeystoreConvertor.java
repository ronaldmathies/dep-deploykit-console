package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.gwt.ui.server.servlet.UploadFile;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.hibernate.executors.CloseInputStreamExecutor;
import nl.sodeso.persistence.hibernate.executors.DeleteFileExecutor;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author Ronald Mathies
 */
public class KeystoreConvertor {

    public static DoKeystore to(DoKeystore doKeystore, UploadFile uploadFile) {
        if (doKeystore == null) {
            doKeystore = new DoKeystore();
            doKeystore.setUuid(UuidUtils.generate());
        }

        doKeystore.setFilename(uploadFile.getFilename());

        try {
            File tempFile = uploadFile.getTempFile();
            FileInputStream fis = new FileInputStream(tempFile);
            doKeystore.setKeystore(fis);

            UnitOfWork unitOfWork = UnitOfWorkFactory.currentUnitOfWork(Constants.PU);
            unitOfWork.addExecutor(new CloseInputStreamExecutor(fis));
            unitOfWork.addExecutor(new DeleteFileExecutor(tempFile));

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }

        return doKeystore;
    }

}
