package nl.sodeso.deploykit.console.domain.accesscontrol.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@DiscriminatorColumn(name = "dtype")
@Table(name = "t_accesscontrol",
        indexes = {
                @Index(columnList = "uuid", name = "ix_uuid")
        },
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
        })
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
public class DoAccessControl extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "placeholder_prefix", nullable = false, length = 255)
    private StringType placeholderPrefix;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_access_control_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group;

    @Type(type = "string")
    @Column(name = "realm", nullable = false, length = 32)
    private StringType realm = null;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "accessControl")
    private List<DoProfile> profiles = new ArrayList<>();

    public DoAccessControl() {}

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public DoGroup getGroup() {
        return group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public StringType getRealm() {
        return realm;
    }

    public void setRealm(StringType realm) {
        this.realm = realm;
    }

    public List<DoProfile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<DoProfile> profiles) {
        this.profiles = profiles;
    }

}
