package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class AccessControlDatasourceElasticSearchMapper implements ElasticSearchMapper<DoAccessControlDatasource, IndexEntry> {

    @Override
    public IndexEntry map(DoAccessControlDatasource doAccessControlDatasource) {
        IndexEntry indexEntry = new IndexEntry(doAccessControlDatasource.getId(), doAccessControlDatasource.getUuid(), doAccessControlDatasource.getVersion().getValue(), doAccessControlDatasource.getLabel().getValue(), Type.LDAP_ACCESS_CONTROL)
                .addKeyword(doAccessControlDatasource.getLabel().getValue())
                .addKeyword(doAccessControlDatasource.getUuid())
                .addKeyword(doAccessControlDatasource.getPlaceholderPrefix().getValue())
                .addKeyword(doAccessControlDatasource.getUserTable().getValue())
                .addKeyword(doAccessControlDatasource.getUserColumn().getValue())
                .addKeyword(doAccessControlDatasource.getCredentialColumn().getValue())
                .addKeyword(doAccessControlDatasource.getRoleTable().getValue())
                .addKeyword(doAccessControlDatasource.getRoleUserColumn().getValue())
                .addKeyword(doAccessControlDatasource.getRoleRoleColumn().getValue());

        return indexEntry;
    }

}
