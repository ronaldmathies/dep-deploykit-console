package nl.sodeso.deploykit.console.domain.connectors.https.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.Hibernate;
import org.hibernate.engine.jdbc.LobCreator;

import javax.persistence.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_keystore", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
public class DoKeystore implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "filename", nullable = false, length = 255)
    private String filename;

    @Column(name = "keystore", nullable = false, length = 4096)
    private Blob keystore;

    @Column(name = "filesize", nullable = false)
    private Integer filesize;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "keystore")
    private List<DoCertificate> certificates = new ArrayList<>();;

    public DoKeystore() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setKeystore(InputStream inputStream) {
        try {
            this.filesize = inputStream.available();

            LobCreator lobCreator = Hibernate.getLobCreator(ThreadSafeSession.getSession(Constants.PU));

            this.keystore = lobCreator.createBlob(inputStream, inputStream.available());
        } catch (IOException e) {
            throw new PersistenceException("Failed to write binary stream to blob.", e);
        }
    }

    public InputStream getKeystore() {
        try {
            return keystore.getBinaryStream();
        } catch (SQLException e) {
            throw new PersistenceException("Failed to retrieve binary stream of blob.", e);
        }
    }

    public Integer getFilesize() {
        return this.filesize;
    }

    public List<DoCertificate> getCertificates() {
        return certificates;
    }
}
