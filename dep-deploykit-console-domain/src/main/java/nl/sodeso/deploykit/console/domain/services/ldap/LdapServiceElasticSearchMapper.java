package nl.sodeso.deploykit.console.domain.services.ldap;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class LdapServiceElasticSearchMapper implements ElasticSearchMapper<DoLdapService, IndexEntry> {

    @Override
    public IndexEntry map(DoLdapService doLdapService) {
        IndexEntry indexEntry = new IndexEntry(doLdapService.getId(), doLdapService.getUuid(), null, doLdapService.getLabel().getValue(), Type.LDAP_SERVICE)
                .addKeyword(doLdapService.getLabel().getValue())
                .addKeyword(doLdapService.getUuid())
                .addKeyword(doLdapService.getDomain().getValue())
                .addKeyword(doLdapService.getPlaceholderPrefix().getValue());

        return indexEntry;
    }

}
