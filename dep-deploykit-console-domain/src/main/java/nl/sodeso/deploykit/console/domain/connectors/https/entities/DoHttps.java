package nl.sodeso.deploykit.console.domain.connectors.https.entities;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.connectors.https.HttpsElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.types.VersionOptionCompositeUserType;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = HttpsElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "httpsidx")
@Entity
@Table(name = "t_https",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
            @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class),
                @TypeDef(name = "int", typeClass = IntTypeUserType.class)
        }
)
public class DoHttps extends DoBaseVersionedEntity {

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_https_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group = null;

    @Type(type = "string")
    @Column(name = "host", nullable = true, length = 255)
    private StringType host = null;

    @Type(type="int")
    @Column(name = "port", nullable = false)
    private IntType port = null;

    @Type(type="int")
    @Column(name = "timeout", nullable = false)
    private IntType timeout = null;

    @JoinColumn(name="key_store_password_id", foreignKey = @ForeignKey(name = "fk_https_keystore_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoCredential keyStorePassword = null;

    @JoinColumn(name="trust_store_password_id", foreignKey = @ForeignKey(name = "fk_https_truststore_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoCredential trustStorePassword = null;

    @JoinColumn(name="key_manager_password_id", foreignKey = @ForeignKey(name = "fk_https_key_manager_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoCredential keyManagerPassword = null;

    @JoinColumn(name="keystore_id", foreignKey = @ForeignKey(name = "fk_https_to_keystore"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoKeystore keystore = null;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy ="https")
    private List<DoProfile> profiles = new ArrayList<>();

    public DoHttps() {}

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public StringType getHost() {
        return host;
    }

    public void setHost(StringType host) {
        this.host = host;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

    public DoCredential getKeyStorePassword() {
        return this.keyStorePassword;
    }

    public void setKeyStorePassword(DoCredential keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public DoCredential getTrustStorePassword() {
        return this.trustStorePassword;
    }

    public void setTrustStorePassword(DoCredential trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }

    public DoCredential getKeyManagerPassword() {
        return this.keyManagerPassword;
    }

    public void setKeyManagerPassword(DoCredential keyManagerPassword) {
        this.keyManagerPassword = keyManagerPassword;
    }

    public DoKeystore getKeystore() {
        return this.keystore;
    }

    public void setKeystore(DoKeystore keystore) {
        this.keystore = keystore;
    }

}
