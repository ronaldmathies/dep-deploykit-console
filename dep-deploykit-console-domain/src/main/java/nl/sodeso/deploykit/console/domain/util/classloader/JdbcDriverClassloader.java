package nl.sodeso.deploykit.console.domain.util.classloader;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverJar;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.hibernate.executors.Executor;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverClassloader extends URLClassLoader {

    public JdbcDriverClassloader(List<DoJdbcDriverJar> jars, ClassLoader classLoader) throws IOException {
        super(new URL[] {}, classLoader);

        for (DoJdbcDriverJar doJdbcDriverJar : jars) {

            final File jarfileTempLocation = new File(FileUtil.getSystemTempFolderAsFile(), doJdbcDriverJar.getFilename());
            UnitOfWork unitOfWork = UnitOfWorkFactory.currentUnitOfWork(Constants.PU);
            unitOfWork.addExecutor(new Executor() {
                @Override
                public Phase[] isApplicableFor() {
                    return new Phase[] {Phase.AfterCommit};
                }

                @Override
                public void execute() {
                    if (jarfileTempLocation.exists()) {
                        jarfileTempLocation.delete();
                    }
                }
            });
            Files.copy(doJdbcDriverJar.getJar(), jarfileTempLocation.toPath(), StandardCopyOption.REPLACE_EXISTING);
            addURL(jarfileTempLocation.toURI().toURL());
        }
    }

}
