package nl.sodeso.deploykit.console.domain.services.webservice.entities;

/**
 * @author Ronald Mathies
 */

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_webservice_property",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    })
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
public class DoWebserviceProperty {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Type(type = "string")
    @Column(name = "property_key", nullable = false)
    private StringType key;

    @Type(type = "string")
    @Column(name = "property_value", nullable = false)
    private StringType value;

    @JoinColumn(name="webservice_id", foreignKey = @ForeignKey(name = "fk_webservice_property_to_webservice"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoWebservice webservice = null;

    public DoWebserviceProperty() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getKey() {
        return key;
    }

    public void setKey(StringType key) {
        this.key = key;
    }

    public StringType getValue() {
        return value;
    }

    public void setValue(StringType value) {
        this.value = value;
    }

    public DoWebservice getWebservice() {
        return this.webservice;
    }

}
