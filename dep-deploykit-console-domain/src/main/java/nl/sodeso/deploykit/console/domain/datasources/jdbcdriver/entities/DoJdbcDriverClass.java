package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_jdbc_driver_class", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
public class DoJdbcDriverClass {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "majorversion", nullable = false)
    private Integer majorVersion;

    @Column(name = "minorversion", nullable = false)
    private Integer minorVersion;

    @Column(name = "classname", nullable = false, length = 255)
    private String classname;

//    @JoinColumn(name="jdbc_driver_id", foreignKey = @ForeignKey(name = "fk_jdbc_driver_class_to_jdbc_driver"))
//    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
//    private DoJdbcDriver jdbcDriver = null;

    public DoJdbcDriverClass() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(int minoVersion) {
        this.minorVersion = minoVersion;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

//    public DoJdbcDriver getJdbcDriver() {
//        return this.jdbcDriver;
//    }

    public OptionType<DefaultOption> getOption() {
        return new OptionType<>(new DefaultOption(uuid, uuid, classname + " (" + majorVersion + "." + minorVersion + ")"));
    }
}
