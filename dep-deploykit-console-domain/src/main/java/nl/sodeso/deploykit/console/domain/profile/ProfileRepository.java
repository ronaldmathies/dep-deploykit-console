package nl.sodeso.deploykit.console.domain.profile;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ProfileRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoProfile> find(Permissions permissions) {
        return BaseRepository.find(DoProfile.class, permissions);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoProfile.class, uuid);
    }

    public DoProfile findByLabelAndVersion(String label, VersionOption version) {
        return BaseRepository.findByLabelAndVersion(DoProfile.class, label, version);
    }

    public DoProfile findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoProfile.class, uuid, version);
    }

    public DoProfile checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoProfile.class, uuid, label);
    }

    public void save(DoProfile doProfile) {
        BaseRepository.save(doProfile);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoProfile.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoProfile> doProfiles = criteria.list();
        for (DoProfile doProfile : doProfiles) {
            doProfile.setLabel(label);
            session.update(doProfile);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoProfile.class, uuid, version);
    }

}
