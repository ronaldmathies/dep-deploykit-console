package nl.sodeso.deploykit.console.domain.security.user;

import nl.sodeso.deploykit.console.client.application.security.user.User;
import nl.sodeso.deploykit.console.client.application.security.user.UserRole;
import nl.sodeso.deploykit.console.client.application.security.user.UserRoles;
import nl.sodeso.deploykit.console.client.application.security.user.UserSummaryItem;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.role.RoleRepository;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.util.UuidUtils;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class UserConvertor {

    private static Session session = ThreadSafeSession.getSession(Constants.PU);

    public static DoUser to(DoUser doUser, User user) {
        if (doUser == null) {
            doUser = new DoUser();
            doUser.setUuid(UuidUtils.generate());
        }

        doUser.setLabel(user.getLabel());
        doUser.setUsername(user.getUsername());
        doUser.setPassword(user.getPassword());

        handleRoles(doUser, user);

        return doUser;
    }

    private static void handleRoles(DoUser doUser, User user) {
        Iterator<DoRole> doRoleIterator = doUser.getRoles().iterator();
        while (doRoleIterator.hasNext()) {
            DoRole doRole = doRoleIterator.next();

            boolean removed = false;
            for (UserRole userRole : user.getUserRoles().getDeletedWidgets()) {
                String profileWebserviceUuid = userRole.getRole().getValue().getKey();

                if (doRole.getUuid().equals(profileWebserviceUuid)) {
                    doRoleIterator.remove();

                    // Since the DoRole is the holder of the relationship we need to remove the user from there.
                    doRole.remove(doUser);
                    session.save(doRole);

                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (UserRole userRole : user.getUserRoles().getWidgets()) {
                    String groupRoleUuid = userRole.getRole().getValue().getKey();

                    boolean found = false;
                    if (doRole.getUuid().equals(groupRoleUuid)) {
                        found = true;
                    }

                    if (!found) {
                        doRoleIterator.remove();

                        // Since the DoRole is the holder of the relationship we need to remove the user from there.
                        doRole.remove(doUser);
                        session.save(doRole);

                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (UserRole userRole : user.getUserRoles().getWidgets()) {
            String groupRoleUuid = userRole.getRole().getValue().getKey();

            boolean exists = false;
            for (DoRole doRole : doUser.getRoles()) {
                if (doRole.getUuid().equalsIgnoreCase(groupRoleUuid)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                DoRole doRole = new RoleRepository().findByUuid(groupRoleUuid);
                doUser.add(doRole);

                // Since the DoRole is the holder of the relationship we need to add the user on the role and then
                // save the role.
                session.save(doRole);
            }

        }
    }

    public static User from(DoUser doUser) {
        User user = new User();

        user.setUuid(doUser.getUuid());
        user.setLabel(doUser.getLabel());
        user.setUsername(doUser.getUsername());
        user.setPassword(doUser.getPassword());
        user.setPasswordVerify(doUser.getPassword());

        fromHandleRoles(user, doUser);

        return user;
    }

    private static void fromHandleRoles(User user, DoUser doUser) {
        UserRoles userRoles = user.getUserRoles();
        for (DoRole doRole : doUser.getRoles()) {
            UserRole userRole = new UserRole();
            userRole.setUuid(doRole.getUuid());
            userRole.setRole(doRole.getOption());
            userRoles.getWidgets().add(userRole);
        }
    }

    public static ArrayList<UserSummaryItem> toSummaries(List<DoUser> users) {
        ArrayList<UserSummaryItem> summaries = new ArrayList<>();
        for (DoUser doUser : users) {
            summaries.add(toSummary(doUser));
        }

        return summaries;
    }

    public static UserSummaryItem toSummary(DoUser doUser) {
        UserSummaryItem summary = new UserSummaryItem();
        summary.setUuid(doUser.getUuid());
        summary.setLabel(doUser.getLabel());
        return summary;
    }

}
