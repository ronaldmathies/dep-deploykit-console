package nl.sodeso.deploykit.console.domain.datasources.connectionpool;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolElasticSearchMapper implements ElasticSearchMapper<DoConnectionPool, IndexEntry> {

    @Override
    public IndexEntry map(DoConnectionPool doConnectionPool) {
        Type type = doConnectionPool instanceof DoConnectionPoolJBoss ?
                Type.JBOSSCONNECTIONPOOL : Type.JETTYCONNECTIONPOOL;

        return new IndexEntry(doConnectionPool.getId(), doConnectionPool.getUuid(), doConnectionPool.getVersion().getValue(), doConnectionPool.getLabel().getValue(), type)
                .addKeyword(doConnectionPool.getLabel().getValue())
                .addKeyword(doConnectionPool.getUuid())
                .addKeyword(doConnectionPool.getConnectionInitSqls().getValue())
                .addKeyword(doConnectionPool.getValidationQuery().getValue());
    }

}
