package nl.sodeso.deploykit.console.domain.security.user.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseEntity;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_user", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoUser extends DoBaseEntity{

    @Type(type = "string")
    @Column(name = "username", nullable = false, length = 64)
    private StringType username = null;

    @Type(type = "string")
    @Column(name = "password", nullable = false, length = 256)
    private StringType password = null;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private List<DoRole> roles = new ArrayList<>();

    public DoUser() {}

    public StringType getUsername() {
        return username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() { return password; }

    public void setPassword(StringType password) {
        this.password = password;
    }

    public List<DoRole> getRoles() {
        return roles;
    }

    public void setRoles(List<DoRole> roles) {
        this.roles = roles;
    }

    public void add(DoRole role) {
        this.roles.add(role);
        role.add(this);
    }

}
