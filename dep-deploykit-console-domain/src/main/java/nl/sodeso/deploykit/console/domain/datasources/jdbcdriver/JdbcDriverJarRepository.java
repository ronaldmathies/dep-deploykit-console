package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverJar;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverJarRepository {

    public DoJdbcDriverJar findByUuid(String uuid) {
        return BaseRepository.findByUuid(DoJdbcDriverJar.class, uuid);
    }


    public void save(DoJdbcDriverJar doJdbcDriverJar) {
        BaseRepository.save(doJdbcDriverJar);
    }

    public void remove(String uuid) {
        BaseRepository.remove(DoJdbcDriverJar.class, uuid);
    }

}
