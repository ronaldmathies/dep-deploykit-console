package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.Hibernate;
import org.hibernate.engine.jdbc.LobCreator;

import javax.persistence.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_jdbc_driver_jar", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
public class DoJdbcDriverJar implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "filename", nullable = false, length = 255)
    private String filename;

    @Column(name = "jar", nullable = false, length = 4096)
    private Blob jar;

    @Column(name = "filesize", nullable = false)
    private Integer filesize;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "jdbcDriverJars")
    private List<DoJdbcDriver> jdbcDrivers = new ArrayList<>();

    public DoJdbcDriverJar() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setJar(InputStream inputStream) {
        try {
            this.filesize = inputStream.available();

            LobCreator lobCreator = Hibernate.getLobCreator(ThreadSafeSession.getSession(Constants.PU));
            this.jar = lobCreator.createBlob(inputStream, inputStream.available());
        } catch (IOException e) {
            throw new PersistenceException("Failed to write binary stream to blob.", e);
        }
    }

    public InputStream getJar() {
        try {
            return jar.getBinaryStream();
        } catch (SQLException e) {
            throw new PersistenceException("Failed to retrieve binary stream of blob.", e);
        }
    }

    public Integer getFilesize() {
        return this.filesize;
    }

    public List<DoJdbcDriver> getJdbcDrivers() {
        return jdbcDrivers;
    }
}
