package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.client.application.security.role.*;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.deploykit.console.domain.security.user.UserRepository;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class RoleConvertor {

    public static DoRole to(DoRole doRole, Role role) {
        if (doRole == null) {
            doRole = new DoRole();
            doRole.setUuid(UuidUtils.generate());
        }

        doRole.setLabel(role.getLabel());

        handleGroups(doRole, role);
        handleUsers(doRole, role);

        return doRole;
    }

    private static void handleGroups(DoRole doRole, Role role) {
        Iterator<DoGroupPermission> doGroupPermissionIterator = doRole.getGroupPermissions().iterator();
        while (doGroupPermissionIterator.hasNext()) {
            DoGroupPermission doGroupPermission = doGroupPermissionIterator.next();

            for (RoleGroupPermission roleGroupPermission : role.getRoleGroupPermissions().getDeletedWidgets()) {
                if (doGroupPermission.getUuid().equals(roleGroupPermission.getUuid())) {
                    doGroupPermissionIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (RoleGroupPermission roleGroupPermission : role.getRoleGroupPermissions().getWidgets()) {
            boolean exists = false;
            for (DoGroupPermission doGroupPermission : doRole.getGroupPermissions()) {
                if (doGroupPermission.getUuid().equalsIgnoreCase(roleGroupPermission.getUuid())) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                DoGroupPermission doGroupPermission = new DoGroupPermission();
                doGroupPermission.setUuid(UuidUtils.generate());
                doGroupPermission.setAllowedToUse(roleGroupPermission.getAllowToUse());
                doGroupPermission.setAllowedToEdit(roleGroupPermission.getAllowToEdit());
                doGroupPermission.setAllowedToDeploy(roleGroupPermission.getAllowToDeploy());
                doGroupPermission.setAllowedToRead(roleGroupPermission.getAllowToRead());

                doGroupPermission.setGroup(new GroupRepository().findByUuid(roleGroupPermission.getGroup().getValue().getCode()));

                doRole.getGroupPermissions().add(doGroupPermission);
            }

        }
    }

    private static void handleUsers(DoRole doRole, Role role) {
        Iterator<DoUser> doUsersIterator = doRole.getUsers().iterator();
        while (doUsersIterator.hasNext()) {
            DoUser doUser = doUsersIterator.next();

            for (RoleUser roleUser : role.getRoleUsers().getDeletedWidgets()) {
                if (doUser.getUuid().equals(roleUser.getUser().getValue().getKey())) {
                    doUsersIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (RoleUser roleUser : role.getRoleUsers().getWidgets()) {
            String userUuid = roleUser.getUser().getValue().getKey();

            boolean exists = false;
            for (DoUser doUser : doRole.getUsers()) {
                if (doUser.getUuid().equalsIgnoreCase(userUuid)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doRole.getUsers().add(new UserRepository().findByUuid(userUuid));
            }

        }
    }

    public static Role from(DoRole doRole) {
        Role role = new Role();

        role.setUuid(doRole.getUuid());
        role.setLabel(doRole.getLabel());

        fromHandleGroups(role, doRole);
        fromHandleUsers(role, doRole);

        return role;
    }

    private static void fromHandleGroups(Role role, DoRole doRole) {
        RoleGroupPermissions roleGroupPermissions = role.getRoleGroupPermissions();
        for (DoGroupPermission doGroupPermission : doRole.getGroupPermissions()) {
            RoleGroupPermission roleGroupPermission = new RoleGroupPermission();

            roleGroupPermission.setUuid(doGroupPermission.getUuid());
            roleGroupPermission.setGroup(doGroupPermission.getGroup().getOption());
            roleGroupPermission.setAllowToUse(doGroupPermission.getAllowedToUse());
            roleGroupPermission.setAllowToDeploy(doGroupPermission.getAllowedToDeploy());
            roleGroupPermission.setAllowToEdit(doGroupPermission.getAllowedToEdit());
            roleGroupPermission.setAllowToRead(doGroupPermission.getAllowedToRead());

            roleGroupPermissions.getWidgets().add(roleGroupPermission);
        }
    }

    private static void fromHandleUsers(Role role, DoRole doRole) {
        RoleUsers roleUsers = role.getRoleUsers();
        for (DoUser doUser : doRole.getUsers()) {
            RoleUser roleUser = new RoleUser();
            roleUser.setUuid(doUser.getUuid());
            roleUser.setUser(doUser.getOption());
            roleUsers.getWidgets().add(roleUser);
        }
    }

    public static ArrayList<RoleSummaryItem> toSummaries(List<DoRole> roles) {
        ArrayList<RoleSummaryItem> summaries = new ArrayList<>();
        for (DoRole doRole : roles) {
            summaries.add(toSummary(doRole));
        }

        return summaries;
    }

    public static RoleSummaryItem toSummary(DoRole doRole) {
        RoleSummaryItem summary = new RoleSummaryItem();
        summary.setUuid(doRole.getUuid());
        summary.setLabel(doRole.getLabel());
        return summary;
    }

}
