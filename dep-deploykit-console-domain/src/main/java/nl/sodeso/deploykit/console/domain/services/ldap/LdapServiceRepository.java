package nl.sodeso.deploykit.console.domain.services.ldap;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class LdapServiceRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoLdapService> find(Permissions permissions) {
        return BaseRepository.find(DoLdapService.class, permissions);
    }

    public List<DoLdapService> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoLdapService.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoLdapService.class, uuid);
    }

    public DoLdapService findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoLdapService.class, uuid, version);
    }

    public DoLdapService checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoLdapService.class, uuid, label);
    }

    public void save(DoLdapService ldapService) {
        BaseRepository.save(ldapService);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoWebservice.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoLdapService> doLdapServices = criteria.list();
        for (DoLdapService doLdapService : doLdapServices) {
            doLdapService.setLabel(label);
            session.update(doLdapService);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoLdapService.class, uuid, version);
    }

}
