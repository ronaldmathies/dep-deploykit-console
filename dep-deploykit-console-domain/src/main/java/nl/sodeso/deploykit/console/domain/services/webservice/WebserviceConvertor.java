package nl.sodeso.deploykit.console.domain.services.webservice;

import nl.sodeso.deploykit.console.client.application.services.webservice.Webservice;
import nl.sodeso.deploykit.console.client.application.services.webservice.WebserviceSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperty;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebserviceProperty;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class WebserviceConvertor {

    public static DoWebservice to(DoWebservice doWebservice, Webservice webservice) {
        if (doWebservice == null) {
            doWebservice = new DoWebservice();
            doWebservice.setUuid(UuidUtils.generate());
        }

        doWebservice.setVersion(webservice.getVersion());
        doWebservice.setLabel(webservice.getLabel());
        doWebservice.setPlaceholderPrefix(webservice.getPlaceholderPrefix());
        doWebservice.setGroup(new GroupRepository().findByUuid(webservice.getGroup().getValue().getKey()));
        doWebservice.setProtocol(webservice.getProtocol());
        doWebservice.setDomain(webservice.getDomain());
        doWebservice.setPort(webservice.getPort());
        doWebservice.setContext(webservice.getContext());
        doWebservice.setPath(webservice.getPath());
        doWebservice.setTimeout(webservice.getTimeout());
        doWebservice.setUseWsAddressing(webservice.getUseWsAddressing());
        doWebservice.setFrom(webservice.getFrom());
        doWebservice.setTo(webservice.getTo());
        doWebservice.setAction(webservice.getAction());
        doWebservice.setReplyTo(webservice.getReplyTo());
        doWebservice.setFaultTo(webservice.getFaultTo());

        if (webservice.getUseWsSecurity().getValue()) {
            doWebservice.setCredential(new CredentialRepository().findByUuidAndVersion(webservice.getCredential().getValue().getKey(), webservice.getCredentialVersion().getValue()));
        } else {
            doWebservice.setCredential(null);
        }

        handleProperties(doWebservice, webservice);

        return doWebservice;
    }

    private static void handleProperties(DoWebservice doWebservice, Webservice webservice) {
        Iterator<DoWebserviceProperty> doWebservicePropertyIterator = doWebservice.getProperties().iterator();
        while (doWebservicePropertyIterator.hasNext()) {
            DoWebserviceProperty doWebserviceProperty = doWebservicePropertyIterator.next();

            for (KeyValueProperty property : webservice.getProperties().getDeletedWidgets()) {
                if (doWebserviceProperty.getUuid().equals(property.getUuid())) {
                    doWebservicePropertyIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (KeyValueProperty property : webservice.getProperties().getWidgets()) {
            String webserviceConnectionPropertyUuid = property.getUuid();

            boolean exists = false;
            for (DoWebserviceProperty doWebserviceProperty : doWebservice.getProperties()) {
                if (doWebserviceProperty.getUuid().equalsIgnoreCase(webserviceConnectionPropertyUuid)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                DoWebserviceProperty doWebserviceProperty = new DoWebserviceProperty();
                doWebserviceProperty.setUuid(UuidUtils.generate());
                doWebserviceProperty.setKey(property.getKey());
                doWebserviceProperty.setValue(property.getValue());
                doWebservice.getProperties().add(doWebserviceProperty);
            }

        }
    }

    public static Webservice from(DoWebservice doWebservice) {
        Webservice webservice = new Webservice();

        webservice.setUuid(doWebservice.getUuid());
        webservice.setVersion(doWebservice.getVersion());
        webservice.setLabel(doWebservice.getLabel());
        webservice.setPlaceholderPrefix(doWebservice.getPlaceholderPrefix());
        webservice.setGroup(doWebservice.getGroup().getOption());

        webservice.setProtocol(doWebservice.getProtocol());
        webservice.setDomain(doWebservice.getDomain());
        webservice.setPort(doWebservice.getPort());
        webservice.setContext(doWebservice.getContext());
        webservice.setPath(doWebservice.getPath());
        webservice.setTimeout(doWebservice.getTimeout());
        webservice.setUseWsAddressing(doWebservice.getUseWsAddressing());
        webservice.setFrom(doWebservice.getFrom());
        webservice.setTo(doWebservice.getTo());
        webservice.setAction(doWebservice.getAction());
        webservice.setReplyTo(doWebservice.getReplyTo());
        webservice.setFaultTo(doWebservice.getFaultTo());

        if (doWebservice.getCredential() != null) {
            webservice.setCredential(doWebservice.getCredential().getOption());
            webservice.setCredentialVersion(doWebservice.getCredential().getVersion());
            webservice.setUseWsSecurity(new BooleanType(true));
        }

        fromHandleProperties(webservice, doWebservice);

        return webservice;
    }

    private static void fromHandleProperties(Webservice webservice, DoWebservice doWebservice) {
        KeyValueProperties properties = webservice.getProperties();
        for (DoWebserviceProperty doWebserviceProperty : doWebservice.getProperties()) {
            KeyValueProperty property = new KeyValueProperty();
            property.setUuid(doWebservice.getUuid());
            property.setKey(doWebserviceProperty.getKey());
            property.setValue(doWebserviceProperty.getValue());
            properties.getWidgets().add(property);
        }
    }

    public static ArrayList<WebserviceSummaryItem> toSummaries(List<DoWebservice> webservices) {
        ArrayList<WebserviceSummaryItem> summaries = new ArrayList<>();
        for (DoWebservice doWebservice : webservices) {
            summaries.add(toSummary(doWebservice));
        }

        return summaries;
    }

    public static WebserviceSummaryItem toSummary(DoWebservice doWebservice) {
        WebserviceSummaryItem summary = new WebserviceSummaryItem();
        summary.setUuid(doWebservice.getUuid());
        summary.setLabel(doWebservice.getLabel());
        return summary;
    }
}
