package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class HttpsRepository {

    private static final Logger LOG = Logger.getLogger(HttpsRepository.class.getName());

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoHttps> find(Permissions permissions) {
        return BaseRepository.find(DoHttps.class, permissions);
    }

    public List<DoHttps> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoHttps.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoHttps.class, uuid);
    }

    public DoHttps findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoHttps.class, uuid, version);
    }

    public DoHttps checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoHttps.class, uuid, label);
    }

    public void save(DoHttps doHttps) {
        BaseRepository.save(doHttps);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoHttps.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoHttps> doHttpss = criteria.list();
        for (DoHttps doHttps : doHttpss) {
            doHttps.setLabel(label);
            session.update(doHttps);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoHttps.class, uuid, version);
    }

}
