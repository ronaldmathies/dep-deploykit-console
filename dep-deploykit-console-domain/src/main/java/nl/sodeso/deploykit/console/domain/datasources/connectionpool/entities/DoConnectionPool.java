package nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@DiscriminatorColumn(name = "dtype")
@Table(name = "t_connection_pool",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@TypeDefs(
    value = {
        @TypeDef(name = "option", typeClass = OptionCompositeUserType.class),
        @TypeDef(name = "string", typeClass = StringTypeUserType.class)
    }
)
public class DoConnectionPool extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "validation_query", length = 2048)
    private StringType validationQuery = null;

    @Type(type = "string")
    @Column(name = "connection_init_sqls", length = 2048)
    private StringType connectionInitSqls = null;

    @Type(type = "option")
    @Columns(columns = {
            @Column(name = "dti_key"),
            @Column(name = "dti_code"),
            @Column(name = "dti_description")
    })
    private OptionType<DefaultOption> defaultTransactionIsolation = null;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_connection_pool_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "connectionPools")
    private List<DoDatasource> datasources = new ArrayList<>();

    public DoConnectionPool() {}

    public DoGroup getGroup() {
        return group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public List<DoDatasource> getDatasources() {
        return this.datasources;
    }

    public StringType getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(StringType validationQuery) {
        this.validationQuery = validationQuery;
    }

    public StringType getConnectionInitSqls() {
        return connectionInitSqls;
    }

    public void setConnectionInitSqls(StringType connectionInitSqls) {
        this.connectionInitSqls = connectionInitSqls;
    }

    public OptionType<DefaultOption> getDefaultTransactionIsolation() {
        return defaultTransactionIsolation;
    }

    public void setDefaultTransactionIsolation(OptionType<DefaultOption> defaultTransactionIsolation) {
        this.defaultTransactionIsolation = defaultTransactionIsolation;
    }
}
