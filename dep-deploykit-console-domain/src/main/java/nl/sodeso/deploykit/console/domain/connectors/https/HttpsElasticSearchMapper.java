package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class HttpsElasticSearchMapper implements ElasticSearchMapper<DoHttps, IndexEntry> {

    @Override
    public IndexEntry map(DoHttps doHttps) {
        return new IndexEntry(doHttps.getId(), doHttps.getUuid(), doHttps.getVersion().getValue(), doHttps.getLabel().getValue(), Type.HTTPS)
                .addKeyword(doHttps.getLabel().getValue())
                .addKeyword(doHttps.getUuid());
    }

}
