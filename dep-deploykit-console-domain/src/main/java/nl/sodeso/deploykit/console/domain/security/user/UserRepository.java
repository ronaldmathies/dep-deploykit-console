package nl.sodeso.deploykit.console.domain.security.user;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.deploykit.console.domain.security.user.entities.DoUser;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class UserRepository {

    private static Session session = ThreadSafeSession.getSession(Constants.PU);

    private Logger LOG = Logger.getLogger(getClass().getName());

    public List<DoUser> find() {
        return BaseRepository.find(DoUser.class);
    }

    public DoUser findByUuid(String uuid) {
        return BaseRepository.findByUuid(DoUser.class, uuid);
    }

    public DoUser findByLabel(String label) {
        return BaseRepository.findByLabel(DoUser.class, label);
    }

    public DoUser findByUsername(String username) {
        LOG.log(Level.INFO, String.format("Finding 'DoUser' by username '%s'.", username));

        Criteria criteria = session.createCriteria(DoUser.class)
                .add(Restrictions.eq("username", new StringType(username)));

        long start = System.currentTimeMillis();
        DoUser doUser = (DoUser)criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Finding 'DoUSer' by username '%s' in %dms..", username, System.currentTimeMillis() - start));

        return doUser;
    }

    public boolean isAllowedTo(Permissions permissions, String forGroupUuid) {
        DoUser doUser = new UserRepository().findByUsername(permissions.getUsername());
        for (DoRole doRole : doUser.getRoles()) {
            for (DoGroupPermission doGroupPermission : doRole.getGroupPermissions()) {
                if (doGroupPermission.getGroup().getUuid().equals(forGroupUuid)) {

                    if ((permissions.isPermissionRequired(Permissions.READ) &&
                        !doGroupPermission.getAllowedToRead().getValue()) ||
                        (permissions.isPermissionRequired(Permissions.USE) &&
                        !doGroupPermission.getAllowedToUse().getValue()) ||
                        (permissions.isPermissionRequired(Permissions.EDIT) &&
                        !doGroupPermission.getAllowedToEdit().getValue()) ||
                        (permissions.isPermissionRequired(Permissions.DEPLOY) &&
                        !doGroupPermission.getAllowedToDeploy().getValue())) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public void save(DoUser doUser) {
        BaseRepository.save(doUser);
    }

    public void remove(String uuid) {
        DoUser doUser = BaseRepository.findByUuid(DoUser.class, uuid);

        for (DoRole doRole : doUser.getRoles()) {
            doRole.remove(doUser);
            session.save(doRole);
        }

        BaseRepository.remove(DoUser.class, uuid);
    }

}
