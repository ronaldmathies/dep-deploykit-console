package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_jdbc_driver",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    }
)
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoJdbcDriver extends DoBaseVersionedEntity {

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_jdbc_driver_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "jdbcDriver")
    private List<DoDatasource> datasources = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_jdbc_driver_jdbc_driver_jar", joinColumns = {
            @JoinColumn(name = "jdbc_driver_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_jdbc_driver_jdbc_driver_jar_to_jdbc_driver")) },
            inverseJoinColumns = { @JoinColumn(name = "jdbc_driver_jar_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_jdbc_driver_jdbc_driver_jar_to_jdbc_driver_jar")) })
    private List<DoJdbcDriverJar> jdbcDriverJars = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "t_jdbc_driver_jdbc_driver_class", joinColumns = {
            @JoinColumn(name = "jdbc_driver_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_jdbc_driver_jdbc_driver_class_to_jdbc_driver")) },
            inverseJoinColumns = { @JoinColumn(name = "jdbc_driver_class_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_jdbc_driver_jdbc_driver_class_to_jdbc_driver_class")) })
    private List<DoJdbcDriverClass> jdbcDriverClasses = new ArrayList<>();

    @JoinColumn(name = "jdbc_driver_class_id", foreignKey = @ForeignKey(name = "fk_jdbc_driver_to_jdbc_driver_class"))
    @OneToOne(cascade = CascadeType.DETACH, orphanRemoval = false, optional = true)
    private DoJdbcDriverClass jdbcDriverClass = null;

    public DoJdbcDriver() {}

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public List<DoDatasource> getDatasources() {
        return this.datasources;
    }

    public List<DoJdbcDriverJar> getJdbcDriverJars() {
        return this.jdbcDriverJars;
    }

    public List<DoJdbcDriverClass> getJdbcDriverClasses() {
        return this.jdbcDriverClasses;
    }

    public DoJdbcDriverClass getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    public void setJdbcDriverClass(DoJdbcDriverClass jdbcDriverClass) {
        this.jdbcDriverClass = jdbcDriverClass;
    }
}
