package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControlSummaryItem;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.DatasourceAccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.LdapAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class AccessControlConvertor {

    public static DoAccessControl to(DoAccessControl doAccessControl, AccessControl accessControl) {
        boolean isLdap = accessControl.getAccessControlType().getValue().getKey().equals("ldap");

        if (doAccessControl == null) {
            if (isLdap) {
                doAccessControl = new DoAccessControlLdap();
            } else {
                doAccessControl = new DoAccessControlDatasource();
            }

            doAccessControl.setUuid(UuidUtils.generate());
        }

        doAccessControl.setGroup(new GroupRepository().findByUuid(accessControl.getGroup().getValue().getKey()));
        doAccessControl.setLabel(accessControl.getLabel());
        doAccessControl.setVersion(accessControl.getVersion());
        doAccessControl.setPlaceholderPrefix(accessControl.getPlaceholderPrefix());

        doAccessControl.setRealm(accessControl.getRealm());

        if (isLdap) {
            AccessControlLdapConvertor.to((DoAccessControlLdap)doAccessControl, accessControl);
        } else {
            AccessControlDatasourceConvertor.to((DoAccessControlDatasource)doAccessControl, accessControl);
        }

        return doAccessControl;
    }

    public static AccessControl from(DoAccessControl doAccessControl) {
        AccessControl accessControl = new AccessControl();

        accessControl.setUuid(doAccessControl.getUuid());
        accessControl.setGroup(doAccessControl.getGroup().getOption());
        accessControl.setLabel(doAccessControl.getLabel());
        accessControl.setVersion(doAccessControl.getVersion());
        accessControl.setPlaceholderPrefix(doAccessControl.getPlaceholderPrefix());

        accessControl.setRealm(doAccessControl.getRealm());

        if (doAccessControl instanceof DoAccessControlLdap) {
            accessControl.setAccessControlType(new OptionType<>(new DefaultOption("ldap", "ldap", "LDAP")));

            LdapAccessControl ldapAccessControl =
                    AccessControlLdapConvertor.from((DoAccessControlLdap) doAccessControl);
            accessControl.setLdapAccessControl(ldapAccessControl);
        } else if (doAccessControl instanceof DoAccessControlDatasource) {
            accessControl.setAccessControlType(new OptionType<>(new DefaultOption("database", "database", "Database")));

            DatasourceAccessControl datasourceAccessControl =
                    AccessControlDatasourceConvertor.from((DoAccessControlDatasource) doAccessControl);
            accessControl.setDatasourceAccessControl(datasourceAccessControl);
        }

        return accessControl;
    }

    public static ArrayList<AccessControlSummaryItem> toSummaries(List<DoAccessControl> authentications) {
        ArrayList<AccessControlSummaryItem> summaries = new ArrayList<>();
        for (DoAccessControl doAccessAccessControl : authentications) {
            summaries.add(toSummary(doAccessAccessControl));
        }

        return summaries;
    }

    public static AccessControlSummaryItem toSummary(DoAccessControl doAccessControl) {
        AccessControlSummaryItem summary = new AccessControlSummaryItem();
        summary.setUuid(doAccessControl.getUuid());
        summary.setLabel(doAccessControl.getLabel());
        return summary;
    }
}
