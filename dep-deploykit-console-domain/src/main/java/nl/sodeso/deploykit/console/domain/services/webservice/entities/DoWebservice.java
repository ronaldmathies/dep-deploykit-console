package nl.sodeso.deploykit.console.domain.services.webservice.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.webservice.WebserviceElasticSearchMapper;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
    mapper = WebserviceElasticSearchMapper.class,
    mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "webserviceidx")
@Entity
@Table(name = "t_webservice",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@TypeDefs(
    value = {
        @TypeDef(name = "option", typeClass = OptionCompositeUserType.class),
        @TypeDef(name = "string", typeClass = StringTypeUserType.class),
        @TypeDef(name = "int", typeClass = IntTypeUserType.class),
        @TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)
    }
)
public class DoWebservice extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "placeholder_prefix", nullable = false, length = 255)
    private StringType placeholderPrefix;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_webservice_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group;

    @Type(type = "option")
    @Columns(columns = {
            @Column(name = "protocol_key"),
            @Column(name = "protocol_code"),
            @Column(name = "protocol_description")
    })
    private OptionType<DefaultOption> protocol = null;

    @Type(type = "string")
    @Column(name = "domain", nullable = false, length = 255)
    private StringType domain = null;

    @Type(type = "string")
    @Column(name = "context", nullable = true, length = 255)
    private StringType context = null;

    @Type(type = "string")
    @Column(name = "path", nullable = true, length = 255)
    private StringType path = null;

    @Type(type = "int")
    @Column(name = "port")
    private IntType port = null;

    @Type(type = "int")
    @Column(name = "timeout")
    private IntType timeout = null;

    @Type(type = "boolean")
    @Column(name = "use_ws_addressing")
    private BooleanType useWsAddressing;

    @Type(type = "string")
    @Column(name = "wsa_from", nullable = true, length = 255)
    private StringType from = null;

    @Type(type = "string")
    @Column(name = "wsa_to", nullable = true, length = 255)
    private StringType to = null;

    @Type(type = "string")
    @Column(name = "wsa_action", nullable = true, length = 255)
    private StringType action = null;

    @Type(type = "string")
    @Column(name = "wsa_reply_to", nullable = true, length = 255)
    private StringType replyTo = null;

    @Type(type = "string")
    @Column(name = "wsa_fault_to", nullable = true, length = 255)
    private StringType faultTo = null;

    @JoinColumn(name="credential_id", foreignKey = @ForeignKey(name = "fk_webservice_to_credential"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoCredential credential;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "webservices")
    private List<DoProfile> profiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "webservice")
    private List<DoWebserviceProperty> properties = new ArrayList<>();

    public DoWebservice() {}

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public OptionType<DefaultOption> getProtocol() {
        return protocol;
    }

    public void setProtocol(OptionType<DefaultOption> protocol) {
        this.protocol = protocol;
    }

    public StringType getDomain() {
        return domain;
    }

    public void setDomain(StringType domain) {
        this.domain = domain;
    }

    public StringType getContext() {
        return context;
    }

    public void setContext(StringType context) {
        this.context = context;
    }

    public StringType getPath() {
        return path;
    }

    public void setPath(StringType path) {
        this.path = path;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

    public BooleanType getUseWsAddressing() {
        return useWsAddressing;
    }

    public void setUseWsAddressing(BooleanType useWsAddressing) {
        this.useWsAddressing = useWsAddressing;
    }

    public StringType getFrom() {
        return from;
    }

    public void setFrom(StringType from) {
        this.from = from;
    }

    public StringType getTo() {
        return to;
    }

    public void setTo(StringType to) {
        this.to = to;
    }

    public StringType getAction() {
        return action;
    }

    public void setAction(StringType action) {
        this.action = action;
    }

    public StringType getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(StringType replyTo) {
        this.replyTo = replyTo;
    }

    public StringType getFaultTo() {
        return faultTo;
    }

    public void setFaultTo(StringType faultTo) {
        this.faultTo = faultTo;
    }

    public DoCredential getCredential() {
        return credential;
    }

    public void setCredential(DoCredential credential) {
        this.credential = credential;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public List<DoWebserviceProperty> getProperties() {
        return this.properties;
    }
}
