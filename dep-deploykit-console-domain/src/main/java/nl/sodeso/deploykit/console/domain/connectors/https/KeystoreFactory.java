package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * @author Ronald Mathies
 */
public class KeystoreFactory {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public DoKeystore findByUuid(String uuid) {
        Criteria criteria = session.createCriteria(DoKeystore.class)
                .add(Restrictions.eq("uuid", uuid));
        return (DoKeystore)criteria.uniqueResult();
    }


    public void save(DoKeystore doKeystore) {
        session.save(doKeystore);
    }

    public void remove(String uuid) {
        Criteria criteria = session.createCriteria(DoKeystore.class)
                .add(Restrictions.eq("uuid", uuid));
        DoKeystore doKeystore = (DoKeystore)criteria.uniqueResult();
        session.delete(doKeystore);
    }
}
