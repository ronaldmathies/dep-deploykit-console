package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.LdapAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.services.ldap.LdapServiceRepository;

/**
 * @author Ronald Mathies
 */
public class AccessControlLdapConvertor {

    protected static void to(DoAccessControlLdap doAccessControlLdap, AccessControl accessControl) {
        LdapAccessControl ldapAccessControl = accessControl.getLdapAccessControl();

        doAccessControlLdap.setUserBaseDn(ldapAccessControl.getUserBaseDn());
        doAccessControlLdap.setUserAttribute(ldapAccessControl.getUserAttribute());
        doAccessControlLdap.setRoleBaseDn(ldapAccessControl.getRoleBaseDn());
        doAccessControlLdap.setRoleMemberAttribute(ldapAccessControl.getRoleMemberAttribute());
        doAccessControlLdap.setRoleNameAttribute(ldapAccessControl.getRoleNameAttribute());
        doAccessControlLdap.setUserPasswordAttribute(ldapAccessControl.getUserPasswordAttribute());
        doAccessControlLdap.setUserObjectClass(ldapAccessControl.getUserObjectClass());
        doAccessControlLdap.setRoleObjectClass(ldapAccessControl.getRoleObjectClass());
        doAccessControlLdap.setLdapService(new LdapServiceRepository().findByUuidAndVersion(ldapAccessControl.getLdapService().getValue().getKey(), ldapAccessControl.getLdapServiceVersion().getValue()));
    }

    protected static LdapAccessControl from(DoAccessControlLdap doAccessControlLdap) {
        LdapAccessControl accessControl = new LdapAccessControl();

        accessControl.setUserBaseDn(doAccessControlLdap.getUserBaseDn());
        accessControl.setUserAttribute(doAccessControlLdap.getUserAttribute());
        accessControl.setRoleBaseDn(doAccessControlLdap.getRoleBaseDn());
        accessControl.setRoleMemberAttribute(doAccessControlLdap.getRoleMemberAttribute());
        accessControl.setRoleNameAttribute(doAccessControlLdap.getRoleNameAttribute());
        accessControl.setUserPasswordAttribute(doAccessControlLdap.getUserPasswordAttribute());
        accessControl.setUserObjectClass(doAccessControlLdap.getUserObjectClass());
        accessControl.setRoleObjectClass(doAccessControlLdap.getRoleObjectClass());
        accessControl.setLdapService(doAccessControlLdap.getLdapService().getOption());
        accessControl.setLdapServiceVersion(doAccessControlLdap.getLdapService().getVersion());

        return accessControl;
    }

}
