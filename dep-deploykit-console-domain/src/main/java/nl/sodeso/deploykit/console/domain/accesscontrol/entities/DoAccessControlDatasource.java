package nl.sodeso.deploykit.console.domain.accesscontrol.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.accesscontrol.AccessControlDatasourceElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = AccessControlDatasourceElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "dsaccesscontrolidx")
@Entity
@DiscriminatorValue(value = "datasource")
@TypeDefs(
        value = {
                @TypeDef(name = "string", typeClass = StringTypeUserType.class)
        }
)
public class DoAccessControlDatasource extends DoAccessControl {

    @JoinColumn(name="datasource_id", foreignKey = @ForeignKey(name = "fk_access_control_to_datasource"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoDatasource datasource = null;

    @Type(type = "string")
    @Column(name = "user_table", nullable = false, length = 32)
    private StringType userTable = null;

    @Type(type = "string")
    @Column(name = "user_column", nullable = false, length = 32)
    private StringType userColumn = null;

    @Type(type = "string")
    @Column(name = "credential_column", nullable = false, length = 32)
    private StringType credentialColumn = null;

    @Type(type = "string")
    @Column(name = "role_table", nullable = false, length = 32)
    private StringType roleTable = null;

    @Type(type = "string")
    @Column(name = "role_user_column", nullable = false, length = 32)
    private StringType roleUserColumn = null;

    @Type(type = "string")
    @Column(name = "role_role_field", nullable = false, length = 32)
    private StringType roleRoleColumn = null;

    public DoAccessControlDatasource() {}

    public DoDatasource getDatasource() {
        return datasource;
    }

    public void setDatasource(DoDatasource datasource) {
        this.datasource = datasource;
    }

    public StringType getUserTable() {
        return userTable;
    }

    public void setUserTable(StringType userTable) {
        this.userTable = userTable;
    }

    public StringType getUserColumn() {
        return userColumn;
    }

    public void setUserColumn(StringType userField) {
        this.userColumn = userField;
    }

    public StringType getCredentialColumn() {
        return credentialColumn;
    }

    public void setCredentialColumn(StringType credentialColumn) {
        this.credentialColumn = credentialColumn;
    }

    public StringType getRoleTable() {
        return roleTable;
    }

    public void setRoleTable(StringType roleTable) {
        this.roleTable = roleTable;
    }

    public StringType getRoleUserColumn() {
        return roleUserColumn;
    }

    public void setRoleUserColumn(StringType roleUserField) {
        this.roleUserColumn = roleUserField;
    }

    public StringType getRoleRoleColumn() {
        return roleRoleColumn;
    }

    public void setRoleRoleColumn(StringType roleRoleField) {
        this.roleRoleColumn = roleRoleField;
    }
}
