package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoGroupPermission;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class GroupPermissionRepository {

    public void save(DoGroupPermission groupPermission) {
        BaseRepository.save(groupPermission);
    }

    public void remove(String uuid) {
        BaseRepository.remove(DoGroupPermission.class, uuid);
    }

}
