package nl.sodeso.deploykit.console.domain.node.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_node", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
@TypeDefs(
        value = {
            @TypeDef(name = "string", typeClass = StringTypeUserType.class),
            @TypeDef(name = "int", typeClass = IntTypeUserType.class)
        }
)
public class DoNode implements HasUuid {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Type(type = "string")
    @Column(name = "label", nullable = false, length = 255)
    private StringType label;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_node_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group;

    @Type(type = "string")
    @Column(name = "hostname", length = 255, nullable = false)
    private StringType hostname;

    @Type(type = "int")
    @Column(name = "port", nullable = false)
    private IntType port;

    @Type(type = "string")
    @Column(name = "username", nullable = false)
    private StringType username;

    @Type(type = "string")
    @Column(name = "password", nullable = false)
    private StringType password;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "nodes")
    private List<DoProfile> profiles = new ArrayList<DoProfile>();

    public DoNode() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StringType getLabel() {
        return label;
    }

    public void setLabel(StringType label) {
        this.label = label;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public StringType getHostname() {
        return hostname;
    }

    public void setHostname(StringType hostname) {
        this.hostname = hostname;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public StringType getUsername() {
        return username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() {
        return password;
    }

    public void setPassword(StringType password) {
        this.password = password;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public OptionType<DefaultOption> getOption() {
        return new OptionType<>(new DefaultOption(uuid, label.getValue().toLowerCase(), label.getValue()));
    }
}
