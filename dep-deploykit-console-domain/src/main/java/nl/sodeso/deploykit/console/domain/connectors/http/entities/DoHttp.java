package nl.sodeso.deploykit.console.domain.connectors.http.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.connectors.http.HttpElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = HttpElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "httpidx")
@Entity
@Table(name = "t_http",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
            @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@TypeDefs(
    value = {
            @TypeDef(name = "string", typeClass = StringTypeUserType.class),
            @TypeDef(name = "int", typeClass = IntTypeUserType.class)
    }
)
public class DoHttp extends DoBaseVersionedEntity {

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_http_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group = null;

    @Type(type = "string")
    @Column(name = "host", nullable = true, length = 255)
    private StringType host = null;

    @Type(type="int")
    @Column(name = "port", nullable = false)
    private IntType port = null;

    @Type(type="int")
    @Column(name = "timeout", nullable = false)
    private IntType timeout = null;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "http", fetch = FetchType.LAZY)
    private List<DoProfile> profiles = new ArrayList<>();

    public DoHttp() {}

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public StringType getHost() {
        return host;
    }

    public void setHost(StringType host) {
        this.host = host;
    }

    public IntType getPort() {
        return port;
    }

    public void setPort(IntType port) {
        this.port = port;
    }

    public IntType getTimeout() {
        return timeout;
    }

    public void setTimeout(IntType timeout) {
        this.timeout = timeout;
    }

}
