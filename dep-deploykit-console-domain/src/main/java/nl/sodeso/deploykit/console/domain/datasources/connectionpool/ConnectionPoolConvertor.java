package nl.sodeso.deploykit.console.domain.datasources.connectionpool;

import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.ConnectionpoolSummaryItem;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.JBossConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.connectionpool.JettyConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ConnectionPoolConvertor {

    public static DoConnectionPool to(DoConnectionPool doConnectionPool, ConnectionPool connectionPool) {
        boolean isJetty = connectionPool.getJavaServerProvider().getValue().getKey().equals("jetty");

        if (doConnectionPool == null) {
            doConnectionPool = isJetty ? new DoConnectionPoolJetty() : new DoConnectionPoolJBoss();
            doConnectionPool.setUuid(UuidUtils.generate());
        }

        if (isJetty) {
            to((DoConnectionPoolJetty)doConnectionPool, connectionPool.getJettyConnectionPool());
        } else {
            to((DoConnectionPoolJBoss)doConnectionPool, connectionPool.getJBossConnectionPool());
        }

        doConnectionPool.setLabel(connectionPool.getLabel());
        doConnectionPool.setVersion(connectionPool.getVersion());
        doConnectionPool.setGroup(new GroupRepository().findByUuid(connectionPool.getGroup().getValue().getKey()));

        return doConnectionPool;
    }

    private static void to(DoConnectionPoolJetty doConnectionPool, JettyConnectionPool connectionPool) {
        doConnectionPool.setInitialSize(connectionPool.getInitialSize());
        doConnectionPool.setMaxTotal(connectionPool.getMaxTotal());
        doConnectionPool.setMaxIdle(connectionPool.getMaxIdle());
        doConnectionPool.setMinIdle(connectionPool.getMinIdle());
        doConnectionPool.setMaxWaitMillis(connectionPool.getMaxWaitMillis());
        doConnectionPool.setValidationQuery(connectionPool.getValidationQuery());
        doConnectionPool.setTestOnCreate(connectionPool.getTestOnCreate());
        doConnectionPool.setTestOnBorrow(connectionPool.getTestOnBorrow());
        doConnectionPool.setTestOnReturn(connectionPool.getTestOnReturn());
        doConnectionPool.setTestWhileIdle(connectionPool.getTestWhileIdle());
        doConnectionPool.setTimeBetweenEvictionRunsMillis(connectionPool.getTimeBetweenEvictionRunsMillis());
        doConnectionPool.setNumTestsPerEvictionRun(connectionPool.getNumTestsPerEvictionRun());
        doConnectionPool.setMinEvictableIdleTimeMillis(connectionPool.getMinEvictableIdleTimeMillis());
        doConnectionPool.setSoftMiniEvictableIdleTimeMillis(connectionPool.getSoftMiniEvictableIdleTimeMillis());
        doConnectionPool.setMaxConnLifetimeMillis(connectionPool.getMaxConnLifetimeMillis());
        doConnectionPool.setLogExpiredConnections(connectionPool.getLogExpiredConnections());
        doConnectionPool.setConnectionInitSqls(connectionPool.getConnectionInitSqls());
        doConnectionPool.setLifo(connectionPool.getLifo());
        doConnectionPool.setDefaultAutoCommit(connectionPool.getDefaultAutoCommit());
        doConnectionPool.setDefaultReadOnly(connectionPool.getDefaultReadOnly());
        doConnectionPool.setDefaultTransactionIsolation(connectionPool.getDefaultTransactionIsolation());
        doConnectionPool.setDefaultCatalog(connectionPool.getDefaultCatalog());
        doConnectionPool.setCacheState(connectionPool.getCacheState());
    }

    private static void to(DoConnectionPoolJBoss doConnectionPool, JBossConnectionPool connectionPool) {
        doConnectionPool.setMinPoolSize(connectionPool.getMinPoolSize());
        doConnectionPool.setMaxPoolSize(connectionPool.getMaxPoolSize());
        doConnectionPool.setInitialPoolSize(connectionPool.getInitialPoolSize());
        doConnectionPool.setPrefill(connectionPool.getPrefill());

        doConnectionPool.setBlockingTimeoutMillis(connectionPool.getBlockingTimeoutMillis());
        doConnectionPool.setIdleTimeoutMinutes(connectionPool.getIdleTimeoutMinutes());
        doConnectionPool.setAllocationRetry(connectionPool.getAllocationRetry());
        doConnectionPool.setAllocationRetryWaitMillis(connectionPool.getAllocationRetryWaitMillis());
        doConnectionPool.setConnectionInitSqls(connectionPool.getConnectionInitSqls());
        doConnectionPool.setValidationQuery(connectionPool.getValidationQuery());
        doConnectionPool.setDefaultTransactionIsolation(connectionPool.getDefaultTransactionIsolation());
    }

    public static ConnectionPool from(DoConnectionPool doConnectionPool) {
        ConnectionPool connectionPool = new ConnectionPool();

        connectionPool.setUuid(doConnectionPool.getUuid());
        connectionPool.setGroup(doConnectionPool.getGroup().getOption());
        connectionPool.setLabel(doConnectionPool.getLabel());
        connectionPool.setVersion(doConnectionPool.getVersion());

        if (doConnectionPool instanceof DoConnectionPoolJetty) {
            connectionPool.setJavaServerProvider(new OptionType<>(new DefaultOption("jetty", "jetty", "Jetty")));
            connectionPool.setJettyConnectionPool(from((DoConnectionPoolJetty)doConnectionPool));
        } else {
            connectionPool.setJavaServerProvider(new OptionType<>(new DefaultOption("jboss", "jboss", "JBoss")));
            connectionPool.setJBossConnectionPool(from((DoConnectionPoolJBoss)doConnectionPool));
        }

        return connectionPool;
    }

    public static JBossConnectionPool from(DoConnectionPoolJBoss doConnectionPool) {
        JBossConnectionPool connectionPool = new JBossConnectionPool();

        connectionPool.setMinPoolSize(doConnectionPool.getMinPoolSize());
        connectionPool.setMaxPoolSize(doConnectionPool.getMaxPoolSize());
        connectionPool.setInitialPoolSize(doConnectionPool.getInitialPoolSize());
        connectionPool.setPrefill(doConnectionPool.getPrefill());

        connectionPool.setBlockingTimeoutMillis(doConnectionPool.getBlockingTimeoutMillis());
        connectionPool.setIdleTimeoutMinutes(doConnectionPool.getIdleTimeoutMinutes());
        connectionPool.setAllocationRetry(doConnectionPool.getAllocationRetry());
        connectionPool.setAllocationRetryWaitMillis(doConnectionPool.getAllocationRetryWaitMillis());
        connectionPool.setConnectionInitSqls(doConnectionPool.getConnectionInitSqls());
        connectionPool.setValidationQuery(doConnectionPool.getValidationQuery());
        connectionPool.setDefaultTransactionIsolation(doConnectionPool.getDefaultTransactionIsolation());

        return connectionPool;
    }

    public static JettyConnectionPool from(DoConnectionPoolJetty doConnectionPool) {
        JettyConnectionPool connectionPool = new JettyConnectionPool();

        connectionPool.setInitialSize(doConnectionPool.getInitialSize());
        connectionPool.setMaxTotal(doConnectionPool.getMaxTotal());
        connectionPool.setMaxIdle(doConnectionPool.getMaxIdle());
        connectionPool.setMinIdle(doConnectionPool.getMinIdle());
        connectionPool.setMaxWaitMillis(doConnectionPool.getMaxWaitMillis());
        connectionPool.setValidationQuery(doConnectionPool.getValidationQuery());
        connectionPool.setTestOnCreate(doConnectionPool.getTestOnCreate());
        connectionPool.setTestOnBorrow(doConnectionPool.getTestOnBorrow());
        connectionPool.setTestOnReturn(doConnectionPool.getTestOnReturn());
        connectionPool.setTestWhileIdle(doConnectionPool.getTestWhileIdle());
        connectionPool.setTimeBetweenEvictionRunsMillis(doConnectionPool.getTimeBetweenEvictionRunsMillis());
        connectionPool.setNumTestsPerEvictionRun(doConnectionPool.getNumTestsPerEvictionRun());
        connectionPool.setMinEvictableIdleTimeMillis(doConnectionPool.getMinEvictableIdleTimeMillis());
        connectionPool.setSoftMiniEvictableIdleTimeMillis(doConnectionPool.getSoftMiniEvictableIdleTimeMillis());
        connectionPool.setMaxConnLifetimeMillis(doConnectionPool.getMaxConnLifetimeMillis());
        connectionPool.setLogExpiredConnections(doConnectionPool.getLogExpiredConnections());
        connectionPool.setConnectionInitSqls(doConnectionPool.getConnectionInitSqls());
        connectionPool.setLifo(doConnectionPool.getLifo());
        connectionPool.setDefaultAutoCommit(doConnectionPool.getDefaultAutoCommit());
        connectionPool.setDefaultReadOnly(doConnectionPool.getDefaultReadOnly());
        connectionPool.setDefaultTransactionIsolation(doConnectionPool.getDefaultTransactionIsolation());
        connectionPool.setDefaultCatalog(doConnectionPool.getDefaultCatalog());
        connectionPool.setCacheState(doConnectionPool.getCacheState());

        return connectionPool;
    }



    public static ArrayList<ConnectionpoolSummaryItem> toSummaries(List<DoConnectionPool> connectionPools) {
        ArrayList<ConnectionpoolSummaryItem> summaries = new ArrayList<>();
        for (DoConnectionPool doConnectionPool : connectionPools) {
            summaries.add(toSummary(doConnectionPool));
        }

        return summaries;
    }

    public static ConnectionpoolSummaryItem toSummary(DoConnectionPool doConnectionPool) {
        ConnectionpoolSummaryItem summary = new ConnectionpoolSummaryItem();
        summary.setUuid(doConnectionPool.getUuid());
        summary.setLabel(doConnectionPool.getLabel());
        return summary;
    }
}
