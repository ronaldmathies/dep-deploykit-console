package nl.sodeso.deploykit.console.elasticsearch;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJBoss;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPoolJetty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class IndexedClasses {

    private static Map<Type, Class> indexedEntities;

    static {
        indexedEntities = new HashMap<>();
        indexedEntities.put(Type.HTTP, DoHttp.class);
        indexedEntities.put(Type.HTTPS, DoHttps.class);
        indexedEntities.put(Type.WEBSERVICE, DoWebservice.class);
        indexedEntities.put(Type.JBOSSCONNECTIONPOOL, DoConnectionPoolJBoss.class);
        indexedEntities.put(Type.JETTYCONNECTIONPOOL, DoConnectionPoolJetty.class);
        indexedEntities.put(Type.DATASOURCE, DoDatasource.class);
        indexedEntities.put(Type.LDAP_SERVICE, DoLdapService.class);
        indexedEntities.put(Type.LDAP_ACCESS_CONTROL, DoAccessControlLdap.class);
        indexedEntities.put(Type.DATASOURCE_ACCESS_CONTROL, DoAccessControlDatasource.class);
        indexedEntities.put(Type.PROFILE, DoProfile.class);
    }

    public static Class[] getIndexedEntities() {
        return indexedEntities.values().toArray(new Class[indexedEntities.size()]);
    }

    public static String[] getAllIndices() {
        String[] indices = new String[indexedEntities.size()];

        int index = 0;
        for (Class indexedClass : getIndexedEntities()) {
            indices[index] = AnnotationUtil.getElasticSearchIndex(indexedClass);

            index++;
        }

        return indices;
    }

}
