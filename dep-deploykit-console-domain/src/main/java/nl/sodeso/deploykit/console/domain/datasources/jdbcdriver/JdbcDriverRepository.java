package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriver;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.entities.DoJdbcDriverClass;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class JdbcDriverRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoJdbcDriver> find(Permissions permissions) {
        return BaseRepository.find(DoJdbcDriver.class, permissions);
    }

    public List<DoJdbcDriver> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoJdbcDriver.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoJdbcDriver.class, uuid);
    }

    public DoJdbcDriverClass findClassByUuid(String uuid) {
        return BaseRepository.findByUuid(DoJdbcDriverClass.class, uuid);
    }

    public DoJdbcDriver findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoJdbcDriver.class, uuid, version);
    }

    public DoJdbcDriver checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoJdbcDriver.class, uuid, label);
    }

    public void save(DoJdbcDriver jdbcDriver) {
        BaseRepository.save(jdbcDriver);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoJdbcDriver.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoJdbcDriver> doJdbcDrivers = criteria.list();
        for (DoJdbcDriver doJdbcDriver : doJdbcDrivers) {
            doJdbcDriver.setLabel(label);
            session.update(doJdbcDriver);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoJdbcDriver.class, uuid, version);
    }

}
