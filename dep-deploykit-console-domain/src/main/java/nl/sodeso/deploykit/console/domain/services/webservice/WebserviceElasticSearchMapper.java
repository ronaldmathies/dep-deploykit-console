package nl.sodeso.deploykit.console.domain.services.webservice;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebserviceProperty;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class WebserviceElasticSearchMapper implements ElasticSearchMapper<DoWebservice, IndexEntry> {

    @Override
    public IndexEntry map(DoWebservice doWebservice) {
        IndexEntry indexEntry = new IndexEntry(doWebservice.getId(), doWebservice.getUuid(), doWebservice.getVersion().getValue(), doWebservice.getLabel().getValue(), Type.WEBSERVICE)
                .addKeyword(doWebservice.getLabel().getValue())
                .addKeyword(doWebservice.getUuid())
                .addKeyword(doWebservice.getAction().getValue())
                .addKeyword(doWebservice.getContext().getValue())
                .addKeyword(doWebservice.getDomain().getValue())
                .addKeyword(doWebservice.getFaultTo().getValue())
                .addKeyword(doWebservice.getFrom().getValue())
                .addKeyword(doWebservice.getPath().getValue())
                .addKeyword(doWebservice.getReplyTo().getValue());

        for (DoWebserviceProperty doWebserviceProperty : doWebservice.getProperties()) {
            indexEntry.addKeyword(doWebserviceProperty.getValue().getValue());
        }

        return indexEntry;
    }

}
