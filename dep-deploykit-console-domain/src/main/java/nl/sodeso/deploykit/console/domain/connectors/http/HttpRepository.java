package nl.sodeso.deploykit.console.domain.connectors.http;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class HttpRepository {

    private static final Logger LOG = Logger.getLogger(HttpRepository.class.getName());

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoHttp> find(Permissions permissions) {
        return find(permissions, null);
    }

    public List<DoHttp> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoHttp.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoHttp.class, uuid);
    }

    public DoHttp findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoHttp.class, uuid, version);
    }

    public DoHttp checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoHttp.class, uuid, label);
    }

    public void save(DoHttp doHttps) {
        BaseRepository.save(doHttps);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoHttp.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoHttp> doHttps = criteria.list();
        for (DoHttp doHttp : doHttps) {
            doHttp.setLabel(label);
            session.update(doHttp);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoHttp.class, uuid, version);
    }

}
