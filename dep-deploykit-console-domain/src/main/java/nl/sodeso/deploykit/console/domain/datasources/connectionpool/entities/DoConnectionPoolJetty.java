package nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolElasticSearchMapper;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.IntTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.IntType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
        mapper = ConnectionPoolElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "connectionpoolidx")
@Entity
@DiscriminatorValue(value = "jetty")
@TypeDefs(
    value = {
            @TypeDef(name = "string", typeClass = StringTypeUserType.class),
            @TypeDef(name = "int", typeClass = IntTypeUserType.class),
            @TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)
    }
)
public class DoConnectionPoolJetty extends DoConnectionPool {

    @Type(type = "int")
    @Column(name = "initial_size")
    private IntType initialSize = null;

    @Type(type = "int")
    @Column(name = "max_total")
    private IntType maxTotal = null;

    @Type(type = "int")
    @Column(name = "max_idle")
    private IntType maxIdle = null;

    @Type(type = "int")
    @Column(name = "min_idle")
    private IntType minIdle = null;

    @Type(type = "int")
    @Column(name = "max_wait_millis")
    private IntType maxWaitMillis = null;

    @Type(type = "boolean")
    @Column(name = "test_on_create")
    private BooleanType testOnCreate = null;

    @Type(type = "boolean")
    @Column(name = "test_on_borrow")
    private BooleanType testOnBorrow = null;

    @Type(type = "boolean")
    @Column(name = "test_on_return")
    private BooleanType testOnReturn = null;

    @Type(type = "boolean")
    @Column(name = "test_while_idle")
    private BooleanType testWhileIdle = null;

    @Type(type = "int")
    @Column(name = "time_between_eviction_runs_millis")
    private IntType timeBetweenEvictionRunsMillis = null;

    @Type(type = "int")
    @Column(name = "num_tests_per_eviction_run")
    private IntType numTestsPerEvictionRun = null;

    @Type(type = "int")
    @Column(name = "min_evictable_idle_time_millis")
    private IntType minEvictableIdleTimeMillis = null;

    @Type(type = "int")
    @Column(name = "soft_mini_evictable_idle_time_millis")
    private IntType softMiniEvictableIdleTimeMillis = null;

    @Type(type = "int")
    @Column(name = "max_conn_lifetime_millis")
    private IntType maxConnLifetimeMillis = null;

    @Type(type = "boolean")
    @Column(name = "log_expired_connections")
    private BooleanType logExpiredConnections = null;

    @Type(type = "boolean")
    @Column(name = "lifo")
    private BooleanType lifo = null;

    @Type(type = "int")
    @Column(name = "default_autocommit")
    private IntType defaultAutoCommit = null;

    @Type(type = "int")
    @Column(name = "default_readonly")
    private IntType defaultReadOnly = null;

    @Type(type = "string")
    @Column(name = "default_catalog")
    private StringType defaultCatalog = null;

    @Type(type = "boolean")
    @Column(name = "cache_state")
    private BooleanType cacheState = null;

    public DoConnectionPoolJetty() {}

    public IntType getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(IntType initialSize) {
        this.initialSize = initialSize;
    }

    public IntType getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(IntType maxTotal) {
        this.maxTotal = maxTotal;
    }

    public IntType getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(IntType maxIdle) {
        this.maxIdle = maxIdle;
    }

    public IntType getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(IntType minIdle) {
        this.minIdle = minIdle;
    }

    public IntType getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(IntType maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public BooleanType getTestOnCreate() {
        return testOnCreate;
    }

    public void setTestOnCreate(BooleanType testOnCreate) {
        this.testOnCreate = testOnCreate;
    }

    public BooleanType getTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(BooleanType testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public BooleanType getTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(BooleanType testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public BooleanType getTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(BooleanType testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public IntType getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(IntType timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public IntType getNumTestsPerEvictionRun() {
        return numTestsPerEvictionRun;
    }

    public void setNumTestsPerEvictionRun(IntType numTestsPerEvictionRun) {
        this.numTestsPerEvictionRun = numTestsPerEvictionRun;
    }

    public IntType getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(IntType minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public IntType getSoftMiniEvictableIdleTimeMillis() {
        return softMiniEvictableIdleTimeMillis;
    }

    public void setSoftMiniEvictableIdleTimeMillis(IntType softMiniEvictableIdleTimeMillis) {
        this.softMiniEvictableIdleTimeMillis = softMiniEvictableIdleTimeMillis;
    }

    public IntType getMaxConnLifetimeMillis() {
        return maxConnLifetimeMillis;
    }

    public void setMaxConnLifetimeMillis(IntType maxConnLifetimeMillis) {
        this.maxConnLifetimeMillis = maxConnLifetimeMillis;
    }

    public BooleanType getLogExpiredConnections() {
        return logExpiredConnections;
    }

    public void setLogExpiredConnections(BooleanType logExpiredConnections) {
        this.logExpiredConnections = logExpiredConnections;
    }

    public BooleanType getLifo() {
        return lifo;
    }

    public void setLifo(BooleanType lifo) {
        this.lifo = lifo;
    }

    public IntType getDefaultAutoCommit() {
        return defaultAutoCommit;
    }

    public void setDefaultAutoCommit(IntType defaultAutoCommit) {
        this.defaultAutoCommit = defaultAutoCommit;
    }

    public IntType getDefaultReadOnly() {
        return defaultReadOnly;
    }

    public void setDefaultReadOnly(IntType defaultReadOnly) {
        this.defaultReadOnly = defaultReadOnly;
    }

    public StringType getDefaultCatalog() {
        return defaultCatalog;
    }

    public void setDefaultCatalog(StringType defaultCatalog) {
        this.defaultCatalog = defaultCatalog;
    }

    public BooleanType getCacheState() {
        return cacheState;
    }

    public void setCacheState(BooleanType cacheState) {
        this.cacheState = cacheState;
    }

}
