package nl.sodeso.deploykit.console.domain.security.role.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.persistence.types.BooleanTypeUserType;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_group_permission", indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
})
@TypeDef(name = "boolean", typeClass = BooleanTypeUserType.class)
public class DoGroupPermission {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Type(type = "boolean")
    @Column(name = "allowed_to_read")
    private BooleanType allowedToRead = new BooleanType(true);

    @Type(type = "boolean")
    @Column(name = "allowed_to_use")
    private BooleanType allowedToUse = new BooleanType(false);

    @Type(type = "boolean")
    @Column(name = "allowed_to_edit")
    private BooleanType allowedToEdit = new BooleanType(false);

    @Type(type = "boolean")
    @Column(name = "allowed_to_deploy")
    private BooleanType allowedToDeploy = new BooleanType(false);

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_group_permission_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group = null;

    @JoinColumn(name="role_id", foreignKey = @ForeignKey(name = "fk_group_permission_to_role_id"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoRole role = null;

    public DoGroupPermission() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public BooleanType getAllowedToRead() {
        return allowedToRead;
    }

    public void setAllowedToRead(BooleanType allowedToRead) {
        this.allowedToRead = allowedToRead;
    }

    public BooleanType getAllowedToUse() {
        return allowedToUse;
    }

    public void setAllowedToUse(BooleanType allowToUse) {
        this.allowedToUse = allowToUse;
    }

    public BooleanType getAllowedToEdit() {
        return allowedToEdit;
    }

    public void setAllowedToEdit(BooleanType allowedToEdit) {
        this.allowedToEdit = allowedToEdit;
    }

    public BooleanType getAllowedToDeploy() {
        return allowedToDeploy;
    }

    public void setAllowedToDeploy(BooleanType allowedToDeploy) {
        this.allowedToDeploy = allowedToDeploy;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public DoRole getRole() {
        return this.role;
    }

    public void setRole(DoRole role) {
        this.role = role;
    }
}
