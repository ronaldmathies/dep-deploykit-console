package nl.sodeso.deploykit.console.domain.connectors.https;

import nl.sodeso.deploykit.console.client.application.connectors.https.HttpsSummaryItem;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoCertificate;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoKeystore;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.exception.UnableToReadKeystoreException;
import nl.sodeso.deploykit.console.client.application.connectors.https.Certificate;
import nl.sodeso.deploykit.console.client.application.connectors.https.Https;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.gwt.ui.client.form.upload.FileUpload;
import nl.sodeso.gwt.ui.client.form.upload.FileUploadType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class HttpsConvertor {

    private static final Logger LOG = Logger.getLogger(HttpsConvertor.class.getName());

    public static DoHttps to(DoHttps doHttps, Https https) {
        if (doHttps == null) {
            doHttps = new DoHttps();
            doHttps.setUuid(UuidUtils.generate());
        }

        doHttps.setGroup(new GroupRepository().findByUuid(https.getGroup().getValue().getKey()));
        doHttps.setLabel(https.getLabel());
        doHttps.setVersion(https.getVersion());

        CredentialRepository credentialRepository = new CredentialRepository();
        doHttps.setKeyStorePassword(credentialRepository.findByUuidAndVersion(https.getKeyStorePassword().getValue().getKey(), https.getKeyStoreVersion().getValue()));
        doHttps.setTrustStorePassword(credentialRepository.findByUuidAndVersion(https.getTrustStorePassword().getValue().getKey(), https.getTrustStoreVersion().getValue()));
        doHttps.setKeyManagerPassword(credentialRepository.findByUuidAndVersion(https.getKeyManagerPassword().getValue().getKey(), https.getKeyManagerVersion().getValue()));
        doHttps.setPort(https.getPort());
        doHttps.setTimeout(https.getTimeout());
        doHttps.setHost(https.getHost());

        // Always reload the certificates, since it is also a check if the passwords, en keystore are still valid.
        FileUpload fileUpload = https.getFileUploadType().getValue();
        DoKeystore doKeystore = new KeystoreFactory().findByUuid(fileUpload.getUuid());
        List<DoCertificate> certificates = getCertificates(doHttps.getKeyStorePassword().getPassword().getValue(), doKeystore.getKeystore());
        for (DoCertificate doCertificate : certificates) {
            doCertificate.setKeystore(doKeystore);

            doKeystore.getCertificates().clear();
            doKeystore.getCertificates().add(doCertificate);
        }

        doHttps.setKeystore(doKeystore);
        
        return doHttps;
    }

    public static Https from(DoHttps doHttps) {
        Https https = new Https();

        https.setUuid(doHttps.getUuid());
        https.setGroup(doHttps.getGroup().getOption());
        https.setLabel(doHttps.getLabel());
        https.setVersion(doHttps.getVersion());
        https.setKeyStorePassword(doHttps.getKeyStorePassword().getOption());
        https.setKeyStoreVersion(doHttps.getKeyStorePassword().getVersion());
        https.setTrustStorePassword(doHttps.getTrustStorePassword().getOption());
        https.setTrustStoreVersion(doHttps.getTrustStorePassword().getVersion());
        https.setKeyManagerPasswordOptionType(doHttps.getKeyManagerPassword().getOption());
        https.setKeyManagerVersion(doHttps.getKeyManagerPassword().getVersion());
        https.setPort(doHttps.getPort());
        https.setTimeout(doHttps.getTimeout());
        https.setHost(doHttps.getHost());

        DoKeystore doKeystore = doHttps.getKeystore();
        https.setFileUploadType(new FileUploadType(new FileUpload(doKeystore.getUuid(), doKeystore.getFilename(), doKeystore.getFilesize())));

        List<Certificate> certificates = new ArrayList<>();
        for (DoCertificate doCertificate : doKeystore.getCertificates()) {
            Certificate certificate = new Certificate();
            certificate.setAlias(doCertificate.getAlias());
            certificate.setSubjectDN(doCertificate.getSubjectDN());
            certificate.setIssuerDN(doCertificate.getIssuerDN());
            certificate.setNotAfter(doCertificate.getNotAfter());
            certificate.setNotBefore(doCertificate.getNotBefore());
            certificate.setSerialNumber(doCertificate.getSerialNumber());
            certificate.setSigAlgName(doCertificate.getSigAlgName());
            certificates.add(certificate);
        }
        https.setCertificates(certificates);

        return https;
    }

    public static ArrayList<HttpsSummaryItem> toSummaries(List<DoHttps> httpses) {
        ArrayList<HttpsSummaryItem> summaries = new ArrayList<>();
        for (DoHttps doHttps : httpses) {
            summaries.add(toSummary(doHttps));
        }

        return summaries;
    }

    public static HttpsSummaryItem toSummary(DoHttps doHttps) {
        HttpsSummaryItem summary = new HttpsSummaryItem();
        summary.setUuid(doHttps.getUuid());
        summary.setLabel(doHttps.getLabel());
        return summary;
    }

    private static List<DoCertificate> getCertificates(String keystorePassword, InputStream inputStream) {
        List<DoCertificate> certificates = new ArrayList<>();

        try {
            KeyStore keystore = KeyStore.getInstance("JKS");

            inputStream.reset(); // Dunno why, but it prefents an invalid keystore.
            keystore.load(inputStream, keystorePassword.toCharArray());
            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();

                java.security.cert.Certificate certificate = keystore.getCertificate(alias);
                if (certificate instanceof X509Certificate) {
                    DoCertificate doCertificate = new DoCertificate();
                    doCertificate.setUuid(UuidUtils.generate());

                    X509Certificate x509Certificate = (X509Certificate) certificate;
                    doCertificate.setAlias(alias);
                    doCertificate.setNotBefore(x509Certificate.getNotBefore());
                    doCertificate.setNotAfter(x509Certificate.getNotAfter());
                    doCertificate.setSubjectDN(x509Certificate.getSubjectDN().getName());
                    doCertificate.setIssuerDN(x509Certificate.getIssuerDN().getName());
                    doCertificate.setSigAlgName(x509Certificate.getSigAlgName());
                    doCertificate.setSerialNumber(x509Certificate.getSerialNumber().toString());

                    certificates.add(doCertificate);
                }
            }
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            LOG.log(Level.SEVERE, "Unable to read keystore.", e);
            throw new UnableToReadKeystoreException("There was a problem reading the keystore.\n" + e.getMessage());
        }

        return certificates;
    }
}
