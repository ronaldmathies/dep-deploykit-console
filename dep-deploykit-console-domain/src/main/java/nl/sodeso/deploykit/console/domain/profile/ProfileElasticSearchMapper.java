package nl.sodeso.deploykit.console.domain.profile;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfileProperty;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class ProfileElasticSearchMapper implements ElasticSearchMapper<DoProfile, IndexEntry> {

    @Override
    public IndexEntry map(DoProfile doProfile) {
        IndexEntry indexEntry = new IndexEntry(doProfile.getId(), doProfile.getUuid(), doProfile.getVersion().getValue(), doProfile.getLabel().getValue(), Type.PROFILE)
                .addKeyword(doProfile.getLabel().getValue())
                .addKeyword(doProfile.getUuid());

        for (DoProfileProperty doProfileProperty : doProfile.getProperties()) {
            indexEntry.addKeyword(doProfileProperty.getValue().getValue());
        }

        return indexEntry;
    }

}
