package nl.sodeso.deploykit.console.domain;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class BaseRepository {

    private static Session session = ThreadSafeSession.getSession(Constants.PU);

    public static <X> List<X> find(Class<X> _class) {
        return find(_class, null, null);
    }

    public static <X> List<X> find(Class<X> _class, Permissions permissions) {
        return find(_class, permissions, null);
    }

    @SuppressWarnings("unchecked")
    public static <X> List<X> find(Class<X> _class, Permissions permissions, String groupUuid) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding all '%s' for group uuid '%s'.", _class.getSimpleName(), groupUuid));

        Criteria criteria = session.createCriteria(_class);

        if (groupUuid != null || permissions != null) {
            criteria.createAlias("group", "group", JoinType.INNER_JOIN);
        }

        addPermissions(criteria, permissions);

        if (groupUuid != null && !groupUuid.isEmpty()) {
            criteria
                .add(Restrictions.disjunction()
                    .add(Restrictions.eq("group.uuid", groupUuid)));
        }

        criteria.setProjection(
            Projections.distinct(
                Projections.projectionList()
                    .add(Projections.property("uuid"), "uuid")
                    .add(Projections.property("label"), "label")));

        criteria.setResultTransformer(Transformers.aliasToBean(_class));

        long start = System.currentTimeMillis();
        List<X> list = (List<X>) criteria.list();
        LOG.log(Level.INFO, String.format("Finding all '%s' for group uuid '%s' in %dms..", _class.getSimpleName(), groupUuid, System.currentTimeMillis() - start));

        return list;
    }

    private static void addPermissions(Criteria criteria, Permissions permissions) {
        if (permissions != null) {
            criteria.createAlias("group.groupPermissions", "groupPermissions", JoinType.INNER_JOIN)
                .createAlias("groupPermissions.role", "role", JoinType.INNER_JOIN)
                .createAlias("role.users", "users", JoinType.INNER_JOIN)
                .add(Restrictions.eq("users.username", new StringType(permissions.getUsername())));

            if (permissions.isPermissionRequired(Permissions.READ)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToRead", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.USE)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToUse", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.EDIT)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToEdit", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.DEPLOY)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToDeploy", new BooleanType(true)));
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<VersionOptionType> versions(Class _class, String uuid) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding all versions for '%s' using uuid '%s'.", _class.getSimpleName(), uuid));

        Criteria criteria = session.createCriteria(_class)
                .add(Restrictions.eq("uuid", uuid));

        criteria.setProjection(
                Projections.projectionList()
                        .add(Projections.property("version")));

        long start = System.currentTimeMillis();
        List<VersionOptionType> list = criteria.list();
        LOG.log(Level.INFO, String.format("Finding all versions for '%s' using uuid '%s' in %dms..", _class.getSimpleName(), uuid, System.currentTimeMillis() - start));

        return list;
    }

    @SuppressWarnings("unchecked")
    public static <X> X checkIfLabelAlreadyExists(Class<X> _class, String uuid, String label) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Checking if label already exists for '%s', using uuid '%s' and label '%s'.", _class.getSimpleName(), uuid, label));

        Criteria criteria = session.createCriteria(_class)
                .add(Restrictions.eq("label", new StringType(label)));

        if (uuid != null) {
            criteria.add(Restrictions.ne("uuid", uuid));
        }

        criteria.setProjection(
            Projections.distinct(
                Projections.projectionList()
                    .add(Projections.property("label"), "label")
                    .add(Projections.property("uuid"), "uuid")
                    .add(Projections.property("group"), "group")));

        criteria.setResultTransformer(Transformers.aliasToBean(_class));

        long start = System.currentTimeMillis();
        X x = (X) criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Checking if label already exists for '%s', using uuid '%s' and label '%s' in %dms..", _class.getSimpleName(), uuid, label, System.currentTimeMillis() - start));

        return x;
    }

    @SuppressWarnings("unchecked")
    public static <X> X findByLabel(Class<X> _class, String label) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding '%s' by label '%s'.", _class.getSimpleName(), label));

        Criteria criteria = session.createCriteria(_class)
            .add(Restrictions.eq("label", new StringType(label)));

        long start = System.currentTimeMillis();
        X x = (X)criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Finding '%s' by label '%s' in %dms..", _class.getSimpleName(), label, System.currentTimeMillis() - start));

        return x;
    }

    @SuppressWarnings("unchecked")
    public static <X> X findByLabelAndVersion(Class<X> _class, String label, VersionOption version) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding '%s' by label '%s' and '%s'.", _class.getSimpleName(), label, version.getDisplayDescription()));

        Criteria criteria = session.createCriteria(_class)
            .add(Restrictions.eq("label", new StringType(label)))
            .add(Restrictions.eq("version", new VersionOptionType(version)));

        long start = System.currentTimeMillis();
        X x = (X) criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Finding '%s' by label '%s' and '%s' in %dms..", _class.getSimpleName(), label, version.getDisplayDescription(), System.currentTimeMillis() - start));

        return x;
    }

    @SuppressWarnings("unchecked")
    public static <X> X findByUuid(Class<X> _class, String uuid) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding '%s' with uuid '%s'.", _class.getSimpleName(), uuid));

        Criteria criteria = session.createCriteria(_class)
                .add(Restrictions.eq("uuid", uuid));

        long start = System.currentTimeMillis();
        X x = (X) criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Finding '%s' with uuid '%s' in %dms..", _class.getSimpleName(), uuid, System.currentTimeMillis() - start));

        return x;
    }

    @SuppressWarnings("unchecked")
    public static <X> X findByUuidAndVersion(Class<X> _class, String uuid, VersionOption version) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Finding '%s' by uuid '%s', and version '%s'.", _class.getSimpleName(), uuid, version.getDisplayDescription()));

        Criteria criteria = session.createCriteria(_class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.eq("version", new VersionOptionType(version)));


        long start = System.currentTimeMillis();
        X x = (X) criteria.uniqueResult();
        LOG.log(Level.INFO, String.format("Finding '%s' by uuid '%s', and version '%s' in %dms.", _class.getSimpleName(), uuid, version.getDisplayDescription(), System.currentTimeMillis() - start));

        return x;
    }

    public static void save(Object object) {
        Logger LOG = Logger.getLogger(object.getClass().getName());

        LOG.log(Level.INFO, String.format("Saving '%s'.", object.getClass().getSimpleName()));

        session.save(object);
    }

//    public static void updateLabel(Class<?> _class, Long id, String uuid, StringType label) {
//        Criteria criteria = session.createCriteria(_class)
//                .add(Restrictions.eq("uuid", uuid))
//                .add(Restrictions.ne("id", id));
//
//        List<Object> doProfiles = criteria.list();
//        for (DoProfile doProfile : doProfiles) {
//            doProfile.setLabel(label);
//            session.update(doProfile);
//        }
//    }

    public static void remove(Class<?> _class, String uuid) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Removing '%s' with uuid '%s'.", _class.getSimpleName(), uuid));

        Criteria criteria = session.createCriteria(_class)
                .add(Restrictions.eq("uuid", uuid));
        Object object = criteria.uniqueResult();
        session.delete(object);
    }

    public static void remove(Class<?> _class, String uuid, VersionOption version) {
        Logger LOG = Logger.getLogger(_class.getName());

        LOG.log(Level.INFO, String.format("Removing '%s' with uuid '%s' and version, '%s'.", _class.getSimpleName(), uuid, version.getDisplayDescription()));

        Object object = findByUuidAndVersion(_class, uuid, version);
        session.delete(object);
    }

}
