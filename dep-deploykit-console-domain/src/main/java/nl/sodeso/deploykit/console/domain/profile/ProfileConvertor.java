package nl.sodeso.deploykit.console.domain.profile;

import nl.sodeso.deploykit.console.client.application.profile.*;
import nl.sodeso.deploykit.console.client.application.profile.service.ldap.ProfileLdapService;
import nl.sodeso.deploykit.console.client.application.profile.service.ldap.ProfileLdapServices;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperty;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.AccessControlRepository;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.http.HttpRepository;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.connectors.https.HttpsRepository;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceRepository;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.deploykit.console.domain.node.NodeRepository;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfileProperty;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.ldap.LdapServiceRepository;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.domain.services.webservice.WebserviceRepository;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ProfileConvertor {

    public static DoProfile to(DoProfile doProfile, Profile profile) {
        if (doProfile == null) {
            doProfile = new DoProfile();
            doProfile.setUuid(UuidUtils.generate());
        }

        doProfile.setVersion(profile.getVersion());
        doProfile.setLabel(profile.getLabel());
        doProfile.setGroup(new GroupRepository().findByUuid(profile.getGroup().getValue().getKey()));

        handleDatasources(doProfile, profile);
        handleWebservices(doProfile, profile);
        handleNodes(doProfile, profile);
        handleCredentials(doProfile, profile);
        handleLdapServices(doProfile, profile);
        handleProperties(doProfile, profile);

        ProfileHttps profileHttps = profile.getProfileHttps();
        if (profileHttps.getUseHttps().getValue()) {
            OptionType<DefaultOption> optionType = profileHttps.getHttpsOptionType();
            DoHttps doHttps = new HttpsRepository().findByUuidAndVersion(optionType.getValue().getKey(), profileHttps.getHttpsVersionType().getValue());
            doProfile.setHttps(doHttps);
        } else {
            doProfile.setHttps(null);
        }

        ProfileHttp profileHttp = profile.getProfileHttp();
        if (profileHttp.getUseHttp().getValue()) {
            OptionType<DefaultOption> optionType = profileHttp.getHttpOptionType();
            DoHttp doHttp = new HttpRepository().findByUuidAndVersion(optionType.getValue().getKey(), profileHttp.getHttpVersionType().getValue());
            doProfile.setHttp(doHttp);
        } else {
            doProfile.setHttp(null);
        }

        ProfileAccessControl profileAccessControl = profile.getProfileAccessControl();
        if (profileAccessControl.getUseAccessControl().getValue()) {
            OptionType<DefaultOption> optionType = profileAccessControl.getAccessControlOption();
            DoAccessControl doAccessControl = new AccessControlRepository().findByUuidAndVersion(optionType.getValue().getKey(), profileAccessControl.getAccessControlVersion().getValue());
            doProfile.setAccessControl(doAccessControl);
        } else {
            doProfile.setAccessControl(null);
        }

        return doProfile;
    }

    private static void handleDatasources(DoProfile doProfile, Profile profile) {
        Iterator<DoDatasource> doDatasourceIterator = doProfile.getDatasources().iterator();
        while (doDatasourceIterator.hasNext()) {
            DoDatasource doDatasource = doDatasourceIterator.next();

            boolean removed = false;
            for (ProfileDatasource profileDatasource : profile.getProfileDatasources().getDeletedWidgets()) {
                String profileDatasourceUuid = profileDatasource.getDatasource().getValue().getKey();
                VersionOption profileDatasourceVersion = profileDatasource.getDatasourceVersion().getValue();

                if (doDatasource.getUuid().equals(profileDatasourceUuid) &&
                        doDatasource.getVersion().getValue().equals(profileDatasourceVersion)) {
                    doDatasourceIterator.remove();
                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (ProfileDatasource profileDatasource : profile.getProfileDatasources().getWidgets()) {
                    String profileDatasourceUuid = profileDatasource.getDatasource().getValue().getKey();
                    VersionOption profileDatasourceVersion = profileDatasource.getDatasourceVersion().getValue();

                    boolean found = false;
                    if (doDatasource.getUuid().equals(profileDatasourceUuid) &&
                            doDatasource.getVersion().getValue().equals(profileDatasourceVersion)) {
                        found = true;
                    }

                    if (!found) {
                        doDatasourceIterator.remove();
                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (ProfileDatasource profileDatasource : profile.getProfileDatasources().getWidgets()) {
            String profileDatasourceUuid = profileDatasource.getDatasource().getValue().getKey();
            VersionOption profileDatasourceVersion = profileDatasource.getDatasourceVersion().getValue();

            boolean exists = false;
            for (DoDatasource doDatasource : doProfile.getDatasources()) {
                if (doDatasource.getUuid().equalsIgnoreCase(profileDatasourceUuid) &&
                        doDatasource.getVersion().getValue().equals(profileDatasourceVersion)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doProfile.getDatasources().add(new DatasourceRepository().findByUuidAndVersion(profileDatasourceUuid, profileDatasource.getDatasourceVersion().getValue()));
            }

        }
    }

    private static void handleWebservices(DoProfile doProfile, Profile profile) {
        Iterator<DoWebservice> doWebserviceIterator = doProfile.getWebservices().iterator();
        while (doWebserviceIterator.hasNext()) {
            DoWebservice doWebservice = doWebserviceIterator.next();

            boolean removed = false;
            for (ProfileWebservice profileWebservice : profile.getProfileWebservices().getDeletedWidgets()) {
                String profileWebserviceUuid = profileWebservice.getWebservice().getValue().getKey();
                VersionOption profileWebserviceVersion = profileWebservice.getWebserviceVersion().getValue();

                if (doWebservice.getUuid().equals(profileWebserviceUuid) &&
                        doWebservice.getVersion().getValue().equals(profileWebserviceVersion)) {
                    doWebserviceIterator.remove();
                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (ProfileWebservice profileWebservice : profile.getProfileWebservices().getWidgets()) {
                    String profileWebserviceUuid = profileWebservice.getWebservice().getValue().getKey();
                    VersionOption profileWebserviceVersion = profileWebservice.getWebserviceVersion().getValue();

                    boolean found = false;
                    if (doWebservice.getUuid().equals(profileWebserviceUuid) &&
                            doWebservice.getVersion().getValue().equals(profileWebserviceVersion)) {
                        found = true;
                    }

                    if (!found) {
                        doWebserviceIterator.remove();
                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (ProfileWebservice profileWebservice : profile.getProfileWebservices().getWidgets()) {
            String profileWebserviceUuid = profileWebservice.getWebservice().getValue().getKey();
            VersionOption profileWebserviceVersion = profileWebservice.getWebserviceVersion().getValue();

            boolean exists = false;
            for (DoWebservice doWebservice : doProfile.getWebservices()) {
                if (doWebservice.getUuid().equalsIgnoreCase(profileWebserviceUuid) &&
                        doWebservice.getVersion().getValue().equals(profileWebserviceVersion)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doProfile.getWebservices().add(new WebserviceRepository().findByUuidAndVersion(profileWebserviceUuid, profileWebservice.getWebserviceVersion().getValue()));
            }

        }
    }

    private static void handleNodes(DoProfile doProfile, Profile profile) {
        Iterator<DoNode> doNodeIterator = doProfile.getNodes().iterator();
        while (doNodeIterator.hasNext()) {
            DoNode doNode = doNodeIterator.next();

            for (ProfileNode profileNode : profile.getProfileNodes().getDeletedWidgets()) {
                if (doNode.getUuid().equals(profileNode.getNode().getValue().getKey())) {
                    doNodeIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (ProfileNode profileNode : profile.getProfileNodes().getWidgets()) {
            String profileNodeUuid = profileNode.getNode().getValue().getKey();

            boolean exists = false;
            for (DoNode doNode : doProfile.getNodes()) {
                if (doNode.getUuid().equalsIgnoreCase(profileNodeUuid)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doProfile.getNodes().add(new NodeRepository().findByUuid(profileNodeUuid));
            }

        }
    }

    private static void handleCredentials(DoProfile doProfile, Profile profile) {
        Iterator<DoCredential> doCredentialIterator = doProfile.getCredentials().iterator();
        while (doCredentialIterator.hasNext()) {
            DoCredential doCredential = doCredentialIterator.next();

            boolean removed = false;
            for (ProfileCredential profileCredential : profile.getProfileCredentials().getDeletedWidgets()) {
                String profileCredentialUuid = profileCredential.getCredential().getValue().getKey();
                VersionOption profileCredentialVersion = profileCredential.getCredentialVersion().getValue();

                if (doCredential.getUuid().equals(profileCredentialUuid) &&
                        doCredential.getVersion().getValue().equals(profileCredentialVersion)) {
                    doCredentialIterator.remove();
                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (ProfileCredential profileCredential : profile.getProfileCredentials().getWidgets()) {
                    String profileCredentialUuid = profileCredential.getCredential().getValue().getKey();
                    VersionOption profileCredentialVersion = profileCredential.getCredentialVersion().getValue();

                    boolean found = false;
                    if (doCredential.getUuid().equals(profileCredentialUuid) &&
                            doCredential.getVersion().getValue().equals(profileCredentialVersion)) {
                        found = true;
                    }

                    if (!found) {
                        doCredentialIterator.remove();
                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (ProfileCredential profileCredential : profile.getProfileCredentials().getWidgets()) {
            String profileCredentialUuid = profileCredential.getCredential().getValue().getKey();
            VersionOption profileCredentialVersion = profileCredential.getCredentialVersion().getValue();

            boolean exists = false;
            for (DoCredential doCredential : doProfile.getCredentials()) {
                if (doCredential.getUuid().equalsIgnoreCase(profileCredentialUuid) &&
                        doCredential.getVersion().getValue().equals(profileCredentialVersion)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doProfile.getCredentials().add(new CredentialRepository().findByUuidAndVersion(profileCredentialUuid, profileCredential.getCredentialVersion().getValue()));
            }

        }
    }

    private static void handleLdapServices(DoProfile doProfile, Profile profile) {
        Iterator<DoLdapService> doLdapServiceIterator = doProfile.getLdapServices().iterator();
        while (doLdapServiceIterator.hasNext()) {
            DoLdapService doLdapService = doLdapServiceIterator.next();

            boolean removed = false;
            for (ProfileLdapService profileLdapService : profile.getProfileLdapServices().getDeletedWidgets()) {
                String profileLdapServiceUuid = profileLdapService.getLdapService().getValue().getKey();
                VersionOption profileLdapServiceVersion = profileLdapService.getLdapServiceVersion().getValue();

                if (doLdapService.getUuid().equals(profileLdapServiceUuid) &&
                        doLdapService.getVersion().getValue().equals(profileLdapServiceVersion)) {
                    doLdapServiceIterator.remove();
                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (ProfileLdapService profileLdapService : profile.getProfileLdapServices().getWidgets()) {
                    String profileLdapServiceUuid = profileLdapService.getLdapService().getValue().getKey();
                    VersionOption profileLdapServiceVersion = profileLdapService.getLdapServiceVersion().getValue();

                    boolean found = false;
                    if (doLdapService.getUuid().equals(profileLdapServiceUuid) &&
                            doLdapService.getVersion().getValue().equals(profileLdapServiceVersion)) {
                        found = true;
                    }

                    if (!found) {
                        doLdapServiceIterator.remove();
                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (ProfileLdapService profileLdapService : profile.getProfileLdapServices().getWidgets()) {
            String profileLdapServiceUuid = profileLdapService.getLdapService().getValue().getKey();
            VersionOption profileLdapServiceVersion = profileLdapService.getLdapServiceVersion().getValue();

            boolean exists = false;
            for (DoCredential doCredential : doProfile.getCredentials()) {
                if (doCredential.getUuid().equalsIgnoreCase(profileLdapServiceUuid) &&
                        doCredential.getVersion().getValue().equals(profileLdapServiceVersion)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doProfile.getLdapServices().add(new LdapServiceRepository().findByUuidAndVersion(profileLdapServiceUuid, profileLdapService.getLdapServiceVersion().getValue()));
            }

        }
    }

    private static void handleProperties(DoProfile doProfile, Profile profile) {
        Iterator<DoProfileProperty> doConnectionPropertyIterator = doProfile.getProperties().iterator();
        while (doConnectionPropertyIterator.hasNext()) {
            DoProfileProperty doProfileProperty = doConnectionPropertyIterator.next();

            for (KeyValueProperty profileProperty : profile.getProperties().getDeletedWidgets()) {
                if (doProfileProperty.getUuid().equals(profileProperty.getUuid())) {
                    doConnectionPropertyIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (KeyValueProperty profileProperty : profile.getProperties().getWidgets()) {
            String profilePropertyUuid = profileProperty.getUuid();

            boolean exists = false;
            for (DoProfileProperty doProfileProperty : doProfile.getProperties()) {
                if (doProfileProperty.getUuid().equalsIgnoreCase(profilePropertyUuid)) {

                    doProfileProperty.setKey(profileProperty.getKey());
                    doProfileProperty.setValue(profileProperty.getValue());

                    exists = true;
                    break;
                }

            }

            if (!exists) {
                DoProfileProperty doProfileProperty = new DoProfileProperty();
                doProfileProperty.setUuid(UuidUtils.generate());
                doProfileProperty.setKey(profileProperty.getKey());
                doProfileProperty.setValue(profileProperty.getValue());
                doProfile.getProperties().add(doProfileProperty);
            }

        }
    }

    public static Profile from(DoProfile doProfile) {
        Profile profile = new Profile();

        profile.setUuid(doProfile.getUuid());
        profile.setLabel(doProfile.getLabel());
        profile.setVersion(doProfile.getVersion());
        profile.setGroup(doProfile.getGroup().getOption());

        fromHandleDatasources(profile, doProfile);
        fromHandleWebservices(profile, doProfile);
        fromHandleNodes(profile, doProfile);
        fromHandleCredentials(profile, doProfile);
        fromHandleLdapServices(profile, doProfile);
        fromHandleProperties(profile, doProfile);

        ProfileHttps profileHttps = new ProfileHttps();
        if (doProfile.getHttps() != null) {
            profileHttps.setGroup(doProfile.getHttps().getGroup().getOption());
            profileHttps.setHttpsOptionType(doProfile.getHttps().getOption());
            profileHttps.setHttpsVersionType(doProfile.getHttps().getVersion());
        }
        profileHttps.setUseHttps(new BooleanType(doProfile.getHttps() != null));
        profile.setProfileHttps(profileHttps);

        ProfileHttp profileHttp = new ProfileHttp();
        if (doProfile.getHttp() != null) {
            profileHttp.setGroup(doProfile.getHttp().getGroup().getOption());
            profileHttp.setHttpOptionType(doProfile.getHttp().getOption());
            profileHttp.setHttpVersionType(doProfile.getHttp().getVersion());
        }
        profileHttp.setUseHttp(new BooleanType(doProfile.getHttp() != null));
        profile.setProfileHttp(profileHttp);

        ProfileAccessControl profileAccessControl = new ProfileAccessControl(profile.getGroup());
        if (doProfile.getAccessControl() != null) {
            profileAccessControl.setAccessControlOption(doProfile.getAccessControl().getOption());
            profileAccessControl.setAccessControlVersion(doProfile.getAccessControl().getVersion());
        }
        profileAccessControl.setUseAccessControl(new BooleanType(doProfile.getAccessControl() != null));
        profile.setProfileAccessControl(profileAccessControl);

        return profile;
    }

    private static void fromHandleDatasources(Profile profile, DoProfile doProfile) {
        ProfileDatasources profileDatasources = profile.getProfileDatasources();
        for (DoDatasource doDatasource : doProfile.getDatasources()) {
            ProfileDatasource profileDatasource = new ProfileDatasource();
            profileDatasource.setUuid(doDatasource.getUuid());
            profileDatasource.setGroup(profile.getGroup()); // doDatasource.getGroup().getOption()
            profileDatasource.setDatasourceVersion(doDatasource.getVersion());
            profileDatasource.setDatasource(doDatasource.getOption());
            profileDatasources.getWidgets().add(profileDatasource);
        }
    }

    private static void fromHandleWebservices(Profile profile, DoProfile doProfile) {
        ProfileWebservices profileWebservices = profile.getProfileWebservices();
        for (DoWebservice doWebservice : doProfile.getWebservices()) {
            ProfileWebservice profileWebservice = new ProfileWebservice();
            profileWebservice.setUuid(doWebservice.getUuid());
            profileWebservice.setGroup(profile.getGroup()); // doWebservice.getGroup().getOption()
            profileWebservice.setWebserviceVersion(doWebservice.getVersion());
            profileWebservice.setWebservice(doWebservice.getOption());
            profileWebservices.getWidgets().add(profileWebservice);
        }
    }

    private static void fromHandleNodes(Profile profile, DoProfile doProfile) {
        ProfileNodes profileNodes = profile.getProfileNodes();
        for (DoNode doNode : doProfile.getNodes()) {
            ProfileNode profileNode = new ProfileNode();
            profileNode.setUuid(doNode.getUuid());
            profileNode.setGroup(profile.getGroup()); // doNode.getGroup().getOption()
            profileNode.setNode(doNode.getOption());
            profileNodes.getWidgets().add(profileNode);
        }
    }

    private static void fromHandleCredentials(Profile profile, DoProfile doProfile) {
        ProfileCredentials profileCredentials = profile.getProfileCredentials();
        for (DoCredential doCredential : doProfile.getCredentials()) {
            ProfileCredential profileCredential = new ProfileCredential();
            profileCredential.setUuid(doCredential.getUuid());
            profileCredential.setGroup(profile.getGroup()); // doCredential.getGroup().getOption()
            profileCredential.setCredential(doCredential.getOption());
            profileCredential.setCredentialVersion(doCredential.getVersion());
            profileCredentials.getWidgets().add(profileCredential);
        }
    }

    private static void fromHandleLdapServices(Profile profile, DoProfile doProfile) {
        ProfileLdapServices profileLdapServices = profile.getProfileLdapServices();
        for (DoLdapService doLdapService : doProfile.getLdapServices()) {
            ProfileLdapService profileLdapService = new ProfileLdapService();
            profileLdapService.setUuid(doLdapService.getUuid());
            profileLdapService.setGroup(profile.getGroup()); // doLdapService.getGroup().getOption()
            profileLdapService.setLdapService(doLdapService.getOption());
            profileLdapService.setLdapServiceVersion(doLdapService.getVersion());
            profileLdapServices.getWidgets().add(profileLdapService);
        }
    }

    private static void fromHandleProperties(Profile datasource, DoProfile doDatasource) {
        KeyValueProperties profileProperty = datasource.getProperties();
        for (DoProfileProperty doProfileProperty : doDatasource.getProperties()) {
            KeyValueProperty datasourceConnectionProperty = new KeyValueProperty();
            datasourceConnectionProperty.setUuid(doProfileProperty.getUuid());
            datasourceConnectionProperty.setKey(doProfileProperty.getKey());
            datasourceConnectionProperty.setValue(doProfileProperty.getValue());
            profileProperty.getWidgets().add(datasourceConnectionProperty);
        }
    }

    public static ArrayList<ProfileSummaryItem> toSummaries(List<DoProfile> profiles) {
        ArrayList<ProfileSummaryItem> summaries = new ArrayList<>();
        for (DoProfile doProfile : profiles) {
            summaries.add(toSummary(doProfile));
        }

        return summaries;
    }

    public static ProfileSummaryItem toSummary(DoProfile doProfile) {
        ProfileSummaryItem summary = new ProfileSummaryItem();
        summary.setUuid(doProfile.getUuid());
        summary.setLabel(doProfile.getLabel());
        return summary;
    }

}
