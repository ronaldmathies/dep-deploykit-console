package nl.sodeso.deploykit.console.domain.profile.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControl;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.node.entities.DoNode;
import nl.sodeso.deploykit.console.domain.profile.ProfileElasticSearchMapper;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchIndex;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapping;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@EntityListeners(value = {ElasticSearchEventListener.class})
@ElasticSearchMapping(
    mapper = ProfileElasticSearchMapper.class,
    mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "profileidx")
@Entity
@Table(name = "t_profile",
    indexes = {
        @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    })
@TypeDefs(
    value = {
        @TypeDef(name = "string", typeClass = StringTypeUserType.class),
    }
)
public class DoProfile extends DoBaseVersionedEntity {

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_profile_to_group"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoGroup group;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_profile_datasource", joinColumns = {
            @JoinColumn(name = "profile_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_datasource_to_profile")) },
            inverseJoinColumns = { @JoinColumn(name = "datasource_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_datasource_to_datasource")) })
    private List<DoDatasource> datasources = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_profile_node", joinColumns = {
            @JoinColumn(name = "profile_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_node_to_profile")) },
            inverseJoinColumns = { @JoinColumn(name = "node_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_node_to_node")) })
    private List<DoNode> nodes = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_profile_credential", joinColumns = {
            @JoinColumn(name = "profile_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_credential_to_profile")) },
            inverseJoinColumns = { @JoinColumn(name = "credential_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_credential_to_credential")) })
    private List<DoCredential> credentials = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_profile_webservice", joinColumns = {
            @JoinColumn(name = "profile_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_webservice_to_profile")) },
            inverseJoinColumns = { @JoinColumn(name = "webservice_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_webservice_to_webservice")) })
    private List<DoWebservice> webservices = new ArrayList<>();

    @JoinColumn(name="https_id", foreignKey = @ForeignKey(name = "fk_profile_to_https"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoHttps https = null;

    @JoinColumn(name="http_id", foreignKey = @ForeignKey(name = "fk_profile_to_http"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoHttp http = null;

    @JoinColumn(name="access_control_id", foreignKey = @ForeignKey(name = "fk_profile_to_access_control"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private DoAccessControl accessControl = null;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "t_profile_ldap_service", joinColumns = {
            @JoinColumn(name = "profile_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_ldap_service_to_profile")) },
            inverseJoinColumns = { @JoinColumn(name = "ldap_service_id",
                    nullable = false, updatable = false, foreignKey = @ForeignKey(name = "fk_profile_ldap_service_to_ldap_service")) })
    private List<DoLdapService> ldapServices = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "profile")
    private List<DoProfileProperty> properties = new ArrayList<>();

    public DoProfile() {}

    public DoGroup getGroup() {
        return group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public List<DoDatasource> getDatasources() {
        return this.datasources;
    }

    public List<DoNode> getNodes() {
        return this.nodes;
    }

    public List<DoCredential> getCredentials() {
        return this.credentials;
    }

    public List<DoWebservice> getWebservices() {
        return this.webservices;
    }

    public DoHttps getHttps() {
        return this.https;
    }

    public void setHttps(DoHttps https) {
        this.https = https;
    }

    public DoHttp getHttp() {
        return http;
    }

    public void setHttp(DoHttp http) {
        this.http = http;
    }

    public DoAccessControl getAccessControl() {
        return this.accessControl;
    }

    public void setAccessControl(DoAccessControl accessControl) {
        this.accessControl = accessControl;
    }

    public List<DoLdapService> getLdapServices() {
        return this.ldapServices;
    }

    public List<DoProfileProperty> getProperties() {
        return this.properties;
    }

}
