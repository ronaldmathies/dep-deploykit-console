package nl.sodeso.deploykit.console.domain.general.group;

import nl.sodeso.deploykit.console.client.application.group.Group;
import nl.sodeso.deploykit.console.client.application.group.GroupSummaryItem;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class GroupConvertor {

    public static DoGroup to(DoGroup doGroup, Group group) {
        if (doGroup == null) {
            doGroup = new DoGroup();
            doGroup.setUuid(UuidUtils.generate());
        }

        doGroup.setLabel(group.getLabel());

        return doGroup;
    }

    public static Group from(DoGroup doGroup) {
        Group group = new Group();

        group.setUuid(doGroup.getUuid());
        group.setLabel(doGroup.getLabel());

        return group;
    }

    public static ArrayList<GroupSummaryItem> toSummaries(List<DoGroup> groups) {
        ArrayList<GroupSummaryItem> summaries = new ArrayList<>();
        for (DoGroup doGroup : groups) {
            summaries.add(toSummary(doGroup));
        }

        return summaries;
    }

    public static GroupSummaryItem toSummary(DoGroup doGroup) {
        GroupSummaryItem summary = new GroupSummaryItem();
        summary.setUuid(doGroup.getUuid());
        summary.setLabel(doGroup.getLabel());
        return summary;
    }

}
