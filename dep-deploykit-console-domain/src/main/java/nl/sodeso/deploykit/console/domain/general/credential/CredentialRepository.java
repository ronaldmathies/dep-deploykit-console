package nl.sodeso.deploykit.console.domain.general.credential;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.general.credential.entities.DoCredential;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CredentialRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoCredential> find(Permissions permissions) {
        return BaseRepository.find(DoCredential.class, permissions);
    }

    public List<DoCredential> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoCredential.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoCredential.class, uuid);
    }


    public DoCredential findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoCredential.class, uuid, version);
    }

    public DoCredential checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoCredential.class, uuid, label);
    }

    public void save(DoCredential credential) {
        BaseRepository.save(credential);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoCredential.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoCredential> doCredentials = criteria.list();
        for (DoCredential doCredential : doCredentials) {
            doCredential.setLabel(label);
            session.update(doCredential);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoCredential.class, uuid, version);
    }

}
