package nl.sodeso.deploykit.console.domain.datasources.datasource;

import nl.sodeso.deploykit.console.client.application.datasources.datasource.Datasource;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceConnectionPool;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceConnectionPools;
import nl.sodeso.deploykit.console.client.application.datasources.datasource.DatasourceSummaryItem;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperties;
import nl.sodeso.deploykit.console.client.application.ui.properties.KeyValueProperty;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.entities.DoConnectionPool;
import nl.sodeso.deploykit.console.domain.datasources.connectionpool.ConnectionPoolRepository;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoConnectionProperty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.JdbcDriverRepository;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class DatasourceConvertor {

    public static DoDatasource to(DoDatasource doDatasource, Datasource datasource) {
        if (doDatasource == null) {
            doDatasource = new DoDatasource();
            doDatasource.setUuid(UuidUtils.generate());
        }

        doDatasource.setVersion(datasource.getVersion());
        doDatasource.setLabel(datasource.getLabel());
        doDatasource.setPlaceholderPrefix(datasource.getPlaceholderPrefix());
        doDatasource.setGroup(new GroupRepository().findByUuid(datasource.getGroup().getValue().getKey()));
        doDatasource.setJdbcDriver(new JdbcDriverRepository().findByUuidAndVersion(datasource.getJdbcDriver().getValue().getKey(), datasource.getJdbcDriverVersion().getValue()));
        doDatasource.setJndi(datasource.getJndi());
        doDatasource.setUrl(datasource.getUrl());
        doDatasource.setCredential(new CredentialRepository().findByUuidAndVersion(datasource.getCredential().getValue().getKey(), datasource.getCredentialVersion().getValue()));

        handleConnectionPools(doDatasource, datasource);
        handleProperties(doDatasource, datasource);

        return doDatasource;
    }

    private static void handleConnectionPools(DoDatasource doDatasource, Datasource datasource) {
        Iterator<DoConnectionPool> doConnectionPoolIterator = doDatasource.getConnectionPools().iterator();
        while (doConnectionPoolIterator.hasNext()) {
            DoConnectionPool doConnectionPool = doConnectionPoolIterator.next();

            boolean removed = false;
            for (DatasourceConnectionPool datasourceConnectionPool : datasource.getDatasourceConnectionPools().getDeletedWidgets()) {
                String datasourceConnectionPoolUuid = datasourceConnectionPool.getConnectionPool().getValue().getKey();
                VersionOption datasourceConnectionPoolVersion = datasourceConnectionPool.getConnectionPoolVersion().getValue();

                if (doConnectionPool.getUuid().equals(datasourceConnectionPoolUuid) &&
                        doConnectionPool.getVersion().getValue().equals(datasourceConnectionPoolVersion)) {
                    doConnectionPoolIterator.remove();
                    removed = true;
                    break;
                }
            }

            if (!removed) {
                for (DatasourceConnectionPool datasourceConnectionPool : datasource.getDatasourceConnectionPools().getWidgets()) {
                    String datasourceConnectionPoolUuid = datasourceConnectionPool.getConnectionPool().getValue().getKey();
                    VersionOption datasourceConnectionPoolVersion = datasourceConnectionPool.getConnectionPoolVersion().getValue();

                    boolean found = false;
                    if (doConnectionPool.getUuid().equals(datasourceConnectionPoolUuid) &&
                            doConnectionPool.getVersion().getValue().equals(datasourceConnectionPoolVersion)) {
                        found = true;
                    }

                    if (!found) {
                        doConnectionPoolIterator.remove();
                        break;
                    }
                }
            }
        }

        // Now add the newly added relations.
        for (DatasourceConnectionPool datasourceConnectionPool : datasource.getDatasourceConnectionPools().getWidgets()) {
            String datasourceConnectionPoolUuid = datasourceConnectionPool.getConnectionPool().getValue().getKey();
            VersionOption datasourceConnectionPoolVersion = datasourceConnectionPool.getConnectionPoolVersion().getValue();

            boolean exists = false;
            for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
                if (doConnectionPool.getUuid().equalsIgnoreCase(datasourceConnectionPoolUuid) &&
                        doConnectionPool.getVersion().getValue().equals(datasourceConnectionPoolVersion)) {
                    exists = true;
                    break;
                }

            }

            if (!exists) {
                doDatasource.getConnectionPools().add(new ConnectionPoolRepository().findByUuidAndVersion(datasourceConnectionPoolUuid, datasourceConnectionPool.getConnectionPoolVersion().getValue()));
            }

        }
    }

    private static void handleProperties(DoDatasource doDatasource, Datasource datasource) {
        Iterator<DoConnectionProperty> doConnectionPropertyIterator = doDatasource.getProperties().iterator();
        while (doConnectionPropertyIterator.hasNext()) {
            DoConnectionProperty doConnectionProperty = doConnectionPropertyIterator.next();

            for (KeyValueProperty datasourceConnectionProperty : datasource.getProperties().getDeletedWidgets()) {
                if (doConnectionProperty.getUuid().equals(datasourceConnectionProperty.getUuid())) {
                    doConnectionPropertyIterator.remove();
                    break;
                }
            }
        }

        // Now add the newly added relations.
        for (KeyValueProperty datasourceConnectionProperty : datasource.getProperties().getWidgets()) {
            String datasourceConnectionPropertyUuid = datasourceConnectionProperty.getUuid();

            boolean exists = false;
            for (DoConnectionProperty doConnectionProperty : doDatasource.getProperties()) {
                if (doConnectionProperty.getUuid().equalsIgnoreCase(datasourceConnectionPropertyUuid)) {

                    doConnectionProperty.setKey(datasourceConnectionProperty.getKey());
                    doConnectionProperty.setValue(datasourceConnectionProperty.getValue());

                    exists = true;
                    break;
                }

            }

            if (!exists) {
                DoConnectionProperty doConnectionProperty = new DoConnectionProperty();
                doConnectionProperty.setUuid(UuidUtils.generate());
                doConnectionProperty.setKey(datasourceConnectionProperty.getKey());
                doConnectionProperty.setValue(datasourceConnectionProperty.getValue());
                doDatasource.getProperties().add(doConnectionProperty);
            }

        }
    }

    public static Datasource from(DoDatasource doDatasource) {
        Datasource datasource = new Datasource();

        datasource.setUuid(doDatasource.getUuid());
        datasource.setVersion(doDatasource.getVersion());
        datasource.setGroup(doDatasource.getGroup().getOption());
        datasource.setLabel(doDatasource.getLabel());
        datasource.setPlaceholderPrefix(doDatasource.getPlaceholderPrefix());
        datasource.setJdbcDriver(doDatasource.getJdbcDriver().getOption());
        datasource.setJndi(doDatasource.getJndi());
        datasource.setUrl(doDatasource.getUrl());
        datasource.setCredential(doDatasource.getCredential().getOption());
        datasource.setCredentialVersion(doDatasource.getCredential().getVersion());

        fromHandleConnectionPools(datasource, doDatasource);
        fromHandleProperties(datasource, doDatasource);

        return datasource;
    }

    private static void fromHandleConnectionPools(Datasource datasource, DoDatasource doDatasource) {
        DatasourceConnectionPools datasourceConnectionPools = datasource.getDatasourceConnectionPools();
        for (DoConnectionPool doConnectionPool : doDatasource.getConnectionPools()) {
            DatasourceConnectionPool datasourceConnectionPool = new DatasourceConnectionPool();
            datasourceConnectionPool.setUuid(doDatasource.getUuid());
            datasourceConnectionPool.setGroup(datasource.getGroup());
            datasourceConnectionPool.setConnectionPoolVersion(doConnectionPool.getVersion());
            datasourceConnectionPool.setConnectionPool(doConnectionPool.getOption());
            datasourceConnectionPools.getWidgets().add(datasourceConnectionPool);
        }
    }

    private static void fromHandleProperties(Datasource datasource, DoDatasource doDatasource) {
        KeyValueProperties datasourceConnectionProperties = datasource.getProperties();
        for (DoConnectionProperty doConnectionProperty : doDatasource.getProperties()) {
            KeyValueProperty datasourceConnectionProperty = new KeyValueProperty();
            datasourceConnectionProperty.setUuid(doConnectionProperty.getUuid());
            datasourceConnectionProperty.setKey(doConnectionProperty.getKey());
            datasourceConnectionProperty.setValue(doConnectionProperty.getValue());
            datasourceConnectionProperties.getWidgets().add(datasourceConnectionProperty);
        }
    }

    public static ArrayList<DatasourceSummaryItem> toSummaries(List<DoDatasource> datasources) {
        ArrayList<DatasourceSummaryItem> summaries = new ArrayList<>();
        for (DoDatasource doDatasource : datasources) {
            summaries.add(toSummary(doDatasource));
        }

        return summaries;
    }

    public static DatasourceSummaryItem toSummary(DoDatasource doDatasource) {
        DatasourceSummaryItem summary = new DatasourceSummaryItem();
        summary.setUuid(doDatasource.getUuid());
        summary.setLabel(doDatasource.getLabel());
        return summary;
    }

}
