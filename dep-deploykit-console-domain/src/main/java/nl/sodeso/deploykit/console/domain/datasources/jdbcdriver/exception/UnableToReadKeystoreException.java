package nl.sodeso.deploykit.console.domain.datasources.jdbcdriver.exception;

/**
 * @author Ronald Mathies
 */
public class UnableToReadKeystoreException extends RuntimeException {

    public UnableToReadKeystoreException(String message) {
        super(message);
    }
}
