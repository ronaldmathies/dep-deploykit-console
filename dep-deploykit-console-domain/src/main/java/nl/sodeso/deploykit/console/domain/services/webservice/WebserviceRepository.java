package nl.sodeso.deploykit.console.domain.services.webservice;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class WebserviceRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoWebservice> find(Permissions permissions) {
        return BaseRepository.find(DoWebservice.class, permissions);
    }

    public List<DoWebservice> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoWebservice.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoWebservice.class, uuid);
    }

    public DoWebservice findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoWebservice.class, uuid, version);
    }

    public DoWebservice checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoWebservice.class, uuid, label);
    }

    public void save(DoWebservice doWebservice) {
        BaseRepository.save(doWebservice);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoWebservice.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoWebservice> doWebservices = criteria.list();
        for (DoWebservice doWebservice : doWebservices) {
            doWebservice.setLabel(label);
            session.update(doWebservice);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoWebservice.class, uuid, version);
    }

}
