package nl.sodeso.deploykit.console.domain.general.group;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class GroupRepository {

    private static Session session = ThreadSafeSession.getSession(Constants.PU);

    @SuppressWarnings("unchecked")
    public List<DoGroup> find(Permissions permissions) {
        Criteria criteria = session.createCriteria(DoGroup.class);

        if (permissions != null) {
            criteria.createAlias("groupPermissions", "groupPermissions", JoinType.INNER_JOIN)
                    .createAlias("groupPermissions.role", "role", JoinType.INNER_JOIN)
                    .createAlias("role.users", "users", JoinType.INNER_JOIN)
                    .add(Restrictions.eq("users.username", new StringType(permissions.getUsername())));

            if (permissions.isPermissionRequired(Permissions.READ)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToRead", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.USE)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToUse", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.EDIT)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToEdit", new BooleanType(true)));
            }
            if (permissions.isPermissionRequired(Permissions.DEPLOY)) {
                criteria.add(Restrictions.eq("groupPermissions.allowedToDeploy", new BooleanType(true)));
            }
        }

        criteria.setProjection(
                Projections.distinct(
                        Projections.projectionList()
                                .add(Projections.property("uuid"), "uuid")
                                .add(Projections.property("label"), "label")));

        criteria.setResultTransformer(Transformers.aliasToBean(DoGroup.class));

        return criteria.list();
    }

    public DoGroup findByLabel(String label) {
        return BaseRepository.findByLabel(DoGroup.class, label);
    }

    public DoGroup findByUuid(String uuid) {
        return BaseRepository.findByUuid(DoGroup.class, uuid);
    }

    public void save(DoGroup doGroup) {
        BaseRepository.save(doGroup);
    }

    public void remove(String uuid) {
        BaseRepository.remove(DoGroup.class, uuid);
    }

}
