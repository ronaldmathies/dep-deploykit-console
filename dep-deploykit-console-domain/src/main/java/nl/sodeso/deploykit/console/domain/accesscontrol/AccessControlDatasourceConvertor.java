package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.AccessControl;
import nl.sodeso.deploykit.console.client.application.accesscontrol.ldap.DatasourceAccessControl;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlDatasource;
import nl.sodeso.deploykit.console.domain.datasources.datasource.DatasourceRepository;

/**
 * @author Ronald Mathies
 */
public class AccessControlDatasourceConvertor {

    protected static void to(DoAccessControlDatasource doAccessControlDatasource, AccessControl accessControl) {
        DatasourceAccessControl datasourceAccessControl = accessControl.getDatasourceAccessControl();

        doAccessControlDatasource.setUserTable(datasourceAccessControl.getUserTable());
        doAccessControlDatasource.setUserColumn(datasourceAccessControl.getUserColumn());
        doAccessControlDatasource.setCredentialColumn(datasourceAccessControl.getCredentialColumn());
        doAccessControlDatasource.setRoleTable(datasourceAccessControl.getRoleTable());
        doAccessControlDatasource.setRoleUserColumn(datasourceAccessControl.getRoleUserColumn());
        doAccessControlDatasource.setRoleRoleColumn(datasourceAccessControl.getRoleRoleColumn());

        doAccessControlDatasource.setDatasource(new DatasourceRepository()
                .findByUuidAndVersion(
                        datasourceAccessControl.getDatasource().getValue().getKey(),
                        datasourceAccessControl.getDatasourceVersion().getValue()));
    }

    protected static DatasourceAccessControl from(DoAccessControlDatasource doAccessControlDatasource) {
        DatasourceAccessControl datasourceAccessControl = new DatasourceAccessControl();

        datasourceAccessControl.setUserTable(doAccessControlDatasource.getUserTable());
        datasourceAccessControl.setUserColumn(doAccessControlDatasource.getUserColumn());
        datasourceAccessControl.setCredentialColumn(doAccessControlDatasource.getCredentialColumn());
        datasourceAccessControl.setRoleTable(doAccessControlDatasource.getRoleTable());
        datasourceAccessControl.setRoleUserColumn(doAccessControlDatasource.getRoleUserColumn());
        datasourceAccessControl.setRoleRoleColumn(doAccessControlDatasource.getRoleRoleColumn());
        datasourceAccessControl.setDatasource(doAccessControlDatasource.getDatasource().getOption());
        datasourceAccessControl.setDatasourceVersion(doAccessControlDatasource.getDatasource().getVersion());

        return datasourceAccessControl;
    }

}
