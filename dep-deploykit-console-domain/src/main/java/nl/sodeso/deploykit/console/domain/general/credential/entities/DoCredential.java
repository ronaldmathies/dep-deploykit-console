package nl.sodeso.deploykit.console.domain.general.credential.entities;

import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.DoBaseVersionedEntity;
import nl.sodeso.deploykit.console.domain.connectors.https.entities.DoHttps;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.domain.general.group.entities.DoGroup;
import nl.sodeso.deploykit.console.domain.profile.entities.DoProfile;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.deploykit.console.domain.services.webservice.entities.DoWebservice;
import nl.sodeso.gwt.persistence.types.StringTypeUserType;
import nl.sodeso.gwt.ui.client.form.combobox.DefaultOption;
import nl.sodeso.gwt.ui.client.form.combobox.OptionType;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import nl.sodeso.persistence.hibernate.util.HasUuid;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@PersistenceEntity(persistenceUnit = Constants.PU)
@Entity
@Table(name = "t_credential",
    indexes = {
            @Index(columnList = "uuid", name = "ix_uuid")
    },
    uniqueConstraints = {
            @UniqueConstraint(columnNames = {"uuid", "ver_key", "ver_type"}, name = "ux_version")
    }
)
@TypeDef(name = "string", typeClass = StringTypeUserType.class)
public class DoCredential extends DoBaseVersionedEntity {

    @Type(type = "string")
    @Column(name = "placeholder_prefix", nullable = false, length = 255)
    private StringType placeholderPrefix = null;

    @Type(type = "string")
    @Column(name = "username", nullable = true, length = 255)
    private StringType username;

    @Type(type = "string")
    @Column(name = "password", nullable = false, length = 255)
    private StringType password;

    @JoinColumn(name="group_id", foreignKey = @ForeignKey(name = "fk_credential_group_id"))
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private DoGroup group;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "credentials")
    private List<DoProfile> profiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "credential")
    private List<DoDatasource> datasources = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "keyStorePassword")
    private List<DoHttps> httpsKeystores = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "trustStorePassword")
    private List<DoHttps> httpsTruststores = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "keyManagerPassword")
    private List<DoHttps> httpsKeymanagers = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "credential")
    private List<DoLdapService> ldapServices = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "credential")
    private List<DoWebservice> webservices = new ArrayList<>();

    public DoCredential() {}

    public StringType getPlaceholderPrefix() {
        return placeholderPrefix;
    }

    public void setPlaceholderPrefix(StringType placeholderPrefix) {
        this.placeholderPrefix = placeholderPrefix;
    }

    public StringType getUsername() {
        return this.username;
    }

    public void setUsername(StringType username) {
        this.username = username;
    }

    public StringType getPassword() {
        return this.password;
    }

    public void setPassword(StringType password) {
        this.password = password;
    }

    public DoGroup getGroup() {
        return this.group;
    }

    public void setGroup(DoGroup group) {
        this.group = group;
    }

    public List<DoDatasource> getDatasources() {
        return this.datasources;
    }

    public List<DoHttps> getHttpsKeystores() {
        return httpsKeystores;
    }

    public List<DoHttps> getHttpsTruststores() {
        return httpsTruststores;
    }

    public List<DoHttps> getHttpsKeymanagers() {
        return httpsKeymanagers;
    }

    public List<DoProfile> getProfiles() {
        return this.profiles;
    }

    public List<DoLdapService> getLdapServices() { return this.ldapServices; }

    public List<DoWebservice> getWebservices() {
        return this.webservices;
    }

}
