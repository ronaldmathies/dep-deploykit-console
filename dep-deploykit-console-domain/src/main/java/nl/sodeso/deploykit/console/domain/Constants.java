package nl.sodeso.deploykit.console.domain;

/**
 * @author Ronald Mathies
 */
public class Constants {

    /**
     * Persistence unit name.
     */
    public final static String PU = "deploykitconsole-pu";
    public final static String ELASTIC_SEARCH_UNIT = "deploykit-eu";

}
