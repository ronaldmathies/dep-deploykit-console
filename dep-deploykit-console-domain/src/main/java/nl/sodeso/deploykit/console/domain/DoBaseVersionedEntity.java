package nl.sodeso.deploykit.console.domain;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.types.VersionOptionCompositeUserType;
import nl.sodeso.gwt.persistence.types.OptionCompositeUserType;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * @author Ronald Mathies
 */
@TypeDefs(
    value = {
        @TypeDef(name = "versionOption", typeClass = VersionOptionCompositeUserType.class)
    }
)
@MappedSuperclass
public abstract class DoBaseVersionedEntity extends DoBaseEntity {

    @Type(type = "versionOption")
    @Columns(columns = {
            @Column(name = "ver_key"),
            @Column(name = "ver_code"),
            @Column(name = "ver_description"),
            @Column(name = "ver_type")
    })
    private VersionOptionType version = null;

    public DoBaseVersionedEntity() {}

    public VersionOptionType getVersion() {
        return version;
    }

    public void setVersion(VersionOptionType version) {
        this.version = version;
    }

    public boolean isRelease() {
        return getVersion().isRelease();
    }

    public boolean isBranch() {
        return getVersion().isBranch();
    }

    public boolean isSnapshot() {
        return getVersion().isSnapshot();
    }

}
