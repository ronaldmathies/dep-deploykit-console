package nl.sodeso.deploykit.console.domain.connectors.http;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class HttpElasticSearchMapper implements ElasticSearchMapper<DoHttp, IndexEntry> {

    @Override
    public IndexEntry map(DoHttp doHttp) {
        return new IndexEntry(doHttp.getId(), doHttp.getUuid(), doHttp.getVersion().getValue(),
                    doHttp.getLabel().getValue(), Type.HTTP)
                .addKeyword(doHttp.getLabel().getValue())
                .addKeyword(doHttp.getUuid());
    }

}
