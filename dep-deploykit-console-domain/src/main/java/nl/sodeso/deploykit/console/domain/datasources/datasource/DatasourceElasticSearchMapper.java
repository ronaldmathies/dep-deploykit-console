package nl.sodeso.deploykit.console.domain.datasources.datasource;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoConnectionProperty;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class DatasourceElasticSearchMapper implements ElasticSearchMapper<DoDatasource, IndexEntry> {

    @Override
    public IndexEntry map(DoDatasource doDatasource) {
        IndexEntry indexEntry = new IndexEntry(doDatasource.getId(), doDatasource.getUuid(), doDatasource.getVersion().getValue(), doDatasource.getLabel().getValue(), Type.DATASOURCE)
                .addKeyword(doDatasource.getJndi().getValue())
                .addKeyword(doDatasource.getUrl().getValue())
                .addKeyword(doDatasource.getLabel().getValue())
                .addKeyword(doDatasource.getUuid());

        for (DoConnectionProperty doConnectionProperty : doDatasource.getProperties()) {
            indexEntry.addKeyword(doConnectionProperty.getValue().getValue());
        }

        return indexEntry;
    }

}
