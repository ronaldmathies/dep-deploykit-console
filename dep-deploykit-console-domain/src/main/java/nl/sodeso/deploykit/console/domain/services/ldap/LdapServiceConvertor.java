package nl.sodeso.deploykit.console.domain.services.ldap;

import nl.sodeso.deploykit.console.client.application.services.ldap.LdapService;
import nl.sodeso.deploykit.console.client.application.services.ldap.LdapServiceSummaryItem;
import nl.sodeso.deploykit.console.domain.general.credential.CredentialRepository;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.deploykit.console.domain.services.ldap.entities.DoLdapService;
import nl.sodeso.gwt.ui.client.types.BooleanType;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class LdapServiceConvertor {

    public static DoLdapService to(DoLdapService doLdapService, LdapService ldapService) {
        if (doLdapService == null) {
            doLdapService = new DoLdapService();
            doLdapService.setUuid(UuidUtils.generate());
        }

        doLdapService.setGroup(new GroupRepository().findByUuid(ldapService.getGroup().getValue().getKey()));
        doLdapService.setLabel(ldapService.getLabel());
        doLdapService.setVersion(ldapService.getVersion());
        doLdapService.setPlaceholderPrefix(ldapService.getPlaceholderPrefix());
        doLdapService.setDomain(ldapService.getDomain());
        doLdapService.setPort(ldapService.getPort());
        if (ldapService.getUseCredentials().getValue()) {
            doLdapService.setCredential(new CredentialRepository().findByUuidAndVersion(ldapService.getCredential().getValue().getKey(), ldapService.getCredentialVersion().getValue()));
        } else {
            doLdapService.setCredential(null);
        }

        return doLdapService;
    }

    public static ArrayList<LdapService> from(List<DoLdapService> doLdapServices) {
        ArrayList<LdapService> list = new ArrayList<>();

        for (DoLdapService doLdapService : doLdapServices) {
            list.add(from(doLdapService));
        }

        return list;
    }

    public static LdapService from(DoLdapService doLdapService) {
        LdapService ldapService = new LdapService();

        ldapService.setUuid(doLdapService.getUuid());
        ldapService.setGroup(doLdapService.getGroup().getOption());
        ldapService.setLabel(doLdapService.getLabel());
        ldapService.setVersion(doLdapService.getVersion());
        ldapService.setPlaceholderPrefix(doLdapService.getPlaceholderPrefix());
        ldapService.setDomain(doLdapService.getDomain());
        ldapService.setPort(doLdapService.getPort());

        if (doLdapService.getCredential() != null) {
            ldapService.setCredential(doLdapService.getCredential().getOption());
            ldapService.setCredentialVersion(doLdapService.getCredential().getVersion());
            ldapService.setUseCredentials(new BooleanType(true));
        }

        return ldapService;
    }

    public static ArrayList<LdapServiceSummaryItem> toSummaries(List<DoLdapService> authentications) {
        ArrayList<LdapServiceSummaryItem> summaries = new ArrayList<>();
        for (DoLdapService doLdapService : authentications) {
            summaries.add(toSummary(doLdapService));
        }

        return summaries;
    }

    public static LdapServiceSummaryItem toSummary(DoLdapService doLdapService) {
        LdapServiceSummaryItem summary = new LdapServiceSummaryItem();
        summary.setUuid(doLdapService.getUuid());
        summary.setLabel(doLdapService.getLabel());
        return summary;
    }
}
