package nl.sodeso.deploykit.console.domain.security.role;

import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.security.role.entities.DoRole;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class RoleRepository {

    private static Session session = ThreadSafeSession.getSession(Constants.PU);

    private Logger LOG = Logger.getLogger(getClass().getName());

    public List<DoRole> find() {
        return BaseRepository.find(DoRole.class);
    }

    public DoRole findByLabel(String label) {
        return BaseRepository.findByLabel(DoRole.class, label);
    }

    public DoRole findByUuid(String uuid) {
        return BaseRepository.findByUuid(DoRole.class, uuid);
    }

    @SuppressWarnings("unchecked")
    public List<DoRole> findByUsername(String username) {

        LOG.log(Level.INFO, String.format("Finding all '%s', retricted for user '%s'.", getClass().getName(), username));

        Criteria criteria = session.createCriteria(DoRole.class)
                .createAlias("users", "users", JoinType.INNER_JOIN)
                .add(Restrictions.eq("users.username", new StringType(username)));

        long start = System.currentTimeMillis();
        List list = criteria.list();
        LOG.log(Level.INFO, String.format("Finding all '%s', retricted for user '%s' in %dms..", getClass().getName(), username, System.currentTimeMillis() - start));

        return list;
    }

    public void save(DoRole doRole) {
        BaseRepository.save(doRole);
    }

    public void remove(String uuid) {
        BaseRepository.remove(DoRole.class, uuid);
    }

}
