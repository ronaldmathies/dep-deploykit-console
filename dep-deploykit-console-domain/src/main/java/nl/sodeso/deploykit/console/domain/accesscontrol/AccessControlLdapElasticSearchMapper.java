package nl.sodeso.deploykit.console.domain.accesscontrol;

import nl.sodeso.deploykit.console.client.application.search.Type;
import nl.sodeso.deploykit.console.domain.accesscontrol.entities.DoAccessControlLdap;
import nl.sodeso.deploykit.console.elasticsearch.indexed.IndexEntry;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class AccessControlLdapElasticSearchMapper implements ElasticSearchMapper<DoAccessControlLdap, IndexEntry> {

    @Override
    public IndexEntry map(DoAccessControlLdap doAccessControlLdap) {
        IndexEntry indexEntry = new IndexEntry(doAccessControlLdap.getId(), doAccessControlLdap.getUuid(), doAccessControlLdap.getVersion().getValue(), doAccessControlLdap.getLabel().getValue(), Type.LDAP_ACCESS_CONTROL)
                .addKeyword(doAccessControlLdap.getLabel().getValue())
                .addKeyword(doAccessControlLdap.getUuid())
                .addKeyword(doAccessControlLdap.getPlaceholderPrefix().getValue())
                .addKeyword(doAccessControlLdap.getUserBaseDn().getValue())
                .addKeyword(doAccessControlLdap.getUserObjectClass().getValue())
                .addKeyword(doAccessControlLdap.getUserPasswordAttribute().getValue())
                .addKeyword(doAccessControlLdap.getUserAttribute().getValue())
                .addKeyword(doAccessControlLdap.getRoleBaseDn().getValue())
                .addKeyword(doAccessControlLdap.getRoleObjectClass().getValue())
                .addKeyword(doAccessControlLdap.getRoleMemberAttribute().getValue())
                .addKeyword(doAccessControlLdap.getRoleNameAttribute().getValue());

        return indexEntry;
    }

}
