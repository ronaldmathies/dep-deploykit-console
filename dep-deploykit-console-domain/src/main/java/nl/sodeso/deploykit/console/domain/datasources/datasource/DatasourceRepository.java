package nl.sodeso.deploykit.console.domain.datasources.datasource;

import nl.sodeso.deploykit.console.client.application.ui.options.VersionOption;
import nl.sodeso.deploykit.console.client.application.ui.options.VersionOptionType;
import nl.sodeso.deploykit.console.domain.BaseRepository;
import nl.sodeso.deploykit.console.domain.Constants;
import nl.sodeso.deploykit.console.domain.Permissions;
import nl.sodeso.deploykit.console.domain.datasources.datasource.entities.DoDatasource;
import nl.sodeso.gwt.ui.client.types.StringType;
import nl.sodeso.persistence.hibernate.ThreadSafeSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class DatasourceRepository {

    private Session session = ThreadSafeSession.getSession(Constants.PU);

    public List<DoDatasource> find(Permissions permissions) {
        return BaseRepository.find(DoDatasource.class, permissions);
    }

    public List<DoDatasource> find(Permissions permissions, String groupUuid) {
        return BaseRepository.find(DoDatasource.class, permissions, groupUuid);
    }

    public List<VersionOptionType> versions(String uuid) {
        return BaseRepository.versions(DoDatasource.class, uuid);
    }

    public DoDatasource findByUuidAndVersion(String uuid, VersionOption version) {
        return BaseRepository.findByUuidAndVersion(DoDatasource.class, uuid, version);
    }

    public DoDatasource checkIfLabelAlreadyExists(String uuid, String label) {
        return BaseRepository.checkIfLabelAlreadyExists(DoDatasource.class, uuid, label);
    }

    public void save(DoDatasource doDatasource) {
        BaseRepository.save(doDatasource);
    }

    @SuppressWarnings("unchecked")
    public void updateLabel(Long id, String uuid, StringType label) {
        Criteria criteria = session.createCriteria(DoDatasource.class)
                .add(Restrictions.eq("uuid", uuid))
                .add(Restrictions.ne("id", id));

        List<DoDatasource> doDatasources = criteria.list();
        for (DoDatasource doDatasource : doDatasources) {
            doDatasource.setLabel(label);
            session.update(doDatasource);
        }
    }

    public void remove(String uuid, VersionOption version) {
        BaseRepository.remove(DoDatasource.class, uuid, version);
    }

}
