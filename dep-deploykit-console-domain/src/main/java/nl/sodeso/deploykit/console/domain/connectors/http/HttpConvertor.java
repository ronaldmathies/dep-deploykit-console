package nl.sodeso.deploykit.console.domain.connectors.http;

import nl.sodeso.deploykit.console.client.application.connectors.http.Http;
import nl.sodeso.deploykit.console.client.application.connectors.http.HttpSummaryItem;
import nl.sodeso.deploykit.console.domain.connectors.http.entities.DoHttp;
import nl.sodeso.deploykit.console.domain.general.group.GroupRepository;
import nl.sodeso.persistence.hibernate.util.UuidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class HttpConvertor {

    public static DoHttp to(DoHttp doHttp, Http http) {
        if (doHttp == null) {
            doHttp = new DoHttp();
            doHttp.setUuid(UuidUtils.generate());
        }

        doHttp.setGroup(new GroupRepository().findByUuid(http.getGroup().getValue().getKey()));
        doHttp.setVersion(http.getVersion());
        doHttp.setLabel(http.getLabel());

        doHttp.setPort(http.getPort());
        doHttp.setTimeout(http.getTimeout());
        doHttp.setHost(http.getHost());

        return doHttp;
    }

    public static ArrayList<HttpSummaryItem> toSummaries(List<DoHttp> doHttpList) {
        ArrayList<HttpSummaryItem> summaries = new ArrayList<>();
        for (DoHttp doHttp : doHttpList) {
            summaries.add(toSummary(doHttp));
        }

        return summaries;
    }

    public static HttpSummaryItem toSummary(DoHttp doHttp) {
        HttpSummaryItem summary = new HttpSummaryItem();
        summary.setUuid(doHttp.getUuid());
        summary.setLabel(doHttp.getLabel());
        return summary;
    }

    public static Http from(DoHttp doHttp) {
        Http http = new Http();

        http.setUuid(doHttp.getUuid());
        http.setGroup(doHttp.getGroup().getOption());
        http.setLabel(doHttp.getLabel());
        http.setVersion(doHttp.getVersion());
        http.setPort(doHttp.getPort());
        http.setTimeout(doHttp.getTimeout());
        http.setHost(doHttp.getHost());

        return http;
    }

}
